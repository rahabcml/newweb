/**
+ * index.js
+ *
+ * @description :: Front-end starting point, including react-router setup, also its related config/routes.js
+ * @help        :: See https://github.com/reactjs/react-router
+ */

import React, { Component } from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, IndexRoute, hashHistory } from 'react-router';
//general
import Container from './components/common/container';
import Login from './components/common/login.jsx';
import Home from './components/common/home.jsx';
import Features from './components/common/features.jsx';
// admin
import Admin from './components/admin/admin_home.jsx';
// addressbook
import Address from './components/admin/address_book/address_book.jsx';
import ConfigCustProfile from './components/admin/address_book/cfg_cus_pro.jsx';
import ManageCustomerProfile from './components/admin/address_book/manage_cus_pro.jsx';
import UploadCustomerProfile from './components/admin/address_book/upload_cus_pro.jsx';
// campain list
import CampaignList from './components/admin/campaign_list/campaign_list.jsx';
import MapInputForm from './components/admin/campaign_list/map_form.jsx';
import AssignCampaignList from './components/admin/campaign_list/assign_list.jsx';
// input form
import InputFormMain from './components/admin/input_form/input_form_home.jsx';
import InputFormEdit from './components/admin/input_form/input_form_edit.jsx';
// voice log mail
import VoiceLogMail from './components/admin/voice_log/voice_log_home.jsx';
// user account
import UserAccountHome from './components/admin/user_account/user_account_home.jsx';
import UserRoles from './components/admin/user_account/user_roles.jsx';
import UserAccounts from './components/admin/user_account/user_accounts.jsx';
//  ====== agent  ======
import Agent from './components/agent/agent_home.jsx';
// inbound home
import InboundPhone from './components/agent/inbound/inbound_phone.jsx';
import InboundHome from './components/agent/inbound/inbound_home.jsx';
import InboundMain from './components/agent/inbound/inbound_main.jsx';
import OutboundPartial from './components/agent/inbound/outbound_partial.jsx';
import NewCustomer from './components/agent/inbound/new_customer.jsx';
// outbound home
import OutbuondHome from './components/agent/outbound/outbound_home.jsx';
import OutboundCall from './components/agent/outbound/outbound_call.jsx';

import NotFound from './components/common/404';
import $ from 'jquery';
import foundation from 'foundation-sites';

export default class App extends Component {
	render() {
		return (
			<Router history={hashHistory}>
				<Route path="/" component={Container}>
					<Route path="login" component={Login} />
					<Route path="home" component={Home} >
						<Route path="features" component={Features} />
						<Route component={Features} />
						<Route path="agent" component={Agent} />
						<Route path="address" component={Address} >
							<Route path="config" component={ConfigCustProfile} />
							<Route path="upload" component={UploadCustomerProfile} />
							<Route path="manage" component={ManageCustomerProfile} />
						</Route>
						<Route path="InputForm" component={InputFormMain} />
						<Route path="InputFormEdit" component={InputFormEdit} />
						<Route path="campaign" component={CampaignList} >
							<Route path="map" component={MapInputForm} />
							<Route path="assign" component={AssignCampaignList} />
						</Route>
						<Route path="UserAccount" component={UserAccountHome} >
							<Route path="Roles" component={UserRoles} />
							<Route path="Accounts" component={UserAccounts} />
						</Route>
						<Route path="VoiceLogMail" component={VoiceLogMail} />
						<Route path="InboundHome" component={InboundHome} />
						<Route path="InboundPhone" component={InboundPhone}>
							<Route path="InboundMain" component={InboundMain} />
							<Route path="NewCustomer" component={NewCustomer} />
							<Route path="OutboundPartial" component={OutboundPartial} />
						</Route>
						<Route path="OutbuondHome" component={OutbuondHome} />
						<Route path="OutboundCall" component={OutboundCall} />
					</Route>
				</Route>
				<Route path="*" component={NotFound} />
			</Router>
		);
	}
}

$(document).ready(() => render((
	<App />
), document.getElementById('app')));
// --------------------------- /routes --------------------------- //
