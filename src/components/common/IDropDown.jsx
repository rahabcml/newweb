/*******************************************
* Notes
* Name: IDropDown.jsx
* Details: A combo box component
* Properties: 
*        valueField: String.
*        displayField: String. 
*        value: String. Current Value.
*        options: Array.
*        onChange: Function.
*        placeholder: (Optional) String. Default is "Select.."
*        clearable: (Optional) Boolearn. Deafult true.
*        disabled: (Optional) Boolearn. Deafult false.

*        Others go https://github.com/JedWatson/react-select for ref.
********************************************/

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import IFun from './IFun.jsx';
let isInteger = IFun.isInteger;

export default class IDropDown extends React.Component {
    constructor(props) {
        super(props);
        let data = this.props.options.slice() || [];
        let reArragedData = [];
        if (data && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                reArragedData.push({
                    value: isInteger(this.props.valueField) ? data[i] : data[i][this.props.valueField],
                    label: isInteger(this.props.displayField) ? data[i] : data[i][this.props.displayField],
                    obj: this.props.options[i]
                })
            }
        }

        this.state = {
            data: reArragedData,
            value: this.props.value || ""
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let data = nextProps.options ? nextProps.options.slice() : [];
            let dataLength = data.length;
            let reArragedData = [];
            if (data && dataLength > 0) {
                for (let i = 0; i < dataLength; i++) {
                    reArragedData.push({
                        value: isInteger(nextProps.valueField) ? data[i] : data[i][nextProps.valueField],
                        label: isInteger(nextProps.displayField) ? data[i] : data[i][nextProps.displayField],
                        obj: nextProps.options[i]
                    })
                }
            }
            this.setState({
                data: reArragedData,
                value: nextProps.value
            });
        }
    }

    dropDownChange(e) {
        if (this.props.onChange) {
            let value = e.target.value;
            this.props.onChange(value);
        }
    }

    render() {
        let oThis = this;
        let name = this.props.value || "";
        let placeholder = this.props.placeholder || "Select...";
        let clearable = this.props.clearable === false ? false : true;
        let disabled = this.props.disabled === true ? true : false;

        let options = [];
        // console.log("this.state.data in drop down");
        // console.log(this.state.data);
        for (let i = 0; i < this.state.data.length; i++) {
            let theData = this.state.data[i];
            let value = theData.value;
            let label = theData.label;
            options.push(<option key={'opt' + i} value={value}>{label}</option>);
        }

        return (
            <select placeholder={placeholder} name='i-drop-down' value={name} onChange={this.dropDownChange.bind(oThis)} disabled={disabled}>
                {options}
            </select>
        );
    }
};