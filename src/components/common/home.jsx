import React, { Component } from 'react'
import { Link } from 'react-router';
import i18nCommon from './i18n.jsx';
import Admin from '../admin/admin_home.jsx';
import Agent from '../agent/agent_home.jsx';

let i18n = i18nCommon.home_variable;

const ModulePage = (modules) => {
  return (
    <Admin
      modules={modules}
    />
  );
}

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: 'modules'
    }
  }

  componentWillMount() {
    window.location.hash = 'home/features';
    // let module = sessionStorage.getItem("modules");
    // if (module == "admin") {
    //   window.location.hash = 'admin';
    // } else {//default
    //   window.location.hash = 'admin';
    // }
  }

  logoutLinkClicked() {
    //sessionStorage.setItem("loggedIn", false);
    localStorage.loggedIn = false;
  }

  tabClicked(tabID) {
    console.log("tabID");
    console.log(tabID);
    console.log("document.getElementById(tab + tabID)");
    console.log(document.getElementById("tab" + tabID));

    var i;
    var x = document.getElementsByClassName("tab-title");
    console.log("x");
    console.log(x);
    for (i = 0; i < x.length; i++) {
      x[i].className = "tab-title";
    }
    let clickedElement = document.getElementById("tab" + tabID)
    clickedElement.className = "tab-title active";
    console.log("clickedElement");
    console.log(clickedElement);
  }

  render() {
    let oThis = this;

    //fullName for the top-bar
    let fullName = sessionStorage.fullName || "";
    //let first = localStorage.first;
    //let last = localStorage.last;
    // TO DO: sessionStorage should be better then localStorage, however when testing with hashHistory, sessionStorage can't be saved
    // let first = sessionStorage.getItem("first");
    // let last = sessionStorage.getItem("last");
    //let fullName = first + " " + last;

    return (

      <div>

        <nav className="top-bar" data-topbar role="navigation">

          <div className="top-bar-left">
            <ul className="title-area">
              <li className="logo">
                <Link to="/home/features">
                  <img src="assets/imgs/wiseplus.png" alt="wise logo" />
                </Link>
              </li>
            </ul>
          </div>

          <section className="top-bar-section">
            <ul className="right">
              <li className="has-dropdown">
                <a href="#">
                  <img src="assets/imgs/may_chan.jpg" alt="" className='circle-top' />
                  {fullName}
                </a>
                <ul className="dropdown">
                  <li><a href="#">1st link in dropdown</a></li>
                  <li className="active"><a href="#">2nd link in dropdown</a></li>
                </ul>
              </li>
              <li className="active"><a href="#">{i18n.logout}</a></li>
            </ul>
          </section>

        </nav>

        {this.props.children}

      </div>
    )
  }
}
