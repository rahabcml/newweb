/*************************************************************************************************
* Notes
* Name: screen_phone.jsx
* Class: ScreenPhone
* Details: Screen phone for outbound and inbound
* Props :
*	changeParent: manual dialed call send to parent
* call function:
*   how to use? In parent: this.refs.child.callClick(tel);
*   Use for? to send telephone number and make call directly in  this component
*************************************************************************************************/
import React, { Component } from 'react';
import IDropDown from './IDropDown.jsx';
import AlertBox from './AlertBox.jsx'
import i18nCommon from './i18n.jsx';
let i18n = i18nCommon.outbound_variable;
export default class ScreenPhone extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isReady: i18n.idle,
			status: i18n.standby,
			dialingNum: "", // Number dialing (for other phone number update)
			displayNum: "", // Number being displayed
			displayNum2: "", // Number being displayed in 2nd input
			//tmpNum: "", // Number dialing during the call
			time: {}, // count down time
			seconds: 120, // 2 mins count down for working
			stopTime: { h: 0, m: 0, s: "00" }, // stopwatch during conversation
			stopSec: 0, // start with 0 seconds
			showTimer: false,
			show3Btns: false,
			showAlert: false,
			showInput1: true,
			showInput2: false,
			conferenceOn: false
		}
		this.timer = 0;
		this.startTimer = this.startTimer.bind(this);
		this.countDown = this.countDown.bind(this);
		this.resetTimer = this.resetTimer.bind(this);
		this.stopwatch = 0;
		this.startStop = this.startStop.bind(this);
		this.countUp = this.countUp.bind(this);
		this.holdStop = this.holdStop.bind(this);
		this.resetStop = this.resetStop.bind(this);
	}
	// called by parent
	callClick(tel) {
		// only start another call when status is Standby or Call ended
		if (this.state.status == i18n.standby || this.state.status == i18n.call_ended) {
			if (typeof this.props.changeParent === 'function') {
				this.props.changeParent(tel, "");
			}
			this.setState({ calling: tel, status: i18n.calling, displayNum: tel, dialingNum: "", showInput1: true });
			this.resetTimer();
			// ============== TBD ==============
			setTimeout(function () { this.setState({ status: i18n.in_conversation, show3Btns: true }, () => this.startStop()); }.bind(this), 1000);
		}
	}

	readyIdleChange(o) {
		console.log("o");
		console.log(o);
		this.setState({ isReady: o });
	}

	callPanelClick(num) {
		if (this.state.showInput2 == false) {
			let dialingNum = this.state.dialingNum.slice();
			dialingNum += num
			this.setState({ dialingNum: dialingNum, displayNum: dialingNum });
		} else {
			// conference call or transfer call
			let displayNum2 = this.state.displayNum2.slice();
			displayNum2 += num
			this.setState({ displayNum2: displayNum2 });
		}

		// turn ended call to dialing (standby) mode
		if (this.state.status == i18n.call_ended) {
			this.resetTimer();
			this.resetStop();
			this.setState({ status: i18n.standby, displayNum: num, dialingNum: num, showInput1: true });
		}
	}

	endCall() {
		console.log("end call clicked");
		this.setState({ status: i18n.call_ended, displayNum: "", displayNum2: "", show3Btns: false, showInput1: false, showInput2: false, conferenceOn: false });
		//this.setState({ status: i18n.call_ended, displayNum: "", tmpNum: "", show3Btns: false, showInput1: false });
		this.holdStop();
		this.startTimer();
	}
	phoneIconClick() {
		let oThis = this;
		// make a custom call
		if (this.state.showInput2 == false) {
			if (this.state.status != i18n.calling && this.state.status != i18n.in_conversation) {
				if (this.state.displayNum.length > 0) {
					// make a call
					let dialingNum = this.state.dialingNum.slice();
					// if (dialingNum.length > 0) {
					// 	if (typeof this.props.changeParent === 'function') {
					// 		this.props.changeParent(dialingNum);
					// 	}
					// }
					if (typeof this.props.changeParent === 'function') {
						this.props.changeParent(this.state.displayNum, dialingNum);
					}
					this.setState({ status: i18n.calling, showInput1: true });
					// TO DO: when connected to the other side
					setTimeout(function () { this.setState({ status: i18n.in_conversation, show3Btns: true }, () => this.startStop()) }.bind(this), 1000);
				} else {
					// no number dialed, but clicked phone icon
					this.setState({ showAlert: true });
				}
			} else {
				// end call
				this.endCall();
			}
		} else {
			if (this.state.status == i18n.on_hold) {
				// make a 2nd call when on hold
				if (this.state.displayNum2.length > 0) {
					this.setState({ status: i18n.calling });
					// TO DO: when connected to the other side
					setTimeout(function () { this.setState({ status: i18n.in_conversation, show3Btns: true }, () => this.startStop()) }.bind(this), 1000);
				}
			} else if (this.state.status == i18n.in_conversation) {
				// end call
				this.endCall();
			}
		}
	}
	// Hold / Conference / Transfer button clicked
	specialBtnClick(type) {
		if (this.state.status == i18n.in_conversation || this.state.status == i18n.on_hold) {
			if (type == "hold") {
				if (this.state.status == i18n.on_hold) {
					this.setState({ status: i18n.in_conversation, showInput2: false });
				} else if (this.state.status == i18n.in_conversation) {
					this.setState({ status: i18n.on_hold, showInput2: true, displayNum2: "" });
				}
			} else if (type == "conference") {
				// only can work when 2nd line is in conversation
				if (this.state.showInput2 == true && this.state.status == i18n.in_conversation) {
					this.setState({ conferenceOn: true });
					// conference call now
				}
			} else { // type == "transfer"
				if (this.state.showInput2 == true && this.state.status == i18n.in_conversation) {
					this.endCall();
				}
			}
		}
	}
	// "End Work Mode" button clicked
	endWorkClick() {
		this.resetTimer();
		this.resetStop();
		this.setState({ status: i18n.standby, showInput1: true, dialingNum: "", displayNum: "" });
	}
	// ============= timer function =============
	componentDidMount() {
		let timeLeftVar = this.secondsToTime(this.state.seconds);
		this.setState({ time: timeLeftVar });
	}
	secondsToTime(secs) {
		let hours = Math.floor(secs / (60 * 60));

		let divisor_for_minutes = secs % (60 * 60);
		let minutes = Math.floor(divisor_for_minutes / 60);

		let divisor_for_seconds = divisor_for_minutes % 60;
		let seconds = Math.ceil(divisor_for_seconds);

		let obj = {
			"h": hours,
			"m": minutes,
			"s": seconds > 9 ? seconds : "0" + seconds
		};
		return obj;
	}
	startTimer() {
		this.setState({ showTimer: true });
		if (this.timer == 0) {
			this.timer = setInterval(this.countDown, 1000);
		}
	}
	resetTimer() {
		if (this.timer != 0) {
			clearInterval(this.timer);
			// reset timer to 2 mins
			this.setState({ seconds: 120 }, () => {
				let timeLeftVar = this.secondsToTime(this.state.seconds);
				this.setState({ time: timeLeftVar, showTimer: false, status: i18n.standby });
				this.timer = 0;
			});
		}
	}
	countDown() {
		// Remove one second, set state so a re-render happens.
		let seconds = this.state.seconds - 1;
		this.setState({
			time: this.secondsToTime(seconds),
			seconds: seconds,
		});

		// Check if we're at zero.
		if (seconds == 0) {
			this.resetTimer();
		}
	}
	// ==== stop watch ====
	startStop() {
		console.log("START STOP !");
		if (this.stopwatch == 0) {
			this.stopwatch = setInterval(this.countUp, 1000);
		}
	}
	holdStop() {
		clearInterval(this.stopwatch);
	}
	resetStop() {
		//if (this.stopwatch != 0) {
		//clearInterval(this.stopwatch);
		// reset stopwatch to 0 seconds
		this.setState({ stopSec: 0, stopTime: { h: 0, m: 0, s: "00" } }, () => {
			let timeLeftVar = this.secondsToTime(this.state.stopSec);
			this.setState({ stopTime: timeLeftVar });
			this.stopwatch = 0;
		});
		//}
	}
	countUp() {
		// Remove one second, set state so a re-render happens.
		let stopSec = this.state.stopSec + 1;
		this.setState({
			stopTime: this.secondsToTime(stopSec),
			stopSec: stopSec,
		});
	}
	componentWillUnmount() {
		// stop internal when leaving the page
		clearInterval(this.timer);
		clearInterval(this.stopwatch);
	}
	render() {
		let oThis = this;
		let callBtnColor = this.state.status == i18n.calling || this.state.status == i18n.in_conversation ? "call-btn-end" : "call-btn-ready"
		let talkkingColor = callBtnColor == "call-btn-end" ? 'color-main-theme-bold' : 'color-light-grey';
		let workColor = this.state.status == i18n.call_ended ? 'color-main-theme-bold' : 'color-light-grey';
		let showTimer = this.state.showTimer;
		let show3Btns = this.state.show3Btns;
		return (
			<div className="sidebar-phone-area">
				<div className='widget-title'>
					<span className='title'>{i18n.screen_phone}</span>
				</div>

				<div className='call-modes'>
					<div className='callmode callmode-select'>
						<IDropDown options={[i18n.ready, i18n.idle, i18n.break]} displayField={0} valueField={0} clearable={false} value={oThis.state.isReady} onChange={oThis.readyIdleChange.bind(oThis)} />
					</div>
					<div className='callmode'>
						<label className={talkkingColor}>{i18n.talking}</label>
					</div>
					<div className='callmode'>
						<label className={workColor}>{i18n.work}</label>
					</div>
				</div>

				<div className='call-status'>
					<span>{this.state.status}</span>
					<span className='right'><span className={this.state.status == i18n.in_conversation ? "blue-circle" : "grey-circle"}></span>
						<span className={this.state.status == i18n.in_conversation || this.state.status == i18n.on_hold ? "" : "color-light-grey"}>{this.state.stopTime.m}&nbsp;:&nbsp;{this.state.stopTime.s}</span></span>
				</div>
				{/* ======================= DIGITS PANEL ABOVE ======================= */}
				<div className='panel-above-height'>
					<div>
						<div className='dialpad-area'>
							<input className={'phone-input' + (oThis.state.showInput1 == true ? "" : " display-none") + (oThis.state.status == i18n.in_conversation && oThis.state.showInput2 == false ? ' coversation-color' : '') + (oThis.state.status == i18n.on_hold || (oThis.state.showInput2 == true && oThis.state.conferenceOn == false) ? " hold-color" : "")} type='tel' value={this.state.displayNum} onChange={e => {
								let numbers = e.target.value;
								// regEx restricts numbers and * and # only, the phone number need to set limit to prevent internet attack
								if ((/^[\d*#]+$/.test(numbers) == true && numbers.length <= 30) || numbers.length == 0) {
									oThis.setState({ displayNum: numbers, dialingNum: numbers });
								}
							}}
							/>
							<input className={'phone-input' + (oThis.state.showInput2 == true ? "" : " display-none") + (oThis.state.status == i18n.in_conversation && oThis.state.showInput2 == true ? ' coversation-color' : '')} type='tel' value={this.state.displayNum2} onChange={e => {
								let numbers = e.target.value;
								// regEx restricts numbers and * and # only, the phone number need to set limit to prevent internet attack
								if ((/^[\d*#]+$/.test(numbers) == true && numbers.length <= 30) || numbers.length == 0) {
									oThis.setState({ displayNum2: numbers });
								}
							}}
							/>
							<div className={'countdown-panel' + (showTimer == true ? "" : " display-none")}>{this.state.time.m}&nbsp;:&nbsp;{this.state.time.s}</div>
						</div>
						{/* ===== 3 buttons ==== */}
						<div className='specal-keys-height'>
							<button className={'end-work-btn' + (showTimer == true ? "" : " display-none")} onClick={this.endWorkClick.bind(oThis)}>{i18n.end_work_mode}</button>
							<div className={'keypad-container' + (show3Btns == true ? "" : " display-none")}>
								<div className='keypad-row'>
									<button title='hold' className='num-button center-text hold-btn' onClick={this.specialBtnClick.bind(oThis, "hold")}>
										<i className='icon-pause-16' />
									</button>
									<button title='conference' className='num-button center-text conference-btn' onClick={this.specialBtnClick.bind(oThis, "conference")}>
										<i className={'icon-conference-16' + (this.state.showInput2 == true && this.state.status == i18n.in_conversation ? '' : ' lightgrey-color')} />
									</button>
									<button title='transfer' className='num-button center-text transfer-btn' onClick={this.specialBtnClick.bind(oThis, "transfer")}>
										<i className={'icon-sync-16' + (this.state.showInput2 == true && this.state.status == i18n.in_conversation ? '' : ' lightgrey-color')} />
									</button>
								</div>
							</div>
						</div>
					</div>
					{/* ===== /3 buttons ==== */}
				</div>
				{/* ======================= /DIGITS PANEL ABOVE ======================= */}
				<div className='keypad-container'>
					<div className='keypad-row'>
						<button className='num-button top-left-radius' onClick={this.callPanelClick.bind(oThis, "7")}>
							<span className='phone-number-val'>7</span>&nbsp;
							<span className='phone-letter-val'>PQRS</span>
						</button>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "8")}>
							<span className='phone-number-val'>8</span>&nbsp;
							<span className='phone-letter-val'>TUV</span>
						</button>
						<button className='num-button top-right-radius' onClick={this.callPanelClick.bind(oThis, "9")}>
							<span className='phone-number-val'>9</span>&nbsp;
							<span className='phone-letter-val'>WXYZ</span>
						</button>
					</div>
					<div className='keypad-row'>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "4")}>
							<span className='phone-number-val'>4</span>&nbsp;
							<span className='phone-letter-val'>GHI</span>
						</button>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "5")}>
							<span className='phone-number-val'>5</span>&nbsp;
							<span className='phone-letter-val'>JKL</span>
						</button>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "6")}>
							<span className='phone-number-val'>6</span>&nbsp;
							<span className='phone-letter-val'>MNO</span>
						</button>
					</div>
					<div className='keypad-row'>
						<button className='num-button center-text' onClick={this.callPanelClick.bind(oThis, "1")}>
							<span className='phone-number-val'>1</span>
						</button>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "2")}>
							<span className='phone-number-val'>2</span>&nbsp;
							<span className='phone-letter-val'>ABC</span>
						</button>
						<button className='num-button' onClick={this.callPanelClick.bind(oThis, "3")}>
							<span className='phone-number-val'>3</span>&nbsp;
							<span className='phone-letter-val'>DEF</span>
						</button>
					</div>
					<div className='keypad-row'>
						<button className='num-button center-text btm-left-radius' onClick={this.callPanelClick.bind(oThis, '#')}>
							<span className='phone-number-val'>#</span>
						</button>
						<button className='num-button center-text' onClick={this.callPanelClick.bind(oThis, "0")}>
							<span className='phone-number-val'>0</span>
						</button>
						<button className='num-button center-text star-sign btm-right-radius' onClick={this.callPanelClick.bind(oThis, '*')}>
							<span className='phone-number-val'>*</span>
						</button>
					</div>
				</div>
				<div className='call-btn-container'>
					<button className={'call-btn ' + callBtnColor} onClick={this.phoneIconClick.bind(oThis)}><i className='hc-icon icon-phone-16' /></button>
				</div>
				<AlertBox showAlert={this.state.showAlert} alertMsg={i18n.please_enter_no} alertClick={() => this.setState({ showAlert: false })} />
			</div>
		);
	}
}
