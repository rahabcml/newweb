/*******************************************
* Notes
* Name: IFun.jsx
* Details: Common Fuctions
* Functions: 
*        isInteger
*
* Example To Use: 
*        import IFun from './IFun.jsx';
*        let isInteger = IFun.isInteger;
********************************************/

//NOTES: Numbers.isInger cannot be applied in IE, so need this function
let IFun = {
    isInteger(num) {
        return (num ^ 0) === num;
    }
}
export default IFun;
