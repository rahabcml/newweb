import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Redirect } from 'react-router'
import i18nCommon from './i18n.jsx';
import AlertBox from './AlertBox.jsx';
let i18n = i18nCommon.login_variable;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: (localStorage && localStorage.username) ? localStorage.username : "",
      pw: (localStorage && localStorage.pw) ? localStorage.pw : "",
      rememberMe: false,
      showAlert: false
    }
  }

  handleSubmit(event) {
    let oThis = this;
    console.log('User Name: ' + this.state.username + ', PW: ' + this.state.pw + ', is logged in');
    event.preventDefault();

    // ========= TO BE UNCOMMENTED ==========
    // ======================= TBD =======================
    if (this.state.username == "abc" && this.state.pw == "123") {
      window.location.hash = 'home';
      sessionStorage.setItem("loggedIn", true);
      sessionStorage.setItem("fullName", "May Chan");
      sessionStorage.setItem("email", "abc.company@wise.com");
      sessionStorage.setItem("company_id", 16);
      sessionStorage.setItem("company_name", "ABC Company");
      sessionStorage.setItem("phone", "27361899");
      return
    }
    // ======================= /TBD =======================
    jQuery.ajax({
      method: 'POST',
      url: 'http://api.wiser/users/login',
      data: { username: this.state.username || "", pw: this.state.pw || "" }
    }).always(function (res) {

      console.log("res");
      console.log(res);
      let jsonData = res.jsonData;
      if (res == undefined || res.status == 400) {
        oThis.setState({ showAlert: true });
      } else {
        console.log("jsonData");
        console.log(jsonData);
        window.location.hash = 'home';
        sessionStorage.setItem("loggedIn", true);
        sessionStorage.setItem("fullName", jsonData["full_name"]);
        sessionStorage.setItem("company_id", jsonData.company_id);
        sessionStorage.setItem("company_name", jsonData.company_name);
        // ====== TBD =====
        sessionStorage.setItem("email", "abc.company@wise.com");
        // ====== /TBD =====
        // Expected to get: company id, user id, first name and last name
      }
    });
    // ========= /TO BE UNCOMMENTED  ==========
    // ========= TBD ==========
    // if (this.state.username == "abc" && this.state.pw == 123) {
    //   //this.setState({ fireRedirect: true });
    //   // sessionStorage.setItem("loggedIn", true);
    //   // sessionStorage.setItem("first", "Robert");
    //   // sessionStorage.setItem("last", "Feffress");
    //   window.location.hash = 'home';
    //   localStorage.setItem("loggedIn", true);
    //   localStorage.setItem("first", "Robert");
    //   localStorage.setItem("last", "Feffress");
    //   //localStorage.loggedIn = true;
    //   //localStorage.first = "Robert";
    //   //localStorage.last = "Feffress";
    // } else {
    //   this.setState({ showAlert: true });
    // }
    //  ========= /TBD ==========

    //window.location = './src/home.jsx';
    /// after checking, if password correct login to the website
  }

  toggleRememberMe() {
    this.setState({
      rememberMe: !this.state.rememberMe
    }, () => {
      if (rememberMe) {
        localStorage.username = this.state.username;
        localStorage.pw = this.state.pw;
      }
    });
  }

  render() {
    let oThis = this;

    return (
      <div className='row column align-center medium-6 large-4 container-padded'>

        <form className='log-in-form' onSubmit={this.handleSubmit.bind(oThis)}>
          <h4>{i18n.log_in}</h4>
          <label>
            {i18n.user_name}
            <input type="text" value={this.state.username} onChange={event => this.setState({ username: event.target.value })} />
          </label>

          <label>
            {i18n.password}
            <input type="password" value={this.state.pw} onChange={event => this.setState({ pw: event.target.value })} />
          </label>

          <label htmlFor="rememberMe">
            <input type="checkbox" id="rememberMe" ref="rememberMe" placeholder="Remember Me" onChange={this.toggleRememberMe.bind(oThis)} />
            <label htmlFor="rememberMe">{i18n.remmber_me}</label>
          </label>

          <input className="button expanded" type="submit" value="Submit" />

        </form>
        <AlertBox alertMsg={i18n.log_in_error} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
      </div>
    )
  }
}
