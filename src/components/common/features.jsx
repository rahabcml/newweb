import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router';
import $ from 'jquery';
import foundation from 'foundation-sites';
import i18nCommon from './i18n.jsx';
import Admin from '../admin/admin_home.jsx';
import Agent from '../agent/agent_home.jsx';

let i18n = i18nCommon.home_variable;


export default class Features extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    //$('#example-tabs').foundation();
    //$(document).foundation();
    $('#example-tabs').foundation();
  }

  render() {
    let oThis = this;

    console.log("oThis.props in admin render");
    console.log(oThis.props);
    return (
      <div className="body-content">
        <div className="header tabs-container">
          <div className="container">
            <div className='content-divide'>
              <h3>Features Dashboard</h3>

              <ul className="tabs" data-tabs id="example-tabs">
                <li className="tabs-title is-active">
                  <a href="#panel1" aria-selected="true">{i18n.admin_features}</a>
                </li>
                <li className="tabs-title">
                  <a data-tabs-target="panel2" href="#panel2">{i18n.agent_features}</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="tabs-content" data-tabs-content="example-tabs">
          <div className="tabs-panel is-active" id="panel1">
            <Admin />
          </div>
          <div className="tabs-panel" id="panel2">
            <Agent />
          </div>
        </div>
      </div>
    );
  }
}
