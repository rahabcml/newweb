import React, { Component } from 'react';

export default class container extends Component {
//module.exports = React.createClass({
    // ------------------ custom events ------------------ //

    // ------------------ /custom events ------------------ //
    // ------------------ app delegate ------------------ //
    // getInitialState() {
    //     return {};
    // }
    componentWillMount() {
        //let loggedIn = sessionStorage.getItem("loggedIn");
        let loggedIn = (localStorage && localStorage.loggedIn)? localStorage.loggedIn : false;
        if(loggedIn !=true){
            window.location.hash ='login';
        }else{
            window.location.hash ='home/features/admin';
        }
    }
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
    // ------------------ /app delegate ------------------ //
}//);
