/*******************************************
* Notes
* Name: AlertBox.jsx
* Details: 
* Properties: 

********************************************/

import React, { Component } from 'react';


export default class AlertBox extends Component {
    constructor(props) {
        super(props);
        console.log("props in alert");
        console.log(props);
        this.state = {
            showAlert: props.showAlert || false,
            showConfirm: props.showConfirm || false,
            alertMsg: props.alertMsg || "",
            confirmMsg: props.confirmMsg || "",
            alertClick: props.alertClick || function () { },
            confirmClick: props.confirmClick || function () { },
            cancelClick: props.cancelClick || function () { }
        }
        if (props.showAlert == true) {
            this.timer = setTimeout(_ => {
                this.state.alertClick();
            }, 2000);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            showAlert: nextProps.showAlert || false,
            showConfirm: nextProps.showConfirm || false,
            alertMsg: nextProps.alertMsg || "",
            confirmMsg: nextProps.confirmMsg || "",
            alertClick: nextProps.alertClick || function () { },
            confirmClick: nextProps.confirmClick || function () { },
            cancelClick: nextProps.cancelClick || function () { }
        });
        if (nextProps.showAlert == true) {
            this.timer = setTimeout(_ => {
                this.state.alertClick();
            }, 2000);
        }
    }

    alertClick() {
        this.state.alertClick();
        //this.setState({ showAlert: false });
    }
    confirmClick() {
        this.state.confirmClick();
        //this.setState({ showConfirm: false });
    }
    cancelClick() {
        this.state.cancelClick();
        //this.setState({ showConfirm: false });
    }

    render() {
        let oThis = this;
        return (
            <div>
                {this.state.showAlert ?
                    this.props.showInfo == true ?
                        <div>
                            <div className='infoAlert'>
                                <p className='message'>{this.state.alertMsg}</p>
                                <button className='confirmButton icon-simple-remove' type="submit" onClick={this.alertClick.bind(oThis)}></button>
                            </div>
                        </div> :
                        <div>
                            <div className='customAlert'>
                                <p className='message'>{this.state.alertMsg}</p>
                                <button className='confirmButton icon-simple-remove' type="submit" onClick={this.alertClick.bind(oThis)}></button>
                            </div>
                        </div>
                    : null}

                {this.state.showConfirm ?
                    <div className='customConfirm'>
                        <p className='confirmMsg'>{this.state.confirmMsg}</p>
                        <div className='btn-div'>
                        <button onClick={this.confirmClick.bind(oThis)}>OK</button>&nbsp;&nbsp;&nbsp;
                        <button onClick={this.cancelClick.bind(oThis)}>Cancel</button>
                        </div>
                    </div> : null}
            </div>
        );
    }
};