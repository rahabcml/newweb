import React, { Component } from 'react';
import { Link } from 'react-router';
import $ from 'jquery';

export default class Sidebar extends Component {
	componentDidMount() {
		$(".sidebar-link").click(function () {
			$(".sidebar-link").removeClass("sidebar-active"); // Remove 'sidebar-active' CSS Class from 'sidebar-link'
			$(this).addClass("sidebar-active");
		});
	}
	render() {
		let oThis = this;
		let details = this.props.details;
		let linkArray = details.linkArray

		let rows = [];
		for (let i = 0; i < linkArray.length; i++) {
			let item = linkArray[i];
			let aClassName = i == 0 ? 'sidebar-link sidebar-active' : 'sidebar-link';
			let iClassName = item.icon + " link-icon";

			rows.push(
				<li key={'sidebar' + i}>
					<Link to={item.link} className={aClassName}>
						<i className={iClassName}></i>
						<span className='sidebar-span link-text'>{item.name}</span>
					</Link>
				</li>
			);
		}
		return (
			<div className='page-container custom-scroll'>
				<div className='container row'>

					<div className='small-12 medium-3 columns remove-space sidebar'>
						<div className='sidebar-card'>
							<div className='sidebar-header'>{details.title}</div>
							<ul className='sidebar-links-group'>

								{rows}

							</ul>
						</div>
					</div>

					<div className='small-12 medium-9 columns remove-space main clearfix'>
						<div className='content-main clearfix'>
							{this.props.childrenContent}
						</div>
					</div>
				</div>
			</div >
		);
	}
}