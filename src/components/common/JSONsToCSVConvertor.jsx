/****************************************************************************************************************************
* Notes
* Name: JSONsToCSVConvertor.jsx
* Details: JSONs to be CSV file to be downloaded
*          example: JSONsToCSVConvertor(data, "Customer Profile Report", true);
* Properties: 
*   JSONData: JSONs.
*   ReportTitle: String. Name of the CSV file. 
*   Headers: Array. Header of the CSV. The length of Report Tile should  be same as JSONS.
****************************************************************************************************************************/
export default function JSONToCSVConvertor(JSONData, ReportTitle, Headers) {
  //NOTES: to avoid error, so have the conditions below
  if (ReportTitle == "" || ReportTitle == null) {
    console.log("ReportTitle is required");
    return;
  } else if (JSONData == "" || JSONData == null || JSONData == []) {
    console.log("JSONData is required");
    return;
  } else if (Headers == "" || Headers == null || Headers == []) {
    console.log("Headers is required");
    return;
  }
  //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
  let arrDatas = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
  //NOTES: to avoid error, so have the conditions below
  if (arrDatas.length != Headers.length) {
    console.log("The length of JSON and header have to be the same");
    return;
  }
  console.log("arrDatas");
  console.log(arrDatas);
  console.log("arrDatas.length");
  console.log(arrDatas.length);
  let CSV = '';
  for (let i = 0; i < arrDatas.length; i++) {
    let arrData = arrDatas[i];
    console.log("i");
    console.log(i);
    console.log("arrData");
    console.log(arrData);

    //Set Header title in first row or line

    CSV += Headers[i] + '\r\n\n';

    //This condition will generate the Label/Header
    let row = "";

    //This loop will extract the label from 1st index of on array
    for (let index in arrData[0]) {

      //Now convert each value to string and comma-seprated
      row += index + ',';
    }

    row = row.slice(0, -1);

    //append Label row with line break
    CSV += row + '\r\n';

    //1st loop is to extract each row
    for (let i = 0; i < arrData.length; i++) {
      let row = "";

      //2nd loop will extract each column and convert it in string comma-seprated
      for (let index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }

      row.slice(0, row.length - 1);

      //add a line break after each row
      CSV += row + '\r\n';
    }
    if(i != arrDatas.length-1){
      CSV += Headers[i] + '\r\n\n';
    }
    console.log("CSV");
    console.log(CSV);
  }

  //Generate a file name
  let fileName = "";
  //let fileName = "MyReport_";
  //this will remove the blank-spaces from the title and replace it with an underscore

  fileName += ReportTitle.replace(/ /g, "_");

  //Initialize file format you want csv or xls
  let uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
  //NOTES for xls file(but will have warning if opened by excel): let uri = 'data:application/vnd.ms-excel,' + escape(CSV);
  //NOTES for xlsx file(but will have errors if opened by excel): let uri = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,' + escape(CSV);

  // Now the little tricky part.
  // you can use either>> window.open(uri);
  // but this will not work in some browsers
  // or you will not get the correct file extension    

  //this trick will generate a temp <a /> tag
  let link = document.createElement("a");
  link.href = uri;

  //set the visibility hidden so it will not effect on your web-layout
  link.style = "visibility:hidden";
  //link.download = fileName + ".csv";
  link.download = fileName + ".csv";

  //this part will append the anchor tag and remove it after automatic click
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
} 