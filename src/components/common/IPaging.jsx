/*******************************************
* Notes
* Name: IPaging.jsx
* Details: A paging component
*           looks like: Page 1 of 2 |< <>  >|
* Properties: 
*        total: Integer.
*        page:  Integer. Page to show now.
*        pageClick: Function. Handle state of page change.
*        numbers: (Optional) Integer. Numbers per page. Default 100.
********************************************/

import React, { Component } from 'react';
import i18nCommon from './i18n.jsx';
let i18n = i18nCommon.paging_variable;

export default class IPaging extends React.Component {
    constructor(props) {
        super(props);
        console.log("props in paging");
        console.log(props);
        let numbers = this.props.numbers || 100;
        let totalPage = Math.ceil(this.props.total / numbers) || 1;
        let page = this.props.page || 1;
        let smallestDisable, smallerDisable, largerDisable, largestDisable = false;
        if (totalPage == 1) {
            smallestDisable = true; smallerDisable = true; largerDisable = true; largestDisable = true;
        } else {
            if (page == 1) {
                smallestDisable = true; smallerDisable = true; largerDisable = false; largestDisable = false;
            }
            else if (1 < page && page < totalPage) {
                smallestDisable = false; smallerDisable = false; largerDisable = false; largestDisable = false;
            }
            else if (page == totalPage) {
                smallestDisable = false; smallerDisable = false; largerDisable = true; largestDisable = true;
            }
        }

        this.state = {
            totalPage: Math.ceil(this.props.total / numbers) || 1,
            page: page,
            numbers: numbers,
            smallestDisable: smallestDisable,
            smallerDisable: smallerDisable,
            largerDisable: largerDisable,
            largestDisable: largestDisable
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps in ipaging");
        console.log(nextProps);
        let numbers = nextProps.numbers || 100;
        let smalizedPage = Math.ceil(nextProps.total / numbers);
        let totalPage = smalizedPage == 0 ? 1 : smalizedPage;
        let page = nextProps.page || 1;
        this.setState({
            totalPage: totalPage,
            page: page,
            numbers: nextProps.number || 100
        });
        if (totalPage == 1) {
            this.setState({ smallestDisable: true, smallerDisable: true, largerDisable: true, largestDisable: true });
        } else {
            if (page == 1) {
                this.setState({ smallerDisable: true, smallestDisable: true, largerDisable: false, largestDisable: false });
            }
            else if (1 < page && page < totalPage) {
                this.setState({ smallerDisable: false, smallestDisable: false, largerDisable: false, largestDisable: false });
            }
            else if (page == totalPage) {
                this.setState({ smallerDisable: false, smallestDisable: false, largerDisable: true, largestDisable: true });
            }
        }
    }

    pageClick(type) { //NOTES: type: smallest //smaller //larger //largest
        let totalPage = this.state.totalPage;
        if (this.props.pageClick) {
            if (type == "smallest") {
                if (this.state.smallestDisable != true) {  // NOTS: verify because for older version of browser the disabled class does not disabled the a tag
                    this.props.pageClick(1);
                    this.setState({ smallestDisable: true, smallerDisable: true });
                }
            } else if (type == "largest") {
                if (this.state.largestDisable != true) {  // NOTS: verify because for older version of browser the disabled class does not disabled the a tag
                    this.props.pageClick(totalPage);
                }
            } else if (type == "larger") {
                if (this.state.largerDisable != true) {  // NOTS: verify because for older version of browser the disabled class does not disabled the a tag
                    let newPage = this.state.page + 1
                    this.props.pageClick(newPage);
                }
            } else if (type == "smaller") {
                if (this.state.smallerDisable != true) {  // NOTS: verify because for older version of browser the disabled class does not disabled the a tag
                    let newPage = this.state.page - 1
                    this.props.pageClick(newPage);
                }
            }
        } else {
            //TO DO: if in debug mode should show waring have properties pageClick for this component
        }
    }


    render() {
        console.log("this.state in render of ipaging");
        console.log(this.state);
        return (
            <div className='right paganate'>
                {i18n.page}&nbsp;{this.state.page}{i18n.of}{this.state.totalPage}&nbsp;
                <a className={this.state.smallerDisable == true ? 'disabled-button' : ""} disabled={this.state.smallestDisable} onClick={this.pageClick.bind(this, "smallest")}><span className='i-icon icon-left-double-arrow-glyph24 double-arrow' /></a>
                {/* <a disabled={this.state.smallerDisable} onClick={this.pageClick.bind(this, "smaller")}><span className='i-icon icon-left-arrow-glyph24' /></a>
                <a disabled={true} onClick={this.pageClick.bind(this, "larger")}><span className='i-icon icon-right-arrow-glyph24' /></a> */}
                <a className={this.state.smallerDisable == true ? 'disabled-button' : ""} onClick={this.pageClick.bind(this, "smaller")}><span className='i-icon icon-left-arrow-glyph24' /></a>
                <a className={this.state.largerDisable == true ? 'disabled-button' : ""} onClick={this.pageClick.bind(this, "larger")}><span className='i-icon icon-right-arrow-glyph24' /></a>
                <a className={this.state.largestDisable == true ? 'disabled-button' : ""} onClick={this.pageClick.bind(this, "largest")}><span className='i-icon icon-right-double-arrow-glyph24 double-arrow' /></a>
            </div>
        );
    }
};