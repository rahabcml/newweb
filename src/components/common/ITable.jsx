/*******************************************
* Notes
* Name: ITable.jsx
* Details: A table component
* Properties: 
* Common properties:
*   name: String. Header name.
*   flex: Interger. For flex-box.
*   render: (Optional) Function. Return properties: 1. text 2. row data
*   searching: (Optional) Boolean. Default false
*   paging: (Optional) Boolean. Default false.
*   info: (Optional) Boolean. Default false. 
*   colvis: (Optional) Bollean. Deafult false.
*   checked: (Optional) Boolean. The row to be selected if have radio button column
* Type property: 
*   EMPTY: none.
*   customText: 
*        convert: Function. send data, return string to be shown.
*   checkbox,switch:
*        onChange: Function.
*   radio:
*        id: String || Interger. Have to be unique and no space
*        radioName: String.
*        onChange: Function. 
*   combo:   
*        value: String.
*        options: Array.
*        onChange: Function.
*        Others go https://github.com/JedWatson/react-select for ref.
*   textField: 
*        onChange: Function.
*   link: 
*        text: String.
*        onClick: Function.
*   icon: 
*        className: String.
*        onClick: Function.
********************************************/

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ICombo from './ICombo.jsx';
import $ from 'jquery';
import foundation from 'foundation-sites';
import DataTable from 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';
import 'datatables.net-buttons/js/buttons.colVis.min.js';

export default class ITable extends React.Component {
    constructor(props) {
        super(props);
        let row_state = [];
        let data = this.props.data || [];
        if (data && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                row_state[i] = data[i].checked;
            }
        }
        this.state = {
            data: data,
            row_state: row_state
        }
    }

    componentDidMount() {

        if (this.props.reloadTable != false) {
            this.reloadTable();
        }

        let data = this.props.data || [];
        let dataLength = data.length
        let row_state = [];
        if (data && dataLength > 0) {
            for (let i = 0; i < dataLength; i++) {
                row_state[i] = data[i].checked
            }
        }
        this.setState({
            data: data,
            row_state: row_state
        });
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps in grid");
        console.log(nextProps);
        let data = nextProps.data || [];
        let dataLength = data.length
        let row_state = [];
        if (data && dataLength > 0) {
            for (let i = 0; i < dataLength; i++) {
                row_state[i] = data[i].checked
            }
        }
        this.setState({
            data: data,
            row_state: row_state
        });
        if (nextProps.reloadTable == true) { this.reloadTable(); }
    }

    toggle(source) {
        checkboxes = document.getElementsByName('foo');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    reloadTable() {
        let oThis = this;
        let tableName = this.props.tableName || "tableName";
        let dataTableName = '#dataTable' + tableName;

        let searching = this.props.searching === true ? true : false;
        let paging = this.props.paging === true ? true : false;
        let info = this.props.info === true ? true : false;
        let buttons = this.props.buttons ? this.props.buttons : [];

        let columnDefs = this.props.columnDefs ? this.props.columnDefs.slice() : [];
        let columnLength = this.props.columns.length;

        if (this.props.colvis == true) {
            buttons.push(
                {
                    extend: 'colvis',
                    columns: ':not(.noVis)',
                    // prefixButtons: [
                    //     {
                    //         extend: 'colvisGroup',
                    //         text: 'Select All',
                    //         // hide: ':visible',
                    //         action: function (e, dt, node, config) {
                    //             if (config.checked == true) {
                    //                 this.active(false);
                    //                 dt.columns('*').visible(false);
                    //             } else {
                    //                 this.active(true);
                    //                 dt.columns('*').visible(true);
                    //             }
                    //             config.checked = !config.checked
                    //         },
                    //         active: false,
                    //         checked: false
                    //     }
                    // ]
                }
            );

            if (columnLength > 6) {
                let targets = [];
                let j = 6;
                if (columnDefs.length > 0) {
                    if (columnDefs[0].targets == 0 && columnDefs[0].className == 'noVis')
                        j = 7;
                }
                for (let i = j; i < columnLength; i++) {
                    targets.push(i);
                }
                columnDefs.push(
                    {
                        "targets": targets,
                        "visible": false
                    });
            }
        }

        oThis.table = $(dataTableName).DataTable(
            {
                stateSave: true,
                "bPaginate": paging,
                "searching": searching,
                "paging": paging,
                "info": info,
                dom: '<"table-top"Bf><"scroll_table"rt><"table-btm"ip>', //NOTES: deails refer to 'DOM positioning' in dataTable website
                "order": [],
                "columnDefs": columnDefs,
                buttons: buttons
            }
        );

        //$(dataTableName).wrap('<div class="scroll_table"></div>');
        //NOTES: When click Column visibility button, check is the visible is six already, if yes, all other rows disable
        $(dataTableName + '_wrapper').on(".buttons-colvis").click(function () {
            var bol = $(".buttons-columnVisibility.active").length > 5;
            if (bol == true) {
                $(".buttons-columnVisibility").not(".active").addClass("disabled");
            }
        });

        //NOTES: Function will be active when selecting collumn in column visibility
        $(dataTableName).on('column-visibility.dt', function (e, settings, column, state) {
            console.log("e");
            console.log(e);
            console.log("settings");
            console.log(settings);
            console.log('Column:', column, "State:", state);
            var bol = $(".buttons-columnVisibility.active").length > 5;
            console.log("bol");
            console.log(bol);
            if (bol == true) {
                $(".buttons-columnVisibility").not(".active").addClass("disabled");
            } else {
                $(".buttons-columnVisibility").not(".active").removeClass("disabled");
            }
        });

        if (this.props.reloadTableTrue) {
            this.props.reloadTableTrue();
        }
        // hight light single click row
        $(dataTableName + ' tbody').on('click', 'tr', function (e) {
            $(dataTableName + ' tbody tr').removeClass('highlight');
            $(this).addClass('highlight')
        });
    }

    onDoubleClick() {
        if (this.props.onDoubleClick) {
            this.props.onDoubleClick();
        }
    }

    render() {
        let oThis = this;
        let headers = [];
        let contentPart = [];
        let tableName = this.props.tableName || "tableName";
        if (typeof this.props.columns !== 'undefined' && this.props.columns.length > 0) {

            for (let i = 0, len = this.props.columns.length, col = this.props.columns; i < len; i++) {

                let style = {
                    //flex: col[i].flex || 1,
                };

                headers.push(
                    <th key={'title' + i} style={style}>
                        <span>{col[i].name ? col[i].name : ""}</span>
                    </th>
                )
            }
            if (typeof this.state.data !== 'undefined' && this.state.data.length > 0) {
                for (let j = 0, len = this.state.data.length, dat = this.state.data; j < len; j++) {
                    let row = [], clickedRow = false, rowRef = "";
                    for (let i = 0, len = this.props.columns.length, col = this.props.columns; i < len; i++) {
                        let style = {
                            //flex: col[i].flex || 1
                        };

                        let name = ""
                        if (col[i].id == "0") {
                            name = name = dat[j]
                        } else {
                            name = dat[j][col[i].id] || "";
                        }

                        if (col[i].type == 'checkbox') {
                            // use row_state, nto data.checked directly, because can improve the table for the field of checkbox, not necessary checked when more than 2 check fields
                            row.push(
                                <td key={tableName + 'col' + i + 'dat' + j} style={style}>
                                    <input type="checkbox" value={"check" + j} onChange={col[i].onChange.bind(this, j, !this.state.row_state[j])} checked={this.state.row_state[j]} />
                                </td>
                            )
                        }

                        else if (col[i].type == 'radio') {
                            this.props.checked == name ? clickedRow = true : clickedRow = false;
                            rowRef = name;
                            row.push(
                                <td key={tableName + 'col' + i + 'dat' + j} style={style} className='radio-buttons'>
                                    <input type="radio" name={col[i].radioName} id={name} value={name} checked={this.props.checked == name} onChange={col[i].onChange.bind(this, j, dat[j])} />
                                    <div className="check"></div>
                                    <label htmlFor={name}></label>
                                </td>
                            )
                        }

                        else if (col[i].type == 'switch') {
                            row.push(
                                <td key={tableName + 'col' + i + 'dat' + j} style={style}>
                                    <div className="switch round small right">
                                        <input id={tableName + 'switch' + j} name={tableName + 'switchName' + j} type="checkbox" value={"switch" + j} onChange={col[i].onChange.bind(this, j, !this.state.row_state[j])} checked={this.state.row_state[j]} />
                                        <label htmlFor={tableName + 'switch' + j}></label>
                                    </div>
                                </td>
                            )
                        }
                        else if (col[i].type == 'customText') {
                            let customText = col[i].convert(j);
                            row.push(
                                <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                    <span>{customText}</span>
                                </td>
                            )
                        }
                        else if (col[i].type == 'combo') {
                            row.push(
                                <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                    <div className='combo-width'>
                                        <ICombo
                                            valueField={col[i].valueField}
                                            displayField={col[i].displayField}
                                            options={col[i].options}
                                            value={name}
                                            onChange={(event) => {
                                                col[i].onChange(event, j)
                                            }}
                                        />
                                    </div>
                                </td>
                            )
                        }
                        else if (col[i].type == 'link') {
                            let linkText = col[i].text || "";
                            row.push(
                                <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                    <a onClick={col[i].onClick.bind(oThis, j, dat[j])}>{linkText}</a>
                                </td>
                            )
                        }
                        else if (col[i].type == 'icon') {
                            row.push(
                                <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                    <a onClick={col[i].onClick.bind(oThis, j, dat[j])} className='table-icon-a-tag'>
                                        <div className={col[i].className + ' table-icon'} />
                                    </a>
                                </td>
                            )
                        }
                        else if (col[i].type == 'textField') {
                            row.push(
                                <td key={tableName + 'col' + i + 'dat' + j} style={style}>
                                    <input type="text" value={name} onChange={col[i].onChange.bind(oThis, j)} />
                                </td>
                            )
                        } else {
                            if (col[i].render) {
                                row.push(
                                    <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                        <span>{col[i].render(name, dat[j])}</span>
                                    </td>
                                )
                            } else {
                                row.push(
                                    <td key={tableName + 'col' + i + 'col' + i + 'dat' + j} style={style}>
                                        <label htmlFor={rowRef}>
                                            <span>{name}</span>
                                        </label>
                                    </td>
                                )
                            }
                        }
                    }
                    let rowClass = clickedRow == true ? "background-light-blue" : "background-transparent"
                    let rowKey = name;//'rowContentCol' + j
                    contentPart.push(//single or double click event can add to the div below
                        <tr key={'rowContentCol' + j} className={rowClass} onDoubleClick={oThis.props.onDoubleClick ? oThis.props.onDoubleClick.bind(oThis, j, dat[j]) : () => { }}>
                            {row}
                        </tr>
                    );
                }

            }
        }

        return (
            <table id={'dataTable' + tableName} className={this.props.tableClass ? this.props.tableClass : 'config_customer_profile_table'}>
                {/* <table style={{ padding: '10px' }}> */}
                <thead>
                    <tr>
                        {/* <tr style={{ display: 'flex' }}> */}
                        {headers}
                    </tr>
                </thead>
                <tbody>
                    {contentPart}
                </tbody>
            </table>
        );
    }
};