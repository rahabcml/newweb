import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import i18nCommon from '../common/i18n.jsx';
let i18n = i18nCommon.home_variable;

export default class Agent extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }

  moduleClicked(module, defaultPage) {
    let href = "";
    if (defaultPage != "") {
      console.log("defaultPage");
      console.log(defaultPage);
      href = 'home/' + module + "/" + defaultPage;
    } else {
      href = 'home/' + module;
    }
    //let href =  'home/' + module +'/default';
    window.location.hash = href;
    console.log("href");
    console.log(href);
  }

  render() {
    let oThis = this;
    const { fireRedirect } = this.state;

    return (
      <div className="dashboard-features">
        <div className="container">
          <div className="row">

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "InboundHome", "")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/addressbook.png" />
                <span className="feature-title">{i18n.inbound}</span>
                <p className="feature-desc remove-space">{i18n.inbound_call}</p>
              </div>
            </div>

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "OutbuondHome", "")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/input_form.png" />
                <span className="feature-title">{i18n.outbound}</span>
                <p className="feature-desc remove-space">{i18n.outbound_call}</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
