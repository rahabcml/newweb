/*************************************************************************************************
* Notes
* Name: outbound_home.jsx
* Class: OutbuondHome
* Details: Home Page of Outbound
*************************************************************************************************/
import React, { Component } from 'react';
import i18nCommon from '../../common/i18n.jsx';
import ICombo from '../../common/ICombo.jsx';
let i18n = i18nCommon.outbound_variable;
import $ from 'jquery';
import DataTable from 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';
import 'datatables.net-buttons/js/buttons.colVis.min.js';
import AlertBox from '../../common/AlertBox.jsx';


export default class OutbuondHome extends Component {
	constructor(props) {
		let anyAll = sessionStorage.getItem("outAnyAll") ? sessionStorage.getItem("outAnyAll") : "any";
		console.log("anyAll in construct of outbound home");
		console.log(anyAll);
		// console.log("addArray in construct of outbound home");
		// console.log(sessionStorage.getItem("outAddArray"));
		let showFilter = sessionStorage.getItem("showFilter") ? sessionStorage.getItem("showFilter") : "false";
		console.log("showFilter in constructor");
		console.log(showFilter);
		let addArray = sessionStorage.getItem("outAddArray") ? JSON.parse(sessionStorage.getItem("outAddArray")) : [{ condition: "", selector: "", text: "" }];
		console.log("addArray in construct of outbound home");
		console.log(addArray);
		let query = props.location.query
		console.log("query.campaignId");
		console.log(query.campaignId);
		super(props);
		this.state = {
			showGrid: false,
			outboundList: [],
			campaignId: query.campaignId ? Number(query.campaignId) : -1,
			campaignName: query.name || "",
			showAlert: false,
			anyAll: anyAll,
			addArray: addArray,
			headers: [],
			showFilter: showFilter,
			gridData: []
		}
		this.any_all_array = [{ name: "Match any of the following", value: 'any' }, { name: "Match all of the following", value: 'all' }];
	}
	componentWillMount() {
		if (this.state.campaignId != -1) {
			if (this.state.addArray != [{ condition: "", selector: "", text: "" }]) {
				this.nextStepClick();
			} else {
				this.applyFilterCick();
			}
		}
		// TO DO: get the outbound campaign list for combo from server
		let outboundList = [{ id: 1, name: "Spring Promotion" }, { id: 2, name: "Summer Promotion" }, { id: 3, name: "Autumn Promotion" }, { id: 4, name: "Winter Promotion" }] 

		this.setState({ outboundList: outboundList });
	}
	componentDidMount() {
		//        get data from server by this.state.campaignId ============= TBD =============
		let data = [{ id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 2, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 3, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 4, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 5, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 6, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 7, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 8, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 9, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 10, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 11, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 12, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 13, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 14, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 15, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
		{ id: 16, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 17, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
		{ id: 18, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }
		]


		let oThis = this;
		//get table header name from server
		let columns = [{ data: "full", title: "Full Name" }, { data: "first", title: "First Name" }, { data: "last", title: "Last Name" }, { data: "email", title: "Email" }, { data: "mobile", title: "Mobile" }, { data: "home", title: "Home" }, { data: "work", title: "Work" }, { data: "other", title: "Other" }, { data: "add1", title: "Add 1" }, { data: "add2", title: "Add 2" }, { data: "add3", title: "Add 3" }, { data: "country", title: "Country" }, { data: "ref", title: "Reference" }, { data: "occ", title: "Occupancy" }, { data: "education", title: "Education" }, { data: "pid", title: "PID" }, { data: "source", title: "Source" }];

		this.setState({ headers: columns });

		columns.unshift(
			{
				data: null,
				className: 'noVis', //NOTES: no column visibility
				orderable: false,
				defaultContent: '<a onClick={} class="editor_edit icon-zoom-in-16"></a>'
			}
		)

		//        ============= //TBD =============
		// get data from server
		let columnDefs = [];
		let columnLength = columns.length || 0;
		let targets = [];
		if (columnLength > 6) {
			for (let i = 7; i < columnLength; i++) {
				targets.push(i);
			}
			columnDefs.push(
				{
					"targets": targets,
					"visible": false
				});
		}
		this.table = $('#agentCallTable').DataTable({
			stateSave: true,
			columns: columns,
			// ====== Uncomment when Server Side is done ======
			// "processing": true,
			// "serverSide": true,
			// "deferLoading": 0,
			// "ajax": {
			//     "url": "http://api.wiser/profiles",
			//     "data": function (d) {
			//         d.filter = JSON.stringify(oThis.state.queryData);
			//     }
			// },
			// ====== //Uncomment when Server Side is done ======
			//====== TBD ======
			data: data,
			//====== /TBD ======
			"bPaginate": true,
			"paging": true,
			"searching": false,
			"info": true,
			dom: '<"table-top"Bf><"scroll_table"rt><"table-btm"ip>',
			columnDefs: columnDefs,
			buttons: [
				{
					extend: 'colvis',
					columns: ':not(.noVis)',
				}
			],
			"order": []
		});

		// NOTES: table row clicked, edit row record

		// select record function
		let selectFn = e => {
			console.log("data");
			console.log(data);
			oThis.state.addArray;
			// remember the last filter, when return to thsi page can use it
			sessionStorage.setItem("outAnyAll", oThis.state.anyAll);
			sessionStorage.setItem("outAddArray", JSON.stringify(oThis.state.addArray));
			let showFilter = $('#showFilter').first().attr('aria-expanded');
			console.log("$('#showFilter')");
			console.log($('#showFilter'));
			console.log("$('#showFilter').first()");
			console.log($('#showFilter').first());
			console.log("showFilter when change page");
			console.log(showFilter);
			sessionStorage.setItem("showFilter", showFilter);
			console.log("JSON.stringify(oThis.state.addArray)");
			console.log(JSON.stringify(oThis.state.addArray));
			let campaignName = encodeURIComponent(oThis.state.campaignName);
			// Redirect to call page
			window.location.hash = "home/OutboundCall?campaignId=" + oThis.state.campaignId + "&recId=" + data.id + "&name=" + campaignName;
		}
		// click icon triger selected
		$('#agentCallTable').on('click', 'a.editor_edit', function (e) {
			e.preventDefault();
			let data = oThis.table.row($(this).parents('tr')).data();
			selectFn(data);
		});
		// double click row triger selected
		$('#agentCallTable tbody').on('dblclick', 'tr', function (e) {
			e.preventDefault();
			let data = oThis.table.row($(this).parents('tr')).data();
			selectFn(data);
		});
		// hight light single click row
		$('#agentCallTable tbody').on('click', 'tr', function (e) {
			$('#agentCallTable tbody tr').removeClass('highlight');
			$(this).addClass('highlight')
		});
		//NOTES: When click Column visibility button, check is the visible is six already, if yes, all other rows disable
		$('#agentCallTable_wrapper').on(".buttons-colvis").click(function () {
			let bol = $(".buttons-columnVisibility.active").length > 5;
			if (bol == true) {
				$(".buttons-columnVisibility").not(".active").addClass("disabled");
			}
		});

		//NOTES: Function will be active when selecting collumn in column visibility
		$('#agentCallTable').on('column-visibility.dt', function (e, settings, column, state) {
			let bol = $(".buttons-columnVisibility.active").length > 5;
			if (bol == true) {
				$(".buttons-columnVisibility").not(".active").addClass("disabled");
			} else {
				$(".buttons-columnVisibility").not(".active").removeClass("disabled");
			}
		});

	}
	addClick() {
		//NOTES: 'Add Condition' button clicked
		let addArray = this.state.addArray.slice();
		addArray.push({ condition: "", selector: i18n.is, text: "" })
		this.setState({ addArray: addArray });
	}

	nextStepClick() {
		if (this.state.campaignId == -1) {
			this.setState({ alertMsg: i18n.please_select_campaign, showAlert: true });
		} else {
			this.setState({ showGrid: true });
			// === TO BE UNCOMMENTED ==
			//$('#agentCallTable').DataTable().ajax.reload();
			// === /TO BE UNCOMMENTED ==
		}
	}
	applyFilterCick() {
		// === TO BE UNCOMMENTED ==
		//$('#agentCallTable').DataTable().ajax.reload();   // or $('#agentCallTable').DataTable().draw();
		// === /TO BE UNCOMMENTED ==
	}
	render() {
		let oThis = this;
		let displayGrid = this.state.showGrid ? 'block' : 'none';

		let conditionRows = [];
		let addArray = this.state.addArray.slice();
		if (addArray.length > 0) {
			for (let i = 0; i < addArray.length; i++) {
				conditionRows.push(
					<div key={"query-rows" + i} className='expanded'>
						<div className="filter-inputs">
							<ICombo placeholder={i18nCommon.choose_condition} displayField='title' valueField='data' options={this.state.headers} value={addArray[i].condition} onChange={(o) => { addArray[i].condition = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
							<ICombo displayField={0} valueField={0} options={i18nCommon.selector_array} value={addArray[i].selector} onChange={(o) => { addArray[i].selector = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
							<div>
								<input type='text' value={addArray[i].text} onChange={event => {
									addArray[i].text = event.target.value;
									this.setState({ addArray: addArray })
								}} />
							</div>
							<div className="remove-icon">
								<div className='icon-trash-16 i-icon' onClick={() => { addArray.splice(i, 1); this.setState({ addArray: addArray }); }} />
							</div>
						</div>
					</div>
				)
			}
		}



		return (
			<div id='outbound_home' className="page-container custom-scroll">
				{/* =========== 1st Part =========== */}
				<div className='container'>
					<div className="main-card">
						<div className="content-divide">
							<h3>{i18n.choose_a_campaign}</h3>
							<div className='row flex-container upload-file'>
								<div className='medium-6 columns'>
									<ICombo placeholder={i18n.select_campaign_list} options={oThis.state.outboundList} displayField='name' valueField='id'
										clearable={false} value={this.state.campaignId} onChange={(o) => {
											oThis.setState({ campaignId: o.value, campaignName: o.label });
										}} />
								</div>
								<div className='medium-6 columns'>
									<button className='button right' onClick={oThis.nextStepClick.bind(oThis)}>{i18nCommon.next_step}</button>
								</div>
							</div>
						</div>
					</div>
					{/* =========== 2nd Part =========== */}
					<div className='main-card' style={{ display: displayGrid }}>
						<div className="content-divide">
							<h3>{i18n.agent_call_list}</h3>
							<div className='moreFilter row'>
								<button className='grey-button right' data-toggle="collapse" data-target="#showFilter" aria-expanded='false'><span className='icon-settings-16 i-icon i-icon-right' /></button>
							</div>
							<div className='row'>
								<div id='showFilter' className={this.state.showFilter == 'true' ? 'collapse in' : 'collapse'}>
									<div className='outer-padding'>
										{/* An extra div container is made because of the animating issue */}
										<div className='inner-padding'>
											<div className='filter-text-select'>
												<ICombo displayField='name' valueField='value' options={this.any_all_array} value={this.state.anyAll} onChange={(o) => { this.setState({ anyAll: o ? o.value : "" }) }} clearable={false} />
											</div>
											{conditionRows}
											<a className='add-condition-link' onClick={oThis.addClick.bind(oThis)}><span className='icon-add-16' />{i18nCommon.add_condition}</a>
											<button className='right' onClick={oThis.applyFilterCick.bind(oThis)}>{i18nCommon.apply_filter}</button>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div id='table_wrapper' className='margin-top-15'>
									<table id='agentCallTable' />
								</div>
							</div>
						</div>
					</div>
					<AlertBox alertMsg={this.state.alertMsg} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
				</div>
			</div>
		);
	}
}
