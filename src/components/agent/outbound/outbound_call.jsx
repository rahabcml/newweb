import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import InputForm from '../../admin/input_form/input_form.jsx'
import ProHisPop from '../../admin/address_book/profile_history_popup.jsx';
import ScreenPhone from '../../common/screen_phone.jsx';
import OutboundOthers from './outbound_others.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.outbound_variable;
// let x = null;//timer
export default class OutboundCall extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);
		let query = props.location.query
		this.state = {
			campaignId: query.campaignId || -1,
			recId: query.recId,
			campaignName: query.name || "",
			data: [],
			imagePreviewUrl: '',
			showProfile: false,
			to: '',
			type: 'phone'
		}
	}

	componentWillMount() {
		// TO DO: get data from server by campaign id and rec id, the img in data should be the link of image
		// let data = {
		// 	"gender": "Male", "First Name": "Kenneth", "Last Name": "Wong", "Email": "kwong@gmail.com", "Mobile": 97091235, "Home": "", "Work": 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", img: "someone.jpg"
		// }
		let data = { from: 23456789, id: 1, title: "Mr.", gender: "Male", full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: [97091235, 55346128], home: [], work: [23147690], other: [53123456], fax: [39972345], add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", img: "someone.jpg" }
		this.setState({ data: data, imagePreviewUrl: data.img })
	}
	// ------------------------------- /delegate ------------------------------- //
	backCampaignClick() {
		window.location.hash = "home/OutbuondHome?campaignId=" + this.state.campaignId + "&name=" + this.state.campaignName;
	}
	userProfileClick() {
		this.setState({ showProfile: true });
	}
	formatPhone(tel) {
		//char = { 0: '(+', 3: ') ', 7: ' ' }; //NOTES: For next version, have country code, become  (+852)1234 5678
		tel = String(tel);
		let numbers = tel.replace(/\D/g, ''),
			char = { 4: ' ' };
		tel = '';
		for (let i = 0; i < numbers.length; i++) {
			tel += (char[i] || '') + numbers[i];
		}
		return tel
	}
	callClick(tel) {
		this.setState({ type: 'phone' }, () => {
			this.refs.child.callClick(tel);
		});
	}
	// called by Screen Phone component
	changeParent(tel, otherNum) {
		this.setState({ to: tel });
		if (otherNum.length > 0) {
			let data = Object.assign({}, this.state.data);
			data.other = [tel];
			this.setState({ data: data, to: tel });
		}
	}
	emailClick(email) {
		this.setState({ type: 'email', to: email });
	}
	smsClick(tel) {
		this.setState({ type: 'sms', to: tel });
		console.log("tel");
		console.log(tel);
	}
	faxClick(tel) {
		this.setState({ type: 'fax', to: tel });
		console.log("tel");
		console.log(tel);
	}
	// ======== POP-UP FUNCTION ========
	//Edit Profile Buttons:
	epConfirmClick() {
		this.setState({ showProfile: false });
	}

	epCancelClick() {
		this.setState({ showProfile: false });
	}

	// shouldComponentUpdate() {
	// 	return false
	// }
	render() {
		let oThis = this;
		//Render the image
		let { imagePreviewUrl } = this.state;
		let $imagePreview = null;
		if (imagePreviewUrl) {
			$imagePreview = (<img className="img-center cp-circle" src={"assets/imgs/" + imagePreviewUrl} alt="" />);
		} else {
			$imagePreview = <div className="icon-focus-24 img-center cp-circle">
			</div>
		}
		// let callBtnColor = this.state.status == i18n.calling || this.state.status == i18n.in_conversation ? "call-btn-end" : "call-btn-ready"

		//
		let mobileContent = [], homeContent = [], workContent = [], otherContent = [], smsContent = [], faxContent = [];
		let { mobile, home, work, other, fax, email, full } = this.state.data;
		let { type, to } = this.state;
		for (let i = 0; i < mobile.length; i++) {
			let theNum = mobile[i];
			mobileContent.push(
				<div key={"mobile" + i} className={this.state.to == theNum && type == 'phone' ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_mobile}
				</div>
			)
			smsContent.push(
				<div key={"sms" + i} className={to == theNum && type == 'sms' ? 'color-main-theme-bold' : ''} onClick={this.smsClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}
				</div>
			)
		}
		for (let i = 0; i < home.length; i++) {
			let theNum = home[i];
			homeContent.push(
				<div key={"home" + i} className={this.state.to == theNum && type == 'phone' ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_home}
				</div>
			)
		}
		for (let i = 0; i < work.length; i++) {
			let theNum = work[i];
			workContent.push(
				<div key={"work" + i} className={this.state.to == theNum && type == 'phone' ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_work}
				</div>
			)
		}
		for (let i = 0; i < other.length; i++) {
			let theNum = other[i];
			otherContent.push(
				<div key={"other" + i} className={this.state.to == theNum ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_other}
				</div>
			)
		}
		for (let i = 0; i < fax.length; i++) {
			let theNum = fax[i];
			console.log("fax num");
			console.log(theNum);
			faxContent.push(
				<div key={"fax" + i} className={this.state.to == theNum && type == 'fax' ? 'color-main-theme-bold' : ''} onClick={this.faxClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}
				</div>
			)
		}

		let rightContent = [];
		switch (this.state.type) {
			case "phone":
				rightContent.push(
					<div key="right-phone" className='right-area'>
						<div className="workspace-area">
							<InputForm recId={this.state.recId} readOnly={false} from={this.state.data.from} to={this.state.to} value={this.state.isReady} page={true} cname={this.state.campaignName} full={full} />
						</div>
						{/* ========== RIGHT PANEL ========== */}
						<ScreenPhone ref="child" changeParent={oThis.changeParent.bind(oThis)} />
					</div>
				);
				break;
			default:
				rightContent.push(
					<div key="right-email" className='right-area'>
						<OutboundOthers type={this.state.type} toDetails={this.state.to} name={this.state.data.full} backCampaignClick={this.backCampaignClick.bind(oThis)} campaignId={this.state.campaignId} campaignName={this.state.campaignName} />
					</div>
				)
				break;
		}
		return (<div>
			{/* ========== TOP BAR ========== */}
			<div className='input-head-container clearfix'>
				<a onClick={oThis.backCampaignClick.bind(oThis)}>{i18n.back_to_campaign}</a>
				<span className='right'>{i18nCommon.campaign_label}&nbsp;{oThis.state.campaignName}</span>
			</div>
			<div id="outbound_call">
				<div className='input-body-container'>
					<div className='input-form-body' id='input-from-edit'>
						{/* ========== LEFT PANEL ========== */}
						<div className="sidebar-profile-area">
							<div className='basic-details'>
								{$imagePreview}
								<div className='profile-name'>
									<span className='name-initial'>{this.state.data.title}</span>&nbsp;
									<span className='name-firstname'>{this.state.data.first},&nbsp;</span>
									<span className='name-lastname'>{this.state.data.last}</span>
								</div>
								<button onClick={this.userProfileClick.bind(this)}>{i18n.user_profile_history}</button>
							</div>

							<div className='widget-title'>
								<span className='title'>{i18n.contact_details}</span>
							</div>
							<div className='sidebar-content-spacer'>
								<div className='contact-group' onClick={this.emailClick.bind(oThis, email)}>
									<span className='hc-icon icon-at-sign-16' />
									<span className={'phone-value' + (this.state.to == email && type == 'email' ? ' color-main-theme-bold' : '')}>{email}</span>
								</div>
								<div className='contact-group'>
									<span className='hc-icon icon-phone-16' />
									<span className='contain-numbers'>
										<span className='phone-value'>{mobileContent}</span>
										<span className='phone-value'>{homeContent}</span>
										<span className='phone-value'>{workContent}</span>
										<span className='phone-value'>{otherContent}</span>
									</span>
								</div>
								<div className='contact-group'>
									<span className='hc-icon icon-chat-16' />
									<span className='contain-numbers'>
										<span className='phone-value'>{smsContent}</span>
									</span>
								</div>
								<div className='contact-group'>
									<span className='hc-icon icon-fax-16' />
									<span className='contain-numbers'>
										{faxContent}
									</span>
								</div>
							</div>
						</div>
						{rightContent}
						{/* ========== MIDDLE PANEL ==========
						<div className="workspace-area">
							<InputForm recId={this.state.recId} readOnly={false} from={this.state.data.from} to={this.state.to} value={this.state.isReady} page={true} cname={this.state.campaignName} />
						</div>*/}
						{/* ========== RIGHT PANEL ========== */}
						{/* <ScreenPhone ref="child" changeParent={oThis.changeParent.bind(oThis)} />  */}
						{/* ========== SHOW POP-UP ========== */}
						{this.state.showProfile ? <ProHisPop data={this.state.data} epConfirmClick={oThis.epConfirmClick.bind(oThis)} epCancelClick={oThis.epCancelClick.bind(oThis)} revealID="profile" /> : null}
					</div>
				</div>
			</div>
		</div>
		);
	}
}