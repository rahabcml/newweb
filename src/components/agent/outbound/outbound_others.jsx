import React, { Component } from 'react';
import ReactFileReader from 'react-file-reader';
import AlertBox from '../../common/AlertBox.jsx'
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.outbound_variable;

export default class OutboundOthers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toName: props.name || "",
            toDetails: props.toDetails || "",
            subject: "",
            message: "",
            remark: "",
            savedFiles: [],
            showAlert: false,
            showError: false,
            alertMsg: ""
        }
        // get user email which saved when login
        this.fromName = sessionStorage.getItem("company_name") || "";
        this.fromEmail = sessionStorage.getItem("email") || "";
        this.fromPhone = sessionStorage.getItem("phone") || "";
        this.fullName = sessionStorage.getItem("fullName") || "";
        let twoDigits = function (num) {
            if (num > 9) {
                return num;
            } else {
                return "0" + num;
            }
        }
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let yyyy = today.getFullYear();
        let hh = today.getHours();
        let mi = today.getMinutes();
        this.dateTime = yyyy + "-" + twoDigits(mm) + "-" + twoDigits(dd) + " " + twoDigits(hh) + ":" + twoDigits(mi);
        console.log("this.dateTime");
        console.log(this.dateTime);
    }
    componentWillReceiveProps(nextProps) {
        let { name, toDetails } = nextProps;
        this.setState({
            toName: name || "",
            toDetails: toDetails || "",
            subject: "",
            message: "",
            remark: "",
            savedFiles: [],
            showAlert: false,
            showError: false,
            alertMsg: ""
        })
    }
    cancelClick() {
        window.location.hash = "home/OutboundCall?campaignId=" + this.props.campaignId + "&recId=" + data.id + "&name=" + this.props.campaignName;
    }
    sendEmailClick() {
        // verify if no content
        if (this.state.subject == "") {
            this.setState({ alertMsg: i18n.subject_cannot_blank, showError: true })
            return
        }
        if (this.state.message == "") {
            this.setState({ alertMsg: i18n.content_cannot_blank, showError: true })
            return
        }
        // verify if no subject
        // send information to server:this.state.subject, this.state.message, this.state.remark, forward-key clicked files's key(?)/ this.state.savedFiles
        this.setState({ showAlert: true });
    }
    chooseFileClick(files) {
        let oThis = this;
        if (files.length > 0) { //NOTES: check the lengh to prevent error when user pressed cancel
            let result = []
            let savedFiles = this.state.savedFiles.slice();
            console.log("savedFiles");
            console.log(savedFiles);
            console.log("files");
            console.log(files);
            if (files.length > 0) {
                for (let i = 0; i < files.length; i++) {
                    let file = files[i];
                    if (file.size < 5000000 && file.name.length < 50) {
                        savedFiles.push(file);
                    }

                }
                this.setState({ savedFiles: savedFiles });
            }
        }
    }
    deleteFile(idx) {
        let savedFiles = this.state.savedFiles.slice();
        savedFiles.splice(idx, 1);
        this.setState({ savedFiles: savedFiles });
    }
    forwardClick() {
        // send this.state.savedFiles to server, get an id for upload later
    }
    returnLastContent() {
        let fileTag = [];
        let type = this.props.type;
        let { savedFiles } = this.state;
        if (savedFiles.length > 0) {
            for (let i = 0; i < savedFiles.length; i++) {
                let fileName = savedFiles[i].name;
                fileTag.push(<span className='file-tag' key={'file' + i}>
                    {fileName} <i className='icon-simple-remove' onClick={this.deleteFile.bind(oThis, i)} />
                </span>)
            }
        }
        return (
            <div className='clearfix' key={this.props.type + "-last"}>
                <div className='medium-6 columns left-half'>
                    <div className='label-row'>{i18n.attachments}</div>
                    <div className='attachment-area clearfix'>
                        <div>
                            <span className='reader-class'>
                                <ReactFileReader handleFiles={this.chooseFileClick.bind(this)} fileTypes={'*'} multipleFiles={true}>
                                    <button className='btn grey-button upload-btn'><i className='icon-attach-16' />&nbsp;{i18n.choose_file} </button>
                                </ReactFileReader>
                            </span>
                            <span>
                                <button className='grey-button forward-btn right' onClick={this.forwardClick.bind(this)}><i className='icon-share-16' /></button>
                            </span>
                        </div>
                        <div className='clearfix'>
                            {fileTag}
                        </div>
                    </div>
                </div>
                <div className='medium-6 columns right-half'>
                    <div className='label-row'>{i18n.internal_remark}</div>
                    <div>
                        <textarea className='remark-textarea' value={this.state.remark} placeholder={i18n.anything_to_add} onChange={e => {
                            let remark = e.target.value.substring(0, 299);
                            this.setState({ remark: remark });
                        }} />
                    </div>
                </div>
            </div>
        )
    }
    render() {
        let oThis = this;


        let title = "";
        console.log("this.props.type");
        console.log(this.props.type);
        let type = this.props.type;
        let subject = [];
        let lastContent = [];
        switch (type) {
            case "email":
                title = i18n.send_email;
                subject.push(
                    <div key={type + "-subject"} className='subject-btm'>
                        <div className='label-row'>{i18n.email_subject}</div>
                        <div className='row'>
                            <input type='text' value={this.state.subjct} placeholder={i18n.what_email_about} onChange={e => {
                                let subject = e.target.value.substring(0, 49);
                                this.setState({ subject: subject });
                            }} />
                        </div>
                    </div>
                );
                lastContent = this.returnLastContent();
                break;
            case "sms":
                title = i18n.send_an_sms;
                lastContent.push(
                    <div key={type + "-last"} >
                        <div className='label-row'>{i18n.internal_remark}</div>
                        <textarea className='remark-textarea' value={this.state.remark} placeholder={i18n.anything_to_add} onChange={e => {
                            let remark = e.target.value.substring(0, 299);
                            this.setState({ remark: remark });
                        }} />
                    </div>
                );
                break;
            case "fax":
                title = i18n.send_a_fax;
                subject.push(
                    <div key={type + "-subject"} className='subject-btm'>
                        <div className='label-row'>{i18n.subject}</div>
                        <div className='row'>
                            <input type='text' value={this.state.subjct} placeholder={i18n.what_fax_about} onChange={e => {
                                let subject = e.target.value.substring(0, 49);
                                this.setState({ subject: subject });
                            }} />
                        </div>
                    </div>
                );
                lastContent = this.returnLastContent();
                break;
        }
        return (
            <div className='page-container custom-scroll outbound-card'>
                <div className='main-card padding-none'>
                    <div className='blank-part'>
                        <h3>{title}</h3>
                        <div className='row'>
                            {i18n.from_label}<span className='strong-name'>{this.fromName}</span>&nbsp;<span>({this.fromEmail})</span>
                            <span className='right'>{i18n.to_label}<span className='strong-name'>{this.state.toName}</span>&nbsp;<span>({this.state.toDetails})</span></span>
                        </div>
                    </div>
                    <div className='grey-part'>
                        {subject}
                        <div className='row'>
                            <div className='label-row'>{i18n.message}</div>
                            <textarea className='message-textarea' value={this.state.message} placeholder={i18n.type_message_here} onChange={e => {
                                let message = e.target.value.substring(0, 999);
                                this.setState({ message: message });
                            }} />
                        </div>
                    </div>
                    <div className='last-blank clearfix'>
                        {lastContent}
                        <div className='last-row clearfix'>
                            <span className='left-part'>{i18n.agent_label}:&nbsp;{this.fullName}</span>
                            <span className='right-part'>{i18n.create_date_time}&nbsp;{this.dateTime}</span>
                        </div>
                        <div>
                            <div className='grey-btn-group right clearfix'>
                                <button onClick={this.cancelClick.bind(oThis)} className='medium-btn'>{i18nCommon.cancel}</button>
                                <button onClick={this.sendEmailClick.bind(oThis)} className='medium-btn'>{i18n.send_email}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <AlertBox showAlert={this.state.showAlert} alertMsg={i18n.email_sent} showInfo={true} alertClick={() => {
                    this.setState({ showAlert: false });
                    if (typeof this.props.backCampaignClick === 'function') {
                        this.props.backCampaignClick();
                    }
                }} />
                <AlertBox showAlert={this.state.showError} alertMsg={this.state.alertMsg} alertClick={() => {
                    this.setState({ showError: false });
                }} />
            </div>
        );
    }
}
