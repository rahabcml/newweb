import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import AlertBox from '../../common/AlertBox.jsx';
import SelectClientPage from './select_client_page.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.inbound_variable;

export default class InboundMain extends Component {
	constructor(props) {
		super(props);
		let query = props.location.query;
		this.state = {
			campaignId: query.campaignId || -1,
			campaignName: query.name || "",
			noticeNo: {},
			selected: ''
		}
	}

	componentWillMount() {
		// TO DO: real time get details by campaign Id from server
		let noticeNo = {
			fb: 1,
			email: 16,
			fax: 2,
			phone: 3,
			sms: 1,
			wechat: 1,
			voice: 2
		}
		this.setState({ noticeNo: noticeNo });
	}

	selectedChange(isActive, type) {
		if (isActive == true) {
			this.setState({ selected: type });
		}
	}

	render() {
		let oThis = this;
		let noticeNo = this.state.noticeNo;
		let fbActive = noticeNo.fb != null && noticeNo.fb > 0 ? true : false;
		let emailActive = noticeNo.email != null && noticeNo.email > 0 ? true : false;
		let faxActive = noticeNo.fax != null && noticeNo.fax > 0 ? true : false;
		let phoneActive = noticeNo.phone != null && noticeNo.phone > 0 ? true : false;
		let smsActive = noticeNo.sms != null && noticeNo.sms > 0 ? true : false;
		let wechatActive = noticeNo.wechat != null && noticeNo.wechat > 0 ? true : false;
		let voiceActive = noticeNo.voice != null && noticeNo.voice > 0 ? true : false;
		console.log("noticeNo"); console.log(noticeNo);
		console.log("fbActive"); console.log(fbActive);
		let selected = this.state.selected;
		{/* ========== MAIN CONTENT ========== */ }
		return (
			<div className='height-100'>
				{/* <div className='flex-one'> */}
				{/* === When all query answered, will return to below === */}
				{selected == '' ? 
				
				<div className='blankstate'>
					<img src='assets/imgs/Flag-smaller.png' />
					<div className="blankstate-info">
                        <div className="blankstate-title">{i18n.no_active_query}</div>
                        <div className="">{i18n.click_active_cues}</div>
                	</div>
				</div> :
					<SelectClientPage type={selected} campaignId={this.state.campaignId} campaignName={this.state.campaignName} />
				}
				{/* ========== BUTTOM BAR ========== */}
				<div className="navbar">
					<a className='contace-cues clearfix'>
						<div>{i18n.contact_cues}</div>
					</a>
					<a className={(selected == 'fb' ? "selected " : "") + (fbActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, fbActive, 'fb')}>
						<div><i className='cc-icon icon-fb-16' /></div>
						<div>{i18nCommon.facebook}</div>
						{fbActive ? <div className='right-upper-no'>{noticeNo.fb}</div> : null}
					</a>
					<a className={(selected == 'email' ? "selected " : "") + (emailActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, emailActive, 'email')}>
						<div><i className='cc-icon icon-at-sign-16' /></div>
						<div>{i18nCommon.email}</div>
						{emailActive ? <div className='right-upper-no'>{noticeNo.email}</div> : null}
					</a>
					<a className={(selected == 'fax' ? "selected " : "") + (faxActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, faxActive, 'fax')}>
						<div><i className='cc-icon icon-fax-16' /></div>
						<div>{i18nCommon.fax}</div>
						{faxActive ? <div className='right-upper-no'>{noticeNo.fax}</div> : null}
					</a>
					<a className={(selected == 'phone' ? "selected " : "") + (phoneActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, phoneActive, 'phone')}>
						<div><i className='cc-icon icon-phone-16' /></div>
						<div>{i18nCommon.phone}</div>
						{phoneActive ? <div className='right-upper-no'>{noticeNo.phone}</div> : null}
					</a>
					<a className={(selected == 'sms' ? "selected " : "") + (smsActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, smsActive, 'sms')}>
						<div><i className='cc-icon icon-chat-16' /></div>
						<div>{i18nCommon.sms}</div>
						{smsActive ? <div className='right-upper-no'>{noticeNo.sms}</div> : null}
					</a>
					<a className={(selected == 'wechat' ? "selected " : "") + (wechatActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, wechatActive, 'wechat')}>
						<div><i className='cc-icon icon-wechat-16' /></div>
						<div>{i18nCommon.wechat}</div>
						{wechatActive ? <div className='right-upper-no'>{noticeNo.wechat}</div> : null}
					</a>
					<a className={(selected == 'voice' ? "selected " : "") + (voiceActive == true ? "active" : "")} onClick={this.selectedChange.bind(oThis, voiceActive, 'voice')}>
						<div><i className='cc-icon icon-voice-16' /></div>
						<div>{i18nCommon.voicemail}</div>
						{voiceActive ? <div className='right-upper-no'>{noticeNo.voice}</div> : null}
					</a>
				</div>
				{/* ========== /BUTTOM BAR ========== */}
			</div>
		);
	}
}
