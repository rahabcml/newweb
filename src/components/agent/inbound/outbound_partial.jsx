import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import InputForm from '../../admin/input_form/input_form.jsx'
import ProHisPop from '../../admin/address_book/profile_history_popup.jsx';
import ScreenPhone from '../../common/screen_phone.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.outbound_variable;
// let x = null;//timer
export default class OutboundPartial extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);
		let query = props.location.query
		this.state = {
			campaignId: props.campaignId || -1,
			recId: query.recId,
			campaignName: query.name || "",
			data: [],
			imagePreviewUrl: '',
			showProfile: false,
			to: '',
		}
	}

	componentWillMount() {
		// TO DO: get data from server by campaign id and rec id, the img in data should be the link of image
		// let data = {
		// 	"gender": "Male", "First Name": "Kenneth", "Last Name": "Wong", "Email": "kwong@gmail.com", "Mobile": 97091235, "Home": "", "Work": 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", img: "someone.jpg"
		// }
		let data = { from: 23456789, id: 1, title: "Mr.", gender: "Male", full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: [97091235, 55346128], home: [], work: [23147690], other: [53123456], fax: 39972345, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", img: "someone.jpg" }
		this.setState({ data: data, imagePreviewUrl: data.img })
	}
	// ------------------------------- /delegate ------------------------------- //
	backCampaignClick() {
		window.location.hash = "home/OutbuondHome?campaignId=" + this.state.campaignId + "&name=" + this.state.campaignName;
	}

	userProfileClick() {
		this.setState({ showProfile: true });
	}
	formatPhone(tel) {
		//char = { 0: '(+', 3: ') ', 7: ' ' }; //NOTES: For next version, have country code, become  (+852)1234 5678
		tel = String(tel);
		let numbers = tel.replace(/\D/g, ''),
			char = { 4: ' ' };
		tel = '';
		for (let i = 0; i < numbers.length; i++) {
			tel += (char[i] || '') + numbers[i];
		}
		return tel
	}
	callClick(tel) {
		console.log("this.props");
		console.log(this.props);
		console.log("this.refs");
		console.log(this.refs);
	}
	// called by Screen Phone component
	changeParent(tel, otherNum) {
		this.setState({ to: tel });
		if (otherNum.length > 0) {
			let data = Object.assign({}, this.state.data);
			data.other = [tel];
			this.setState({ data: data, to: tel });
		}
	}
	smsClick(tel) {
		console.log("tel");
		console.log(tel);
	}
	faxClick(tel) {
		console.log("tel");
		console.log(tel);
	}
	// ======== POP-UP FUNCTION ========
	//Edit Profile Buttons:
	epConfirmClick() {
		this.setState({ showProfile: false });
	}

	epCancelClick() {
		this.setState({ showProfile: false });
	}

	// shouldComponentUpdate() {
	// 	return false
	// }
	render() {
		let oThis = this;
		//Render the image
		let { imagePreviewUrl } = this.state;
		let $imagePreview = null;
		if (imagePreviewUrl) {
			$imagePreview = (<img className="img-center cp-circle" src={"assets/imgs/" + imagePreviewUrl} alt="" />);
		} else {
			$imagePreview = <div className="icon-focus-24 img-center cp-circle">
			</div>
		}
		// let callBtnColor = this.state.status == i18n.calling || this.state.status == i18n.in_conversation ? "call-btn-end" : "call-btn-ready"

		//
		let mobileContent = [], homeContent = [], workContent = [], otherContent = [], smsContent = [], faxContent = [];
		let { mobile, home, work, other, fax } = this.state.data;
		for (let i = 0; i < mobile.length; i++) {
			let theNum = mobile[i];
			mobileContent.push(
				<div key={"mobile" + i} className={this.state.displayNum == theNum ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_mobile}
				</div>
			)
			smsContent.push(
				<div key={"sms" + i} onClick={this.smsClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}
				</div>
			)
		}
		for (let i = 0; i < home.length; i++) {
			let theNum = home[i];
			homeContent.push(
				<div key={"home" + i} className={this.state.displayNum == theNum ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_home}
				</div>
			)
		}
		for (let i = 0; i < work.length; i++) {
			let theNum = work[i];
			workContent.push(
				<div key={"work" + i} className={this.state.displayNum == theNum ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_work}
				</div>
			)
		}
		for (let i = 0; i < other.length; i++) {
			let theNum = other[i];
			otherContent.push(
				<div key={"other" + i} className={this.state.displayNum == theNum ? 'color-main-theme-bold' : ''} onClick={this.callClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}&nbsp;{i18n.curly_other}
				</div>
			)
		}
		for (let i = 0; i < fax.length; i++) {
			let theNum = fax[i];
			faxContent.push(
				<div key={"fax" + i} onClick={this.faxClick.bind(oThis, theNum)}>
					{this.formatPhone(theNum)}
				</div>
			)
		}

		return (<div>
			<div id="outbound_call">
				<div className='input-body-container'>
					<div className='input-form-body' id='input-from-edit'>
						{/* ========== LEFT PANEL ========== */}
						<div className="sidebar-profile-area">
							<div className='basic-details'>
								{$imagePreview}
								<div className='profile-name'>
									<span className='name-initial'>{this.state.data.title}</span>
									{/* <span className='name-initial'>{this.state.data.gender == "Male" ? "Mr. " : ""}</span> */}
									<span className='name-firstname'>{this.state.data.first},&nbsp;</span>
									<span className='name-lastname'>{this.state.data.last}</span>
								</div>
								<button onClick={this.userProfileClick.bind(this)}>{i18n.user_profile_history}</button>
							</div>

							<div className='widget-title'>
								<span className='title'>{i18n.contact_details}</span>
							</div>
							<div className='sidebar-content-spacer'>
								<div className='contact-group'>
									<span className='hc-icon icon-at-sign-16' />
									<span className='phone-value'>{this.state.data.email}</span>
								</div>
								<div className='contact-group'>
									<span className='hc-icon icon-phone-16' />
									<span className='contain-numbers'>
										<span className='phone-value'>{mobileContent}</span>
										<span className='phone-value'>{homeContent}</span>
										<span className='phone-value'>{workContent}</span>
										<span className='phone-value'>{otherContent}</span>
									</span>
								</div>
								<div className='contact-group' onClick={this.smsClick.bind(oThis, this.state.data.mobile)}>
									<span className='hc-icon icon-chat-16' />
									<span className='contain-numbers'>
										<span className='phone-value'>{smsContent}</span>
									</span>
								</div>
								{(this.state.data.fax != null && this.state.data.fax.length != 0) ? <div className='contact-group' onClick={this.faxClick.bind(oThis, this.state.data.fax)}>
									<span className='hc-icon icon-fax-16' />
									<span className='phone-value'>{this.formatPhone(this.state.data.fax)}</span>
								</div> : null}
							</div>
						</div>
						{/* ========== MIDDLE PANEL ========== */}
						<div className="workspace-area">
							<InputForm recId={this.state.recId} readOnly={false} from={this.state.data.from} to={this.state.to} value={this.state.isReady} page={true} cname={this.state.campaignName} />
						</div>
					</div>
				</div>
			</div>
		</div>
		);
	}
}