import React, { Component } from 'react';
import ScreenPhone from '../../common/screen_phone.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.inbound_variable;

export default class newCustomer extends Component {
	constructor(props) {
		super(props);
		let query = props.location.query;
		console.log("query in inbound phone");
		console.log(query);
		this.state = {
			campaignId: query.campaignId || -1,
			campaignName: query.name || "",
		}
	}

	backCampaignClick() {
		window.location.hash = "home/InboundHome";
	}

	render() {
		let oThis = this;
		console.log("this.props.children");
		console.log(this.props.children);
		return (
			<div id='inbound_main'>
				<div className='input-head-container clearfix'>
					<a onClick={oThis.backCampaignClick.bind(oThis)}>{i18n.back_to_campaign}</a>
					<span className='right'>{i18nCommon.campaign_label + oThis.state.campaignName}</span>
				</div>
				<div className='page-content'>
					<div className='flex-one'>
						{this.props.children}
					</div>
					{/* ========== RIGHT PANEL ========== */}
					<div className='phone-panel'>
						<ScreenPhone ref="child" />
					</div>
				</div>
			</div>
		);
	}
}
