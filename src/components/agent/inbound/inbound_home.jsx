import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import AlertBox from '../../common/AlertBox.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.inbound_variable;

export default class InboundHome extends Component {
	constructor(props) {
		super(props);
		this.state = {
			inboundList: [],
			campaignId: -1,
			campaignName: "",
			showAlert: false
		}
	}
	componentWillMount() {
		// TO DO: get the outbound campaign list for combo from server
		let inboundList = [{ id: 1, name: "Spring Promotion" }, { id: 2, name: "Summer Promotion" }, { id: 3, name: "Autumn Promotion" }, { id: 4, name: "Winter Promotion" }];
		this.setState({ inboundList: inboundList });
	}
	// clicked Next button
	nextStepClick() {
		if (this.state.campaignId == -1) {
			this.setState({ alertMsg: i18n.please_select_campaign, showAlert: true });
		} else {
			// Direct to call page
			let href = "";




			// if (defaultPage != "") {
			//   console.log("defaultPage");
			//   console.log(defaultPage);
			//   href = 'home/' + module + "/" + defaultPage;
			// } else {
			//   href = 'home/' + module;
			// }
			// //let href =  'home/' + module +'/default';
			// window.location.hash = href;
			// console.log("href");
			// console.log(href);
			let campaignName = encodeURIComponent(this.state.campaignName);
			window.location.hash = "home/InboundPhone/InboundMain?campaignId=" + this.state.campaignId + "&name=" + campaignName;
		}
	}

	render() {
		let oThis = this;
		return (
			<div id='inbound_home' className="page-container custom-scroll">
				<div className='container'>
					<div className="main-card">
						<div className="content-divide">
							<h3>{i18n.choose_a_campaign}</h3>
							<div className='row flex-container upload-file'>
								<div className='medium-6 columns'>
									<ICombo placeholder={i18n.select_campaign_list} options={oThis.state.inboundList} displayField='name' valueField='id'
										clearable={false} value={this.state.campaignId} onChange={(o) => {
											oThis.setState({ campaignId: o.value, campaignName: o.label });
										}} />
								</div>
								<div className='medium-6 columns'>
									<button className='button right' onClick={oThis.nextStepClick.bind(oThis)}>{i18nCommon.next_step}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<AlertBox alertMsg={this.state.alertMsg} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
			</div>
		);
	}
}
