import React, { Component } from 'react';
import ITable2 from '../../common/ITable2.jsx';
import ICombo from '../../common/ICombo.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.inbound_select_variable;
import $ from 'jquery';
import foundation from 'foundation-sites';
import DataTable from 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';
import 'datatables.net-buttons/js/buttons.colVis.min.js';

export default class InboundMain extends Component {
	constructor(props) {
		super(props);
		this.state = {
			info: {},
			simpleSearch: true,
			simpleText: "",
			showResult: false,
			anyAll: "any",
			addArray: [{ condition: "", selector: "", text: "" }],
			queryData: {}
		}
	}

	componentWillMount() {
		this.getInfo(this.props.type);
		this.any_all_array = [{ name: "Match any of the following", value: 'any' }, { name: "Match all of the following", value: 'all' }];
		// get columns from serve
		this.columns = [
			{ id: "id", name: "ID" },
			{ id: "full", name: "Customer Name" },
			{ id: "mobile", name: "Mobile Number" },
			{ id: "home", name: "Home Number" },
			{ id: "add1", name: "Address 1" },
			{ id: "add2", name: "Address 2" },
			{ id: "limit", name: "Credit Limit" }//,
			// { id: "first", name: "First Name" },
			// { id: "last", name: "Last Name" },
			// { id: "other", name: "Other Phone" }
		]
		this.columns.unshift(
			{
				id: "empty", name: "", type: "icon", className: 'icon-zoom-in-16', onClick: (index, data) => {
					this.tblRowSelected(data);
					// let name = encodeURIComponent(data);
					// window.location.hash = "home/InputFormEdit?isInbound=" + isInbound + "&name=" + name;
				}
			}
		)
		let columns = [{ data: "full", title: "Full" }, { data: "first", title: "First" }, { data: "last", title: "Last" }, { data: "email", title: "Email" }, { data: "mobile", title: "Mobile" }, { data: "home", title: "Home" }, { data: "work", title: "Work" }, { data: "other", title: "Other" }, { data: "add1", title: "Add 1" }, { data: "add2", title: "Add 2" }, { data: "add3", title: "Add 3" }, { data: "country", title: "Country" }, { data: "ref", title: "Reference" }, { data: "occ", title: "Occupancy" }, { data: "education", title: "Education" }, { data: "pid", title: "PID" }, { data: "source", title: "Source" }];

	}

	componentWillReceiveProps(nextProps) {
		// update auto search
		this.getInfo(nextProps.type);
		// reseta search
		this.setState({
			simpleSearch: true,
			simpleText: "",
			showResult: false,
			anyAll: "any",
			addArray: [{ condition: "", selector: "", text: "" }],
			queryData: {}
		});
	}

	componentDidMount() {
		let oThis = this;
		//TO DO: Get add condition combobox value from server
		//TO DO: the columns should get from server side as below

		let columns = [{ data: "full", title: "Full" }, { data: "first", title: "First" }, { data: "last", title: "Last" }, { data: "email", title: "Email" }, { data: "mobile", title: "Mobile" }, { data: "home", title: "Home" }, { data: "work", title: "Work" }, { data: "other", title: "Other" }, { data: "add1", title: "Add 1" }, { data: "add2", title: "Add 2" }, { data: "add3", title: "Add 3" }, { data: "country", title: "Country" }, { data: "ref", title: "Reference" }, { data: "occ", title: "Occupancy" }, { data: "education", title: "Education" }, { data: "pid", title: "PID" }, { data: "source", title: "Source" }];

		// let columns = [{ data: "full", title: "Full" }, { data: "first", title: "First" }, { data: "last", title: "Last" }, { data: "email", title: "Email" }, { data: "mobile", title: "Mobile" }, { data: "home", title: "Home" }, { data: "work", title: "Work", visible: false }, { data: "other", title: "Other", visible: false }, { data: "add1", title: "Add 1", visible: false }, { data: "add2", title: "Add 2", visible: false }, { data: "add3", title: "Add 3", visible: false }, { data: "country", title: "Country", visible: false }, { data: "ref", title: "Reference", visible: false }, { data: "occ", title: "Occupancy", visible: false }, { data: "education", title: "Education", visible: false }, { data: "pid", title: "PID", visible: false }, { data: "source", title: "Source", visible: false }];


		//TBD: when servider side done. delete below
		let data = {
			total: 2,
			result: [{ id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 2, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 3, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 4, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 5, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 6, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 7, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 8, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 9, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 10, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 11, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 12, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 13, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 14, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 15, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
			{ id: 16, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 17, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
			{ id: 18, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }
			]
		}

		columns.unshift(
			{
				data: null,
				className: 'noVis', //NOTES: no column visibility
				orderable: false,
				defaultContent: '<a onClick={} class="editor_edit icon-edit-16"></a>'
			}
		)


		let columnDefs = [];
		let columnLength = columns.length || 0;
		let targets = [];
		if (columnLength > 6) {
			for (let i = 7; i < columnLength; i++) {
				targets.push(i);
			}
			columnDefs.push(
				{
					"targets": targets,
					"visible": false
				});
		}


		//NOTES: DataTable reference: https://datatables.net/extensions/buttons/examples/html5/columns.html
		oThis.table = $('#resultDataTable').DataTable({
			stateSave: true,
			columns: columns,
			//NOTS: Due To Server Side not comleted yet, so use below, should be deleted when server side done
			data: data.result,
			"bPaginate": true,
			"paging": true,
			"searching": true,
			"info": true,
			dom: '<"table-top"Bf><"scroll_table"rt><"table-btm"ip>',
			columnDefs: columnDefs,
			buttons: [
				{
					extend: 'colvis',
					columns: ':not(.noVis)',
					// prefixButtons: [
					//     {
					//         extend: 'colvisGroup',
					//         text: 'Select All',
					//         // hide: ':visible',
					//         action: function (e, dt, node, config) {
					//             if (config.checked == true) {
					//                 this.active(false);
					//                 dt.columns('*').visible(false);
					//             } else {
					//                 this.active(true);
					//                 dt.columns('*').visible(true);
					//             }
					//             config.checked = !config.checked
					//         },
					//         active: false,
					//         checked: false
					//     }
					// ]
				}
			],
			//NOTES: Uncomment below when Server Side is done
			// "processing": true,
			// "serverSide": true,
			// "deferLoading": 0,
			// "ajax": {
			//     "url": "http://api.wiser/profiles",
			//     "data": function (d) {
			//         d.filter = JSON.stringify(oThis.state.queryData);
			//     }
			// },
			/* No ordering applied by DataTables during initialisation */
			"order": []
		});
		// click icon triger selected
		$('#resultDataTable').on('click', 'a.editor_edit', function (e) {
			e.preventDefault();
			let data = oThis.table.row($(this).parents('tr')).data();
			oThis.tblRowSelected(data);
			//oThis.setState({ profileData: data, showProfile: true });
		});
		// double click row triger selected
		$('#resultDataTable tbody').on('dblclick', 'tr', function (e) {
			console.log("ROW DOUBLE CLICKED");
			e.preventDefault();
			let data = oThis.table.row($(this)).data();
			oThis.tblRowSelected(data);
			//oThis.setState({ profileData: data, showProfile: true });
		});
		// hight light single click row
		$('#resultDataTable tbody').on('click', 'tr', function (e) {
			$('#resultDataTable tbody tr').removeClass('highlight');
			$(this).addClass('highlight')
		});
		//NOTES: When click Column visibility button, check is the visible is six already, if yes, all other rows disable
		$('#resultDataTable_wrapper').on(".buttons-colvis").click(function () {
			let bol = $(".buttons-columnVisibility.active").length > 5;
			if (bol == true) {
				$(".buttons-columnVisibility").not(".active").addClass("disabled");
			}
		});

		//NOTES: Function will be active when selecting collumn in column visibility
		$('#resultDataTable').on('column-visibility.dt', function (e, settings, column, state) {
			let bol = $(".buttons-columnVisibility.active").length > 5;
			if (bol == true) {
				$(".buttons-columnVisibility").not(".active").addClass("disabled");
			} else {
				$(".buttons-columnVisibility").not(".active").removeClass("disabled");
			}
		});
		// tabs set foundation
		$('#inbound-select').foundation();
	}

	getInfo(type) {
		// get the info by this.props.type
		console.log("get dat now, type is:");
		console.log(type);
		let info = [];
		// =================== TBD ===================
		let result = [{ id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", limit: "$50,000" },
		{ id: 2, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02", limit: "$4,000" },
		{ id: 3, full: "May Cheung", first: "May", last: "Cheung", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02", limit: "$3,900" }];
		let result2 = [{ id: 4, full: "Simon Chung", first: "Simon", last: "Chung", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", limit: "$1,500" }];
		let result3 = [{ id: 3, full: "May Cheung", first: "May", last: "Cheung", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02", limit: "$3,900" },
		{ id: 4, full: "Simon Chung", first: "Simon", last: "Chung", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02", limit: "$2,000" }];
		switch (type) {
			case 'fb':
				info = {
					email: "peterkwan@gmail.com",
					result: result
				}
				this.setState({ info: info });
				break;
			case 'email':
				info = {
					email: "Peter Kwan(peterkwan@gmail.com)",
					result: result3,
					to: "ABC Company(abc@wisermail.com)",
					content: "Hi ABC Company, \n I want to know more of your product, please reply me. \n Best regards, \n Peter",
					attachments: [{ file_name: "test.docx", file_url: "http://yahoo.com" }, { file_name: "hellow_world.docx", file_url: "http://google.com" }]
				}
				this.setState({ info: info });
				break;
			case 'fax':
				info = {
					tel: 65402899,
					file_name: "fax-document.docx",
					file_url: 'http://google.com',
					result: result2
				}
				this.setState({ info: info });
				break;
			case 'phone':
				info = {
					tel: 65402899,
					result: result
				}
				break;
			case 'sms':
				info = {
					tel: 65402899,
					result: result
				}
				break;
			case 'wechat':
				info = {
					tel: 65402899,
					result: result
				}
				break;
			case 'voice':
				info = {
					tel: 65402899,
					file_name: "v_mail.wma",
					file_url: 'http://google.com',
					result: result3
				}
				break;
		}
		// =================== /TBD ===================
		console.log("new info");
		console.log(info);
		this.setState({ info: info });
		//this.setState({ info: info }, () => { this.setState({ reloadTable: true }) });
	}
	tblRowSelected(data) {
		console.log("data");
		console.log(data);
		let recId = data.id || -1;
		let campaignName = encodeURIComponent(this.props.campaignName);
		window.location.hash = "home/InboundPhone/OutboundPartial?campaignId=" + this.props.campaignId + "&name=" + campaignName + "&recId=" + recId;
		//window.location.hash = "home/InboundPhone/OutboundPartial";
		//window.location.hash = "home/InboundPhone/OutboundPartial?campaignId=" + this.props.campaignId + "&name=" + campaignName + "&recId=" + recId;
		//window.location.hash = "home/InboundPhone/InboundInput?campaignId=" + this.props.campaignId + "&name=" + campaignName;
	}
	addCustomerClick(type) {
		let campaignName = encodeURIComponent(this.props.campaignName);
		console.log("this.props");
		console.log(this.props);
		console.log("this.state");
		console.log(this.state);
		window.location.hash = "home/InboundPhone/NewCustomer?campaignId=" + this.props.campaignId + "&name=" + campaignName;
	}
	// switch of simple search and advance search
	filterClick(e) {
		this.setState({ simpleSearch: !this.state.simpleSearch, simpleText: "" });
		console.log(e);
	}
	// add filter in search
	addClick() {
		//NOTES: 'Add Condition' button clicked
		let addArray = this.state.addArray.slice();
		addArray.push({ condition: "", selector: i18n.is, text: "" })
		this.setState({ addArray: addArray });
	}
	searchClick() {
		console.log(this.state);
		// create ajax request for getting customer profile
        /*
        jQuery.ajax({
            method		: 'GET',
            url			: '/profiles?filter',
        }).always(function(res) {
            // failed
            if (!res.status) {
                // message dialog
            // succeeded
            } else {
                let jsondata = res.data || {};
            }
        });
        */
		let oThis = this;
		let data = {}

		data.simpleSearch = this.state.simpleSearch;
		if (data.simpleSearch == true) {
			data.query = this.state.simpleText
		} else {
			data.anyAll = this.state.anyAll
			data.query = this.state.addArray
		}
		this.setState({ queryData: data, showResult: true }, () => {
			//$('#resultDataTable').DataTable().draw();
		});
	}
	emailHandler(email) {
		console.log("email before handle");
		console.log(email);
		let tmpEmail = email.replace("(", "\n(");
		console.log("email after handle");
		console.log(tmpEmail);
		return tmpEmail;
	}
	render() {
		let oThis = this;
		let type = this.props.type;
		let activeContent = [];
		let info = this.state.info;
		switch (type) {
			case "fb":
				activeContent.push(
					<div key='fb' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.facebook}</div>
						</div>
						<div className='columns medium-10'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.email}</div>
						</div>
					</div>
				)
				break;
			case "email":
				let attachments_content = [];
				let attachments = info.attachments;
				for (let i = 0; i < attachments.length; i++) {
					attachments_content.push(
						<a key={'email-attach-' + i} className='email-attach-tag' href={attachments[i].file_url}>
							{attachments[i].file_name}
						</a>
					)
				}
				activeContent.push(
					<div key='email'>
						<div className='row'>
							<div className='columns medium-2'>
								<div className='gray-label'>{i18n.session_type}</div>
								<div>{i18nCommon.email}</div>
							</div>
							<div className='columns medium-2'>
								<div className='gray-label'>{i18n.contact_information}</div>
								<div>{info.email}</div>
							</div>
							<div className='columns medium-8'>
								<button className='grey-button'>{i18n.view_email_content}</button>
							</div>
						</div>
						<div className='content-ender row email-part'>
							<div className='flex-row'>
								<div className='left-part'>
									<div className='first-box'>{i18n.from_label}&nbsp;&nbsp;&nbsp;</div>
									<div className='second-box'>{this.emailHandler(info.email)}</div>
								</div>
								<div className='right-part'>
									<div className='third-box'>{i18n.to_label}&nbsp;&nbsp;&nbsp;</div>
									<div className='fourth-box'>{this.emailHandler(info.email)}</div>
								</div>
							</div>
							<div className='gray-label title-row'>{i18n.message_content_label}</div>
							<div className='pre-wrap-text'>
								{info.content}
							</div>
							<div className='gray-label title-row'>{i18n.attachments_label}</div>
							<div>
								{attachments_content}
							</div>
						</div>

					</div >
				)
				break;
			case "fax":
				activeContent.push(
					<div key='fax' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.fax}</div>
						</div>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.tel}</div>
						</div>
						<div className='columns medium-8'>
							<div className='gray-label'>{i18n.attached_fax_file}</div>
							<a className='font-weight-bold' href={info.file_url}>{info.file_name}</a>
						</div>
					</div>
				)
				break;
			case "phone":
				activeContent.push(
					<div key='phone' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.phone}</div>
						</div>
						<div className='columns medium-10'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.tel}</div>
						</div>
					</div>
				)
				break;
			case "sms":
				activeContent.push(
					<div key='sms' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.sms}</div>
						</div>
						<div className='columns medium-10'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.tel}</div>
						</div>
					</div>
				)
				break;
			case "wechat":
				activeContent.push(
					<div key='wechat' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.wechat}</div>
						</div>
						<div className='columns medium-10'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.tel}</div>
						</div>
					</div>
				)
				break;
			case "voice":
				activeContent.push(
					<div key='voice' className='row'>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.session_type}</div>
							<div>{i18nCommon.voicemail}</div>
						</div>
						<div className='columns medium-2'>
							<div className='gray-label'>{i18n.contact_information}</div>
							<div>{info.tel}</div>
						</div>
						<div className='columns medium-8'>
							<div className='gray-label'>{i18n.attached_voice_mail}</div>
							<a className='font-weight-bold' href={info.file_url}>{info.file_name}</a>
						</div>
					</div>
				)
				break;
		}
		//Generate Condition Rows Below
		let conditionRows = [];
		let addArray = this.state.addArray.slice();
		if (addArray.length > 0) {
			for (let i = 0; i < addArray.length; i++) {
				conditionRows.push(
					<div key={"query-rows" + i} className='expanded'>
						<div className="filter-inputs">
							<ICombo placeholder={i18nCommon.choose_condition} displayField="name" valueField="id" options={oThis.columns} value={addArray[i].condition} onChange={(o) => { addArray[i].condition = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
							<ICombo displayField={0} valueField={0} options={i18nCommon.selector_array} value={addArray[i].selector} onChange={(o) => { addArray[i].selector = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
							<div>
								<input type='text' value={addArray[i].text} onChange={event => {
									addArray[i].text = event.target.value;
									this.setState({ addArray: addArray })
								}} />
							</div>
							<div className="remove-icon">
								<div className='icon-trash-16 i-icon' onClick={() => { addArray.splice(i, 1); this.setState({ addArray: addArray }); }} />
							</div>
						</div>
					</div>
				)
			}
		}
		let displayResult = this.state.showResult ? 'block' : 'none'; // show search result or not
		return (
			<div id='select_client' className="page-container custom-scroll select_client-container">
				{/* // ========== Top Part: INFO ========= */}
				<div className='sc-main-card'>
					<h3><i className='icon-active-stroke-24' />{i18n.active_session_info}</h3>
					{activeContent}
				</div>
				{/* // ========== Middle Part ========= */}
				<div className="tabs-container-2 margin-top">
					<div className="sc-container">
						<ul className="tabs" data-tabs id="inbound-select">
							<li className="tabs-title is-active">
								<a href="#panel1" aria-selected="true">{i18n.related_customers}</a>
							</li>
							<li className="tabs-title">
								<a data-tabs-target="panel2" href="#panel2">{i18n.manual_search}</a>
							</li>
						</ul>
					</div>
				</div>

				<div className='sc-container'>
					<div className="tabs-content-2" data-tabs-content="inbound-select">
						{/* ========== 1st tab content ========= */}
						<div className="tabs-panel is-active" id="panel1">
							<h3><i className='icon-folder-search-24' />{i18n.related_customers_label}</h3>
							<ITable2 columns={oThis.columns} data={oThis.state.info.result} tableName='auto-search-result'
								onDoubleClick={(idx, data) => oThis.tblRowSelected(data)} />
						</div>
						{/* ========== /1st tab content ========= */}
						{/* ========== 2nd tab content ========= */}
						<div className="tabs-panel" id="panel2">
							<h3><i className='icon-person-search-24' />{i18n.manual_search_label}</h3>
							<div className='row'>
								<div className='medium-6 columns'>
									<input type='text' style={{ display: 'inline', width: '300px' }} value={this.state.simpleText} onChange={event => this.setState({ simpleText: event.target.value })} placeholder={i18n.type_in_sth} disabled={!this.state.simpleSearch} />
								</div>
								<div className='medium-6 columns moreFilter'>
									<button className='grey-button right' data-toggle="collapse" data-target="#showFilter" aria-expanded="false" onClick={this.filterClick.bind(oThis)}><span className='icon-settings-16 i-icon i-icon-right' /></button>
								</div>
							</div>

							<div id='showFilter' className='collapse'>
								<div className='outer-padding'>
									{/* An extra div container is made because of the animating issue */}
									<div className='inner-padding'>
										<div className='filter-text-select'>
											<ICombo displayField='name' valueField='value' options={this.any_all_array} value={this.state.anyAll} onChange={(o) => { this.setState({ anyAll: o ? o.value : "" }) }} clearable={false} />
										</div>
										{conditionRows}
										<a className='add-condition-link' onClick={oThis.addClick.bind(oThis)}><span className='icon-add-16' />{i18nCommon.add_condition}</a>
									</div>
								</div>
							</div>
							<div className='content-ender'>
								<button onClick={oThis.searchClick.bind(oThis)}>{i18nCommon.search}</button>
							</div>
						</div>
						{/* ========== /2nd tab content ========= */}
					</div>
				</div>
				{/*  ========== Lowest Part: SEARCH RESULT (show conditionally) ================ */}
				<div style={{ display: displayResult }} >
					{/* Extra div is just to make the illusion of 2 separted div */}
					< div className='sc-main-card' >
						<div className='row'>
							<div>
								<h3>{i18n.campaign_records_list}</h3>
							</div>
						</div>
						<div id='table_wrapper'>
							<table id='resultDataTable' />
						</div>
					</div >
				</div >
				<div>
					<button className='right sc-button' onClick={this.addCustomerClick.bind(oThis, type)}><span className='icon-add-16 i-icon' />&nbsp;&nbsp;{i18n.add_new_customer}</button>
				</div>
			</div >
		);
	}
}
