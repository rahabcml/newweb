/*************************************************************************************************
* Notes
* Name: InputFormEdit
* Details: Input Form's Edit Page
* Plug-in used: 
*        JQuery UI
*        CKEditor - for details: https://ckeditor.com/blog/CKEditor-for-jQuery/ 
* Data (except id, all are strings): 
*        label: label to be shown on left of the field
*        format:
*                Link: '^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?$'
*                Combo/dropdown: '^(combo1|combo2)$'  or '^(label1:combo1|label2:combo2)$' (default label same with option)
*                Check-box: '^(,?(female:F|male:M))*$' or '^(,?(Female|Male)*$'
*                radio: '^(-?(radio1:Radio 1|radio2:Radio 2))*$' or '^(-?(Radio 1|Radio 2))*$'
*                Date: '^([0-9]{4})-([0-9]{2})-([0-9]{2})$'
*                Date Time: '^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$'
*                HTML: '^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$' // "^[a-zA-Z]{2,40}\s*[a-zA-Z]{2,500}*$" 
*                Label: ''^.+$''
*                File: '.(jpe?g|png|gif)$'
*                Header: '/[A-Za-z\s-\/!%&()+=\'"~,.]/'
*                Number :'^[0-9.]+$'
*                text:  '^.*$',
*        default: 
*                If answer is empty string, the deafult will be the answer
*        answer: 
*                Link: it is the hyperlink 
*                Combo/dropdown: the selection. e.g. 'combo2'
*                Check-box: checked box(es). e.g. 'Opt1Default,Opt2Default'
*                HTML: default value of in text area
*                Label: default value in text field
*        condition: The field will only be shown when condition matched. The path have to have condition's id
*                rules : ["id","conditioner","answer"] <= one or more in '[]' seperated by comma(,)
*                e.g.'[["1","=","combo2"],["3","=","Opt2Default"]]'
*        order: 'Page-UpDown-(LeftRight/column-id)-order'
*                UpDown can only be 0 or 1. 0: up, 1: down
*                LeftRight can only be 0 or 1. 0: left, 1: right 
*        mandatory: Bollean.
*************************************************************************************************/
import React, { Component } from 'react'
import ReactFileReader from 'react-file-reader';
import formatConverter from './formatConverter.jsx';
import '../../../../styles/inputForm.css'
import i18nCommon from '../../common/i18n.jsx';
import AlertBox from '../../common/AlertBox.jsx';
let i18n = i18nCommon.input_form_variable;

let formatTypeSwitch = function (format) {
    console.log("formatConverter.identify(format)");
    console.log(formatConverter.identify(format));
    return formatConverter.identify(format);
};

let getOptions = function (type, format) {
    console.log("type");
    console.log(type);
    console.log("format");
    console.log(format);
    let options = [];
    // get Drop Down option
    if (/^dropdown$/i.test(type)) { //NTOES: /i stands for ignore case in the given string. Usually referred to as case-insensitive as pointed out in the comment.
        // get options
        if (Array.isArray(format) == true) {
            for (let i = 0; i < format.length; i++) {
                if (format[i] == String(/^\^\[a-z0-9A-Z\-\/ ]\+=\.\*\$$/) || format[i] == String(
                    /^\^\([a-z0-9\-\/ ]+(:.*)?(\|[a-z0-9\-\/ ]+(:.*)?)*\)\$$/i)) {
                    console.log("Empty STRING");
                    options.push("");
                } else {
                    options.push(/^\^\[a-z0-9A-Z\]\+=\.\*\$$/i.test(format) ? [] : (format.substr(2, format.length - 4).split('|') || []));
                }
            }
        } else {
            options = /^\^\[a-z0-9A-Z\]\+=\.\*\$$/i.test(format) ? [] : (format.substr(2, format.length - 4).split('|') || []);
        }
        // get Checkbox option
    } else if (/^checkbox$/i.test(type)) {
        // get options
        if (format == String(/^((\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$)|(\^\(,\?\([a-z0-9]+(:.*)?(\|[a-z0-9]+(:.*)?)*\)\)\*\$))$/i)) {
            options = ["label1:"];
        } else {
            options = /^\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$$/i.test(format) ? [] : (format.substr(5, format.length - 9).split('|') || []);
        }
    }
    // get Radio option
    else if (/^radio$/i.test(type)) {
        // get options
        if (format == String(/^((\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$)|(\^\(,\?\([a-z0-9]+(:.*)?(\|[a-z0-9]+(:.*)?)*\)\)\*\$))$/i)) {
            options = ["label1:"];
        } else {
            console.log("format of radio");
            console.log(format);
            console.log("format substring of radio");
            console.log(format.substr(5, format.length - 9));
            options = /^\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$$/i.test(format) ? [] : (format.substr(5, format.length - 9).split('|') || []);
        }
        console.log("options for radio");
        console.log(options);
    }
    return options.filter(function (v) { return v.length > 0; });
};

export default class InputFormEdit extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        super(props);
        let query = props.location.query
        console.log("query in props");
        console.log(query);
        let showBlank = (query.copy == "" && query.new == "true") ? true : false;
        console.log("showBlank");
        console.log(showBlank);
        this.state = {
            isInbound: query.isInbound,
            campaignName: query.name.toString(),
            copy: query.copy ? query.copy : "",
            new: query.new && query.new == "true" || false,
            showAlert: false,
            showConfirm: false,
            pageId: 0,
            totalPages: 1,
            data: [],
            btnOrder: {},
            tmpOrder: {},  // when column number changed from 2 to 1, will save the order, in case changed back from 1 to 2
            showBlank: showBlank, // when this is a new form and blank, will be shown, disappear when first item dropped
            showSave: false, // save button settings, when made any changes will turn to true,
            fileName: {},
            copyArray: [],
            largestId: 1,
            cfgSecCheck: false,
            hideSecObj: {},
            secId: 0
        }
        this.didMount = false;
        this.switchNo = 0;
        this.labelFollow = false;
    }

    componentWillMount() {
        //TO DO: this.state.isInbound and this.state.name getting detailed data of input form
        //TO DO: if popupComobo have data and new is true, then copy the config of the input form
        let data = null;
        try {
            // parse cache data from cookies
            data = JSON.parse(Cookies.get('data') || '');
        } catch (e) {
            // error occurred, maybe no cache data found, it doesn't matter
            data = null;
        } finally {
            // use cache data or setup dummy data
            console.log(this.state.new);
            console.log(this.state.popupCombo);
            if (this.state.new == true && this.state.copy == "") {
                //blank new form
                data = [];
            } else {
                //TO DO: get data from serever, the id need to be order ascendingly, sort it if necessary
                data = data || [
                    {
                        id: 1,
                        format: '^(Not Reach Customer|Presented - Consider|Presented - Reject|Presented - Success)$',
                        label: 'Call Result 1',
                        default: 'Presented - Success',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-0-0-1',
                        mandatory: true
                    },
                    {
                        id: 2,
                        format: '^(No Answer|Not Here|Voice Mail)$',
                        label: 'Call Result 2',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Not Reach Customer"]]',
                        path: '1',
                        order: '0-0-0-2',
                        mandatory: true
                    },
                    {
                        id: 3,
                        format: '^(Follow Up - After Present|Consider Follow Up|Not in Hong Kong)$',
                        label: 'Call Result 2',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Consider"]]',
                        path: '1',
                        order: '0-0-0-3',
                        mandatory: true
                    },
                    {
                        id: 4,
                        format: '^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$',
                        label: 'Call Back Date Time',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Consider"]]',
                        path: '1',
                        order: '0-0-0-4',
                        mandatory: true
                    },
                    {
                        id: 5,
                        format: '^(Already Have Insurance|Coverage Not Enough|No Need / No Interest)$',
                        label: 'Call Result 2',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Reject"]]',
                        path: '1',
                        order: '0-0-0-5',
                        mandatory: true
                    },
                    {
                        id: 6,
                        format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                        label: '',
                        default: 'Customer Information',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-1-0-0',
                        mandatory: false
                    },
                    {
                        id: 7,
                        format: '^.+$',
                        label: 'Item_No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-1',
                        mandatory: false
                    },
                    {
                        id: 8,
                        format: '^.+$',
                        label: 'Batch Prefix',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-2',
                        mandatory: false
                    },
                    {
                        id: 9,
                        format: '^.+$',
                        label: 'Holder Name',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-3',
                        mandatory: false
                    },
                    {
                        id: 10,
                        format: '^.+$',
                        label: 'Address1',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-4',
                        mandatory: false
                    },
                    {
                        id: 11,
                        format: '^.+$',
                        label: 'Address 2',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-5',
                        mandatory: false
                    },
                    {
                        id: 12,
                        format: '^.+$',
                        label: 'Address 3',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-6',
                        mandatory: false
                    },
                    {
                        id: 13,
                        format: '^.+$',
                        label: 'Address 4',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-7',
                        mandatory: false
                    },
                    {
                        id: 14,
                        format: '^.+$',
                        label: 'Email',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-8',
                        mandatory: false
                    },
                    {
                        id: 15,
                        format: '^.+$',
                        label: 'Phone1 No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-9',
                        mandatory: false
                    },
                    {
                        id: 16,
                        format: '^.+$',
                        label: 'Phone2 No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-10',
                        mandatory: false
                    },
                    {
                        id: 17,
                        format: '^.+$',
                        label: 'Mobile No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-11',
                        mandatory: false
                    },
                    {
                        id: 18,
                        format: '^.+$',
                        label: 'HKID',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-12',
                        mandatory: false
                    },
                    {
                        id: 19,
                        format: '^.+$',
                        label: 'DOB',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-13',
                        mandatory: false
                    },
                    {
                        id: 20,
                        format: '^.+$',
                        label: 'Gender',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-14',
                        mandatory: false
                    },
                    {
                        id: 21,
                        format: '^.+$',
                        label: 'Business Line',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-15',
                        mandatory: false
                    },
                    {
                        id: 22,
                        format: '^.+$',
                        label: 'Product Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-16',
                        mandatory: false
                    },
                    {
                        id: 23,
                        format: '^.+$',
                        label: 'Product Desc',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-17',
                        mandatory: false
                    },
                    {
                        id: 24,
                        format: '^.+$',
                        label: 'Agent No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-18',
                        mandatory: false
                    },
                    {
                        id: 25,
                        format: '^.+$',
                        label: 'Agent Group',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-19',
                        mandatory: false
                    },
                    {
                        id: 26,
                        format: '^.+$',
                        label: 'Policy No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-20',
                        mandatory: false
                    },
                    {
                        id: 27,
                        format: '^.+$',
                        label: 'Client No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-21',
                        mandatory: false
                    },
                    {
                        id: 28,
                        format: '^.+$',
                        label: 'Policy Year',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-22',
                        mandatory: false
                    },
                    {
                        id: 29,
                        format: '^.+$',
                        label: 'Age Group',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-23',
                        mandatory: false
                    },
                    {
                        id: 30,
                        format: '^.+$',
                        label: 'Application Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-0-24',
                        mandatory: false
                    },
                    {
                        id: 31,
                        format: '^.+$',
                        label: 'Policy Start',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-0',
                        mandatory: false
                    },
                    {
                        id: 32,
                        format: '^.+$',
                        label: 'Policy End',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-1',
                        mandatory: false
                    },
                    {
                        id: 33,
                        format: '^.+$',
                        label: 'Org Commence Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-2',
                        mandatory: false
                    },
                    {
                        id: 34,
                        format: '^.+$',
                        label: 'Commence Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-3',
                        mandatory: false
                    },
                    {
                        id: 35,
                        format: '^.+$',
                        label: 'Date Cancel',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-4',
                        mandatory: false
                    },
                    {
                        id: 36,
                        format: '^.+$',
                        label: 'Document No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-5',
                        mandatory: false
                    },
                    {
                        id: 37,
                        format: '^.+$',
                        label: 'Document Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-6',
                        mandatory: false
                    },
                    {
                        id: 38,
                        format: '^.+$',
                        label: 'Country Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-7',
                        mandatory: false
                    },
                    {
                        id: 39,
                        format: '^.+$',
                        label: 'Benefit Club Member',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-8',
                        mandatory: false
                    },
                    {
                        id: 40,
                        format: '^.+$',
                        label: 'Status Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-9',
                        mandatory: false
                    },
                    {
                        id: 41,
                        format: '^.+$',
                        label: 'Plan Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-10',
                        mandatory: false
                    },
                    {
                        id: 42,
                        format: '^.+$',
                        label: 'Rider Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-11',
                        mandatory: false
                    },
                    {
                        id: 43,
                        format: '^.+$',
                        label: 'Premium',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-12',
                        mandatory: false
                    },
                    {
                        id: 44,
                        format: '^.+$',
                        label: 'No Of Insured',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-13',
                        mandatory: false
                    },
                    {
                        id: 45,
                        format: '^.+$',
                        label: ' No Of Days ',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-14',
                        mandatory: false
                    },
                    {
                        id: 46,
                        format: '^.+$',
                        label: 'Cover Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-15',
                        mandatory: false
                    },
                    {
                        id: 47,
                        format: '^.+$',
                        label: 'Channel',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-16',
                        mandatory: false
                    },
                    {
                        id: 48,
                        format: '^.+$',
                        label: 'Other Inforce GI',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-17',
                        mandatory: false
                    },
                    {
                        id: 49,
                        format: '^.+$',
                        label: 'Staff Indicator',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-18',
                        mandatory: false
                    },
                    {
                        id: 50,
                        format: '^.+$',
                        label: 'Enrollment Freq',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-19',
                        mandatory: false
                    },
                    {
                        id: 51,
                        format: '^.+$',
                        label: 'Enrollment Freq Since Nov 2017',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-20',
                        mandatory: false
                    },
                    {
                        id: 52,
                        format: '^.+$',
                        label: 'Call ID',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-21',
                        mandatory: false
                    },
                    {
                        id: 53,
                        format: '^.+$',
                        label: 'Campaign Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-22',
                        mandatory: false
                    },
                    {
                        id: 54,
                        format: '^.+$',
                        label: 'Staff No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '0-2-1-23',
                        mandatory: false
                    },
                    {
                        id: 55,
                        format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                        label: '',
                        default: 'Reminder for Sales',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-0-0-0',
                        mandatory: false
                    },
                    {
                        id: 56,
                        format: '^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$',
                        label: '',
                        default: 'html default',
                        answer: '<p>同事必須詢問客人現在是否身存香港? 並告訴客人以下對話將有錄音</p>',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-0-0-1',
                        mandatory: false
                    }, {
                        id: 57,
                        format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                        label: '',
                        default: 'Sales Information',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-0-0-2',
                        mandatory: false
                    }, {
                        id: 58,
                        format: '^.*$',
                        label: 'Insured Id',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-1-0-0',
                        mandatory: false
                    },
                    {
                        id: 59,
                        format: '^(HKID|Passport)$',
                        label: 'ID Type',
                        default: 'HKID',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-1-1-0',
                        mandatory: true
                    },
                    {
                        id: 60,
                        format: '^.*$',
                        label: 'Insured Full Name',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-2-0-0',
                        mandatory: true
                    },
                    {
                        id: 61,
                        format: '^.*$',
                        label: 'Insured Surname',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-2-0-1',
                        mandatory: true
                    },
                    {
                        id: 62,
                        format: '^([0-9]{4})-([0-9]{2})-([0-9]{2})$',
                        label: 'Insured Birth Date',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-3-0-0',
                        mandatory: true
                    },
                    {
                        id: 63,
                        format: '^(Male|Female)$',
                        label: 'Insured Sex',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-3-1-0',
                        mandatory: true
                    },
                    {
                        id: 64,
                        format: '^(,?(new:&nbsp;))*$',
                        label: 'Using New address?',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-4-0-0',
                        mandatory: true
                    },
                    {
                        id: 65,
                        format: '^.*$',
                        label: 'Insured Address Line 1',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-4-0-1',
                        mandatory: true
                    },
                    {
                        id: 66,
                        format: '^.*$',
                        label: 'Insured Address Line 2',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-4-0-2',
                        mandatory: true
                    },
                    {
                        id: 67,
                        format: '^.*$',
                        label: 'Insured Address Line 3',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-4-0-3',
                        mandatory: true
                    },
                    {
                        id: 68,
                        format: '^[0-9.]+$',
                        label: 'Insured Telephone 1',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-5-0-0',
                        mandatory: true
                    },
                    {
                        id: 69,
                        format: '^[0-9.]+$',
                        label: 'Insured Telephone 2',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-5-1-0',
                        mandatory: true
                    },
                    {
                        id: 70,
                        format: '^[0-9.]+$',
                        label: 'Insured Telephone (Mobile)',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-6-0-0',
                        mandatory: true
                    },
                    {
                        id: 71,
                        format: '^(-?(Opt-Out|Opt-In|Unknown))*$',
                        label: 'Opt for Direct Marketing',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-6-0-1',
                        mandatory: true
                    },
                    {
                        id: 72,
                        format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                        label: '',
                        default: 'Payment Information',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-6-0-2',
                        mandatory: true
                    },
                    {
                        id: 73,
                        format: '^(Credit Card|Bank A/C)$',
                        label: 'Payment Method',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-6-0-3',
                        mandatory: false
                    },
                    {
                        id: 74,
                        format: '^[0-9.]+$',
                        label: 'Card Number',
                        default: '',
                        answer: '',
                        condition: '[["73","=","Credit Card"]]',
                        path: '1,73',
                        order: '1-7-0-0',
                        mandatory: true
                    },
                    {
                        id: 75,
                        format: '^(2018|2019|2020|2021|2022|2023|2024)$',
                        label: 'Card Expiry Year',
                        default: '',
                        answer: '',
                        condition: '[["73","=","Credit Card"]]',
                        path: '1,73',
                        order: '1-8-0-0',
                        mandatory: true
                    },
                    {
                        id: 76,
                        format: '^(Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Nov|Dec)$',
                        label: 'Card Expiry Month',
                        default: '',
                        answer: '',
                        condition: '[["73","=","Credit Card"]]',
                        path: '1,73',
                        order: '1-8-1-0',
                        mandatory: true
                    },
                    {
                        id: 77,
                        format: '^.*$',
                        label: 'Account Holder Name',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-0-1',
                        mandatory: true
                    },
                    {
                        id: 78,
                        format: '^(Level 1 = $121|Level 2 = $254|Level 3 = $410)$',
                        label: 'Plan Level',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-1-1',
                        mandatory: true
                    },
                    {
                        id: 79,
                        format: '^[0-9.]+$',
                        label: 'Total Premium',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-0-2',
                        mandatory: true
                    },
                    {
                        id: 80,
                        format: '^[0-9.]+$',
                        label: 'Total ANP',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-1-2',
                        mandatory: true
                    },
                    {
                        id: 81,
                        format: '^(,?(spouse:&nbsp;))*$',
                        label: 'Spouse',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-0-3',
                        mandatory: true
                    },
                    {
                        id: 82,
                        format: '^(0|1)$',
                        label: 'No. of Child ',
                        default: '',
                        answer: '',
                        condition: '[["1","=","Presented - Success"]]',
                        path: '1',
                        order: '1-8-1-3',
                        mandatory: true
                    },
                    {
                        id: 83,
                        format: '^.+$',
                        label: 'Item_No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-1',
                        mandatory: false
                    },
                    {
                        id: 84,
                        format: '^.+$',
                        label: 'Batch Prefix',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-2',
                        mandatory: false
                    },
                    {
                        id: 85,
                        format: '^.+$',
                        label: 'Holder Name',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-3',
                        mandatory: false
                    },
                    {
                        id: 86,
                        format: '^.+$',
                        label: 'Address1',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-4',
                        mandatory: false
                    },
                    {
                        id: 87,
                        format: '^.+$',
                        label: 'Address 2',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-5',
                        mandatory: false
                    },
                    {
                        id: 88,
                        format: '^.+$',
                        label: 'Address 3',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-6',
                        mandatory: false
                    },
                    {
                        id: 89,
                        format: '^.+$',
                        label: 'Address 4',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-7',
                        mandatory: false
                    },
                    {
                        id: 90,
                        format: '^.+$',
                        label: 'Email',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-8',
                        mandatory: false
                    },
                    {
                        id: 91,
                        format: '^.+$',
                        label: 'Phone1 No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-9',
                        mandatory: false
                    },
                    {
                        id: 92,
                        format: '^.+$',
                        label: 'Phone2 No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-10',
                        mandatory: false
                    },
                    {
                        id: 93,
                        format: '^.+$',
                        label: 'Mobile No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-11',
                        mandatory: false
                    },
                    {
                        id: 94,
                        format: '^.+$',
                        label: 'HKID',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-12',
                        mandatory: false
                    },
                    {
                        id: 95,
                        format: '^.+$',
                        label: 'DOB',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-13',
                        mandatory: false
                    },
                    {
                        id: 96,
                        format: '^.+$',
                        label: 'Gender',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-14',
                        mandatory: false
                    },
                    {
                        id: 97,
                        format: '^.+$',
                        label: 'Business Line',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-15',
                        mandatory: false
                    },
                    {
                        id: 98,
                        format: '^.+$',
                        label: 'Product Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-16',
                        mandatory: false
                    },
                    {
                        id: 99,
                        format: '^.+$',
                        label: 'Product Desc',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-17',
                        mandatory: false
                    },
                    {
                        id: 100,
                        format: '^.+$',
                        label: 'Agent No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-18',
                        mandatory: false
                    },
                    {
                        id: 101,
                        format: '^.+$',
                        label: 'Agent Group',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-19',
                        mandatory: false
                    },
                    {
                        id: 102,
                        format: '^.+$',
                        label: 'Policy No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-20',
                        mandatory: false
                    },
                    {
                        id: 103,
                        format: '^.+$',
                        label: 'Client No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-21',
                        mandatory: false
                    },
                    {
                        id: 104,
                        format: '^.+$',
                        label: 'Policy Year',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-22',
                        mandatory: false
                    },
                    {
                        id: 105,
                        format: '^.+$',
                        label: 'Age Group',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-23',
                        mandatory: false
                    },
                    {
                        id: 106,
                        format: '^.+$',
                        label: 'Application Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-0-24',
                        mandatory: false
                    },
                    {
                        id: 107,
                        format: '^.+$',
                        label: 'Policy Start',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-0',
                        mandatory: false
                    },
                    {
                        id: 108,
                        format: '^.+$',
                        label: 'Policy End',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-1',
                        mandatory: false
                    },
                    {
                        id: 109,
                        format: '^.+$',
                        label: 'Org Commence Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-2',
                        mandatory: false
                    },
                    {
                        id: 110,
                        format: '^.+$',
                        label: 'Commence Date',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-3',
                        mandatory: false
                    },
                    {
                        id: 111,
                        format: '^.+$',
                        label: 'Date Cancel',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-4',
                        mandatory: false
                    },
                    {
                        id: 112,
                        format: '^.+$',
                        label: 'Document No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-5',
                        mandatory: false
                    },
                    {
                        id: 113,
                        format: '^.+$',
                        label: 'Document Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-6',
                        mandatory: false
                    },
                    {
                        id: 114,
                        format: '^.+$',
                        label: 'Country Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-7',
                        mandatory: false
                    },
                    {
                        id: 115,
                        format: '^.+$',
                        label: 'Benefit Club Member',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-8',
                        mandatory: false
                    },
                    {
                        id: 116,
                        format: '^.+$',
                        label: 'Status Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-9',
                        mandatory: false
                    },
                    {
                        id: 117,
                        format: '^.+$',
                        label: 'Plan Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-10',
                        mandatory: false
                    },
                    {
                        id: 118,
                        format: '^.+$',
                        label: 'Rider Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-11',
                        mandatory: false
                    },
                    {
                        id: 119,
                        format: '^.+$',
                        label: 'Premium',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-12',
                        mandatory: false
                    },
                    {
                        id: 120,
                        format: '^.+$',
                        label: 'No Of Insured',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-13',
                        mandatory: false
                    },
                    {
                        id: 121,
                        format: '^.+$',
                        label: ' No Of Days ',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-14',
                        mandatory: false
                    },
                    {
                        id: 122,
                        format: '^.+$',
                        label: 'Cover Type',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-15',
                        mandatory: false
                    },
                    {
                        id: 123,
                        format: '^.+$',
                        label: 'Channel',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-16',
                        mandatory: false
                    },
                    {
                        id: 124,
                        format: '^.+$',
                        label: 'Other Inforce GI',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-17',
                        mandatory: false
                    },
                    {
                        id: 125,
                        format: '^.+$',
                        label: 'Staff Indicator',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-18',
                        mandatory: false
                    },
                    {
                        id: 126,
                        format: '^.+$',
                        label: 'Enrollment Freq',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-19',
                        mandatory: false
                    },
                    {
                        id: 127,
                        format: '^.+$',
                        label: 'Enrollment Freq Since Nov 2017',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-20',
                        mandatory: false
                    },
                    {
                        id: 128,
                        format: '^.+$',
                        label: 'Call ID',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-21',
                        mandatory: false
                    },
                    {
                        id: 129,
                        format: '^.+$',
                        label: 'Campaign Code',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-22',
                        mandatory: false
                    },
                    {
                        id: 130,
                        format: '^.+$',
                        label: 'Staff No',
                        default: '',
                        answer: '',
                        condition: '',
                        path: '',
                        order: '1-9-1-23',
                        mandatory: false
                    },
                ]
            }
            let btnOrder = { 0: 0 } // PAGE: SECTION TO INSERT AFTER, FOR SECTION -1, means the buttons put above all sections
            this.data = data;
            let hideSecObj = { 1: [9] }; // get from server
            let largestId = data.length > 0 ? data[data.length - 1].id : 1;
            this.setState({ data: data, btnOrder: btnOrder, largestId: largestId, hideSecObj: hideSecObj });
        }
    }

    componentDidMount() {
        let oThis = this;
        let saveTimer = null;
        // let data = null;
        // ----------------------------- setup cache data ----------------------------- //
        let data = this.data;
        // sort data by order
        // data = data.sort(function (a, b) {
        //     return (a.order).localeCompare(b.order);
        //     //return a.order.localeCompare(b) < 0;
        // });
        // ----------------------------- /setup cache data ----------------------------- //




        // for components
        // NOTES: helper: If set to "clone", then the element will be cloned and the clone will be dragged.
        $('#browser div .toolbar-item').draggable({
            helper: 'clone'
        });

        this.viewerConfigs = {
            sortable: {
                handle: 'a.move',
                connectWith: '.column',
                update: function (event, ui) {
                    // middle panel moved / added item, show Save button
                    if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
                }
            },
            droppable: {
                accept: '#browser div .toolbar-item',
                drop: function (e, ui) {
                    oThis.createRow(e, ui);
                }
            }
        };

        this.btnConfigs = {
            handle: 'a.btnMove',
            //connectWith: '.column',
            //connectWith: 'section',
            update: function (event, ui) {
                // button panel moved, show changes
                if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
                console.log("event,ui");
                console.log(event, ui);
                if (!ui.sender) {
                    let prevSecId = $(ui.item).prev().attr('data-section-id');
                    let pageId = $(ui.item).parent().attr('data-page-id');
                    console.log("prevSecId");
                    console.log(prevSecId);
                    console.log("pageId");
                    console.log(pageId);
                    console.log("oThis.state");
                    console.log(oThis.state);
                    let btnOrder = Object.assign({}, oThis.state.btnOrder);
                    btnOrder[pageId] = prevSecId || -1;
                    oThis.setState({ btnOrder: btnOrder });
                }
            }
        };
        // configs for columns
        this.configs = {
            tree: {
                sortable: {
                    connectWith: "#structure .scroll-area ul",
                    //NOTES: handle: Restricts sort start click to the specified element: only click the move icon can be sortable
                    handle: 'a.move',
                    //NOTES: This event is triggered when the user stopped sorting and the DOM position has changed.
                    update: function (e, ui) {
                        // left structure panel moved, show Save button
                        if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
                        // Why commented below? 
                        // Do not update the path right th way, because when submit for just update 1 time will be faster
                        // if want to update path in real time, other then uncomment below, when deleted the parent, the children will need to change the path also
                        // console.log(" ***************** UPDATE ******************");
                        // //NOTES: the !ui.sender if statement prevent the update being repeated
                        // if (!ui.sender) {
                        //     //  ===================== update drag element path ===================== //
                        //     //NOTES: ui.item: The jQuery object representing the current dragged element.: the ui.item here is the li tag
                        //     let id = $(ui.item).attr('data-field-id');
                        //     // get parent id
                        //     let parentId = $(ui.item).parents('li[data-field-id]:first').attr('data-field-id') || '';
                        //     // get parent path
                        //     let path = (data[parentId] || {}).path || '';
                        //     // add comma
                        //     if (path.length > 0 && parentId != null && parentId != "") { path += ','; }
                        //     //if (path.length < 0) path += ',';
                        //     // append parent id to path. path = parent's path + parent id
                        //     path += parentId;
                        //     // update path of moving record    
                        //     data[id].path = path;
                        //     //  ===================== update drag element's children and their children path ===================== //
                        //     let children = $(ui.item).children().find('>.node');
                        //     let childChangePath = function (nodes, pid, pPath) {
                        //         console.log(" ***************** childChangePath ******************");
                        //         let nodesLen = nodes.length;
                        //         if (nodesLen > 0) {
                        //             for (let i = 0; i < nodesLen; i++) {
                        //                 let fieldId = nodes[i].getAttribute("data-field-id");
                        //                 for (let j in data) {
                        //                     if (data[j].id == fieldId) {
                        //                         data[j].path = pPath + "," + pid
                        //                         //get the child's children
                        //                         let nodeChildren = $(nodes[i]).children().find('>.node');
                        //                         if (nodeChildren.length > 0) { childChangePath(nodeChildren, fieldId, data[j].path); }
                        //                         break; //Stop this loop, we found it!
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if (children.length > 0) { childChangePath(children, id, path); }
                        // }
                    }
                }
            }
        };

        // for tree
        $('#structure > .scroll-area > ul').sortable(this.configs.tree.sortable);
        // update properties
        $([
            '#editor div[data-property-name="label"]',
            '#editor div[data-property-name="mandatory"]',
            '#editor div[data-property-name="format"]',
            '#editor div[data-property-name="default"]'
            // save
        ].join(',')).children(':input').bind('change', function (e) {
            // config changed, show Save button
            if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
            // get editing field id
            let id = $('#editor').attr('data-editing-field');
            // get property name
            let propertyName = $(e.target).parents('div[data-property-name]').attr('data-property-name');
            // get value
            let value = e.target.value;
            console.log("e");
            console.log(e);
            console.log("value");
            console.log(value);
            let idIdx = -1;
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == id) {
                    idIdx = i;
                    break;
                }
            }
            // update value of data
            if (propertyName == "mandatory") {
                data[idIdx].mandatory = !data[idIdx].mandatory;
                console.log("data[idIdx]");
                console.log(data[idIdx]);
                console.log("data[idIdx].mandatory");
                console.log(data[idIdx].mandatory);
            } else {
                console.log("value when format combobox change!!!!!!!!");
                console.log(value);
                data[idIdx][propertyName] = value;
            }
            console.log("data[id][propertyName]");
            console.log(data[idIdx][propertyName]);
            console.log('data[id]');
            console.log(data[idIdx]);
            console.log("data");
            console.log(data);
            // reload config
            oThis.loadConfig(id);
            // reload field
            oThis.renderRow(data[idIdx]);
            // hot load
        }).bind('keyup', function (e) {
            if (saveTimer) {
                clearTimeout(saveTimer);
                saveTimer = null;
            }
            saveTimer = setTimeout(function () {
                clearTimeout(saveTimer);
                saveTimer = null;
                $(e.target).trigger('change');
            }, 350);
        });

        // for condition
        $('#editor > div[data-property-name="condition"] :input[name="condition[operator]"]').bind('change', function (e) {
            // condition condition and operator changed, show Save button
            if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
            //NOTES: next(): Get the immediately following sibling of each element in the set of matched elements. If a selector is provided, it retrieves the next sibling only if it matches that selector.
            //NOTES: valu(): Get the current value of the first element in the set of matched elements or set the value of every matched element.
            oThis.setCondition($(this).next().val());
        });

        //NTOES: If no sort of the data, children will have nothing to append, disappeared in left panel
        // didMount ensure load only once in DidMount as initial
        if (this.didMount == false) {
            if (data.length > 0) {
                // ================== RENDER LEFT PANEL ==================
                // data = data.sort(function (a, b) {
                //     return (a.order).localeCompare(b.order);
                //     //return a.order.localeCompare(b) < 0;
                // });
                // console.log("data after sort of order");
                // console.log(data);

                // function compare(a, b) {
                //     let aPath = JSON.parse("[" + a.path + "]");
                //     let bPath = JSON.parse("[" + b.path + "]");

                //     if (aPath.length < bPath.length)
                //         return -1;
                //     if (aPath.length > bPath.length)
                //         return 1;
                //     return 0;
                // }

                let compareOrder = function (tmpData) {
                    tmpData.sort(function (a, b) { //warning: 10 will be prior to 2 in localeCompare, so cannot just sort by localCompare
                        const ao = a.order.split('-');
                        const bo = b.order.split('-');
                        if ((Number(ao[0])) > Number(bo[0])) {
                            return 1
                        } else if ((Number(ao[0])) < Number(bo[0])) {
                            return -1
                        } else if ((Number(ao[1])) > Number(bo[1])) {
                            return 1
                        } else if ((Number(ao[1])) < Number(bo[1])) {
                            return -1
                        } else if ((Number(ao[2])) > Number(bo[2])) {
                            return 1
                        } else if ((Number(ao[2])) < Number(bo[2])) {
                            return -1
                        } else if ((Number(ao[3])) > Number(bo[3])) {
                            return 1
                        } else if ((Number(ao[3])) < Number(bo[3])) {
                            return -1
                        } else {
                            return 0
                        }
                    });
                    return tmpData
                }
                let newData = data.slice();
                newData.sort(function (a, b) {
                    const aplen = a.path.length;
                    const bplen = b.path.length;
                    return aplen > bplen
                        ? 1
                        : (aplen < bplen ? -1 : 0);
                });
                console.log("newData after arranged");
                console.log(newData);

                //let pathMaxLength = data[data.length - 1].path.length;
                let pathBegin = 0;
                let pathLength = 0;
                let finalData = [];
                for (let i = 0; i < newData.length; i++) {
                    //new begin
                    console.log("newData[i].path.length");
                    console.log(newData[i].path.length);
                    if (newData[i].path.length > pathLength) {
                        console.log("data[i] that's bigger");
                        console.log(newData[i]);
                        let tmpData = [];
                        for (let j = pathBegin; j < i; j++) {
                            tmpData.push(newData[j]);
                        }
                        console.log("tmpData1");
                        console.log(tmpData);
                        tmpData = compareOrder(tmpData);
                        // tmpData.sort(function (a, b) {
                        //     return (a.order).localeCompare(b.order);
                        // });
                        finalData.push.apply(finalData, tmpData);
                        pathBegin = i;
                        pathLength = newData[i].path.length;
                    } else if (i == newData.length - 1) {
                        let tmpData = [];
                        for (let j = pathBegin; j < i + 1; j++) {
                            tmpData.push(newData[j]);
                        }
                        tmpData = compareOrder(tmpData);
                        finalData.push.apply(finalData, tmpData);
                    }
                }

                for (let i in finalData) this.renderStructure(data[i]);
                // ================== RENDER MIDDLE PANEL ==================
                let structureData = data.slice();
                structureData = compareOrder(structureData);
                for (let j in structureData) this.renderView(structureData[j]);

                //for (let i in data) renderRow(data[i]);
            } else {
                //blank form
                this.adjustPages($('#pages'), [0, 0, 0, 0]);
            }
            // reminder being shown when no any component added for first new blank form
            console.log("this.state.showBlank");
            console.log(this.state.showBlank);
            if (this.state.showBlank == true) {
                console.log("showBlank == true");
                let firstBox = $('#pages').children('.page');
                $('<div id="blankImg">' +

                    '<div class="blankstate">' +
                    '<div class="">' +
                    '<img src="assets/imgs/blankstate-img-form-build.png"/>' +
                    '</div>' +
                    '<div class="blankstate-info">' +
                    '<div class="blankstate-title">No form elements yet</div>' +
                    '<div class="">Drag form elements from the form elements drawer to start creating your form.</div>' +
                    '</div>' +
                    '</div>').insertAfter(firstBox);
            }
            this.didMount = true; // set didMount true make initial will not render again
        }
    }

    // ------------------------------- /delegate ------------------------------- //
    // ------------------------------- handlers ------------------------------- //
    adjustPages(container, order) {
        let oThis = this;
        let data = this.data;
        console.log(" ====================== adjustPages ====================== ");
        console.log("container,order in adjust pages");
        console.log(container, order);
        // setup maximum
        let maximum = parseInt(order[0] || '0');
        //NOTES: get pages, for the first time load, no any page, will be only []
        let pages = container.children('.page[data-page-id]').get();
        console.log("pages in adjust pages");
        console.log(pages);
        if (pages.length == 1) {
            $('#nextBtn').addClass('disabled');
        } else { $('#nextBtn').removeClass('disabled'); }
        //oThis.setState({ totalPages: pages.length });
        // get total pages number
        let totalPages = pages.length;
        // setup empty page id container
        let emptyPages = [], validPages = [];  //validPages is for the button
        // check empty page
        for (let pageId in pages) {
            // is it empty page?
            console.log("pageId in adjust Pages loop");
            console.log(pageId);
            if (this.adjustSections($(pages[pageId]), maximum == pageId ? order : [pageId, 0, 0])) {
                // count as empty page
                emptyPages.push(pageId);
            } else {
                validPages.push(pageId);
            }
        }

        console.log("emptyPages");
        console.log(emptyPages);
        console.log("validPages");
        console.log(validPages);
        // need to create new page?(no empty page and at least one page)
        if (emptyPages.length < 1 && totalPages > 0 && maximum < totalPages) {
            // new page required
            maximum = totalPages;
        }
        console.log("maximum before creating tabs");
        console.log(maximum);
        // create pages
        for (let pageId = 0; pageId <= maximum; pageId++) {
            // get tab
            console.log("pageId in adding tabs loop");
            console.log(pageId);
            let tab = $('#tabs').children(':eq(' + pageId + ')');
            // page not found?
            if (tab.length < 1) {
                // create a new page
                let tab = $('<div class="tab" data-page-id="' + pageId + '">' +
                    '<a href="javascript:;">' + (pageId + 1) + '</a>' +
                    '</div>');
                // bind events
                tab.children('a').bind('click', function (e) {
                    // get target page id
                    let targetPageId = $(e.target).parent().attr('data-page-id');
                    // set the present page Id
                    oThis.setState({ pageId: pageId }, () => oThis.disableBackNext());
                    // get target page
                    let targetPage = $('#pages .page[data-page-id="' + targetPageId + '"]');
                    // page exists?
                    if (targetPage.length > 0) {
                        // hide all siblings
                        targetPage.siblings('.page:visible').hide();
                        // show the page
                        targetPage.show();
                    }
                });
                // append page to container
                tab.appendTo('#tabs');
            }
            // get page
            let page = container.children(':eq(' + pageId + ')');
            // page not found?
            if (page.length < 1) {
                // create a new page
                page = $('<div class="page" data-page-id="' + pageId + '"></div>');
                // setup sections
                this.adjustSections(page, [pageId, 0, 0]);
                // append page to container
                page.appendTo(container);
                // new page should be an empty page
                emptyPages.push(pageId);
            }
        }
        // order index is greater than total?
        if (maximum > totalPages - 1) {
            // update total
            totalPages = maximum + 1;
        }

        // set remark and button name
        // reduce validPages if the last page display none
        let reduced = false;
        for (let i = validPages.length - 1; i > -1; --i) {
            let pageId = validPages[i];
            let pageFieldLen = $(pages[pageId]).find('.field').length;
            let pageNoneLen = $(pages[pageId]).find('.field[style="display: none;"]').length;
            if (pageFieldLen == pageNoneLen) {
                // if the page to be reduced have button group, remove it
                let page = $(pages[pageId]);
                let buttonGroup = page.children('div.button-group');
                if (buttonGroup != undefined) {
                    buttonGroup.remove();
                }
                validPages.pop();
                reduced = true;
            } else {
                if (reduced == true) {
                    break;
                }
            }
        }
        for (let i in validPages) {
            // get page id
            let pageId = validPages[i];
            // get page
            let page = $(pages[pageId]);
            console.log("i in validPages");
            console.log(i);
            // get button text
            //let status = (i >= validPages.length - 1 ? 'Submit' : 'Next');
            // get button group
            let buttonGroup = page.children('div.button-group');
            console.log("i in validPages");
            console.log(i);
            // find button group
            //let previousBtn = "<span></span>";
            //if (i > 0) { previousBtn = '<button class="previousBtn">Previous</button>' };
            if (buttonGroup.length < 1) {

                // get page first section
                let selectedSec = [];
                console.log("pageId in validages");
                console.log(pageId);
                console.log("this.state.btnOrder[pageId]");
                console.log(this.state.btnOrder[pageId]);
                if (this.state.btnOrder[pageId] != undefined) {
                    if (this.state.btnOrder[pageId] == -1) {
                        selectedSec = $(pages[pageId]).children('section[data-section-id="' + 0 + '"]')[0];
                    } else {
                        selectedSec = $(pages[pageId]).children('section[data-section-id="' + this.state.btnOrder[pageId] + '"]')[0];
                    }
                    console.log("selectedSec searched");
                    console.log(selectedSec);
                    if (selectedSec == undefined) {
                        // default after 1st section
                        selectedSec = $(pages[pageId]).children('section')[0];
                    }
                } else {
                    // default after 1st section
                    selectedSec = $(pages[pageId]).children('section')[0];
                }

                console.log("selectedSec");
                console.log(selectedSec);

                let remarkBtn = '<div class="button-group name text-center">' +
                    i18n.internal_btn_here +
                    '&nbsp;<a class="btnMove" href="javascript:;">' +
                    '<i class="ion-arrow-move"/>' +
                    '</a>' +
                    '</div>';
                // create button group
                if (this.state.btnOrder[pageId] == -1) {
                    buttonGroup = $(remarkBtn).insertBefore(selectedSec);
                } else {
                    buttonGroup = $(remarkBtn).insertAfter(selectedSec);
                }


                // bind events
                $('.page').sortable(this.btnConfigs).disableSelection();
                //$('.page').sortable();
            } else {
                //buttonGroup.children('button.final').text(status);
            }
            // update status
            //buttonGroup.removeClass(/^next$/i.test(status) ? 'submit' : 'next').addClass(status.toLowerCase());
        }

        // return status
        return totalPages <= 1 && emptyPages.length >= 1;
    }
    // copy data
    copySection(pageId, secId) {
        // get the fields which are not hidden
        let fields = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field:not([style*="display: none"])');
        let copyArray = [];
        let data = this.data;
        for (let i = 0; i < fields.length; i++) {
            let fieldId = fields[i].getAttribute('data-field-id');
            for (let j = 0; j < data.length; j++) {
                if (data[j].id == fieldId) {
                    copyArray.push(data[j]);
                }
            }
        }
        this.setState({ copyArray: copyArray });
    }
    pasteSection(pageId, secId) {
        let copyArray = this.state.copyArray;
        let data = this.data;
        for (let i = 0; i < copyArray.length; i++) {
            let rec = copyArray[i];
            let order = rec.order.split('-');
            let row = {
                id: this.state.largestId + 1, //id cannot be data.length, because the data could be deleted
                format: rec.format,
                label: rec.label,
                default: rec.default,
                answer: rec.answer,
                condition: rec.condition,
                path: rec.path,
                order: pageId + '-' + secId + '-' + order[2] + '-0',
                mandatory: rec.mandatory
            };
            data.push(row);
            this.setState({ largestId: this.state.largestId + 1 });
            // render row
            this.renderRow(row);
            if (this.state.showSave == false) { this.setState({ showSave: true }); }
        }
    }
    configSection(pageId, secId) {
        console.log("pageId, secId");
        console.log(pageId, secId);
        console.log("config section!");
        $('#editor > div').hide();
        $('#editor > div[data-property-name="config-section"]').show();
        let hideSecObj = Object.assign({}, this.state.hideSecObj);
        console.log("hideSecObj");
        console.log(hideSecObj);
        let cfgSecCheck = false;
        let pageArr = hideSecObj[pageId];
        if (pageArr != null) {
            console.log("pageArr");
            console.log(pageArr);
            for (let i = 0; i < pageArr.length; i++) {
                console.log("i"); console.log(i);
                if (pageArr[i] == secId) {
                    cfgSecCheck = true;
                }
            }
        }
        this.setState({ cfgSecCheck: cfgSecCheck, secId: secId });
        // let input = $('#editor > div[data-property-name="config-section"] :input');
        // input.prop('checked', cfgSecCheck);
    }
    tryHideSection(pageId, secId) {
        // sections that not include the sections
        let sections = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section:not([data-section-id=' + secId + '])');
        console.log("sections");
        console.log(sections);
        let fieldsLen = sections.children('.content').find('li.field').length;
        let HiddenFieldsLen = sections.children('.content').find('li.field[style*="display: none"]').length;
        console.log("HiddenFieldsLen");
        console.log(HiddenFieldsLen);
        console.log("fieldsLen");
        console.log(fieldsLen);
        if (HiddenFieldsLen == fieldsLen) {
            let hideFields = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
            console.log("hideFields");
            console.log(hideFields);
            for (let i = 0; i < hideFields.length; i++) {
                console.log("hideFields[i]");
                console.log(hideFields[i]);
                $(hideFields[i]).hide();
            }
        }
    }
    showSection(pageId, secId) {
        console.log("showSection now");
        console.log("fields");
        console.log(fields);
        let fields = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
        console.log("fields");
        console.log(fields);
        let data = this.data;
        for (let i = 0; i < fields.length; i++) {
            let fieldId = fields[i].getAttribute('data-field-id');
            for (let j = 0; j < data.length; j++) {
                if (data[j].id == fieldId) {
                    //this.renderView(data[j]);
                    this.showHideField($(fields[i]), data[j].condition);
                }
            }
        }
    }
    cfgSecChange(e) {
        let checked = e.target.checked;
        console.log("checked");
        console.log(checked);
        let hideSecObj = Object.assign({}, this.state.hideSecObj);
        let { pageId, secId } = this.state;
        let pageArr = hideSecObj[pageId];
        if (checked == false) { // true -> false
            let secIdx = pageArr.indexOf(secId);
            if (secIdx > -1) { pageArr.splice(secIdx, 1); }
        } else { // false -> true
            if (pageArr == undefined) { // not this page config
                hideSecObj[pageId] = [secId];
            } else { // already have this page config
                hideSecObj[pageId].push(secId);
            }
        }
        this.setState({ cfgSecCheck: checked, hideSecObj: hideSecObj }, () => {
            if (checked == true) {
                this.tryHideSection(pageId, secId);
            } else if (checked == false) {
                this.showSection(pageId, secId);
            }
        });
        if (this.state.showSave == false) { this.setState({ showSave: true }); }
    }
    adjustSections(container, order) {
        let oThis = this;
        console.log(" ======================= adjustSections ======================= ");
        console.log("container, order");
        console.log(container, order);
        // setup maximum
        let maximum = parseInt(order[1] || '0');
        console.log("maximum");
        console.log(maximum);
        // get sections
        let sections = container.children('section[data-section-id]').get();
        console.log("sections");
        console.log(sections);
        // get total section number
        let totalSections = sections.length;
        console.log("totalSections");
        console.log(totalSections);
        // setup empty section id container
        let emptySections = [];
        // check empty section
        for (let sectionId in sections) {
            // get section
            let section = $(sections[sectionId]);
            //set column value
            if (order[1] == sectionId && order[2] == "1") {
                //section.find('> .tools select').val("2");
                // changed from combo-box to swtich
                section.find('> .tools > div > input')[0].setAttribute("checked", "true");
            }
            // get max
            //NOTE:find: Get the descendants of each element in the current set of matched elements, filtered by a selector, jQuery object, or element. > direct child
            //let max = section.find('> .tools select').val(); //NOTES value of the combobox
            // changed from combo-box to swtich
            let max = section.find('> .tools >div >input')[0].checked == "true" || section.find('> .tools >div >input')[0].checked == true ? 2 : 1;
            console.log("section.find('> .tools > div >input')[0]");
            console.log(section.find('> .tools >div >input')[0]);
            console.log(" === max ===");
            console.log(max);
            // is it empty section?
            if (oThis.adjustColumns(section.children('.content'), max)) {
                // count as empty section
                emptySections.push(sectionId);
            }
        }
        console.log("emptySections.length");
        console.log(emptySections.length);
        // need to create new section? (no empty section and at least one section)
        if (emptySections.length < 1 && totalSections > 0 && maximum < totalSections) {
            // new page required
            maximum = totalSections;
        }
        // create sections
        for (let sectionId = 0; sectionId <= maximum; sectionId++) {
            console.log("sectionId");
            console.log(sectionId);
            // get section  //NOTES: .eq(): Reduce the set of matched elements to the one at the specified index.
            let section = container.children('section[data-section-id="' + sectionId + '"]');
            //let section = container.children(':eq(' + sectionId + ')');
            console.log(" ============ section ==========");
            console.log(section);
            // no section found?
            if (section.length < 1) {
                // add new section
                // section = $('<section data-section-id="' + sectionId + '">' +
                //     '<div class="tools">' +
                //     '<div>' +
                //     '<label>Columns</label>' +
                //     '&nbsp;' +
                //     '<select>' +
                //     '<option value="1">1</option>' +
                //     '<option value="2">2</option>' +
                //     '</select>' +
                //     '</div>' +
                //     '</div>' +
                //     '<div class="content"></div>' +
                //     '</section>');
                let swtichId = "switch" + this.switchNo;

                section = $('<section data-section-id="' + sectionId + '">' +
                    '<div class="tools">' +
                    '<a>Copy</a>&nbsp;<a>Paste</a>&nbsp;<a>Config</a>&nbsp;&nbsp;' +
                    '<label for="' + swtichId + '">2 Col.</label>' +
                    // '<div class="switch round small right">' +
                    // '<input id="switch"+"' + sectionId + '" name="switchName" type="checkbox" value=$(this).checked />' +
                    // '<label htmlFor="switch"+"' + sectionId + '"></label>'+//alert($(this).checked onChange={console.log($(this))}
                    // '</div>' +
                    '<div class="switch round small right">' +
                    '<input id="' + swtichId + '" + type="checkbox" />' +
                    '<label for="' + swtichId + '"></label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="content"></div>' +
                    '</section>');

                this.switchNo = Number(this.switchNo) + 1;

                console.log(" = section.find(> .tools > div >input)[0] =");
                console.log(section.find('> .tools > div > input').first());

                // bind events
                section.find('> .tools > div >input').first().bind('change', function (e) {
                    // columns changed, show Save button
                    if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
                    // update columns
                    let columnValue = e.target.checked == "true" || e.target.checked == true ? 2 : 1;
                    oThis.adjustColumns($(e.target).parents('section:first').children('.content'), columnValue);
                });
                let pageId = Number(order.slice(0, 1));
                section.find('> .tools > a').first().on('click', function (e) {
                    oThis.copySection(pageId, sectionId);
                });

                section.find('> .tools > a:eq(1)').on('click', function (e) {
                    console.log("paste clicked!")
                    oThis.pasteSection(pageId, sectionId);
                });

                section.find('> .tools > a').last().on('click', function (e) {
                    oThis.configSection(pageId, sectionId);
                });

                // setup columns
                this.adjustColumns(section.children('.content'), 1);
                // add new section to container
                section.appendTo(container);

                // new section should be an empty section as well
                emptySections.push(sectionId);
            }
        }
        // order index is greater than total?
        if (maximum > totalSections - 1) {
            // update total
            totalSections = maximum + 1;
        }
        // return status
        return totalSections <= 1 && emptySections.length >= 1;
    }
    //NOTES: when the columns combo-box selected 1 or 2
    adjustColumns(container, maximum) {
        let oThis = this;
        let data = this.data;
        console.log(" ======================= adjustColumns ======================= ");
        console.log("container, maximum");
        console.log(container, maximum);
        // get columns
        let emptyColumns = [];
        let columns = container.children('.column[data-column-id]').get();
        let totalColumns = columns.length;
        console.log("totalColumns");
        console.log(totalColumns);
        // check empty columns
        for (let columnId in columns) {
            // is it empty?
            if ($(columns[columnId]).children().length < 1) {
                emptyColumns.push(columnId);
            }
        }
        // setup max of columns, 0,1 two columns only now
        maximum = maximum || 1;
        // need to reduce?
        // save tmpOrder to resotre the order when columns change back to 1 to 2 
        let tmpOrder = Object.assign({}, oThis.state.tmpOrder);
        console.log("tmpOrder[container]");
        console.log(tmpOrder[container]);
        //undefined
        if (totalColumns > maximum) {
            let objectArray = [];
            //NOTES:Columns 2 -> 1
            for (let columnId = columns.length - 1; columnId < totalColumns; columnId++) {
                //get column
                let column = $(columns[columnId]);
                //change the order of data in right
                let fields = column.children('.field[data-field-id]').get();   //.attr('data-field-id');
                //let fields = column.children('.field[data-field-id]').get();
                console.log("fields");
                console.log(fields);
                //get the last order of the previous column
                let lastOrder = $(columns[columnId - 1]).children().get().length;//.length;
                console.log("lastOrder");
                console.log(lastOrder);
                let lastOldOrder = 0;
                for (let idx in fields) {
                    console.log("idx");
                    console.log(idx);
                    let fieldId = fields[idx].getAttribute('data-field-id');
                    console.log("fieldId");
                    console.log(fieldId);
                    for (let i in data) {
                        if (data[i].id == fieldId) {
                            console.log(" ***************** fieldId ******************");
                            let dataOrder = data[i].order.slice();
                            // Page number and sections can be over 10, so need to get the index of 2nd desh's index
                            let firsttDeshIdx = dataOrder.indexOf('-');
                            let secondDeshIdx = dataOrder.indexOf('-', firsttDeshIdx + 1);
                            data[i].order = dataOrder.substr(0, secondDeshIdx + 1) + (columnId - 1) + '-' + lastOrder;
                            lastOrder += 1;
                            let newOrder = data[i].order.slice();
                            let orderObj = { id: data[i].id, newOrder: newOrder }
                            objectArray.push(orderObj);
                            console.log("orderObj");
                            console.log(orderObj);
                            console.log(data[i].order);
                            break; //Stop this loop, we found it!
                        }
                    }
                }
                // save the temp order for restoring the order (change back 1 -> 2) later if needed
                tmpOrder[container] = objectArray;
                oThis.setState({ tmpOrder: tmpOrder });
                console.log("tmpOrder after update");
                console.log(tmpOrder);
                //move fields to previous column //appendTo: Insert every element in the set of matched elements to the end of the target.
                column.children().appendTo(columns[columnId - 1]);
                //delete column
                column.empty().removeData().remove();
            }
            // need to create
        } else if (totalColumns < maximum) {
            //NOTES:Columns 1 -> 2
            let column = null
            // start from the top
            for (let columnId = 0; columnId < maximum; columnId++) {
                // get column
                column = container.children(':eq(' + columnId + ')');
                // need to create?
                if (column.length < 1) {
                    // create column
                    column = $('<ul class="column" data-column-id="' + columnId + '"></ul>');
                    // bind events
                    column.sortable(oThis.viewerConfigs.sortable)
                        .disableSelection()
                        .droppable(oThis.viewerConfigs.droppable);
                    // append to container
                    column.appendTo(container);
                    // new column should be a empty column
                    emptyColumns.push(columnId);
                }
            }
            // restore the old order
            let tmpOrderArr = tmpOrder[container];
            if (tmpOrderArr != undefined) {
                for (let i = 0; i < tmpOrderArr.length; i++) {
                    for (let j = 0; j < data.length; j++) {
                        let dataOrder = data[j].order;
                        if (tmpOrderArr[i].id == data[j].id && tmpOrderArr[i].newOrder == dataOrder) {
                            console.log("tmpOrderArr[i]");
                            console.log(tmpOrderArr[i]);
                            // update data's older
                            let firsttDeshIdx = dataOrder.indexOf('-');
                            let secondDeshIdx = dataOrder.indexOf('-', firsttDeshIdx + 1);
                            // if column number is more then 2 in future, need to edit below
                            data[j].order = dataOrder.substr(0, secondDeshIdx + 1) + 1 + '-' + i;
                            console.log("data[j].order");
                            console.log(data[j].order);
                            // append to column 2
                            console.log("columns");
                            console.log(columns);
                            console.log("column");
                            console.log(column);
                            console.log("columns.children[0]");
                            console.log(columns[0]);
                            console.log("$(columns[0]).children('.field')");
                            console.log($(columns[0]).children('.field'));
                            $(columns[0]).children('.field[data-field-id=' + data[j].id + ']').appendTo(column);
                            // remove from first column
                        }
                    }
                }
            }
        }
        // return status
        return emptyColumns.length >= maximum;
    }
    // NOTES: Build Laft Panel Row
    renderStructure(row, isRenderView) {
        let oThis = this;
        console.log(" ======================= renderStructure ======================= ");
        console.log(row);
        // get order (0:page, 1:section, 2:column, 3:idx)
        let order = row.order.split('-');
        // setup pages
        this.adjustPages($('#pages'), order);
        // ---------- render tree node ---------- //
        // get path
        let path = (row.path || '').split(',').filter(function (v) { return v.length > 0; }) || [];
        // get parent id
        let parentId = path[path.length - 1] || null;
        // parent selector
        let parentSelector = (parentId ? '#structure .scroll-area .ui-sortable .node[data-field-id="' + parentId + '"] > ul' : '#structure > .scroll-area > ul') + ':first';
        console.log("parentSelector");
        console.log(parentSelector);
        // get existing node
        let node = $('#structure .scroll-area .ui-sortable .node[data-field-id="' + row.id + '"]');
        // no existing node found?
        if (node.length < 1) {
            // create new node
            node = $('<li class="node" data-field-id="' + row.id + '">' +
                '<div class="name">' +
                '<div class="label">' + (row.label || 'Untitled') + '</div>' +
                '<div class="tools">' +
                '<a class="config" href="javascript:;">' +
                '<i class="ion-gear-b"/>' +
                '</a>' +
                '<a class="move" href="javascript:;">' +
                '<i class="ion-arrow-move"/>' +
                '</a>' +
                '<a class="trash" href="javascript:;">' +
                '<i class="ion-trash-a"/>' +
                '</a>' +
                '</div>' +
                '</div>' +
                '<ul class="childs"></ul>' +
                '</li>');
            // bind events
            node.children('ul').sortable(this.configs.tree.sortable).disableSelection();
            node.find('.config:first').bind('click', function (e) {
                oThis.loadCondition($(e.target).parents('.node[data-field-id]').attr('data-field-id'));
            });
            node.find('.trash:first').bind('click', function (e) {
                oThis.deleteData($(e.target).parents('.node[data-field-id]').attr('data-field-id'));
            });
        } else {
            // update label
            node.find('> .name > .label').text(row.label);
        }
        // position changed or new field
        if (node.parents(parentSelector).length < 1) {
            // append node to related container
            node.appendTo(parentSelector);
        }
        // ---------- /render tree node ---------- //
        if (isRenderView == true) { oThis.renderView(row) };
    }
    showHideField(field, rowCondition) {
        let data = this.data;
        // set status
        let show = true, conditions = [];
        // parse conditions
        try { conditions = JSON.parse(rowCondition); } catch (e) { conditions = []; }
        // check condition
        while (show && conditions.length > 0) {
            // get condition NOTES: take out the first from the array
            let condition = conditions.shift();
            console.log("condition");
            console.log(condition);
            // get target field info
            let targetIdx = -1;
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == condition[0]) {
                    targetIdx = i;
                    break;
                }
            }
            let targetRow = data[targetIdx];
            // get target row type
            let targetFormatType = formatTypeSwitch(targetRow.format);
            console.log("targetFormatType");
            console.log(targetFormatType);
            // get value
            let value = targetRow.answer || targetRow.default;
            console.log("value");
            console.log(value);
            // check condition ============ = ============
            if (/^=$/.test(condition[1])) {
                // update status
                show = new RegExp('^' + condition[2] + '$').test(value);
                console.log("show");
                console.log(show);
            } else {
                // date related?
                if (/^date(time)?$/i.test(targetFormatType)) {
                    // parse condition
                    condition[2] = new Date(condition[2].replace(/-/g, '/')).getTime();
                    // parse value
                    value = new Date(value.replace(/-/g, '/')).getTime();
                } else {
                    // parse condition
                    condition[2] = parseFloat(condition[2]);
                    // parse value
                    value = parseFloat(value);
                }
                //  ============ > ============
                if (/^>$/.test(condition[1])) {
                    // check value
                    show = value > condition[2];
                    // ============ < ============
                } else if (/^<$/.test(condition[1])) {
                    // check value
                    show = value < condition[2];
                    // ============ >= ============
                } else if (/^>=$/.test(condition[1])) {
                    // check value
                    show = value >= condition[2];
                    // ============ <= ============
                } else if (/^<=$/.test(condition[1])) {
                    // check value
                    show = value <= condition[2];
                }
            }
        }
        // --------------------------- /input field processing --------------------------- //
        // ---------- /render column field ---------- //
        // set status
        field[show ? 'show' : 'hide']();
        return show;
    }
    //NOTES: Build Middle Panel Row
    renderView(row) {
        console.log(" ======================= renderView ======================= ");
        console.log("row");
        console.log(row);
        let oThis = this;
        let data = this.data;
        let order = row.order.split('-');
        // ---------- render column field ---------- //
        // get existing field
        let field = $('#viewer .field[data-field-id="' + row.id + '"]');
        // get type
        let type = formatConverter.getType(row.format);
        //let type = formatConverter.identify(row.format);
        console.log("type in renderView");
        console.log(type);
        // no existing field found?
        if (field.length < 1) {
            console.log("Create Field Label");
            // create new field
            field = $('<li class="field" data-field-type="' + type + '" data-field-id="' + (row.id || 0) + '">' +
                '<div class="wrapper">' +
                '<div class="name">' +
                '<div class="label">' +
                '<label><span class="label-normal">' + (row.label || '') + '</span><span class="color-red">' + (row.mandatory == true ? ' *' : '') + '</span></label> ' +
                '</div>' +
                '<div class="tools">' +
                '<a class="config" href="javascript:;">' +
                '<i class="ion-gear-b"/>' +
                '</a>' +
                '<a class="move" href="javascript:;">' +
                '<i class="ion-arrow-move"/>' +
                '</a>' +
                '<a class="trash" href="javascript:;">' +
                '<i class="ion-trash-a"/>' +
                '</a>' +
                '</div>' +
                '</div>' +
                '<div class="input-row">' +
                '<div class="input-field"></div>' +
                '</div>' +
                '</div>' +
                '</li>');
            // bind events
            field.find('.config').bind('click', function (e) {
                // load and show config
                oThis.loadConfig($(e.target).parents('.field:first').attr('data-field-id'));
            });
            field.find('.trash').bind('click', function (e) {
                oThis.deleteData($(e.target).parents('.field:first').attr('data-field-id'));
            });
            // append field to related column
            field.appendTo([
                '#pages .page[data-page-id="' + order[0] + '"]',
                'section[data-section-id="' + order[1] + '"]',
                '.column[data-column-id="' + order[2] + '"]'
            ].join(' '));
        } else {
            console.log("Update Field Label");
            // (config updated, so) update view's label and mandatory 
            field.find('label').children('.label-normal').text(row.label || '');
            field.find('label').children('.color-red').text(row.mandatory == true ? ' *' : '');
        }
        // setup label
        let label = field.find('.input-row .label');
        // set status
        label[(label.is(':empty') ? 'hide' : 'show')]();

        // --------------------------- input field processing --------------------------- //
        // get input container
        let inputContainer = field.find('.input-field');
        // clear existing input
        inputContainer.children().empty().removeData().remove();
        // get input value
        let inputValue = row.answer || row.default;
        console.log("row");
        console.log(row);
        console.log("inputValue");
        console.log(inputValue);
        // get variable
        let variable = inputValue.replace(/^\$\{([0-9]+)\}$/, '$1');
        // it's a variable
        if (inputValue != variable) {
            // update input value
            inputValue = (data[variable].answer || data[variable].default) || '';
        }
        // get template
        let template = formatConverter.render(type).replace('{VALUE}', inputValue);
        //let template = null;
        // for security, the value can more than 50 digits
        let value = row.label.substring(0, 49);
        // if (type == 'header') {
        //     template = formatConverter.render(type).replace('{TITLE}', value || '');
        // } else {
        //     template = formatConverter.render(type).replace('{VALUE}', inputValue);
        // }
        // create input field
        console.log("template");
        console.log(template);
        console.log("type");
        console.log(type);
        let input = $(template);
        // dropdown?
        if (/^dropdown$/i.test(type)) {
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                $('<option value="' + value + '">' + label + '</option>').appendTo(input);
            }
            // set value
            console.log("combobox inputvlaue");
            console.log(inputValue);
            input.val(inputValue);
            // checkbox?
        } else if (/^checkbox$/i.test(type)) {
            // get answer
            let answer = inputValue.split(',');
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                input.append(
                    '<input type="checkbox" name="' + row.id + '[]" id="' + row.id + '-' + i + '" value="' + value + '"/>' +
                    '<label for="' + row.id + '-' + i + '">' + label + '</label>'
                );
                // set checked
                if (answer.indexOf(value) >= 0) {
                    input.children('input#' + row.id + '-' + i).attr('checked', true);
                }
            }
        } else if (/^radio$/i.test(type)) {
            // get answer
            let answer = inputValue.split(',');
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                input.append(
                    '<input type="radio" name="' + row.id + '[]" id="' + row.id + '-' + i + '" value="' + value + '"/>' +
                    '<label for="' + row.id + '-' + i + '">' + label + '</label>'
                );
                // set checked
                if (answer.indexOf(value) >= 0) {
                    input.children('input#' + row.id + '-' + i).attr('checked', true);
                }
            }
        } else if (/^file$/i.test(type)) {
            input.bind('change', function (e) {
                oThis.upload(row.id);
            });
            let fileName = this.state.fileName[row.id];
        } else if (/^html$/i.test(type)) {
            // setup ckeditor
            input.ckeditor(function (textarea) {
                // callback
            }, {
                    // config
                });
        }

        // update input field
        input.bind('change', function (e) {
            // middle page input changed, show Save button
            if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
            // get id
            let id = $(e.target).parents('li[data-field-id]').attr('data-field-id');

            let idIdx = -1;
            for (let i in data) {
                if (data[i].id == id) {
                    idIdx = i;
                    break;
                }
            }

            if (typeof e.target.name != "undefined" && e.target.name) {
                // get options
                let options = $('#viewer input[name="' + e.target.name + '"]:checked').get(), values = [];
                // get values
                for (let i in options) values.push(options[i].value);
                // append value to value container
                $(e.target).is(':checked') && values.indexOf(e.target.value) < 0 && values.push(e.target.value);
                // delete value from value container
                !$(e.target).is(':checked') && values.indexOf(e.target.value) >= 0 && values.slice(values.indexOf(e.target.value), 1);
                // update value
                data[idIdx].answer = values.join(',');
            } else {
                // update value
                console.log("event.target.value");
                console.log(e);
                data[idIdx].answer = e.target.value;
            }
            // reload field
            oThis.renderRow(data[idIdx]);
            // get sub questions
            let subQuestions = $('#structure .node[data-field-id="' + id + '"] > .childs').children().get();
            // reload sub questions
            console.log("subQuestions");
            console.log(subQuestions);
            let subQuesIdx = -1;
            for (let i in subQuestions) {
                let subQuesId = $(subQuestions[i]).attr('data-field-id');
                console.log("subQuesId");
                console.log(subQuesId);
                for (let j = 0; j < data.length; j++) {
                    if (data[j].id == subQuesId) {
                        subQuesIdx = j;
                        break;
                    }
                }
                console.log("subQuesIdx");
                console.log(subQuesIdx);
                // to show the subQuest or not
                oThis.renderView(data[subQuesIdx]);
                //oThis.renderRow(data[subQuesIdx]);
            }
            //for (let i in subQuestions) oThis.renderRow(data[subQuesIdx]);
        }).appendTo(inputContainer);
        // set status
        // let show = true, conditions = [];
        // // parse conditions
        // try { conditions = JSON.parse(row.condition); } catch (e) { conditions = []; }
        // // check condition
        // while (show && conditions.length > 0) {
        //     // get condition NOTES: take out the first from the array
        //     let condition = conditions.shift();
        //     console.log("condition");
        //     console.log(condition);
        //     // get target field info
        //     let targetIdx = -1;
        //     for (let i = 0; i < data.length; i++) {
        //         if (data[i].id == condition[0]) {
        //             targetIdx = i;
        //             break;
        //         }
        //     }
        //     let targetRow = data[targetIdx];
        //     // get target row type
        //     let targetFormatType = formatTypeSwitch(targetRow.format);
        //     console.log("targetFormatType");
        //     console.log(targetFormatType);
        //     // get value
        //     let value = targetRow.answer || targetRow.default;
        //     console.log("value");
        //     console.log(value);
        //     // check condition ============ = ============
        //     if (/^=$/.test(condition[1])) {
        //         // update status
        //         show = new RegExp('^' + condition[2] + '$').test(value);
        //         console.log("show");
        //         console.log(show);
        //     } else {
        //         // date related?
        //         if (/^date(time)?$/i.test(targetFormatType)) {
        //             // parse condition
        //             condition[2] = new Date(condition[2].replace(/-/g, '/')).getTime();
        //             // parse value
        //             value = new Date(value.replace(/-/g, '/')).getTime();
        //         } else {
        //             // parse condition
        //             condition[2] = parseFloat(condition[2]);
        //             // parse value
        //             value = parseFloat(value);
        //         }
        //         //  ============ > ============
        //         if (/^>$/.test(condition[1])) {
        //             // check value
        //             show = value > condition[2];
        //             // ============ < ============
        //         } else if (/^<$/.test(condition[1])) {
        //             // check value
        //             show = value < condition[2];
        //             // ============ >= ============
        //         } else if (/^>=$/.test(condition[1])) {
        //             // check value
        //             show = value >= condition[2];
        //             // ============ <= ============
        //         } else if (/^<=$/.test(condition[1])) {
        //             // check value
        //             show = value <= condition[2];
        //         }
        //     }
        // }
        // // --------------------------- /input field processing --------------------------- //
        // // ---------- /render column field ---------- //
        // // set status
        // field[show ? 'show' : 'hide']();
        let show = this.showHideField(field, row.condition);
        // adjust pages
        this.adjustPages($('#pages'), order);
        // get cloest page id
        console.log("field.closest('.page')");
        console.log(field.closest('.page'));
        let secId = field.closest('section')[0].getAttribute('data-section-id');
        let pageId = field.closest('.page')[0].getAttribute('data-page-id');
        console.log("secId");
        console.log(secId);
        console.log("pageId");
        console.log(pageId);
        let hideSecObj = this.state.hideSecObj;
        let pageArr = hideSecObj[pageId];
        if (pageArr) {
            console.log("pageArr");
            console.log(pageArr);
            for (let i = 0; i < pageArr.length; i++) {
                // change others
                if (show == true) {
                    if (secId != pageArr[i]) { // to prevent unlimited loop
                        this.showSection(pageId, pageArr[i]);
                    } else {
                        this.tryHideSection(pageId, pageArr[i]);
                    }
                } else {
                    this.tryHideSection(pageId, pageArr[i]);
                }
                // if (show == true) {
                //     if (secId != pageArr[i]) { // to prevent unlimited loop
                //         this.showSection(pageId, pageArr[i]);
                //     }
                // } else {
                //     this.tryHideSection(pageId, pageArr[i]);
                // }
                // // original setting
                // if (secId == pageArr[i]) {
                //     this.tryHideSection(pageId, pageArr[i]);
                // }
            }
        }
    }
    // render field
    renderRow(row) {
        this.renderStructure(row, true);
    }
    //NOTES: trigger when drage the browser(right panel bottom part) to the viewer(middle panel) 
    createRow(e, ui) {
        if (this.state.showBlank == true) {
            this.setState({ showBlank: false });
            $('#blankImg').remove();
        }
        if (this.state.showSave == false) { this.setState({ showSave: true }); }
        let data = this.data;
        // get page id
        let pageId = $(e.target).parents('.page:first').attr('data-page-id');
        console.log("pageId");
        console.log(pageId);
        // get section id
        let sectionId = $(e.target).parents('section:first').attr('data-section-id');
        console.log("sectionId");
        console.log(sectionId);
        // get column id
        let columnId = $(e.target).attr('data-column-id');
        console.log("columnId");
        console.log(columnId);
        console.log("ui.helper");
        console.log(ui.helper);
        console.log("ui.helper.parent()");
        console.log(ui.helper.parent());
        console.log("ui.helper.parent().attr('data-field-type')");
        console.log(ui.helper.parent().attr('data-field-type'));
        let label = 'Untitled';
        let answer = ''
        let type = ui.helper.parent().attr('data-field-type');
        if (type == 'header') {
            label = '';
            answer = 'Untitled';
        } else if (type == 'html') {
            label = '';
        }
        let format = '';
        switch (type) {
            case 'radio':
                format = "^(-?(label 1:label 1))*$";
                break;
            case 'dropdown':
                format = '^(label 1:label 1)$';
                break;
            case 'checkbox':
                format = '^(,?(label 1:label 1))*$'
                break;
            default:
                format = formatConverter.getFormat(type);
                break;
        }
        let row = {
            id: this.state.largestId + 1,//data.length > 0 ? (data[data.length - 1].id + 1) : 1, //id cannot be data.length, because the data could be deleted
            format: format,
            label: label,
            default: '',
            answer: answer,
            condition: '',
            path: '',
            order: pageId + '-' + sectionId + '-' + columnId + '-0'
        };
        this.setState({ largestId: this.state.largestId + 1 });
        // append to data container
        data.push(row);
        console.log("data");
        console.log(data);
        // render row
        this.renderRow(row);
    };
    //NOTES: when editor(upper right corner) format is drop down or checkbox, will need to add option
    addOption(option) {
        console.log(" ====================== addOption ====================== ")
        let oThis = this;
        let data = this.data;
        // split option
        option = option.split(':');
        // get value
        let value = option.shift();
        // get label
        let label = option.shift() || value;
        // get container
        let container = $('#editor > div[data-property-name="options"]');
        // add option
        let optionRow = $('<div>' +
            '<input name="options[value][]" type="text" placeholder="option" value="' + value + '" "/>' +
            '<input name="options[label][]" type="text" placeholder="label" value="' + label + '" "/>' +
            '</div>');
        // bind events
        let OptionChangeFn = function (e, isLabel, isBlur) {
            // config's combo-box changed, show Save button
            if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
            console.log("e");
            console.log(e);
            console.log("e.target.value");
            console.log(e.target.value);
            // get index
            let idx = $(e.target).parent().index() - 1;
            // get position
            let position = /^options\[value\]\[\]$/i.test(e.target.name) ? 'first' : 'last';
            // get editing field id
            let id = $('#editor').attr('data-editing-field');
            //get idx
            let idIdx = -1;
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == id) {
                    idIdx = i;
                    break;
                }
            }
            // get type
            let type = formatTypeSwitch(data[idIdx].format);
            // setup format data container
            let format = [];
            // get all options
            let options = $('#editor > div[data-property-name="options"] > div').get();
            // convert options to format
            for (let i in options) {
                // get value
                let value = $(options[i]).children('input[name="options[value][]"]').val();
                console.log("value in OptionChangeFn");
                console.log(value);
                // get label
                let label = oThis.labelFollow == true ? value : $(options[i]).children('input[name="options[label][]"]').val();
                console.log("label in OptionChangeFn");
                console.log(label);
                // append to format data container
                value.length > 0 && format.push(value + (label.length > 0 ? ':' : ':&nbsp;') + label);
            }
            // update format
            console.log("format");
            console.log(format);

            // blank data
            if (format.length == 0) {
                // empty format for checkbox or dropdown
                if (/^checkbox$/i.test(type)) {
                    data[idIdx].format = '^(&?[a-z0-9A-Z]+=.*)*$';
                } else if (/^dropdown$/i.test(type)) {
                    data[idIdx].format = '^()$';
                } else { // radio
                    data[idIdx].format = '^(-?())$';
                }
                //data[idIdx].format = /^checkbox$/i.test(type) ? '^(&?[a-z0-9A-Z]+=.*)*$' : '^()$';//'^[*$';
            } else {
                if (/^checkbox$/i.test(type)) {
                    data[idIdx].format = '^(,?(' + format.join('|') + '))*$';
                } else if (/^dropdown$/i.test(type)) {
                    data[idIdx].format = '^(' + format.join('|') + ')$';
                } else { // radio
                    data[idIdx].format = '^(-?(' + format.join('|') + '))*$';
                }
                //data[idIdx].format = /^checkbox$/i.test(type) ? '^(,?(' + format.join('|') + '))*$' : '^(' + format.join('|') + ')$';
            }

            console.log("data[idIdx].format");
            console.log(data[idIdx].format);
            // reload field
            oThis.renderRow(data[idIdx]);
            // why loadConfig? if last option filled, will have a new option, config fields

            if (isLabel == false) {
                return oThis.loadConfig(id);
            }
        }

        optionRow.children('[name="options[value][]"]').bind('blur keyup', function (e) { //NOTES: when enter anything in the option row and keyup function
            e.preventDefault();
            // next Div (next parent of the field)
            let nextDiv = $(this).parent().next();
            // get the index
            let nextDivIdx = $('#editor > div[data-property-name="options"]').children().index(nextDiv);
            // current Div (next parent of the field)
            let curDiv = $(this).parent();
            // current div index
            let curDivIdx = $('#editor > div[data-property-name="options"]').children().index(curDiv);
            // label field val
            let labelVal = $(this).next().val().slice();
            // to labelVal is blank at the beginning
            if (oThis.labelFollow == false && labelVal == "") { oThis.labelFollow = true }
            // clicked enter
            if (e.keyCode === 13) {
                console.log(" =================== key ===============")
                // to focus next field when loadConfig function finished
                let loadConfigEnd = OptionChangeFn(e, false);
                //if (e.type === 'blur' || e.keyCode === 13) {
                // to focus next field when loadConfig function finished
                if (loadConfigEnd == true) {
                    let focusField = null;
                    if (e.keyCode === 13) {
                        focusField = $('#editor > div[data-property-name="options"]').children().eq(nextDivIdx).children('[name="options[value][]"]');
                    } else {
                        focusField = $('#editor > div[data-property-name="options"]').children().eq(curDivIdx).children('[name="options[label][]"]');
                    }
                    focusField.focus();
                    // to move to the cursor to the end
                    let tmpStr = focusField.val();
                    focusField.val('');
                    focusField.val(tmpStr);
                    return
                }
            } else if (e.type != 'blur' & e.keyCode != 13) {
                // to focus next field when loadConfig function finished
                OptionChangeFn(e, false);
                // to focus current field when loadConfig function finished
                let focusField = $('#editor > div[data-property-name="options"]').children().eq(curDivIdx).children('[name="options[value][]"]');
                let labelField = $('#editor > div[data-property-name="options"]').children().eq(curDivIdx).children('[name="options[label][]"]');

                if (oThis.labelFollow == false && labelField.val() == "") { oThis.labelFollow = true }
                focusField.focus();
                // to move to the cursor to the end
                let tmpStr = focusField.val();
                focusField.val('');
                focusField.val(tmpStr);

                return
            } else if (e.type == 'blur') {
                console.log(" =================== blur option ===================")
                oThis.labelFollow = false;
            }
        });

        optionRow.children('[name="options[label][]"]').bind('keyup', function (e) { //NOTES: when enter anything in the option row and keyup function
            e.preventDefault();
            OptionChangeFn(e, true);
            // press enter focus next field
            // if (e.keyCode == 13 || e.keyCode === 9) {
            if (e.keyCode == 13) {
                console.log("this");
                console.log(this);
                let nextSib = $(this).parent().next().children('[name="options[value][]"]');
                console.log("nextSib");
                console.log(nextSib);
                nextSib.focus();
                // to move to the cursor to the end
                let tmpStr = nextSib.val();
                nextSib.val('');
                nextSib.val(tmpStr);
            }
        });

        // append to container
        optionRow.appendTo(container);
    };
    //NOTES: When 1. clicked Tile button of the field or 2. make any changes in eidtor(right upper column) will load this function
    loadConfig(id) {
        let oThis = this;
        let data = this.data;
        console.log(" ************************ load config ************************");
        // =========================== 1. HIDE all properties ===========================
        // hide all properties
        $('#editor > div').hide();
        // set editing field
        $('#editor').attr('data-editing-field', id);
        // get row
        let row = null;
        console.log("id");
        console.log(id);
        for (let i = 0; i < data.length; i++) { //NOTES: it's because row(id) can be deleted, so cannot just let row = data[id];
            if (data[i].id == id) {
                row = data[i];
                break;
            }
        }
        console.log("row.format in loadConfig");
        console.log(row.format);
        // get type
        let type = formatConverter.getType(row.format);
        //let type = formatTypeSwitch(row.format);
        console.log("~~~~~~~~~~~~~ type ~~~~~~~~~~~~~");
        console.log(type);
        // =========================== 2. Set value ===========================
        for (let key in row) {
            // get property input field
            console.log("key");
            console.log(key);
            let input = $('#editor > div[data-property-name="' + key + '"] :input');
            console.log("input of #editor > div[data-property-name= in loadconfig");
            console.log(input);
            // valid input
            if (input.length > 0) {
                // Select the combobox of "Format"
                if (/^format$/i.test(key)) {
                    console.log("format");
                    // dropdown?
                    if (/^dropdown$/i.test(type)) {
                        console.log("dropdown");
                        input.val('^()$');
                        //input.val('^[a-z0-9A-Z]+=.*$');
                        // checkbox?
                    } else if (/^checkbox$/i.test(type)) {
                        console.log("checkbox");
                        // input.val('^(,?())*$');
                        input.val('^(&?[a-z0-9A-Z]+=.*)*$');
                    } else if (/^radio$/i.test(type)) {
                        input.val('^(-?())*$');
                    } else if (/^date/i.test(type)) {
                        input.val('^([0-9]{4})-([0-9]{2})-([0-9]{2})$');
                    } else if (/^datetime/i.test(type)) {
                        input.val('^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$');
                    } else if (/^file$/i.test(type)) {
                        input.val('.(jpe?g|png|gif)$');
                    } else if (/^label$/i.test(type)) {
                        input.val('^.+$');
                    } else if (/^link$/i.test(type)) {
                        input.val('^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%:\/~+#-]*[\w@?^=%\/~+#-])?$');
                        //input.val('^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?$');
                        //input.val('^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?$');
                    } else if (/^html$/i.test(type)) {
                        input.val('^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$');
                    }
                    else if (/^number$/i.test(type)) {
                        input.val('^[0-9.]+$');
                    }
                    console.log("input after setting value");
                    console.log(input);
                    // SET VALUE FOR OTHER FIELDS IN CONFIG
                } else {
                    console.log("else");
                    if (key == 'mandatory') {
                        console.log('key == mandatory');
                        input.prop('checked', row[key]);
                    } else {
                        input.val(row[key]);
                    }
                }
            }
        }
        // =========================== 3. Set Options for dropdown / checkbox ===========================
        // set selector to show
        //let selector = '';
        // if (/^header$/i.test(type)) {
        //     selector = '#editor > div:not([data-property-name="condition"],[data-property-name="default"] )';
        //     //$('#editor > div[data-property-name="default"]').hide();
        // } else if (/^html$/i.test(type)) {
        //     selector = '#editor > div:not([data-property-name="condition"],[data-property-name="label"] )';
        // } else {
        //     selector = '#editor > div:not([data-property-name="condition"])';
        // }
        let selector = '#editor > div:not([data-property-name="condition"], [data-property-name="-section"])';
        // is dropdown?
        console.log("type in selector");
        console.log(type);
        if (/^dropdown$/i.test(type)) {
            console.log(" ================================ type = drop down ====================== ")
            // get options
            let options = getOptions(type, row.format);
            // clear existing options
            $('#editor > div[data-property-name="options"] > div').empty().removeData().remove();
            // add blank option
            options.push('');
            // set options
            for (let i in options) oThis.addOption(options[i]);
            // checkbox
        } else if (/^checkbox$/i.test(type)) {
            console.log(" ================================ type = checkbox ====================== ")
            // get options
            let options = getOptions(type, row.format);
            // clear existing options
            $('#editor > div[data-property-name="options"] > div').empty().removeData().remove();
            // add blank option
            options.push('');
            // set options
            for (let i in options) oThis.addOption(options[i]);
            // not dropdown or check box
        } else if (/^radio$/i.test(type)) {
            console.log(" ================================ type = radio ====================== ")
            // get options
            let options = getOptions(type, row.format);
            // clear existing options
            $('#editor > div[data-property-name="options"] > div').empty().removeData().remove();
            // add blank option
            options.push('');
            // set options
            for (let i in options) oThis.addOption(options[i]);
            // not dropdown or check box
        } else {
            console.log(" ================================ type = else ====================== ")
            // hide options
            selector += ':not([data-property-name="options"])';
        }
        console.log("selector to be shown");
        console.log("$(selector)");
        console.log($(selector));
        // =========================== 4. Show specific fields ===========================
        // show related properties
        $(selector).show();
        // if type is header hide default as no need
        // if (/^header$/i.test(type)) {
        //     $('#editor > div[data-property-name="default"]').hide();
        // } else if (/^html$/i.test(type)) {
        //     $('#editor > div[data-property-name="label"]').hide();
        // }
        // for option enter trigger focus now
        return true
    }

    loadCondition(id) {
        let oThis = this;
        let data = this.data;
        console.log(" ************************ loadCondition ************************");
        console.log("id");
        console.log(id);
        // hide all properties
        $('#editor > div').hide();
        // set editing field
        $('#editor').attr('data-editing-field', id);
        // get data row
        let row = null;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                row = data[i];
                break;
            }
        }

        // get parent id
        //let parentId = path[path.length - 1] || null;
        let parentId = $('li.node[data-field-id=' + id + ']').parent('.childs').parent('li.node').attr('data-field-id');
        console.log("parentId");
        console.log(parentId);
        // get parent info
        let parentRow = null;
        console.log("data");
        console.log(data);
        for (let i = 0; i < data.length; i++) {
            console.log("i");
            console.log(i);
            console.log('data[i].id.toString()');
            console.log(data[i]);
            if (data[i].id == parentId) {
                parentRow = data[i];
                console.log("parentRow in loop");
                console.log(parentRow);
                break; //Stop this loop, we found it!
            }
        }
        console.log("parentRow");
        console.log(parentRow);
        // get parent answer type
        let parentAnswerType = formatTypeSwitch(parentRow.format);
        console.log("parentAnswerType");
        console.log(parentAnswerType);
        // condition container
        let conditions = [];
        // parse conditions
        try { conditions = JSON.parse(row.condition); } catch (e) { conditions = []; }
        console.log("conditions");
        console.log(conditions);
        // set condition field value
        $('#editor > div[data-property-name="condition"] select[name="condition[field]"]').append(
            '<option value="' + parentRow.id + '">' +
            parentRow.label +
            '</option>'
        ).val(parentRow.id);
        // get template
        //BUG FOUND: templates is not defined
        //let template = templates[parentAnswerType].replace('{VALUE}', '');
        let template = formatConverter.render(parentAnswerType).replace('{VALUE}', '');
        // delete existing value field
        $('#editor > div[data-property-name="condition"] [name="condition[value]"]').empty().removeData().remove();
        // create input
        let input = $(template)
            // set name
            .attr('name', 'condition[value]')
            // bind events
            .bind('change', function (e) {
                // condition input changed, show Save button
                if (oThis.state.showSave == false) { oThis.setState({ showSave: true }); }
                oThis.setCondition(e.target.value);
            })
            // append input
            .appendTo('#editor > div[data-property-name="condition"]');
        // dropdown?
        if (/^dropdown$/i.test(parentAnswerType)) {
            // get options
            let options = getOptions(parentAnswerType, parentRow.format);
            // append options
            for (let i in options) {
                // split option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append option
                $('<option value="' + value + '">' + label + '</option>').appendTo(input);
            }
            // is checkbox?
        } else if (/^checkbox$/i.test(parentAnswerType)) {
            // get selected options
            let selectedOptions = ((conditions[0] || [])[2] || '').split(',').filter(function (v) { return v.length > 0; }) || [];
            // get options
            let options = getOptions(parentAnswerType, parentRow.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // get checkbox name
                let name = parentRow.id + '-' + row.id;
                // append to input
                input.append(
                    '<input type="checkbox" name="' + name + '[]" id="' + name + '-' + i + '" value="' + value + '"/>' +
                    '<label for="' + name + '-' + i + '">' + label + '</label>'
                );
                // checked?
                if (selectedOptions.indexOf(value) >= 0) {
                    // for checkbox
                    input.children(':input[value="' + value + '"]').attr('checked', true);
                }
            }
        } else if (/^radio$/i.test(parentAnswerType)) {
            // get selected options
            let selectedOptions = ((conditions[0] || [])[2] || '').split(',').filter(function (v) { return v.length > 0; }) || [];
            // get options
            let options = getOptions(parentAnswerType, parentRow.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // get checkbox name
                let name = parentRow.id + '-' + row.id;
                // append to input
                input.append(
                    '<input type="checkbox" name="' + name + '[]" id="' + name + '-' + i + '" value="' + value + '"/>' +
                    '<label for="' + name + '-' + i + '">' + label + '</label>'
                );
                // checked?
                if (selectedOptions.indexOf(value) >= 0) {
                    // for checkbox
                    input.children(':input[value="' + value + '"]').attr('checked', true);
                }
            }
        }
        // set operator
        $('#editor > div[data-property-name="condition"] :input[name="condition[operator]"]').val((conditions[0] || [])[1] || '=');
        // set input value
        input.val((conditions[0] || [])[2] || '');
        // show related properties
        $('#editor > div[data-property-name="condition"]').show();
    };
    setCondition(value) {
        let data = this.data;
        console.log(" ************************ setCondition ************************");
        // get field id
        let id = $('#editor').attr('data-editing-field');
        // get dataIdx
        let dataIdx = -1;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                dataIdx = i;
                break;
            }
        }
        // get parent id
        let parentId = $('#editor > div[data-property-name="condition"] :input[name="condition[field]"]').val();
        // get parent idx
        let parentIdx = -1;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == parentId) {
                parentIdx = i;
                break;
            }
        }
        // get parent type
        let parentType = formatTypeSwitch(data[parentIdx].format);
        // get operator
        let operator = $('#editor > div[data-property-name="condition"] :input[name="condition[operator]"]').val();
        // for checkbox
        if (/^checkbox$/i.test(parentType)) {
            // get selected options
            let values = [], selectedOptions = $('#editor > div[data-property-name="condition"] [name="condition[value]"] :input:checked').get();
            // get values
            for (let i in selectedOptions) values.push(selectedOptions[i].value);
            // update value
            value = values.join(',');
        } else if (/^radio$/i.test(parentType)) {
            // get selected options
            let values = [], selectedOptions = $('#editor > div[data-property-name="condition"] [name="condition[value]"] :input:checked').get();
            // get values
            for (let i in selectedOptions) values.push(selectedOptions[i].value);
            // update value
            value = values.join(',');
        }
        // update condition
        data[dataIdx].condition = JSON.stringify([[parentId, operator, value]]);
        console.log("data[dataIdx]");
        console.log(data[dataIdx]);
        // reload field
        this.renderView(data[dataIdx]);//this.renderRow(data[dataIdx]);
        // reload condition
        this.loadCondition(id);
    };

    deleteData(fieldId) {
        // deleted any data, show Save button
        if (this.state.showSave == false) { this.setState({ showSave: true }); }
        // ================= REMOVE th DATA in data =================
        let data = this.data;
        console.log("fieldId");
        console.log(fieldId);
        for (let j in data) {
            if (data[j].id == fieldId) {
                data.splice(j, 1);
                break; //Stop this loop, we found it!
            }
        }
        console.log("data");
        console.log(data);
        // ================= REMOVE MIDDLE pnael node =================
        $('li.field[data-field-id=' + fieldId + ']').remove();
        // ================= REMOVE LEFT pnael node =================
        //check is parent
        let children = $('li.node[data-field-id=' + fieldId + ']').children('.childs').children('li');
        console.log("children");
        console.log(children);
        //==== if no children ====
        if (children == null) {
            $('li.node[data-field-id=' + fieldId + ']').remove();
        } else {
            //==== if have children ====
            // div = the node to remove
            var div = $('li.node[data-field-id=' + fieldId + ']');
            console.log("div");
            console.log(div);
            // the children that to clone
            console.log('div.children(.childs)');
            console.log(div.children('.childs'));
            var tmp = div.children('.childs').children('li').clone();
            console.log("tmp");
            console.log(tmp);
            // previous node of deleted node
            let previous = div.prev();
            // if previous is children of another node
            if (!previous.hasClass('node')) {
                previous = div.parent();
                div.remove();
                tmp.appendTo(previous);
            } else {
                // previous node is not children of another node
                div.remove();
                tmp.insertAfter(previous);
            }
            console.log("previous");
            console.log(previous);
            // remove the node

            // append to the prvious node of the removed node 

        }
    }
    // == clicked CANCEL button on right top bar ==
    cancelClick(showConfirm) {
        if (showConfirm == true) {
            this.setState({ showConfirm: true });
        } else {
            window.location.hash = "home/InputForm"
        }
    }
    // == clicked SAVE button on right top bar ==
    saveClick() {
        let data = this.data;
        //NOTES: Collect Data, all data auto-udated, except 1. Ckeditor, 2. order and 3.path, return to server with data and btnOrder
        // ================ 1. DATA: UPDATE CKEDITOR ================
        let textAreas = $(".ckeditor");
        //let textAreas = $("textarea");
        for (let i = 0; i < textAreas.length; i++) {
            let dataId = $(textAreas[i]).parents('.field[data-field-id]').attr('data-field-id');
            console.log("dataId");
            console.log(dataId);
            let input = $(textAreas[i]);
            let editor = $(textAreas[i]).ckeditorGet();
            console.log("editor.checkDirty()");
            console.log(editor.checkDirty());
            if (editor.checkDirty() == true) {
                for (let j in data) {
                    if (data[j].id == dataId) {
                        data[j].answer = input.val();
                        break; //Stop this loop, we found it!
                    }
                }
            }
        }
        // ================ 2. DATA: UPDATE ORDER  ================
        $('.page').each(function (pId) {
            $('.page[data-page-id=' + pId + '\]').children('section').each(function (secId) {
                $('.page[data-page-id=' + pId + '\]').children('section[data-section-id=' + secId + '\]').children('.content').children('.column').each(function (colId) {
                    $('.page[data-page-id=' + pId + '\]').children('section[data-section-id=' + secId + '\]').children('.content').children('.column[data-column-id=' + colId + '\]').children('ul.column li').each(function (order) {
                        let fieldId = $(this).attr('data-field-id');
                        for (let i = 0; i < data.length; i++) {
                            if (data[i].id == fieldId) {
                                data[i].order = pId + "-" + secId + "-" + colId + "-" + order;
                                break;
                            }
                        }
                    });
                });
            });
        });
        // ================ 3. DATA: UPDATE PATH  ================
        // loop function
        let loopFn = function (theValue, parentIds) {
            $(theValue).children('.childs').children('.node').each(function (grandIdx, grandValue) {
                let grandId = grandValue.getAttribute("data-field-id");
                for (let i = 0; i < data.length; i++) {
                    if (data[i].id == grandId) {
                        data[i].path = parentIds;
                        console.log("data[i].path in grand each");
                        console.log(data[i].path);
                        break;
                    }
                }
                let newParentIds = parentIds + "," + grandId;
                if ($(grandValue).children('.childs').children('.node').length > 0) {
                    loopFn(grandValue, newParentIds);
                }
            });
        }
        // set parent path
        $('#structure').children('.scroll-area').children('.ui-sortable').children('.node').each(function (idx, value) {
            // get data-field-id of the parent from the html tag (value)
            let parentId = value.getAttribute("data-field-id");
            console.log("value in structure each");
            console.log(value);
            console.log("parentId in structure each");
            console.log(parentId);
            // update path of the node
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == parentId) {
                    data[i].path = "";
                    break;
                }
            }

            // set children path
            $(value).children('.childs').children('.node').each(function (chdIdx, chdValue) {
                let cldId = chdValue.getAttribute("data-field-id");
                for (let i = 0; i < data.length; i++) {
                    if (data[i].id == cldId) {
                        data[i].path = parentId;
                        break;
                    }
                }
                // set all grand(*n) children path
                loopFn(chdValue, parentId + "," + cldId);
            });
        })
        // ================ DATA TO SERVER  ================
        //TO DO: send this.data, this.state.btnOrder to server 
        console.log(data);
        console.log(this.state.btnOrder);
        // if success alert
        this.setState({ showAlert: true });
    }
    backClick(e) {
        let oThis = this;
        $('#tabs .tab[data-page-id="' + (oThis.state.pageId - 1) + '"] a').trigger('click');
    }
    nextClick(e) {
        let oThis = this;
        $('#tabs .tab[data-page-id="' + (oThis.state.pageId + 1) + '"] a').trigger('click');
    }
    disableBackNext() {
        // disable back and next if necessary
        console.log("this.state.page in disableBackNext");
        console.log(this.state.pageId);
        // console.log("this.state.totalPages in disableBackNext");
        // console.log(this.state.totalPages);
        let totalPages = $('#pages').children('.page').length;
        console.log("totalPages in disableBackNext");
        console.log(totalPages);
        //let totalPages = this.state.totalPages;
        let pageId = this.state.pageId;
        if (pageId == 0) {
            $('#backBtn').addClass('disabled-button');
        } else {
            $('#backBtn').removeClass('disabled-button');
        }

        if (totalPages == pageId + 1) {
            $('#nextBtn').addClass('disabled-button');
        } else {
            $('#nextBtn').removeClass('disabled-button');
        }
    }
    upload(id) {
        console.log("document.querySelector('input[type=file]')");
        console.log(document.querySelector('input[type=file]'));
        console.log("this.state.fileName");
        console.log(this.state.fileName);
        let photos = null;
        if (document.querySelector('input[type=file]')) {
            photos = document.querySelector('input[type=file]').files;
            let fileName = Object.assign({}, this.state.fileName);
            let name = ""
            for (let i = 0; i < photos.length; i++) {
                fileName[id] += photos[i].name;
            }
            this.setState({ fileName: fileName })
        }
        console.log(" ======= photos ======= ");
        console.log(photos);
        return false;
    }
    // ------------------------------- /handlers ------------------------------- /
    // ------------------------------- interface ------------------------------- //
    render() {
        let oThis = this;
        console.log("this.state in render of input_form_edit");
        console.log(this.state);
        return (
            <div id='input_form_edit' >
                {this.state.showSave == true ?
                    <div className='input-head-container clearfix'>
                        <a className='right cancel-btn' onClick={oThis.cancelClick.bind(oThis, true)}>Cancel</a>
                        <button className='right save-btn' onClick={oThis.saveClick.bind(oThis)}>Save</button>
                        <span className='right'>Save changes?</span>
                    </div> :
                    <div className='input-head-container clearfix'>
                        <a className='right' onClick={oThis.cancelClick.bind(oThis, false)}>{i18n.back_to_menu}</a>
                    </div>
                }
                <div className='input-body-container'>
                    <div className='input-form-body' id='input-form-edit'>
                        {/* -------------------------- left side -------------------------- */}
                        <div id="structure">
                            <div className='widget-title'>
                                <span className='title'>Form Structure Order</span>
                            </div>
                            <div className='scroll-area'>
                                <ul></ul>
                            </div>
                        </div>
                        {/* -------------------------- middle -------------------------- */}
                        <div id="viewer" className='scroll-area'>
                            {/*<div className='row'>
                                <h6>{this.state.new == true ? i18nCommon.new : i18nCommon.edit}&nbsp;{oThis.state.isInbound == 'true' ? "Inbound" : "Outbound"}&nbsp;{i18n.campaign}</h6>
                            </div>*/}
                            <div className='row'>
                                <div className='form-head-controls'>
                                    <div className='campaign-title'>
                                        <h3>{i18n.campaign_name_label} {oThis.state.campaignName}</h3>
                                    </div>
                                    <div className='page-controls right'>
                                        <button id='backBtn' className='disabled-button' onClick={this.backClick.bind(oThis)}>Back</button>
                                        <span id="tabs">
                                        </span>
                                        <button className='right' id='nextBtn' onClick={this.nextClick.bind(oThis)}>Next</button>
                                    </div>
                                </div>
                            </div>
                            <div id="pages" className='pages' />
                        </div>
                        {/* -------------------------- right side -------------------------- */}
                        <div id="components">
                            <div className='editor-container'>
                                <div className='widget-title'>
                                    <span className='title'>Context Information</span>
                                </div>
                                <div className='scroll-area'>
                                    <div id="editor">
                                        <div data-property-name="label">
                                            <label className='name'>Name</label>
                                            <input name="label" type="text" maxLength="50" />
                                        </div>
                                        <div data-property-name="condition">
                                            <label className='name'>Display when</label>
                                            <select name="condition[field]" disabled></select>
                                            <select name="condition[operator]">
                                                <option value="=">=</option>
                                                <option value="&gt;">&gt;</option>
                                                <option value="&gt;=">&gt;=</option>
                                                <option value="&lt;">&lt;</option>
                                                <option value="&lt;=">&lt;=</option>
                                            </select>
                                            <select name="condition[value]">
                                                <option value="">-- Please Select</option>
                                            </select>
                                        </div>
                                        <div data-property-name="mandatory">
                                            <label className='name' htmlFor='config-mandatory'>Mandatory</label>
                                            <input name="mandatory" id='config-mandatory' type="checkbox" />
                                        </div>
                                        <div data-property-name="format">
                                            <label className='name'>Format</label>
                                            <select className='dropdown' name="format">
                                                <option value=".*">Any</option>
                                                <option value="^(&?[a-z0-9A-Z]+=.*)*$">Checkbox</option>
                                                <option value="^([0-9]{4})-([0-9]{2})-([0-9]{2})$">Date</option>
                                                <option value="^([0-9]{4})-([0-9]{2})-([0-9]{2})&nbsp;([0-9]{2}):([0-9]{2}):([0-9]{2})$">Datetime</option>
                                                <option value='^()$'>Drop Down</option>
                                                <option value=".(jpe?g|png|gif)$">File</option>
                                                <option value='/[A-Za-z\s-\/!%&amp;()+=\&#39;&quot;~,.]/'>Header</option>
                                                <option value="^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$">HTML</option>
                                                <option value="^.+$">Label</option>
                                                <option value="^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?$">Link</option>
                                                <option value="^[0-9.]+$">Number</option>
                                                <option value="^(-?())*$">Radio</option>
                                            </select>
                                        </div>
                                        <div data-property-name="options" className='options'>
                                            <label className='name'>Options</label>
                                            <div>
                                                <input name="options[key][]" type="text" placeholder="option" maxLength="50" />
                                                <input name="options[value][]" type="text" placeholder="label" maxLength="50" />
                                            </div>
                                        </div>
                                        <div data-property-name="default">
                                            <label className='name'>Default</label>
                                            <input name="default" type="text" maxLength="50" />
                                        </div>
                                        <div data-property-name='config-section'>
                                            <div className='row'><label htmlFor='config-section'>{i18n.hide_this_section}</label>
                                                {/* <input name="config-section" id='config-section' type="checkbox"
                                                    onChange={oThis.cfgSecCheckChange.bind(oThis)} /></div> */}
                                                <input name="config-section" id='config-section' type="checkbox" checked={oThis.state.cfgSecCheck}
                                                    onChange={oThis.cfgSecChange.bind(oThis)} /></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='browser-container'>
                                <div className='widget-title'>
                                    <span className='title'>Form Elements</span>
                                </div>
                                <div className='scroll-area'>
                                    <div id="browser" className='row'>
                                        <div data-field-type="dropdown" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-cursor-menu-30 i-icon'></span>
                                                <span>Drop Down</span>
                                            </div>
                                        </div>
                                        <div data-field-type="label" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-tag-30 i-icon'></span>
                                                <span>Label</span>
                                            </div>
                                        </div>
                                        <div data-field-type="header" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-dev-30 i-icon'></span>
                                                <span>Header</span>
                                            </div>
                                        </div>
                                        <div data-field-type="text" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-caps-small-30 i-icon'></span>
                                                <span>Text</span>
                                            </div>
                                        </div>
                                        <div data-field-type="number" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-list-numbers-30 i-icon'></span>
                                                <span>Number</span>
                                            </div>
                                        </div>
                                        <div data-field-type="radio" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-ic_radio_button_checked_24px i-icon'></span>
                                                <span>Radio</span>
                                            </div>
                                        </div>
                                        <div data-field-type="date" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-calendar-grid-30 i-icon'></span>
                                                <span>Date</span>
                                            </div>
                                        </div>
                                        <div data-field-type="file" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-attach-30 i-icon'></span>
                                                <span>File</span>
                                            </div>
                                        </div>
                                        <div data-field-type="checkbox" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-check-square-30 i-icon'></span>
                                                <span>Checkbox</span>
                                            </div>
                                        </div>

                                        <div data-field-type="link" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-link-30 i-icon'></span>
                                                <span>Link</span>
                                            </div>
                                        </div>
                                        <div data-field-type="html" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-dev-30 i-icon'></span>
                                                <span>HTML</span>
                                            </div>
                                        </div>
                                        <div data-field-type="datetime" className='small-12 medium-6 columns'>
                                            <div className='toolbar-item'>
                                                <span className='icon-time-clock-30 i-icon'></span>
                                                <span>Datetime</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <AlertBox />
                    </div>
                </div>
                <AlertBox alertMsg="Form Submitted" showInfo={true} showAlert={this.state.showAlert == false ? false : true} alertClick={() => { window.location.hash = "home/InputForm"; oThis.setState({ showAlert: false }) }}
                    confirmMsg={i18n.are_you_sure_to_return}
                    showConfirm={oThis.state.showConfirm}
                    confirmClick={() => { oThis.setState({ showConfirm: false }); window.location.hash = "home/InputForm"; }} cancelClick={() => oThis.setState({ showConfirm: false })}
                />
            </div>
        );
    }
    // ------------------------------- delegate ------------------------------- //
    componentWillUnmount() {
        //NOTES: if not destroy all CKEDITOR instances, when refreshing the page will have error P.S. warning is still here by ckeditor
        let textAreas = $("textarea");
        for (let i = 0; i < textAreas.length; i++) {
            let input = $(textAreas[i]);
            let editor = $(textAreas[i]).ckeditorGet();
            let editorName = editor.name;
            if (typeof CKEDITOR != 'undefined') { //NOTES: CKEDITOR is Object now
                let cki = CKEDITOR.instances[editorName];
                cki ? cki.destroy() : null;
            }
        }

        $('#pages').find('.page').remove();
        $('#tabs').find('.tab').remove();
        // ------------------------------- /delegate ------------------------------- //
    }
}
