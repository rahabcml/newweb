/*************************************************************************************
* Notes
* Name: input_form_home.jsx
* Class: InputFormMain
* Details: Main Page of Input Form (including its pop-up to creat or copy input form)
* Properties: 
**************************************************************************************/
import React, { Component } from 'react'
import $ from 'jquery';
import foundation from 'foundation-sites';
import i18nCommon from '../../common/i18n.jsx';
import ITable from '../../common/ITable.jsx';
import AlertBox from '../../common/AlertBox.jsx';
import ICombo from '../../common/ICombo.jsx';
let i18n = i18nCommon.input_form_variable;

export default class InputFormMain extends Component {
  // ------------------------------- delegate ------------------------------- //
  constructor(props) {
    super(props);
    this.state = {
      showCreatePopup: false,
      isInboundToCreate: true,
      popupName: "",
      popupCombo: "",
      showAlert: false
    }
  }

  tblRowSelected(data, isInbound) {
    //NOTES: Direct to input form edit page now
    let name = encodeURIComponent(data);
    window.location.hash = "home/InputFormEdit?isInbound=" + isInbound + "&name=" + name;
  }

  createColumn(isInbound) {
    return [
      { id: [0], name: i18n.input_form_title },
      {
        id: "empty", name: i18n.action, type: "icon", className: 'icon-edit-16', onClick: (index, data) => {
          this.tblRowSelected(data, isInbound);
          // let name = encodeURIComponent(data);
          // window.location.hash = "home/InputFormEdit?isInbound=" + isInbound + "&name=" + name;
        }
      }
    ]
  }

  componentWillMount() {
    //TO DO: Get the tables data
    this.jsonData = {
      inbound: ["Summer Promotion Form", "50% off Discount Form", "Dec-Fec Campaign Form", "Premier Event Campaign Form"],
      outbound: ["Winter Promotion Form", "30% off Discount Form", "March Campaign Form", "Premier Night Event Campaign Form"]
    };
  }

  componentDidMount() {
    $('#input-tabs').foundation();
    $('#create-form-tabs').foundation();
  }
  // ------------------------------- /delegate ------------------------------- //
  // ------------------------------- handlers ------------------------------- //
  popCloce() {
    //NOTES: reset pop-up value, and close the pop-up
    this.setState({ popupName: "", showCreatePopup: false, selectedComobo: "" });
  }
  popBuildFormClick() {
    if (this.state.popupName == "") {
      this.setState({ showAlert: true })
      //alert(i18n.please_enter_name);
      return
    };
    let popupName = this.state.popupName;
    let name = encodeURIComponent(this.state.popupName);
    let popupCombo = encodeURIComponent(this.state.popupCombo);
    let url = "home/InputFormEdit?isInbound=" + this.state.isInboundToCreate + "&name=" + name + "&copy=" + popupCombo + "&new=true";
    console.log(url);
    window.location.hash = url;
  }
  // ------------------------------- /handlers ------------------------------- /
  // ------------------------------- interface ------------------------------- //
  render() {
    let oThis = this;
    let showCreatePopup = this.state.showCreatePopup == false ? 'none' : 'block';
    console.log("oThis in render");
    console.log(oThis);
    return (
      <div className="body-content">
        <div className="tabs-container-2 margin-top">
          <div className="container">
            <ul className="tabs" data-tabs id="input-tabs">
              <li className="tabs-title is-active">
                <a href="#panel1" aria-selected="true">{i18n.inbound_campaigns}</a>
              </li>
              <li className="tabs-title">
                <a data-tabs-target="panel2" href="#panel2">{i18n.outbound_campaigns}</a>
              </li>
            </ul>
          </div>
        </div>

        <div className='container'>
          <div className="tabs-content-2" data-tabs-content="input-tabs">
            <div className="tabs-panel is-active" id="panel1">
              <h5 className='left'>{i18n.manage_inbound_forms}</h5>
              <button className='right' onClick={() => oThis.setState({ showCreatePopup: !oThis.state.showCreatePopup, isInboundToCreate: true })}><div className='icon-add-16 i-icon i-icon-left' />{i18n.create_a_form}</button>
              <ITable tableName="inbound-input-form" data={oThis.jsonData.inbound} columns={oThis.createColumn(true)} searching={false} columnDefs={[{
                "targets": -1,
                className: 'noVis', //NOTES: no column visibility
                "orderable": false
              }]} onDoubleClick={(index, data) => { oThis.tblRowSelected(data, true) }} />
            </div>

            <div className="tabs-panel" id="panel2">
              <h5 className='left'>{i18n.manage_outbound_forms}</h5>
              <button className='right' onClick={() => oThis.setState({ showCreatePopup: !oThis.state.showCreatePopup, isInboundToCreate: false })}><div className='icon-add-16 i-icon i-icon-left' />{i18n.create_a_form}</button>
              <ITable tableName="outbound-input-form" data={oThis.jsonData.outbound} columns={oThis.createColumn(false)} searching={false} columnDefs={[{
                "targets": -1,
                className: 'noVis', //NOTES: no column visibility
                "orderable": false
              }]} onDoubleClick={(index, data) => { oThis.tblRowSelected(data, false) }} />
            </div>
          </div>
        </div>

        {/* Create Form Pop-up --- because have tabs inside, so need to render now */}
        <div style={{ display: showCreatePopup }}>
          <div className='i-popup'>
            <div className='pop-up-content-small'>

              <div className='pop-header'>
                <h3 className='pop-title'>{i18n.create_a_new_form}</h3>
                <button className='pop-exit-btn right' onClick={this.popCloce.bind(oThis)}>
                  <div className="icon-simple-remove" />
                </button>
              </div>

              <div className='pop-body custom-scroll'>
                <div className='pop-content-divide'>
                  <label>{i18n.form_name_label}</label>
                  <input type='text' value={oThis.state.popupName} placeholder={i18n.what_shell_we_call} onChange={event => {
                    this.setState({ popupName: event.target.value });
                  }} />
                </div>
                <div className='pop-content-divide'>
                  <label>{i18n.method_of_create}</label>

                  <ul className="option-tabs" data-tabs id="create-form-tabs">
                    <li className="tabs-title is-active"><a href="#create-panel1" aria-selected="true"><span className="icon-add-stroke-24" /> {i18n.create_a_blank}</a></li>
                    <li className="tabs-title "><a data-tabs-target="create-panel2" href="#create-panel2"><span className="icon-copy-stroke-24" /> {i18n.copy_existing_form}</a></li>
                  </ul>
                  <div className='clear'></div>
                </div>

                <div className="tabs-content" data-tabs-content="create-form-tabs">
                  <div className="tabs-panel is-active" id="create-panel1">
                  </div>
                  <div className="tabs-panel" id="create-panel2">
                    <label>{i18n.choose_existing_form}</label>
                    <ICombo displayField={0} valueField={0} options={oThis.state.isInboundToCreate ? this.jsonData.inbound : this.jsonData.outbound} value={oThis.state.popupCombo} onChange={
                      (o) => {
                        if (o && o.value != null) {
                          oThis.setState({ popupCombo: o.value });
                        } else {
                          oThis.setState({ popupCombo: "" });
                        }
                      }
                    } />
                  </div>

                </div>

                <div className="bottom-nav clearfix">
                  <button
                    className="btn-half"
                    onClick={this.popCloce.bind(oThis)}>{i18nCommon.cancel}</button>
                  <button
                    className="btn-half"
                    onClick={oThis.popBuildFormClick.bind(oThis)}>
                    {i18n.build_form}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <AlertBox alertMsg={i18n.please_enter_name} showAlert={this.state.showAlert} alertClick={() => oThis.setState({ showAlert: false })} />
      </div>
    );
  }
  // -------------------------------- /interface ------------------------------- //
}
