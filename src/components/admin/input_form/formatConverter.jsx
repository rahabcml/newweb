/*******************************************
* Notes
* options and rules
* rules, use options, add /^ in front, if have [ + . * \ $ need to add \ in front
********************************************/

let formatConverter = {
    formats: {
        number: {
            options: ['^[0-9.]+$'],
            rules: /^\^\[0-9\.\]\+\$$/,
            html: '<input type="number" placeholder="Number" value="{VALUE}" />',
            htmlD: '<input type="number" placeholder="Number" value="{VALUE}" disabled/>'
        },
        dropdown: {
            options: ['^[a-z0-9A-Z\-\/=$ ]+=.*$', '^(\|?[a-z0-9\-\/=$ ]+(:.*)?)+$'],
            rules: [/^\^\[a-z0-9A-Z\-\/=$ ]\+=\.\*\$$/, /^\^\([a-z0-9\-\/=$ ]+(:.*)?(\|[a-z0-9\-\/=$ ]+(:.*)?)*\)\$$/i],
            html: '<select class="margin-bottom-0"><option value="" disabled selected>Select...</option></select>',
            htmlD: '<select disabled class="margin-bottom-0"><option value="" disabled selected>Select...</option></select>'
        },
        checkbox: {
            options: ['^(&?[a-z0-9A-Z ]+=.*)*$'],
            rules: /^((\^\(&\?\[a-z0-9A-Z\ ]\+=\.\*\)\*\$)|(\^\(,\?\([a-z0-9 ]+(:.*)?(\|[a-z0-9 ]+(:.*)?)*\)\)\*\$))$/i,
            html: '<div class="checkbox clearfix"></div>',
            htmlD: '<div class="checkbox clearfix" disabled></div>'
        },
        radio: {
            options: ['^(-?())*$'],
            rules: /^\^\(-\?\([\w:,\s|]+\)\)\*\$/,
            html: '<div class="radio clearfix"></div>',
            htmlD: '<div class="radio clearfix" disabled></div>'
        },
        datetime: {
            options: ['^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$'],
            rules: /^\^\(\[0-9\]\{4\}\)-\(\[0-9\]\{2\}\)-\(\[0-9\]\{2\}\) \(\[0-9\]\{2\}\):\(\[0-9\]\{2\}\):\(\[0-9\]\{2\}\)\$$/,
            html: '<input class="margin-bottom-0" type="datetime-local" placeholder="DateTime" value="{VALUE}" />',
            htmlD: '<input class="margin-bottom-0" type="datetime-local" placeholder="DateTime" value="{VALUE}" disabled/>',
        },
        date: {
            options: ['^([0-9]{4})-([0-9]{2})-([0-9]{2})$'],
            rules: /^\^\(\[0-9\]\{4\}\)-\(\[0-9\]\{2\}\)-\(\[0-9\]\{2\}\)\$$/,
            html: '<input class="margin-bottom-0" type="date" placeholder="Date" value="{VALUE}"/>',
            htmlD: '<input class="margin-bottom-0" type="date" placeholder="Date" value="{VALUE}" disabled/>'
        },
        link: {
            options: ['^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%:\/~+#-]*[\w@?^=%\/~+#-])?$'],
            //   ['^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?$'],
            rules: /^\^\(http|ftp|https\)\:\/\/\[\w-]+(\.[\w-]+)+([\w.,@?^=%:\/~+#-]*[\w@?^=%\/~+#-])?$/i,
            // /^\^\(http|ftp|https\)\:\/\/\[\w-]+(\.[\w-]+)+([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?$/i,
            html: '<a class="link" href="{VALUE}" target="_blank">Click Here</a>',
            htmlD: '<a class="link" href="{VALUE}" target="_blank" disabled>Click Here</a>'
        },
        file: {
            options: ['.(jpe?g|png|gif)$'],
            rules: /^\.\(jpe\?g\|png\|gif\)\$$/i,
            html: '<input type="file" name="File" accept="image/*" multiple/>',
            htmlD: '<input type="file" name="File" disabled/>'
        },
        html: {
            options: ['^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$'],
            rules: /^\^<\[a-zA-Z0-9]\+>\.\*<\\\/\[a-zA-Z0-9]\+>\$/g,
            html: '<textarea class="ckeditor">{VALUE}</textarea>',
            htmlD: '<textarea disabled>{VALUE}</textarea>',
        },
        label: {
            options: ['^.+$'],
            rules: /^\^\.\+\$$/,
            html: '<div class="label-field">{VALUE}</div>',
            htmlD: '<div class="label-field" disabled>{VALUE}</div>'
        },
        text: {
            options: ['^.*$'],
            rules: /^\.\*$/,
            html: '<input type="text" placeholder="Text" value="{VALUE}" />',
            htmlD: '<input type="text" placeholder="Text" value="{VALUE}" disabled/>'
        },
        header: {
            options: ['A-Za-z\s-/!%&()+=\'"~,.'],
            rules: /[A-Za-z\s-/!%&()+=\'"~,.]/,
            html: '<p class="p-margin"><strong>{VALUE}</strong></p><hr class="hr-margin" />',
            htmlD: '<p class="p-margin"><strong>{VALUE}</strong></p><hr class="hr-margin" />'
        }
    },
    getFormat: function (type) { //by type get Format, used when create new record
        console.log("type in getFormat");
        console.log(type);
        for (let alias in formatConverter.formats) {
            if (alias == type) {
                return formatConverter.formats[alias].options[0];
            }
        }
        return ""
    },
    getType: function (format) { //by format get the type, used in loadConfig to set the format combo-box
        console.log("format in getType");
        console.log(format);
        for (let alias in formatConverter.formats) {
            if (formatConverter.formats[alias].options[0] == format) {
                return alias; // the type to return
            }
        }
        // format for checkbox, dropdow, radio need checking below
        // empty kind
        if (format == '^()$') {
            return 'dropdown';
        } else if (format == '^(&?[a-z0-9A-Z]+=.*)*$') {
            return 'checkbox';
        } else if (format == '^(-?())*$') {
            return 'radio';
        }
        let typeToCheck = ["checkbox", "radio", "dropdown"];
        for (let i = 0; i < typeToCheck.length; i++) {
            let theType = typeToCheck[i];
            console.log("theType");
            console.log(theType);
            let rules = formatConverter.formats[theType].rules;
            // only drop down rules.length > 0
            if (rules.length > 0) {
                for (let j in rules) {
                    if (rules[j].test(format) || rules[j] == format) {
                        return theType;
                    }
                }
            } else {
                // radio or checkbox
                console.log("rules.test(format)");
                console.log(rules.test(format));
                if (rules.test(format)) {
                    return theType;
                }
            }
            // for checkbox
            // if (rules.test(format)) {
            //     return theType;
            // }
        }
    },
    identify: function (value) {
        // set default format
        let type = 'text';
        // value for blank dop down
        console.log("value in identify");
        console.log(value);
        if (value == '^()$') {
            type = "dropdown";
            return type;
        } else if (value == '^<[a-zA-Z0-9]+>.*</[a-zA-Z0-9]+>$') {
            type = 'html';
            return type;
        } else if (value == '^.*$') {
            type = 'text';
            return type;
        }

        if (typeof formatConverter.formats[value] != 'undefined') {
            // set format as type
            type = formatConverter.formats[value].rules;
            return type;
            // get type
        } else {
            // check formats
            for (let alias in formatConverter.formats) {
                // setup format container
                let rules = formatConverter.formats[alias].rules;
                // string format?
                if (!Array.isArray(rules)) {
                    // convert to array
                    rules = [rules];
                }
                // for new row, format must be the same (just checkbox and dropdown will need further checking)
                if (rules == value) {
                    // set type
                    type = alias;
                    return type;
                }
                // format for checkbox and dropdow need checking below
                for (let i in rules) {
                    // matches
                    if (rules[i].test(value) || rules[i] == value) {
                        // set type
                        type = alias;
                        return type;
                    }
                }
            }
        }

    },
    render: function (type, disabled) {
        // invalid type, is it a format?
        if (typeof formatConverter.formats[type] == "undefined") {
            // check type
            type = formatConverter.identify(type);
        }
        // return the template, (default: text)
        if (disabled == true) {
            return formatConverter.formats[type || 'text'].htmlD;
        }
        return formatConverter.formats[type || 'text'].html;
    }
};

export default formatConverter