/*************************************************************************************************
* Notes
* Name: input_form.jsx
* Class: InputForm
* Details: Used in Contact History's call type & Used in Outbound Call page
* Data (except id, all are strings): 
*        label: label to be shown on left of the field
*        format:
*                Link: '^(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?$'
*                Combo: '^(combo1|combo2)$'
*                Check-box: '^(,?(female:F|male:M))*$' or '^(,?(Female|Male)*$'
*                radio: '^(-?(radio1:Radio 1|radio2:Radio 2))*$' or '^(-?(Radio 1|Radio 2))*$'
*                Date: '^([0-9]{4})-([0-9]{2})-([0-9]{2})$'
*                Date Time: '^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$'
*                HTML: '^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$'
*                Label - any: '/^\.\*$/'
*                Label - any: '^[0-9.]+$'
*                File: '.(jpe?g|png|gif)$'
*                Header: '/[A-Za-z\s-\/!%&()+=\'"~,.]/'
*        default: 
*                If answer is empty string, the deafult will be the answer
*        answer: 
*                Link: it is the hyperlink 
*                Combo: the selection. e.g. 'combo2'
*                Check-box: checked box(es). e.g. 'Opt1Default,Opt2Default'
*                HTML: default value of in text area
*                Label: default value in text field
*        condition: The field will only be shown when condition matched. The path have to have condition's id
*                rules : ["id","conditioner","answer"] <= one or more in '[]' seperated by comma(,)
*                e.g.'[["1","=","combo2"],["3","=","Opt2Default"]]'
*        order: 'Page-UpDown-(LeftRight/column-id)-order'
*                UpDown can only be 0 or 1. 0: up, 1: down
*                LeftRight can only be 0 or 1. 0: left, 1: right 
*        mandatory: Bollean.
* Properties: 
*        page: true == outbound call, false/null == contact history
*              in outbound call all tags with id will have I prefix, contact history don't
*              in outbound call no campaign name(already stated in other place), contact history have
*************************************************************************************************/
import React, { Component } from 'react'
import formatConverter from './formatConverter.jsx';
import '../../../../styles/inputForm.css'
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.input_form_variable;

let formatTypeSwitch = function (format) {
    return formatConverter.identify(format);
};

let getOptions = function (type, format) {
    let options = [];
    // get Drop Down option
    if (/^dropdown$/i.test(type)) { //NTOES: /i stands for ignore case in the given string. Usually referred to as case-insensitive as pointed out in the comment.
        // get options
        if (Array.isArray(format) == true) {
            for (let i = 0; i < format.length; i++) {
                if (format[i] == String(/^\^\[a-z0-9A-Z\-\/ ]\+=\.\*\$$/) || format[i] == String(
                    /^\^\([a-z0-9\-\/ ]+(:.*)?(\|[a-z0-9\-\/ ]+(:.*)?)*\)\$$/i)) {
                    console.log("Empty STRING");
                    options.push("");
                } else {
                    options.push(/^\^\[a-z0-9A-Z\]\+=\.\*\$$/i.test(format) ? [] : (format.substr(2, format.length - 4).split('|') || []));
                }
            }
        } else {
            options = /^\^\[a-z0-9A-Z\]\+=\.\*\$$/i.test(format) ? [] : (format.substr(2, format.length - 4).split('|') || []);
        }
        // get Checkbox option
    } else if (/^checkbox$/i.test(type)) {
        // get options
        if (format == String(/^((\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$)|(\^\(,\?\([a-z0-9]+(:.*)?(\|[a-z0-9]+(:.*)?)*\)\)\*\$))$/i)) {
            options = ["label1:"];
        } else {
            options = /^\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$$/i.test(format) ? [] : (format.substr(5, format.length - 9).split('|') || []);
        }
    }
    // get Radio option
    else if (/^radio$/i.test(type)) {
        // get options
        if (format == String(/^((\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$)|(\^\(,\?\([a-z0-9]+(:.*)?(\|[a-z0-9]+(:.*)?)*\)\)\*\$))$/i)) {
            options = ["label1:"];
        } else {
            console.log("format of radio");
            console.log(format);
            console.log("format substring of radio");
            console.log(format.substr(5, format.length - 9));
            options = /^\^\(&\?\[a-z0-9A-Z\]\+=\.\*\)\*\$$/i.test(format) ? [] : (format.substr(5, format.length - 9).split('|') || []);
        }
        console.log("options for radio");
        console.log(options);
    }
    return options.filter(function (v) { return v.length > 0; });
};

export default class InputForm extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        super(props);
        console.log("props in input form");
        console.log(props);
        let ckeditorActive = props.ckeditorActive ? props.ckeditorActive == true ? true : false : false;
        this.state = {
            recId: props.recId,
            details: {},
            data: [],
            btnOrder: {},
            pageId: 0,
            totalPages: 1,
            readOnly: props.readOnly == false ? false : true || true,
            remark: "",
            to: this.props.to || "",
            ckeditorActive: ckeditorActive,
            hideSecObj: {}
        }
        this.fromName = sessionStorage.getItem("company_name") || "";
        this.didMount = false;
        this.switchNo = 0; //switch is will not show, just for mark down 2 cols or not
        console.log(" props.readOnly");
        console.log(props.readOnly);
        console.log(" props.readOnly || true");
        console.log(props.readOnly || true);
        console.log("this.state.readOnly in construction input form");
        console.log(this.state.readOnly);
    }

    componentWillMount() {
        this.reloadRecords(true);
    }

    componentWillReceiveProps(nextProps) {
        console.log("nextProps in inputform");
        console.log(nextProps);
        let toUpdate = this.state.to != nextProps.to ? true : false;
        this.setState({
            recId: nextProps.recId,
            details: {},
            data: [],
            btnOrder: {},
            pageId: 0,
            totalPages: 1,
            readOnly: nextProps.readOnly == false ? false : true || true,
            remark: "",
            from: nextProps.from || "",
            to: nextProps.to || "",
            fileName: {}
        }, () => { if (toUpdate == true) { this.forceUpdate() } });
    }

    componentDidMount() {
        // if (this.didMount == false) {
        //     this.reloadPage();
        //     this.didMount = true; // set didMount true make initial will not render again
        // }
    }
    // ------------------------------- handlers ------------------------------- /
    reloadPage() {
        let compareOrder = function (tmpData) {
            tmpData.sort(function (a, b) { //warning: 10 will be prior to 2 in localeCompare, so cannot just sort by localCompare
                const ao = a.order.split('-');
                const bo = b.order.split('-');
                if ((Number(ao[0])) > Number(bo[0])) {
                    return 1
                } else if ((Number(ao[0])) < Number(bo[0])) {
                    return -1
                } else if ((Number(ao[1])) > Number(bo[1])) {
                    return 1
                } else if ((Number(ao[1])) < Number(bo[1])) {
                    return -1
                } else if ((Number(ao[2])) > Number(bo[2])) {
                    return 1
                } else if ((Number(ao[2])) < Number(bo[2])) {
                    return -1
                } else if ((Number(ao[3])) > Number(bo[3])) {
                    return 1
                } else if ((Number(ao[3])) < Number(bo[3])) {
                    return -1
                } else {
                    return 0
                }
            });
            return tmpData
        }
        let structureData = this.data.slice();
        //let structureData = this.state.data.slice();
        structureData = compareOrder(structureData);
        for (let j in structureData) {
            this.renderView(structureData[j]);
        }
        this.addRemarkBtn();
        this.disableBackNext();
    }
    reloadRecords(firstLoad) {
        //TO DO: get form data by recId
        console.log("this.state.recId");
        console.log(this.state.recId);

        if (firstLoad != true) {
            if (this.props.page == true) {
                $('#Ipages').find('.page').remove();
                $('#Itabs').find('.tab').remove();
            } else {
                $('#pages').find('.page').remove();
                $('#tabs').find('.tab').remove();
            }
            // $('#pages').find('.page').remove();
            // $('#tabs').find('.tab').remove();
        }
        console.log("get data from server");
        // details: Outbound: NO NEED to get campaign name (this.props.cname) ; Contact history: NEED to get campaign name
        let data = [], details = { answer: {} };
        let btnOrder = { 0: 0, 1: 8 }
        let hideSecObj = { 1: [9] }; // get from server
        // New Customer
        // in backend make all the input field become label field: /^\.\*$/ => /^\^\.\+\$$/
        if (this.state.recId == -1) {
            data = [
                {
                    id: 1,
                    format: '^(Single|Married|Divorced|Widowed)$',
                    label: 'Marital status',
                    default: 'Last Name default',
                    answer: 'Single',
                    condition: '',
                    path: '',
                    order: '0-0-0-2',
                    mandatory: true
                }
            ]
        } else {
            details = {
                fm: "33456780", to: "62345676",
                cname: "Christmas Promotion",
                answer: { 1: "Not Reach Customer" },
                v0: 'linkHere', v1s: 10, v2s: 359, total: 369, v1: 'AnotherLinkHere', rno: "3345", agent: "Peter Chan", remark: "VIP, need to pay more attention!"
            };
            data = [
                {
                    id: 1,
                    format: '^(Not Reach Customer|Presented - Consider|Presented - Reject|Presented - Success)$',
                    label: 'Call Result 1',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-0-0-1',
                    mandatory: true
                },
                {
                    id: 2,
                    format: '^(No Answer|Not Here|Voice Mail)$',
                    label: 'Call Result 2',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Not Reach Customer"]]',
                    path: '1',
                    order: '0-0-0-2',
                    mandatory: true
                },
                {
                    id: 3,
                    format: '^(Follow Up - After Present|Consider Follow Up|Not in Hong Kong)$',
                    label: 'Call Result 2',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Consider"]]',
                    path: '1',
                    order: '0-0-0-3',
                    mandatory: true
                },
                {
                    id: 4,
                    format: '^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$',
                    label: 'Call Back Date Time',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Consider"]]',
                    path: '1',
                    order: '0-0-0-4',
                    mandatory: true
                },
                {
                    id: 5,
                    format: '^(Already Have Insurance|Coverage Not Enough|No Need / No Interest)$',
                    label: 'Call Result 2',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Reject"]]',
                    path: '1',
                    order: '0-0-0-5',
                    mandatory: true
                },
                {
                    id: 6,
                    format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                    label: '',
                    default: 'Customer Information',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-1-0-0',
                    mandatory: true
                },
                {
                    id: 7,
                    format: '^.+$',
                    label: 'Item_No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-1',
                    mandatory: false
                },
                {
                    id: 8,
                    format: '^.+$',
                    label: 'Batch Prefix',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-2',
                    mandatory: false
                },
                {
                    id: 9,
                    format: '^.+$',
                    label: 'Holder Name',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-3',
                    mandatory: false
                },
                {
                    id: 10,
                    format: '^.+$',
                    label: 'Address1',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-4',
                    mandatory: false
                },
                {
                    id: 11,
                    format: '^.+$',
                    label: 'Address 2',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-5',
                    mandatory: false
                },
                {
                    id: 12,
                    format: '^.+$',
                    label: 'Address 3',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-6',
                    mandatory: false
                },
                {
                    id: 13,
                    format: '^.+$',
                    label: 'Address 4',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-7',
                    mandatory: false
                },
                {
                    id: 14,
                    format: '^.+$',
                    label: 'Email',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-8',
                    mandatory: false
                },
                {
                    id: 15,
                    format: '^.+$',
                    label: 'Phone1 No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-9',
                    mandatory: false
                },
                {
                    id: 16,
                    format: '^.+$',
                    label: 'Phone2 No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-10',
                    mandatory: false
                },
                {
                    id: 17,
                    format: '^.+$',
                    label: 'Mobile No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-11',
                    mandatory: false
                },
                {
                    id: 18,
                    format: '^.+$',
                    label: 'HKID',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-12',
                    mandatory: false
                },
                {
                    id: 19,
                    format: '^.+$',
                    label: 'DOB',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-13',
                    mandatory: false
                },
                {
                    id: 20,
                    format: '^.+$',
                    label: 'Gender',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-14',
                    mandatory: false
                },
                {
                    id: 21,
                    format: '^.+$',
                    label: 'Business Line',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-15',
                    mandatory: false
                },
                {
                    id: 22,
                    format: '^.+$',
                    label: 'Product Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-16',
                    mandatory: false
                },
                {
                    id: 23,
                    format: '^.+$',
                    label: 'Product Desc',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-17',
                    mandatory: false
                },
                {
                    id: 24,
                    format: '^.+$',
                    label: 'Agent No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-18',
                    mandatory: false
                },
                {
                    id: 25,
                    format: '^.+$',
                    label: 'Agent Group',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-19',
                    mandatory: false
                },
                {
                    id: 26,
                    format: '^.+$',
                    label: 'Policy No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-20',
                    mandatory: false
                },
                {
                    id: 27,
                    format: '^.+$',
                    label: 'Client No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-21',
                    mandatory: false
                },
                {
                    id: 28,
                    format: '^.+$',
                    label: 'Policy Year',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-22',
                    mandatory: false
                },
                {
                    id: 29,
                    format: '^.+$',
                    label: 'Age Group',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-23',
                    mandatory: false
                },
                {
                    id: 30,
                    format: '^.+$',
                    label: 'Application Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-0-24',
                    mandatory: false
                },
                {
                    id: 31,
                    format: '^.+$',
                    label: 'Policy Start',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-0',
                    mandatory: false
                },
                {
                    id: 32,
                    format: '^.+$',
                    label: 'Policy End',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-1',
                    mandatory: false
                },
                {
                    id: 33,
                    format: '^.+$',
                    label: 'Org Commence Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-2',
                    mandatory: false
                },
                {
                    id: 34,
                    format: '^.+$',
                    label: 'Commence Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-3',
                    mandatory: false
                },
                {
                    id: 35,
                    format: '^.+$',
                    label: 'Date Cancel',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-4',
                    mandatory: false
                },
                {
                    id: 36,
                    format: '^.+$',
                    label: 'Document No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-5',
                    mandatory: false
                },
                {
                    id: 37,
                    format: '^.+$',
                    label: 'Document Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-6',
                    mandatory: false
                },
                {
                    id: 38,
                    format: '^.+$',
                    label: 'Country Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-7',
                    mandatory: false
                },
                {
                    id: 39,
                    format: '^.+$',
                    label: 'Benefit Club Member',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-8',
                    mandatory: false
                },
                {
                    id: 40,
                    format: '^.+$',
                    label: 'Status Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-9',
                    mandatory: false
                },
                {
                    id: 41,
                    format: '^.+$',
                    label: 'Plan Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-10',
                    mandatory: false
                },
                {
                    id: 42,
                    format: '^.+$',
                    label: 'Rider Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-11',
                    mandatory: false
                },
                {
                    id: 43,
                    format: '^.+$',
                    label: 'Premium',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-12',
                    mandatory: false
                },
                {
                    id: 44,
                    format: '^.+$',
                    label: 'No Of Insured',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-13',
                    mandatory: false
                },
                {
                    id: 45,
                    format: '^.+$',
                    label: ' No Of Days ',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-14',
                    mandatory: false
                },
                {
                    id: 46,
                    format: '^.+$',
                    label: 'Cover Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-15',
                    mandatory: false
                },
                {
                    id: 47,
                    format: '^.+$',
                    label: 'Channel',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-16',
                    mandatory: false
                },
                {
                    id: 48,
                    format: '^.+$',
                    label: 'Other Inforce GI',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-17',
                    mandatory: false
                },
                {
                    id: 49,
                    format: '^.+$',
                    label: 'Staff Indicator',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-18',
                    mandatory: false
                },
                {
                    id: 50,
                    format: '^.+$',
                    label: 'Enrollment Freq',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-19',
                    mandatory: false
                },
                {
                    id: 51,
                    format: '^.+$',
                    label: 'Enrollment Freq Since Nov 2017',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-20',
                    mandatory: false
                },
                {
                    id: 52,
                    format: '^.+$',
                    label: 'Call ID',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-21',
                    mandatory: false
                },
                {
                    id: 53,
                    format: '^.+$',
                    label: 'Campaign Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-22',
                    mandatory: false
                },
                {
                    id: 54,
                    format: '^.+$',
                    label: 'Staff No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '0-2-1-23',
                    mandatory: false
                },
                {
                    id: 55,
                    format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                    label: '',
                    default: 'Reminder for Sales',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-0-0-0',
                    mandatory: false
                },
                {
                    id: 56,
                    format: '^<[a-zA-Z0-9]+>.*<\/[a-zA-Z0-9]+>$',
                    label: '',
                    default: 'html default',
                    answer: '<p>同事必須詢問客人現在是否身存香港? 並告訴客人以下對話將有錄音</p>',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-0-0-1',
                    mandatory: false
                }, {
                    id: 57,
                    format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                    label: '',
                    default: 'Sales Information',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-0-0-2',
                    mandatory: false
                }, {
                    id: 58,
                    format: '^.*$',
                    label: 'Insured Id',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-1-0-0',
                    mandatory: false
                },
                {
                    id: 59,
                    format: '^(HKID|Passport)$',
                    label: 'ID Type',
                    default: 'HKID',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-1-1-0',
                    mandatory: true
                },
                {
                    id: 60,
                    format: '^.*$',
                    label: 'Insured Full Name',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-2-0-0',
                    mandatory: true
                },
                {
                    id: 61,
                    format: '^.*$',
                    label: 'Insured Surname',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-2-0-1',
                    mandatory: true
                },
                {
                    id: 62,
                    format: '^([0-9]{4})-([0-9]{2})-([0-9]{2})$',
                    label: 'Insured Birth Date',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-3-0-0',
                    mandatory: true
                },
                {
                    id: 63,
                    format: '^(Male|Female)$',
                    label: 'Insured Sex',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-3-1-0',
                    mandatory: true
                },
                {
                    id: 64,
                    format: '^(,?(new:&nbsp;))*$',
                    label: 'Using New address?',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-4-0-0',
                    mandatory: true
                },
                {
                    id: 65,
                    format: '^.*$',
                    label: 'Insured Address Line 1',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-4-0-1',
                    mandatory: true
                },
                {
                    id: 66,
                    format: '^.*$',
                    label: 'Insured Address Line 2',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-4-0-2',
                    mandatory: true
                },
                {
                    id: 67,
                    format: '^.*$',
                    label: 'Insured Address Line 3',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-4-0-3',
                    mandatory: true
                },
                {
                    id: 68,
                    format: '^[0-9.]+$',
                    label: 'Insured Telephone 1',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-5-0-0',
                    mandatory: true
                },
                {
                    id: 69,
                    format: '^[0-9.]+$',
                    label: 'Insured Telephone 2',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-5-1-0',
                    mandatory: true
                },
                {
                    id: 70,
                    format: '^[0-9.]+$',
                    label: 'Insured Telephone (Mobile)',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-6-0-0',
                    mandatory: true
                },
                {
                    id: 71,
                    format: '^(-?(Opt-Out|Opt-In|Unknown))*$',
                    label: 'Opt for Direct Marketing',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-6-0-1',
                    mandatory: true
                },
                {
                    id: 72,
                    format: '/[A-Za-z\s-\/!%&()+=\'"~,.]/',
                    label: '',
                    default: 'Payment Information',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-6-0-2',
                    mandatory: true
                },
                {
                    id: 73,
                    format: '^(Credit Card|Bank A/C)$',
                    label: 'Payment Method',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-6-0-3',
                    mandatory: true
                },
                {
                    id: 74,
                    format: '^[0-9.]+$',
                    label: 'Card Number',
                    default: '',
                    answer: '',
                    condition: '[["73","=","Credit Card"]]',
                    path: '1,73',
                    order: '1-7-0-0',
                    mandatory: true
                },
                {
                    id: 75,
                    format: '^(2018|2019|2020|2021|2022|2023|2024)$',
                    label: 'Card Expiry Year',
                    default: '',
                    answer: '',
                    condition: '[["73","=","Credit Card"]]',
                    path: '1,73',
                    order: '1-8-0-0',
                    mandatory: true
                },
                {
                    id: 76,
                    format: '^(Jan|Feb|Mar|Apr|May|Jun|July|Aug|Sep|Oct|Nov|Dec)$',
                    label: 'Card Expiry Month',
                    default: '',
                    answer: '',
                    condition: '[["73","=","Credit Card"]]',
                    path: '1,73',
                    order: '1-8-1-0',
                    mandatory: true
                },
                {
                    id: 77,
                    format: '^.*$',
                    label: 'Account Holder Name',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-0-1',
                    mandatory: true
                },
                {
                    id: 78,
                    format: '^(Level 1 = $121|Level 2 = $254|Level 3 = $410)$',
                    label: 'Plan Level',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-1-1',
                    mandatory: true
                },
                {
                    id: 79,
                    format: '^[0-9.]+$',
                    label: 'Total Premium',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-0-2',
                    mandatory: true
                },
                {
                    id: 80,
                    format: '^[0-9.]+$',
                    label: 'Total ANP',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-1-2',
                    mandatory: true
                },
                {
                    id: 81,
                    format: '^(,?(spouse:&nbsp;))*$',
                    label: 'Spouse',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-0-3',
                    mandatory: true
                },
                {
                    id: 82,
                    format: '^(0|1)$',
                    label: 'No. of Child ',
                    default: '',
                    answer: '',
                    condition: '[["1","=","Presented - Success"]]',
                    path: '1',
                    order: '1-8-1-3',
                    mandatory: true
                },
                {
                    id: 83,
                    format: '^.+$',
                    label: 'Item_No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-1',
                    mandatory: false
                },
                {
                    id: 84,
                    format: '^.+$',
                    label: 'Batch Prefix',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-2',
                    mandatory: false
                },
                {
                    id: 85,
                    format: '^.+$',
                    label: 'Holder Name',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-3',
                    mandatory: false
                },
                {
                    id: 86,
                    format: '^.+$',
                    label: 'Address1',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-4',
                    mandatory: false
                },
                {
                    id: 87,
                    format: '^.+$',
                    label: 'Address 2',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-5',
                    mandatory: false
                },
                {
                    id: 88,
                    format: '^.+$',
                    label: 'Address 3',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-6',
                    mandatory: false
                },
                {
                    id: 89,
                    format: '^.+$',
                    label: 'Address 4',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-7',
                    mandatory: false
                },
                {
                    id: 90,
                    format: '^.+$',
                    label: 'Email',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-8',
                    mandatory: false
                },
                {
                    id: 91,
                    format: '^.+$',
                    label: 'Phone1 No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-9',
                    mandatory: false
                },
                {
                    id: 92,
                    format: '^.+$',
                    label: 'Phone2 No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-10',
                    mandatory: false
                },
                {
                    id: 93,
                    format: '^.+$',
                    label: 'Mobile No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-11',
                    mandatory: false
                },
                {
                    id: 94,
                    format: '^.+$',
                    label: 'HKID',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-12',
                    mandatory: false
                },
                {
                    id: 95,
                    format: '^.+$',
                    label: 'DOB',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-13',
                    mandatory: false
                },
                {
                    id: 96,
                    format: '^.+$',
                    label: 'Gender',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-14',
                    mandatory: false
                },
                {
                    id: 97,
                    format: '^.+$',
                    label: 'Business Line',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-15',
                    mandatory: false
                },
                {
                    id: 98,
                    format: '^.+$',
                    label: 'Product Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-16',
                    mandatory: false
                },
                {
                    id: 99,
                    format: '^.+$',
                    label: 'Product Desc',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-17',
                    mandatory: false
                },
                {
                    id: 100,
                    format: '^.+$',
                    label: 'Agent No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-18',
                    mandatory: false
                },
                {
                    id: 101,
                    format: '^.+$',
                    label: 'Agent Group',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-19',
                    mandatory: false
                },
                {
                    id: 102,
                    format: '^.+$',
                    label: 'Policy No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-20',
                    mandatory: false
                },
                {
                    id: 103,
                    format: '^.+$',
                    label: 'Client No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-21',
                    mandatory: false
                },
                {
                    id: 104,
                    format: '^.+$',
                    label: 'Policy Year',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-22',
                    mandatory: false
                },
                {
                    id: 105,
                    format: '^.+$',
                    label: 'Age Group',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-23',
                    mandatory: false
                },
                {
                    id: 106,
                    format: '^.+$',
                    label: 'Application Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-0-24',
                    mandatory: false
                },
                {
                    id: 107,
                    format: '^.+$',
                    label: 'Policy Start',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-0',
                    mandatory: false
                },
                {
                    id: 108,
                    format: '^.+$',
                    label: 'Policy End',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-1',
                    mandatory: false
                },
                {
                    id: 109,
                    format: '^.+$',
                    label: 'Org Commence Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-2',
                    mandatory: false
                },
                {
                    id: 110,
                    format: '^.+$',
                    label: 'Commence Date',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-3',
                    mandatory: false
                },
                {
                    id: 111,
                    format: '^.+$',
                    label: 'Date Cancel',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-4',
                    mandatory: false
                },
                {
                    id: 112,
                    format: '^.+$',
                    label: 'Document No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-5',
                    mandatory: false
                },
                {
                    id: 113,
                    format: '^.+$',
                    label: 'Document Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-6',
                    mandatory: false
                },
                {
                    id: 114,
                    format: '^.+$',
                    label: 'Country Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-7',
                    mandatory: false
                },
                {
                    id: 115,
                    format: '^.+$',
                    label: 'Benefit Club Member',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-8',
                    mandatory: false
                },
                {
                    id: 116,
                    format: '^.+$',
                    label: 'Status Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-9',
                    mandatory: false
                },
                {
                    id: 117,
                    format: '^.+$',
                    label: 'Plan Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-10',
                    mandatory: false
                },
                {
                    id: 118,
                    format: '^.+$',
                    label: 'Rider Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-11',
                    mandatory: false
                },
                {
                    id: 119,
                    format: '^.+$',
                    label: 'Premium',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-12',
                    mandatory: false
                },
                {
                    id: 120,
                    format: '^.+$',
                    label: 'No Of Insured',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-13',
                    mandatory: false
                },
                {
                    id: 121,
                    format: '^.+$',
                    label: ' No Of Days ',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-14',
                    mandatory: false
                },
                {
                    id: 122,
                    format: '^.+$',
                    label: 'Cover Type',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-15',
                    mandatory: false
                },
                {
                    id: 123,
                    format: '^.+$',
                    label: 'Channel',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-16',
                    mandatory: false
                },
                {
                    id: 124,
                    format: '^.+$',
                    label: 'Other Inforce GI',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-17',
                    mandatory: false
                },
                {
                    id: 125,
                    format: '^.+$',
                    label: 'Staff Indicator',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-18',
                    mandatory: false
                },
                {
                    id: 126,
                    format: '^.+$',
                    label: 'Enrollment Freq',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-19',
                    mandatory: false
                },
                {
                    id: 127,
                    format: '^.+$',
                    label: 'Enrollment Freq Since Nov 2017',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-20',
                    mandatory: false
                },
                {
                    id: 128,
                    format: '^.+$',
                    label: 'Call ID',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-21',
                    mandatory: false
                },
                {
                    id: 129,
                    format: '^.+$',
                    label: 'Campaign Code',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-22',
                    mandatory: false
                },
                {
                    id: 130,
                    format: '^.+$',
                    label: 'Staff No',
                    default: '',
                    answer: '',
                    condition: '',
                    path: '',
                    order: '1-9-1-23',
                    mandatory: false
                },
            ]
        }

        this.data = data;

        this.setState({ data: data, details: details, btnOrder: btnOrder, remark: details.remark || "", hideSecObj: hideSecObj }, () => this.reloadPage());
    }
    showHideField(field, rowCondition) {
        let details = this.state.details;
        let data = this.data;
        // set status
        let show = true, conditions = [];
        // parse conditions
        try { conditions = JSON.parse(rowCondition); } catch (e) { conditions = []; }
        // check condition
        while (show && conditions.length > 0) {
            // get condition NOTES: take out the first from the array
            let condition = conditions.shift();
            console.log("condition");
            console.log(condition);
            // get target field info
            let targetIdx = -1;
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == condition[0]) {
                    targetIdx = i;
                    break;
                }
            }
            let targetRow = data[targetIdx];
            // get target row type
            let targetFormatType = formatTypeSwitch(targetRow.format);
            console.log("targetFormatType");
            console.log(targetFormatType);
            // get value
            console.log("details.answer[condition[0]]");
            console.log(details.answer[condition[0]]);
            let value = details.answer[condition[0]] ? details.answer[condition[0]] : targetRow.answer || targetRow.default;
            //let value = targetRow.answer || targetRow.default;
            console.log("value");
            console.log(value);
            // check condition ============ = ============
            if (/^=$/.test(condition[1])) {
                // update status
                show = new RegExp('^' + condition[2] + '$').test(value);
                console.log("show");
                console.log(show);
            } else {
                // date related?
                if (/^date(time)?$/i.test(targetFormatType)) {
                    // parse condition
                    condition[2] = new Date(condition[2].replace(/-/g, '/')).getTime();
                    // parse value
                    value = new Date(value.replace(/-/g, '/')).getTime();
                } else {
                    // parse condition
                    condition[2] = parseFloat(condition[2]);
                    // parse value
                    value = parseFloat(value);
                }
                //  ============ > ============
                if (/^>$/.test(condition[1])) {
                    // check value
                    show = value > condition[2];
                    // ============ < ============
                } else if (/^<$/.test(condition[1])) {
                    // check value
                    show = value < condition[2];
                    // ============ >= ============
                } else if (/^>=$/.test(condition[1])) {
                    // check value
                    show = value >= condition[2];
                    // ============ <= ============
                } else if (/^<=$/.test(condition[1])) {
                    // check value
                    show = value <= condition[2];
                }
            }
        }
        // set status
        field[show ? 'show' : 'hide']();
        return show;
    }
    renderView(row) {
        console.log(" ======================= renderView ======================= ");
        console.log("row");
        console.log(row);
        let oThis = this;
        let data = this.data;
        let order = row.order.split('-');
        let details = this.state.details;
        // get existing field
        let field = null;
        if (this.props.page == true) {
            field = $('#Iviewer .field[data-field-id="' + row.id + '"]');
        } else {
            field = $('#viewer .field[data-field-id="' + row.id + '"]');
        }
        // --------------------------- /input field processing --------------------------- //
        // adjust pages
        if (this.props.page == true) {
            this.adjustPages($('#Ipages'), order);
        } else {
            this.adjustPages($('#pages'), order);
        }
        //this.adjustPages($('#pages'), order);
        // ---------- render column field ---------- //

        //let field = $('#viewer .field[data-field-id="' + row.id + '"]');
        // get type
        let type = formatConverter.identify(row.format);
        console.log("type in renderView");
        console.log(type);
        let labelContent = [];
        if (type == 'header') {
            console.log("order[3]");
            console.log(order[3]);
            console.log("type == 'header' && order[3] != '0' && row.label == ''");
            console.log(type == 'header' && order[3] != '0' && row.label == '');
        }

        if (type != 'html' && row.label != '') {
            labelContent = '<div class="name">' +
                '<div class="label">' +
                '<label><span class="label-normal">' + (row.label || '') + '</span><span class="color-red">' + (row.mandatory == true ? ' *' : '') + '</span></label> ' +
                '</div></div>';
        }
        if (type == 'header' && order[3] != '0' && row.label == '') {
            labelContent = '<div class="name">' +
                '<div class="label">' +
                '<label><span class="label-normal">' + (row.label || '') + '</span></label> ' +
                '</div></div>';
        }
        // no existing field found?
        if (field.length < 1) {
            // create new field 
            field = $('<li class="field" data-field-type="' + type + '" data-field-id="' + (row.id || 0) + '">' +
                '<div class="wrapper">' +
                labelContent +
                '<div class="input-row">' +
                '<div class="input-field"></div>' +
                '</div>' +
                '</div>' +
                '</li>');

            let appendArray = [];
            if (this.props.page == true) {
                appendArray = [
                    '#Ipages .page[data-page-id="' + order[0] + '"]',
                    'section[data-section-id="' + order[1] + '"]',
                    '.column[data-column-id="' + order[2] + '"]'
                ];
            } else {
                appendArray = [
                    '#pages .page[data-page-id="' + order[0] + '"]',
                    'section[data-section-id="' + order[1] + '"]',
                    '.column[data-column-id="' + order[2] + '"]'
                ];
            }
            let preFieldId = Number(order[3]) - 1;
            console.log("preFieldId");
            console.log(preFieldId);
            let preField = null;
            // if have field before it and it is being shown now
            if (preFieldId >= 0) {
                preField = $('#pages .page[data-page-id="' + order[0]).children('section[data-section-id="' + order[1] + '"]').children('.column[data-column-id="' + order[2] + '"]').find('li .field[data-field-id="' + preFieldId + '"]');
                if (preField.length > 0) {
                    appendArray.push('li .field[data-field-id="' + preFieldId + '"]');
                }
            }
            // if have no field before it, but first element is already there
            if (preFieldId == -1) {
                preField = $('#pages .page[data-page-id="' + order[0]).children('section[data-section-id="' + order[1] + '"]').children('.column[data-column-id="' + order[2] + '"]').children().first();
                // already exist 1st one
                if (preField.length > 0) {
                    appendArray.push('li .field[data-field-id="' + preFieldId + '"]');
                    field.insertBefore(appendArray.join(' '));
                } else {
                    // not exist first one
                    field.appendTo(appendArray.join(' '));
                }
            } else {
                field.appendTo(appendArray.join(' '));
            }
            console.log("preField");
            console.log(preField);
            // append field to related column

        } else {
            // update label
            field.find('label').children('.label-normal').text(row.label || '');
            field.find('label').children('.color-red').text(row.mandatory == true ? ' *' : '');
            // field.find('.input-row .label').text(row.label || '');
        }
        // setup label
        let label = field.find('.input-row .label');
        // set status
        label[(label.is(':empty') ? 'hide' : 'show')]();

        // --------------------------- input field processing --------------------------- //
        // get input container
        let inputContainer = field.find('.input-field');
        // clear existing input
        inputContainer.children().empty().removeData().remove();
        // get input value


        let inputValue = details.answer[row.id] || row.answer || row.default;
        console.log("row");
        console.log(row);
        console.log("inputValue");
        console.log(inputValue);
        // get variable
        let variable = inputValue.replace(/^\$\{([0-9]+)\}$/, '$1');
        // it's a variable
        if (inputValue != variable) {
            // update input value
            inputValue = (data[variable].answer || data[variable].default) || '';
        }
        // get template
        console.log("oThis.state.readOnly before getting template");
        console.log(oThis.state.readOnly);
        let template = formatConverter.render(type, oThis.state.readOnly).replace('{VALUE}', inputValue);
        // for security, the value can more than 50 digits
        let value = row.label.substring(0, 49);
        // create input field
        console.log("template");
        console.log(template);
        console.log("type");
        console.log(type);
        let input = $(template);
        // dropdown?
        if (/^dropdown$/i.test(type)) {
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                $('<option value="' + value + '">' + label + '</option>').appendTo(input);
            }
            // set value
            input.val(inputValue);
            // checkbox?
        } else if (/^checkbox$/i.test(type)) {
            // get answer
            let answer = inputValue != "" ? inputValue.split(',') : row.answer.split(',');
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                if (oThis.state.readOnly == true) {
                    input.append(
                        '<input type="checkbox" name="' + row.id + '[]" id="' + row.id + '-' + i + '" value="' + value + '" disabled />' +
                        '<label for="' + row.id + '-' + i + '">' + label + '</label>'
                    );
                } else {
                    input.append(
                        '<input type="checkbox" name="' + row.id + '[]" id="' + row.id + '-' + i + '" value="' + value + '"/>' +
                        '<label for="' + row.id + '-' + i + '">' + label + '</label>'
                    );
                }
                // set checked
                if (answer.indexOf(value) >= 0) {
                    input.children('input#' + row.id + '-' + i).attr('checked', true);
                }
            }
        } else if (/^radio$/i.test(type)) {
            // get answer
            let answer = inputValue.split(',');
            // get options
            let options = getOptions(type, row.format);
            // append options
            for (let i in options) {
                // get option
                let option = options[i].split(':');
                // get value
                let value = option.shift();
                // get label
                let label = option.shift() || value;
                // append to input
                input.append(
                    '<input type="radio" name="' + row.id + '[]" id="' + row.id + '-' + i + '" value="' + value + '"/>' +
                    '<label for="' + row.id + '-' + i + '">' + label + '</label>'
                );
                // set checked
                if (answer.indexOf(value) >= 0) {
                    input.children('input#' + row.id + '-' + i).attr('checked', true);
                }
            }
        } else if (/^file$/i.test(type)) {
            input.bind('change', function (e) {
                oThis.upload(row.id);
            });
            let fileName = this.state.fileName[row.id];
        } else if (/^html$/i.test(type)) {
            console.log("input in html"); console.log(input);
            // setup 
            if (this.state.ckeditorActive == true) {
                if (oThis.state.readOnly != true) {
                    input.ckeditor(function (textarea) {
                        // callback
                    }, {
                            // config
                        });
                } else {
                    input = $(inputValue);
                }
            } else {
                input = $(inputValue);
            }
        }

        // update input field
        input.bind('change', function (e) {
            // Get id
            let id = $(e.target).parents('li[data-field-id]').attr('data-field-id');
            // Get the index of the id in data array
            let idIdx = -1;
            for (let i in data) {
                if (data[i].id == id) {
                    idIdx = i;
                    break;
                }
            }
            // Checkbox or Radio field change
            if (typeof e.target.name != "undefined" && e.target.name) {
                // get options
                let options = null, values = [];
                if (oThis.props.page == true) {
                    options = $('#Iviewer input[name="' + e.target.name + '"]:checked').get();
                } else {
                    options = $('#viewer input[name="' + e.target.name + '"]:checked').get();
                }
                // get values
                for (let i in options) values.push(options[i].value);
                // append value to value container
                $(e.target).is(':checked') && values.indexOf(e.target.value) < 0 && values.push(e.target.value);
                // delete value from value container
                !$(e.target).is(':checked') && values.indexOf(e.target.value) >= 0 && values.slice(values.indexOf(e.target.value), 1);
                // update value
                details.answer[id] = values.join(',');
                //data[idIdx].answer = values.join(',');
            } else {
                // update value
                console.log("event.target.value");
                console.log(e);
                console.log("details[id] before");
                console.log(details.answer[id]);
                details.answer[id] = e.target.value;
                console.log("details[id] after");
                console.log(details.answer[id]);
                //data[idIdx].answer = e.target.value;
            }
            console.log("id");
            console.log(id);
            console.log("idIdx");
            console.log(idIdx);
            console.log("data[idIdx]");
            console.log(data[idIdx]);
            // reload field
            oThis.setState({ details: details }, () => { oThis.renderView(data[idIdx]); oThis.addRemarkBtn(); oThis.disableBackNext(); });
            //oThis.renderView(data[idIdx]);
            // get sub questions
            let subQuestions = [];
            for (let i in data) {
                let path = (data[i].path || '').split(',').filter(function (v) { return v.length > 0; }) || [];
                if (path.length > 0) {
                    let lastPath = path[path.length - 1];
                    if (lastPath == id) {
                        subQuestions.push(data[i]);
                    }
                }
            }

            //let subQuestions = $('#structure .node[data-field-id="' + id + '"] > .childs').children().get();
            // reload sub questions
            console.log("subQuestions");
            console.log(subQuestions);
            if (subQuestions.length > 0) {
                for (let i in subQuestions) {
                    oThis.renderView(subQuestions[i]);
                }
            }
            oThis.addRemarkBtn();
            oThis.disableBackNext();
            // if page is empty hidden



            // for (let i in subQuestions) {
            //     let subQuesId = $(subQuestions[i]).attr('data-field-id');
            //     console.log("subQuesId");
            //     console.log(subQuesId);
            //     for (let j = 0; j < data.length; j++) {
            //         if (data[j].id == subQuesId) {
            //             subQuesIdx = j;
            //             break;
            //         }
            //     }
            //     console.log("subQuesIdx");
            //     console.log(subQuesIdx);
            //     // to show the subQuest or not
            //     oThis.renderView(data[subQuesIdx]);
            // }
        }).appendTo(inputContainer);
        // //  ==================================  SHOW or not  ============================
        // set status
        // let show = true, conditions = [];
        // // parse conditions
        // try { conditions = JSON.parse(row.condition); } catch (e) { conditions = []; }
        // // check condition
        // while (show && conditions.length > 0) {
        //     // get condition NOTES: take out the first from the array
        //     let condition = conditions.shift();
        //     console.log("condition");
        //     console.log(condition);
        //     // get target field info
        //     let targetIdx = -1;
        //     for (let i = 0; i < data.length; i++) {
        //         if (data[i].id == condition[0]) {
        //             targetIdx = i;
        //             break;
        //         }
        //     }
        //     let targetRow = data[targetIdx];
        //     // get target row type
        //     let targetFormatType = formatTypeSwitch(targetRow.format);
        //     console.log("targetFormatType");
        //     console.log(targetFormatType);
        //     // get value
        //     console.log("details.answer[condition[0]]");
        //     console.log(details.answer[condition[0]]);
        //     let value = details.answer[condition[0]] ? details.answer[condition[0]] : targetRow.answer || targetRow.default;
        //     //let value = targetRow.answer || targetRow.default;
        //     console.log("value");
        //     console.log(value);
        //     // check condition ============ = ============
        //     if (/^=$/.test(condition[1])) {
        //         // update status
        //         show = new RegExp('^' + condition[2] + '$').test(value);
        //         console.log("show");
        //         console.log(show);
        //     } else {
        //         // date related?
        //         if (/^date(time)?$/i.test(targetFormatType)) {
        //             // parse condition
        //             condition[2] = new Date(condition[2].replace(/-/g, '/')).getTime();
        //             // parse value
        //             value = new Date(value.replace(/-/g, '/')).getTime();
        //         } else {
        //             // parse condition
        //             condition[2] = parseFloat(condition[2]);
        //             // parse value
        //             value = parseFloat(value);
        //         }
        //         //  ============ > ============
        //         if (/^>$/.test(condition[1])) {
        //             // check value
        //             show = value > condition[2];
        //             // ============ < ============
        //         } else if (/^<$/.test(condition[1])) {
        //             // check value
        //             show = value < condition[2];
        //             // ============ >= ============
        //         } else if (/^>=$/.test(condition[1])) {
        //             // check value
        //             show = value >= condition[2];
        //             // ============ <= ============
        //         } else if (/^<=$/.test(condition[1])) {
        //             // check value
        //             show = value <= condition[2];
        //         }
        //     }
        // }
        // // set status
        // field[show ? 'show' : 'hide']();
        let show = oThis.showHideField(field, row.condition);
        // get cloest page id
        console.log("field.closest('.page')");
        console.log(field.closest('.page'));
        let secId = field.closest('section')[0].getAttribute('data-section-id');
        let pageId = field.closest('.page')[0].getAttribute('data-page-id');
        let hideSecObj = this.state.hideSecObj;
        let pageArr = hideSecObj[pageId];
        if (pageArr) {
            console.log("pageArr");
            console.log(pageArr);
            for (let i = 0; i < pageArr.length; i++) {
                if (show == true) {
                    if (secId != pageArr[i]) { // to prevent unlimited loop
                        this.showSection(pageId, pageArr[i]);
                    } else {
                        this.tryHideSection(pageId, pageArr[i]);
                    }
                } else {
                    this.tryHideSection(pageId, pageArr[i]);
                }
            }
        }
        // ---------- /render column field ---------- //
    }
    submitClick() {
        if (this.props.page == true) {
            console.log("this.data");
            console.log(this.data);
            console.log("this.state.details");
            console.log(this.state.details);
            console.log("this.state.remark");
            console.log(this.state.remark);
        } else {
            // only update remark in contact history
            console.log("this.state.remark");
            console.log(this.state.remark);
        }
    }
    addRemarkBtn() {
        // ========= remark and button name ======
        let oThis = this;
        let pages = null;
        if (this.props.page == true) {
            pages = $('#Ipages').children('.page[data-page-id]').get();
        } else {
            pages = $('#pages').children('.page[data-page-id]').get();
        }
        for (let i = 0; i < pages.length; i++) {
            let fields = $(pages[i]).find('.field');
            let hiddenFields = $(pages[i]).find('.field[style*="display: none"]');
            if (fields.length == hiddenFields.length) { pages.splice(i, 1); }
        }
        console.log("pages in addRemarkBtn");
        console.log(pages);
        let validPages = [];
        for (let pageId in pages) {
            validPages.push(pageId);
        }
        for (let i in validPages) {
            // get page id
            let pageId = validPages[i];
            // get page
            let page = $(pages[pageId]);
            console.log("i in validPages");
            console.log(i);
            // get button text
            let status = (i >= validPages.length - 1 ? 'Submit' : 'Next');
            console.log("status");
            console.log(status);
            // get button group
            //let buttonGroup = lastSec.children('div.button-group');
            let buttonGroup = page.children('div.button-group');
            console.log("buttonGroup.length");
            console.log(buttonGroup.length);
            // find button group
            let previousBtn = "<span></span>";
            if (i > 0) { previousBtn = '<button class="previousBtn">Previous</button>' };
            if (buttonGroup.length < 1) {
                // get page first section
                let selectedSec = [];
                console.log("pageId in validages");
                console.log(pageId);
                console.log("this.state.btnOrder[pageId]");
                console.log(this.state.btnOrder[pageId]);
                if (this.state.btnOrder[pageId] != undefined) {
                    if (this.state.btnOrder[pageId] == -1) {
                        selectedSec = $(pages[pageId]).children('section[data-section-id="' + 0 + '"]')[0];
                    } else {
                        selectedSec = $(pages[pageId]).children('section[data-section-id="' + this.state.btnOrder[pageId] + '"]')[0];
                    }
                    console.log("selectedSec searched");
                    console.log(selectedSec);
                    if (selectedSec == undefined) {
                        // default after 1st section
                        selectedSec = $(pages[pageId]).children('section')[0];
                    }
                } else {
                    // default after 1st section
                    selectedSec = $(pages[pageId]).children('section')[0];
                }

                console.log("selectedSec");
                console.log(selectedSec);
                console.log("remark");
                console.log(this.state.remark);
                // create button group
                let remarkId = "";
                if (this.props.page == true) {
                    remarkId = "Iremark";
                } else {
                    remarkId = "remark";
                }
                let remarkBtn = '<div class="button-group">' +
                    '<div class="remark-label name"><span>' + i18n.internal_remark + '</span>' +
                    '<textarea class=' + remarkId + ' placeholder="Remark Here" maxlength="100">' +
                    this.state.remark +
                    '</textarea></div><div class="content-ender">' +
                    previousBtn +
                    '&nbsp;<button class="final">' +
                    status +
                    '</button>' +
                    '</div></div>';
                if (this.state.btnOrder[pageId] == -1) {
                    buttonGroup = $(remarkBtn).insertBefore(selectedSec);
                } else {
                    buttonGroup = $(remarkBtn).insertAfter(selectedSec);
                }

                // bind events: Previous Button
                buttonGroup.children('.content-ender').children('button.previousBtn').bind('click', function (e) {
                    // get page
                    let page = $(e.target).parents('.page[data-page-id]');
                    // get page id
                    let pageId = parseInt(page.attr('data-page-id'));
                    if (oThis.props.page == true) {
                        $('#Itabs .tab[data-page-id="' + (pageId - 1) + '"] a').trigger('click');
                    } else {
                        $('#tabs .tab[data-page-id="' + (pageId - 1) + '"] a').trigger('click');
                    }
                });
                // bind events: Next Button or Submit button
                buttonGroup.children('.content-ender').children('button.final').bind('click', function (e) {
                    // get page
                    let page = $(e.target).parents('.page[data-page-id]');
                    // get page id
                    let pageId = parseInt(page.attr('data-page-id'));
                    // get status
                    let status = $(e.target).text();
                    // next page?
                    if (/^next$/i.test(status)) {
                        console.log("next click!");
                        // go to next page
                        if (oThis.props.page == true) {
                            $('#Itabs .tab[data-page-id="' + (pageId + 1) + '"] a').trigger('click');
                        } else {
                            $('#tabs .tab[data-page-id="' + (pageId + 1) + '"] a').trigger('click');
                        }
                        // submit
                    } else {
                        console.log("submit click!");
                        oThis.submitClick();
                    }
                });
                // bind events: Remark textbox onChange
                $("." + remarkId).bind('input propertychange', function (e) {
                    e.preventDefault();
                    let value = e.target.value;
                    oThis.setState({ remark: value });
                    $("." + remarkId).val(value);
                });
            } else {
                // change the status of final
                console.log("buttonGroup");
                console.log(buttonGroup);
                console.log("buttonGroup.children('.content-ender')");
                console.log(buttonGroup.children('.content-ender'));
                buttonGroup.children('.content-ender').children('button.final').text(status);
                //buttonGroup.children('button.final').text(status);
            }
            // update status
            buttonGroup.removeClass(/^next$/i.test(status) ? 'submit' : 'next').addClass(status.toLowerCase());
        }
    }
    adjustPages(container, order) {
        let oThis = this;
        let data = this.data;
        //let data = this.state.data;
        console.log(" ====================== adjustPages ====================== ");
        console.log("container in adjust pages");
        console.log(container);
        console.log("order in adjust pages");
        console.log(order);
        // setup maximum
        let maximum = parseInt(order[0] || '0');
        console.log("maximum");
        console.log(maximum);
        //NOTES: get pages, for the first time load, no any page, will be only []
        console.log("('#Is').children('.page')");
        console.log($('#Ipages').children('.page'));
        let pages = container.children('.page[data-page-id]').get();
        console.log("pages in adjust pages");
        console.log(pages);
        // get total pages number
        let totalPages = pages.length;
        console.log("totalPages in adjustPages");
        console.log(totalPages);
        // setup empty page id container
        let emptyPages = [], validPages = [];
        // check empty page
        for (let pageId in pages) {
            // is it empty page?
            console.log("pageId");
            console.log(pageId);
            console.log("maximum == pageId ? order : [pageId, 0, 0]");
            console.log(maximum == pageId ? order : [pageId, 0, 0]);
            if (this.adjustSections($(pages[pageId]), maximum == pageId ? order : [pageId, 0, 0])) {
                //if (this.adjustSections($(pages[pageId]), maximum == pageId ? order : [pageId, 0, 0])) {
                // count as empty page
                emptyPages.push(pageId);
                console.log("emptyPages pageID");
                console.log(emptyPages);
            } else {
                validPages.push(pageId);
                // validPages=[0,1];
            }
        }
        //validPages = ["0", "1"];
        console.log("order");
        console.log(order);
        console.log("emptyPages after push");
        console.log(emptyPages);
        console.log("validPages after push");
        console.log(validPages);
        // set remark and button name
        // reduce validPages if the last page display none
        let reduced = false;
        for (let i = validPages.length - 1; i > -1; --i) {
            let pageId = validPages[i];
            let pageFieldLen = $(pages[pageId]).find('.field').length;
            let pageNoneLen = $(pages[pageId]).find('.field[style="display: none;"]').length;
            if (pageFieldLen == pageNoneLen) {
                // if the page to be reduced have button group, remove it
                let page = $(pages[pageId]);
                let buttonGroup = page.children('div.button-group');
                if (buttonGroup != undefined) {
                    buttonGroup.remove();
                }
                validPages.pop();
                reduced = true;
            } else {
                if (reduced == true) {
                    break;
                }
            }
        }
        // create pages
        for (let pageId = 0; pageId <= maximum; pageId++) {
            // get tab
            let tab = null;
            if (this.props.page == true) {
                tab = $('#Itabs').children(':eq(' + pageId + ')');
            } else {
                tab = $('#tabs').children(':eq(' + pageId + ')');
            }
            // let tab = $('#tabs').children(':eq(' + pageId + ')');
            // page not found?
            if (tab.length < 1) {
                // create a new page
                let tab = $('<div class="tab" data-page-id="' + pageId + '">' +
                    '<a href="javascript:;">' + (pageId + 1) + '</a>' +
                    '</div>');
                // bind events
                tab.children('a').bind('click', function (e) {
                    console.log("TAB CLICKED!");
                    // get target page id
                    let targetPageId = $(e.target).parent().attr('data-page-id');
                    // set the present page Id
                    oThis.setState({ pageId: pageId }, () => oThis.disableBackNext());
                    // get target page
                    let targetPage = null;
                    if (oThis.props.page == true) {
                        targetPage = $('#Ipages .page[data-page-id="' + targetPageId + '"]');
                    } else {
                        targetPage = $('#pages .page[data-page-id="' + targetPageId + '"]');
                    }
                    // let targetPage = $('#pages .page[data-page-id="' + targetPageId + '"]');
                    // page exists?
                    if (targetPage.length > 0) {
                        // hide all siblings
                        targetPage.siblings('.page:visible').hide();
                        // show the page
                        targetPage.show();
                        console.log("targetPage.parent().parent().parent()");
                        console.log(targetPage.parent().parent().parent());
                        //window.scrollTo(0, 0);
                        //$(targetPage.parent().parent().parent()).scrollTop(0);
                        $(targetPage.parent().parent().parent()).offset().top;
                    }
                });
                // append page to container
                if (oThis.props.page == true) {
                    tab.appendTo('#Itabs');
                } else {
                    tab.appendTo('#tabs');
                }
                //tab.appendTo('#tabs');
            }
            // get page
            let page = container.children(':eq(' + pageId + ')');
            // page not found?
            if (page.length < 1) {
                // create a new page
                page = $('<div class="page if-padding-del" data-page-id="' + pageId + '"></div>');
                // setup sections
                this.adjustSections(page, [pageId, 0, 0]);
                // append page to container
                page.appendTo(container);
                // new page should be an empty page
                emptyPages.push(pageId);
            }
        }
        // order index is greater than total?
        if (maximum > totalPages - 1) {
            // update total
            totalPages = maximum + 1;
        }
        // return status
        return totalPages <= 1 && emptyPages.length >= 1;
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("nextProps");
        console.log(nextProps);
        console.log("nextState");
        console.log(nextState);
        return false;
    }

    tryHideSection(pageId, secId) {
        // sections that not include the sections
        let sections = [];
        if (this.props.page == true) {
            sections = $('#Ipages').children('.page[data-page-id=' + pageId + ']').children('section:not([data-section-id=' + secId + '])');
        } else {
            sections = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section:not([data-section-id=' + secId + '])');
        }
        console.log("sections");
        console.log(sections);
        let fieldsLen = sections.children('.content').find('li.field').length;
        let HiddenFieldsLen = sections.children('.content').find('li.field[style*="display: none"]').length;
        console.log("HiddenFieldsLen");
        console.log(HiddenFieldsLen);
        console.log("fieldsLen");
        console.log(fieldsLen);
        if (HiddenFieldsLen == fieldsLen) {
            let hideFields = [];
            if (this.props.page == true) {
                hideFields = $('#Ipages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
            } else {
                hideFields = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
            }
            for (let i = 0; i < hideFields.length; i++) {
                $(hideFields[i]).hide();
            }
        }
    }
    showSection(pageId, secId) {
        console.log("showSection now");
        console.log("fields");
        console.log(fields);
        let fields = [];
        if (this.props.page == true) {
            fields = $('#Ipages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
        } else {
            fields = $('#pages').children('.page[data-page-id=' + pageId + ']').children('section[data-section-id=' + secId + ']').children('.content').find('li.field');
        }
        console.log("fields");
        console.log(fields);
        let data = this.data;
        for (let i = 0; i < fields.length; i++) {
            let fieldId = fields[i].getAttribute('data-field-id');
            for (let j = 0; j < data.length; j++) {
                if (data[j].id == fieldId) {
                    //this.renderView(data[j]);
                    this.showHideField($(fields[i]), data[j].condition);
                }
            }
        }
    }

    adjustSections(container, order) {
        let oThis = this;
        console.log(" ======================= adjustSections ======================= ");
        console.log("container");
        console.log(container);
        console.log("order");
        console.log(order);
        // setup maximum
        let maximum = parseInt(order[1] || '0');
        console.log("maximum");
        console.log(maximum);
        // get sections
        let sections = container.children('section[data-section-id]').get();
        console.log("sections");
        console.log(sections);
        // get total section number
        let totalSections = sections.length;
        console.log("totalSections");
        console.log(totalSections);
        // setup empty section id container
        let emptySections = [];
        // check empty section
        for (let sectionId in sections) {
            // get section
            let section = $(sections[sectionId]);
            //set column value
            if (order[1] == sectionId && order[2] == "1") {
                /// ==================== set 2 columns for drop box ====================
                section.find('> .tools > input')[0].setAttribute("checked", "true");
            }
            // max = no. of column(s) in sectinon by settings
            let max = section.find('> .tools >input')[0].checked == "true" || section.find('> .tools >input')[0].checked == true ? 2 : 1;
            console.log("section.find('> .tools > div >input')[0]");
            console.log(section.find('> .tools >div >input')[0]);
            console.log(" === max ===");
            console.log(max);
            // is it empty section?
            // ================== no. of empty column >= no. of columns by settings
            if (oThis.adjustColumns(section.children('.content'), max)) {
                // count as empty section
                emptySections.push(sectionId);
            }
        }
        // need to create new section? (no empty section and at least one section)
        if (emptySections.length < 1 && totalSections > 0 && maximum < totalSections) {
            // new page required
            maximum = totalSections;
        }
        // create sections
        for (let sectionId = 0; sectionId <= maximum; sectionId++) {
            console.log("sectionId");
            console.log(sectionId);
            // get section  //NOTES: .eq(): Reduce the set of matched elements to the one at the specified index.
            let section = container.children(':eq(' + sectionId + ')');
            console.log("section");
            console.log(section);
            // no section found?
            if (section.length < 1) {
                // add new section
                let swtichId = "switch" + this.switchNo;
                let switchFn = function () { alert("go") };
                section = $('<section data-section-id="' + sectionId + '">' +
                    '<div class="tools custom-tools display-none">' +
                    // '<label>2 Col.</label>' +
                    // '<div class="switch round small right">' +
                    '<input class="hidden" id="' + swtichId + '" + type="checkbox" />' +
                    // '<label for="' + swtichId + '"></label>' +
                    // '</div>' +
                    '</div>' +
                    '<div class="content"></div>' +
                    '</section>');

                this.switchNo = Number(this.switchNo) + 1;
                // setup columns
                this.adjustColumns(section.children('.content'), 1);
                // add new section to container
                section.appendTo(container);
                // new section should be an empty section as well
                emptySections.push(sectionId);
            }
        }
        // order index is greater than total?
        if (maximum > totalSections - 1) {
            // update total
            totalSections = maximum + 1;
        }
        // return status
        console.log("totalSections");
        console.log(totalSections);
        console.log("emptySections");
        console.log(emptySections);
        console.log("totalSections <= 1 && emptySections.length >= 1");
        console.log(totalSections <= 1 && emptySections.length >= 1);
        return totalSections <= 1 && emptySections.length >= 1;
    }
    //NOTES: when the columns combo-box selected 1 or 2
    adjustColumns(container, maximum) {
        let oThis = this;
        let data = this.data;
        //let oThis = this;
        console.log(" ======================= adjustColumns ======================= ");
        console.log("container, maximum");
        console.log(container, maximum);
        // get columns
        let emptyColumns = [];
        let columns = container.children('.column[data-column-id]').get();
        let totalColumns = columns.length;
        console.log("totalColumns");
        console.log(totalColumns);
        // check empty columns
        for (let columnId in columns) {
            // is it empty?
            if ($(columns[columnId]).children().length < 1) {
                emptyColumns.push(columnId);
            }
        }
        // setup max of columns, 0,1 two columns only now
        maximum = maximum || 1;
        // need to reduce?
        if (totalColumns > maximum) {
            //NOTES:Columns 2 -> 1
            for (let columnId = columns.length - 1; columnId < totalColumns; columnId++) {
                //get column
                let column = $(columns[columnId]);
                //change the order of data in right
                let fields = column.children('.field[data-field-id]').get();   //.attr('data-field-id');
                //let fields = column.children('.field[data-field-id]').get();
                console.log("fields");
                console.log(fields);
                //get the last order of the previous column
                let lastOrder = $(columns[columnId - 1]).children().get().length;//.length;
                console.log("lastOrder");
                console.log(lastOrder);
                for (let idx in fields) {
                    console.log("idx");
                    console.log(idx);
                    let fieldId = fields[idx].getAttribute('data-field-id');
                    //let fieldId = fields[idx].attributes['data-field-id'];
                    console.log("fieldId");
                    console.log(fieldId);
                    for (let i in data) {
                        //console.log("data[i].order");
                        //console.log(data[i].order);
                        if (data[i].id == fieldId) {
                            console.log(" ***************** fieldId ******************");
                            data[i].order = data[i].order.substr(0, 4) + (columnId - 1) + '-' + lastOrder;
                            lastOrder += 1;
                            console.log(data[i].order);
                            break; //Stop this loop, we found it!
                        }
                    }
                }
                //move fields to previous column //appendTo: Insert every element in the set of matched elements to the end of the target.
                column.children().appendTo(columns[columnId - 1]);
                //delete column
                column.empty().removeData().remove();
            }
            // need to create
        } else if (totalColumns < maximum) {
            //NOTES:Columns 1 -> 2
            // start from the top
            for (let columnId = 0; columnId < maximum; columnId++) {
                // get column
                let column = container.children(':eq(' + columnId + ')');
                // need to create?
                if (column.length < 1) {
                    // create column
                    column = $('<ul class="column input-column" data-column-id="' + columnId + '"></ul>');
                    // append to container
                    column.appendTo(container);
                    // new column should be a empty column
                    emptyColumns.push(columnId);
                }
            }
        }
        // return status
        return emptyColumns.length >= maximum;
    }
    backClick(e) {
        if (this.props.page == true) {
            $('#Itabs .tab[data-page-id="' + (this.state.pageId - 1) + '"] a').trigger('click');
        } else {
            $('#tabs .tab[data-page-id="' + (this.state.pageId - 1) + '"] a').trigger('click');
        }
    }
    nextClick(e) {
        console.log("this.state.page");
        console.log(this.state.pageId);
        console.log("this.state.totalPages");
        console.log(this.state.totalPages);
        if (this.props.page == true) {
            $('#Itabs .tab[data-page-id="' + (this.state.pageId + 1) + '"] a').trigger('click');
        } else {
            $('#tabs .tab[data-page-id="' + (this.state.pageId + 1) + '"] a').trigger('click');
        }
    }
    disableBackNext() {
        // disable back and next if necessary
        console.log("this.state.page in disableBackNext");
        console.log(this.state.pageId);
        // console.log("this.state.totalPages in disableBackNext");
        // console.log(this.state.totalPages);
        let totalPages = null;
        let pages = null;
        if (this.props.page == true) {
            totalPages = $('#Ipages').children('.page').length;
            pages = $('#Ipages').children('.page');
        } else {
            totalPages = $('#pages').children('.page').length;
            pages = $('#pages').children('.page');
        }
        for (let i = 0; i < pages.length; i++) {
            let fields = $(pages[i]).find('.field');
            let hiddenFields = $(pages[i]).find('.field[style*="display: none"]');
            if (fields.length == hiddenFields.length) { totalPages = totalPages - 1; }
        }
        this.setState({ totalPages: totalPages }, () => { this.forceUpdate() });
        // let totalPages = $('#pages').children('.page').length;
        console.log("totalPages in disableBackNext");
        console.log(totalPages);
        //let totalPages = this.state.totalPages;
        let pageId = this.state.pageId;
        if (pageId == 0) {
            if (this.props.page == true) {
                $('#IbackBtn').addClass('disabled-button');
            } else {
                $('#backBtn').addClass('disabled-button');
            }
            // $('#backBtn').addClass('disabled-button');
        } else {
            if (this.props.page == true) {
                $('#IbackBtn').removeClass('disabled-button');
            } else {
                $('#backBtn').removeClass('disabled-button');
            }
            // $('#backBtn').removeClass('disabled-button');
        }

        if (totalPages == pageId + 1) {
            console.log("totalPages == pageId + 1");
            if (this.props.page == true) {
                console.log("this.props.page == true");
                $('#InextBtn').addClass('disabled-button');
            } else {
                console.log("this.props.page != true");
                $('#nextBtn').addClass('disabled-button');
            }
        } else {
            console.log("totalPages != pageId + 1");
            if (this.props.page == true) {
                $('#InextBtn').removeClass('disabled-button');
            } else {
                $('#nextBtn').removeClass('disabled-button');
            }
            // $('#nextBtn').removeClass('disabled-button');
        }
    }
    formatPhone(tel) {
        //char = { 0: '(+', 3: ') ', 7: ' ' }; //NOTES: For next version, have country code, become  (+852)1234 5678
        tel = String(tel);
        let numbers = tel.replace(/\D/g, ''),
            char = { 4: ' ' };
        tel = '';
        for (let i = 0; i < numbers.length; i++) {
            tel += (char[i] || '') + numbers[i];
        }
        return tel
    }
    upload(id) {
        console.log("document.querySelector('input[type=file]')");
        console.log(document.querySelector('input[type=file]'));
        console.log("this.state.fileName");
        console.log(this.state.fileName);
        let photos = null;
        if (document.querySelector('input[type=file]')) {
            photos = document.querySelector('input[type=file]').files;
            let fileName = Object.assign({}, this.state.fileName);
            let name = ""
            for (let i = 0; i < photos.length; i++) {
                fileName[id] += photos[i].name;
            }
            this.setState({ fileName: fileName })
        }
        console.log(" ======= photos ======= ");
        console.log(photos);
        return false;
    }
    // ------------------------------- /handlers ------------------------------- /
    // ------------------------------- interface ------------------------------- //
    render() {
        let oThis = this;
        console.log("this.state in render of input form view");
        console.log(this.state);
        let spaceFrom = this.formatPhone(this.state.from || this.state.details.fm || "");
        let spaceTo = this.state.to == "" ? "" : this.formatPhone(this.state.to || this.state.details.to || "");
        let idPrefix = this.props.page == true ? "I" : ""
        let title = this.props.page == true ? i18n.make_a_call : i18n.call_result

        // reder view
        // let data = this.state.data.slice();
        // data = data.sort(function (a, b) {
        //     return (a.order).localeCompare(b.order);
        //     //return a.order.localeCompare(b) < 0;
        // });
        //this.renderView(data);

        return (
            <div id={idPrefix + 'input-form'} className='input-form-container custom-scroll'>
                <div className='input-body-container'>
                    <div className='input-form-body' id='input-from-edit'>
                        <div id={idPrefix + "viewer"} className='scroll-area'>
                            <div className='pages input-form-card'>
                                <h3>{title}</h3>
                                {this.props.recId != -1 ?
                                    <div>
                                        {this.props.page == true ? null :
                                            <div className='campaign-title'>
                                                <h3>{i18n.campaign_name_label} {this.props.cname || oThis.state.details.cname}</h3>
                                            </div>}
                                        <div>
                                            <span> From:&nbsp;<span className='font-weight-bold'>{this.fromName}&nbsp;</span>({spaceFrom})</span><span className='right'>To:&nbsp;<span className='font-weight-bold'>{this.props.full}&nbsp;</span>({spaceTo})</span>
                                        </div></div> : null}
                                <hr className="if-hr" />
                                <div className='row if-page-font'><span className='right'>Page&nbsp;{this.state.pageId + 1}&nbsp;of&nbsp;{this.state.totalPages}</span></div>
                                <div className='row hidden'>
                                    <div className='page-controls right'>
                                        <button id={idPrefix + 'backBtn'} className='disabled-button' onClick={this.backClick.bind(oThis)}>Back</button>
                                        <span id={idPrefix + "tabs"}>
                                        </span>
                                        <button className='right' id={idPrefix + 'nextBtn'} onClick={this.nextClick.bind(oThis)}>Next</button>
                                    </div>
                                </div>
                                <div className='if-border-del' id={idPrefix + "pages"} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    // ------------------------------- delegate ------------------------------- //
    componentWillUnmount() {
        //NOTES: if not destroy all CKEDITOR instances, when refreshing the page will have error P.S. warning is still here by ckeditor
        if (this.state.ckeditorActive == true) {
            if (this.state.ckeditorActive == true) {
                let textAreas = $(".ckeditor");
                for (let i = 0; i < textAreas.length; i++) {
                    let input = $(textAreas[i]);
                    let editor = $(textAreas[i]).ckeditorGet();
                    let editorName = editor.name;
                    if (typeof CKEDITOR != 'undefined') { //NOTES: CKEDITOR is Object now
                        let cki = CKEDITOR.instances[editorName];
                        cki ? cki.destroy() : null;
                    }
                }
                console.log("textAreas");
                console.log(textAreas);
            }
        }
        if (this.props.page == true) {
            $('#Ipages').find('.Ipage').remove();
            $('#Itabs').find('.Itab').remove();
        } else {
            $('#pages').find('.page').remove();
            $('#tabs').find('.tab').remove();
        }

        // ------------------------------- /delegate ------------------------------- //
    }
}
