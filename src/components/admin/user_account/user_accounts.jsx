/*************************************************************************************************
* Notes
* Name: user_accounts.jsx
* Class: UserAccounts
* Details: Inside User Account feature, User Accounts function
*************************************************************************************************/
import React, { Component } from 'react';
import ReactFileReader from 'react-file-reader';
import ITable from '../../common/ITable.jsx'
import AlertBox from '../../common/AlertBox.jsx'
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.user_account_variable;

export default class UserAccounts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tblData: [],
			showPopup: false,
			isCreate: true,
			//POP-UP
			userDetails: {},
			imgUrl: "",
			name: "",
			email: "",
			pw: "",
			pw2: "",
			selectedId: -1,
			selectedIdIdx: -1,
			roleId: -1,
			roleName: "",
			showConfirm: false,
			showAlert: false,
			alertMsg: "",
			radioList: [],
			file: null
		}
	}
	componentWillMount() {
		this.loadTblData();
		this.columns = [
			{ id: "name", name: i18n.name },
			{ id: "email", name: i18n.email },
			{ id: "role", name: i18n.role },
			{
				id: "icon", name: i18nCommon.acction, type: 'icon', className: 'icon-edit-16', onClick: (index, data) => {
					this.tblRowSelected(index, data);
				}
			}
		];
	}
	tblRowSelected(index, data) {
		//TO DO: load userDetails from server
		// ==== TBD ====
		let roleId = -1;
		switch (data.role) {
			case "Super Administrator":
				roleId = 1;
				break;
			case "Administrator":
				roleId = 2;
				break;
			case "Team Leader":
				roleId = 3;
				break;
			case "Agent":
				roleId = 4;
				break;
		}
		console.log("data.id == 1");
		console.log(data.id == 1);
		let userDetails = {
			imgUrl: data.id == 1 || data.id == 3 ? 'assets/imgs/may_chan.jpg' : "",
			userName: data.name ? data.name.replace(/\s+/g, '').toLowerCase() : "",
			pw: 1234567,
			pw2: 12345672,
			roleId: roleId
		}
		// ==== /TBD ====
		console.log("userDetails");
		console.log(userDetails);
		this.setState({ showPopup: true, isCreate: false, name: data.name, selectedId: data.id, selectedIdIdx: index, roleName: data.role, roleId: userDetails.roleId, imgUrl: userDetails.imgUrl, userName: userDetails.userName, pw: userDetails.pw, pw2: userDetails.pw, email: data.email });
	}
	loadTblData() {
		// TO DO: load table data from server
		let tblData = [
			{ id: 1, name: "Kwong Wong", email: "kwong@gmail.com", role: "Super Administrator" },
			{ id: 2, name: "Simon Tan", email: "simontan@gmail.com", role: "Agent" },
			{ id: 3, name: "Alexander Ford", email: "alexandaford@gmail.com", role: "Administrator" },
			{ id: 4, name: "Eren Miranda", email: "erenmiranda@gmail.com", role: "Team Leader" },
			{ id: 5, name: "Samantha Evan", email: "samatha@gmail.com", role: "Administrator" },
			{ id: 6, name: "Barbara Sander", email: "barbara@gmail.com", role: "Administrator" },
			{ id: 7, name: "Henry Wong", email: "henrywong@gmail.com", role: "Administrator" },
			{ id: 8, name: "May Lin", email: "maylin@gmail.com", role: "Administrator" },
			{ id: 9, name: "Mia Chua", email: "miachua@gmail.com", role: "Administrator" },
			{ id: 10, name: "Kevin Kim", email: "kelvinkim@gmail.com", role: "Administrator" },
			{ id: 11, name: "Noel Cheung", email: "noelchung@gmail.com", role: "Administrator" },
			{ id: 12, name: "Rahab Chung", email: "rahabchung@gmail.com", role: "Administrator" },
			{ id: 13, name: "Selin Dion", email: "selindeon@gmail.com", role: "Administrator" },
		]
		//TO DO: get radio list from server for the pop-up radio list
		let radioList = [{ id: 1, name: "Super Administrator" },
		{ id: 2, name: "Administrator" },
		{ id: 3, name: "Team Leader" },
		{ id: 4, name: "Agent" }
		];
		this.setState({ tblData: tblData, radioList: radioList });
	}
	// =========================== POP-UP FUNCTIONS =========================== 
	popupCloseClick() {
		this.setState({ showPopup: false });
	}
	popCreateUpdateClick() {
		//verify username typed
		if (this.state.name.length == 0) {

			return;
		}

		//verify email is in correct format
		let emailValid = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(this.state.email);
		if (emailValid == false) {
			this.setState({ showAlert: true, alertMsg: i18n.please_enter_email });
			return;
		}

		//verify pw is is correct format  (no space allowed)
		let pwValid = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/.test(this.state.pw);
		console.log("pwValid");
		console.log(pwValid);

		//verify password and re-enter password is the same
		if (this.state.pw != this.state.pw2) {
			this.setState({ showAlert: true, alertMsg: i18n.password_is_not });
			return;
		}
		//verify role selected
		console.log("this.state.roleId");
		console.log(this.state.roleId);
		if (this.state.roleId == -1) {
			this.setState({ showAlert: true, alertMsg: i18n.please_select_role });
			return;
		}


		// TO DO: create or updat the user
		// TO DO: potential error: username is already exists i18n.user_already_exists, as the username is not downloaded in front-end, need to search by back-end

		//if success, update the grid Data
		let tblData = this.state.tblData.slice();
		console.log("this.state.selectedIdIdx");
		console.log(this.state.selectedIdIdx);
		console.log("tblData[this.state.selectedIdIdx]");
		console.log(tblData[this.state.selectedIdIdx]);
		tblData[this.state.selectedIdIdx].name = this.state.name;
		tblData[this.state.selectedIdIdx].email = this.state.email;
		tblData[this.state.selectedIdIdx].role = this.state.roleName;
		this.setState({ tblData: tblData, showPopup: false });
	}
	radioChange(e) {
		//let value = e.target.value;
		let radioList = this.state.radioList;
		let roleId = e.target.id;
		let roleName = ""
		for (let i = 0; i < radioList.length; i++) {
			let radioObject = radioList[i];
			if (radioObject.id == roleId) {
				roleName = radioObject.name
				break;
			}
		}

		this.setState({ roleId: roleId, roleName: roleName });
	}
	handleFiles(files) {
		let oThis = this;
		if (files.length > 0) { //NOTES: check the lengh to prevent error when user pressed cancel
			let fileName = files[0].name
			let result = fileName.match(".*\\.(png|jpg|gif)");
			if (result == null) {
				//alert(i18n.warn_photo);
				this.setState({ showAlert: true });
				return
			}
			let file = files[0];
			let reader = new FileReader();
			reader.onload = function (e) {
				oThis.setState({
					file: file,
					imgUrl: reader.result
				});
			}
			reader.readAsDataURL(file);
		}
	}
	render() {
		let oThis = this;
		// create pop-up role-list
		let radioContent = [];
		let radioList = this.state.radioList;
		for (let i = 0; i < radioList.length; i++) {
			radioContent.push(
				<div className="radio-buttons" key={"radio-part" + i}>
					<input key={"radio" + i} type="radio" name="radioList" id={radioList[i].id} value={radioList[i].naem} checked={this.state.roleName === radioList[i].name} onChange={this.radioChange.bind(oThis)} />
					<div className="check"></div>
					<label key={"radio-label" + i} className="slim-label" htmlFor={radioList[i].id}>{radioList[i].name}</label>
				</div>
			);
		}

		//Render pop-up image
		let { imgUrl } = this.state;
		let $imagePreview = null;
		console.log("IMG_URL!!!!!!!!");
		console.log(imgUrl);
		if (imgUrl) {
			console.log("IMG_URL!!!!!!!!");
			console.log(imgUrl);
			$imagePreview = (<img className="img-center cp-circle" src={imgUrl} alt="" />);
		} else {
			$imagePreview = <div className="icon-focus-24 img-center cp-circle">
			</div>
		}

		return (
			<div className='main-card'>
				<div className='content-divide'>
					<h3>{i18n.manage_registered_accounts}</h3>
					<button className='right' onClick={() => oThis.setState({ showPopup: true, isCreate: true, name: "", email: "", pw: "", pw2: "", selectedId: -1, checked: [], selectedIdIdx: -1 })}><div className='icon-add-16 i-icon i-icon-left' />{i18n.create_new_user}</button>
					<div>
						<ITable columns={oThis.columns} data={oThis.state.tblData} tableName='user-accounts' paging={true}
							columnDefs={[{
								"targets": [-1],
								"orderable": false
							}
							]} onDoubleClick={(index, data) => { oThis.tblRowSelected(index, data) }} />
					</div>
				</div>

				{/* =========================== CREATE ROLE POP-UP =========================== */}
				{oThis.state.showPopup ?

					<div className='i-popup'>
						<div className='pop-up-content-small'>

							<div className='pop-header'>
								<h3 className='pop-title'>{this.state.isCreate == true ? i18n.create_an_user : i18n.update_an_user}</h3>
								<button className='pop-exit-btn right' onClick={oThis.popupCloseClick.bind(oThis)}>
									<div className="icon-simple-remove" />
								</button>
							</div>

							<div className='pop-body'>
								<label>{i18n.profile_photo}</label>
								
								{$imagePreview}
								<ReactFileReader handleFiles={oThis.props.handleFiles ? oThis.props.handleFiles : oThis.handleFiles.bind(oThis)} fileTypes={'image/*'}>
									<div className='cp-choose-pic' style={{}}>
										<a>{i18n.choose_a_photo}</a>
									</div>
								</ReactFileReader>

								<div className='row-p'>
									<label>{i18n.full_name}</label>
									<input type='text' value={oThis.state.name} onChange={event => this.setState({ name: event.target.value })} />
								</div>

								<div className='row-p'>
									<label>{i18n.email_address}</label>
									<input type='text' value={oThis.state.email} onChange={event => this.setState({ email: event.target.value })} />
								</div>

								<div className='row-p'>
									<label>{i18n.user_name}</label>
									<input type='text' value={oThis.state.userName} onChange={event => this.setState({ userName: event.target.value })} />
								</div>

								<div className='row-p'>
									<label>{i18n.password}</label>
									<input type='text' value={oThis.state.pw} onChange={event => this.setState({ pw: event.target.value })} />
								</div>

								<div className='row-p'>
									<label>{i18n.reenter_password}</label>
									<input type='text' value={oThis.state.pw2} onChange={event => this.setState({ pw2: event.target.value })} />
								</div>

								<div className='row-p'>
									{radioContent}
								</div>

								{this.state.isCreate ? null :
									<div className='row-p'>
										<button onClick={() => {
											console.log("delete clicked");
											oThis.setState({ showConfirm: true })
										}}>{i18n.delete_user}</button>
									</div>
								}
							</div>

							<div className="bottom-nav clearfix">
								<button
									className="btn-half"
									onClick={() => oThis.setState({ showPopup: false })}>{i18nCommon.cancel}</button>
								<button
									className="btn-half"
									onClick={oThis.popCreateUpdateClick.bind(oThis)}>
									{this.state.isCreate == true ? i18n.create_user : i18n.update_user}</button>
							</div>

						</div>
						<AlertBox
							// setting of show confirm
							confirmMsg={i18n.delete_user_ques}
							showConfirm={oThis.state.showConfirm}
							confirmClick={() => {
								// TO DO: send selectedId to server
								console.log("this.state.selectedId");
								console.log(this.state.selectedId);
								// TO DO: when server replied success, splice the record from table
								let tblData = oThis.state.tblData.slice();
								tblData.splice(this.state.selectedIdIdx, 1);
								oThis.setState({ showPopup: false, tblData: tblData });
							}}
							cancelClick={() => oThis.setState({ showConfirm: false })}
							// setting of show alert
							showAlert={oThis.state.showAlert}
							alertMsg={this.state.alertMsg}
							alertClick={() => { oThis.setState({ showAlert: false }) }}
						/>
					</div>
					: null}

			</div>
		);
	}
}