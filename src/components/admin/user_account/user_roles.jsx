/*************************************************************************************************
* Notes
* Name: user_roles.jsx
* Class: UserRoles
* Details: Inside User Account feature, User Roles function, including its pop-up
*************************************************************************************************/
import React, { Component } from 'react';
import ITable from '../../common/ITable.jsx'
import AlertBox from '../../common/AlertBox.jsx'
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.user_account_variable;

export default class UserRoles extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);

		this.state = {
			data: [],
			showPopup: false,
			isCreate: true,
			//POP-UP
			name: "",
			selectedId: -1,
			selectedIdIdx: -1,
			checked: [],
			showConfirm: false,
			showAlert: false,
			alertMsg: ""
		}

	}
	componentWillMount() {
		let oThis = this;
		// TO DO: get roles and its capable features
		let data = [{ id: 1, name: "Super Administrator", checked: ["all"] },
		{ id: 2, name: "Administrator", checked: ["config", "upload", "manage"] },
		{ id: 3, name: "Team Leader", checked: ["config", "upload", "manage"] },
		{ id: 4, name: "Agent", checked: ["config", "upload", "manage"] }
		];
		this.setState({ data: data });
		this.columns = [
			{ id: "name", name: i18n.role_name },
			{
				id: "icon", name: i18nCommon.acction, type: 'icon', className: 'icon-edit-16', onClick: (index, data) => {
					this.tblRowSelected(index, data);
				}
			}
		];
	}
	tblRowSelected(index, data) {
		this.setState({ showPopup: true, isCreate: false, name: data.name, selectedId: data.id, checked: data.checked, selectedIdIdx: index })
	}
	// =========================== POP-UP FUNCTIONS =========================== 
	popupCloseClick() {
		this.setState({ showPopup: false });
	}
	checkChange(type, e) {
		let isChecked = e.target.checked;
		let checked = this.state.checked.slice();
		let index = checked.indexOf(type);
		if (isChecked == true) {
			// add type to array if not yet exists
			if (index == -1) {
				checked.push(type);
			}
		} else {
			// remove if not yet checked
			if (index > -1) {
				// remove from array
				checked.splice(index, 1);
			}
		}
		this.setState({ checked: checked });
	}
	checkAll(type, e) {
		console.log("CheckAll");
		console.log("type");
		console.log(type);
		let isChecked = e.target.checked;
		let checked = this.state.checked.slice();
		let loop = [];
		switch (type) {
			case "adddress":
				loop = ["config", "upload", "manage"];
				break;
			case "campaign":
				loop = ["map", "assign"];
				break;
			case "UserAccount":
				loop = ["Roles", "Accounts"];
				break;
			default:
				loop = ["config", "upload", "manage"];
		}
		if (isChecked == true) {
			// add type to array if not yet exists
			for (let i = 0; i < loop.length; i++) {
				if (checked.indexOf(loop[i]) == -1) { checked.push(loop[i]); }
			}
		} else {
			// remove if not yet checked
			for (let i = 0; i < loop.length; i++) {
				let index = checked.indexOf(loop[i]);
				if (index > -1) { checked.splice(index, 1); }
			}

		}
		console.log("checked");
		console.log(checked);
		this.setState({ checked: checked });
	}
	popCancel() {
		this.setState({ showPopup: false });
	}
	popCreateUpdateClick() {
		console.log(this.state);
		console.log(this.state);
		// verify filled role title is not empty
		if (this.state.name.length == 0) {
			this.setState({ showAlert: true, alertMsg: i18n.please_fill_title });
			return;
		}
		// verify role title not already exists
		let data = this.state.data;
		let name = this.state.name;
		for (let i = 0; i < data.length; i++) {
			if (data[i].name == name && this.state.selectedIdIdx != i) {
				this.setState({ showAlert: true, alertMsg: i18n.title_already_exists });
				return
			}

		}
		// Send this.state.selectedId, this.state.checked, this.state.name to server to create or update role (determinded by this.state.isCreate)
		// for new create reload grid
		this.setState({ showPopup: false });
	}
	// ------------------------------- /delegate ------------------------------- //
	render() {
		let oThis = this;
		// =========================== POP-UP SETTINGS ===========================
		let isAddAllChecked = (this.state.checked.indexOf("config") > -1 && this.state.checked.indexOf("upload") > -1 && this.state.checked.indexOf("manage") > -1) ? true : false
		let isCampaignAllChecked = (this.state.checked.indexOf("map") > -1 && this.state.checked.indexOf("assign") > -1) ? true : false
		let isUserAllChecked = (this.state.checked.indexOf("Roles") > -1 && this.state.checked.indexOf("Accounts") > -1) ? true : false

		return (
			<div id='user_roles' className='main-card'>
				<div className='content-divide'>
					<h3>{i18n.manage_registered_roles}</h3>
					<button className='right' onClick={() => oThis.setState({ showPopup: true, isCreate: true, name: "", selectedId: -1, checked: [], selectedIdIdx: -1 })}><div className='icon-add-16 i-icon i-icon-left' />{i18n.create_a_role}</button>
					<div>
						<ITable columns={oThis.columns} data={oThis.state.data} tableName='user-roles'
							columnDefs={[{
								"targets": [1],
								"orderable": false
							}
							]} onDoubleClick={(index, data) => { oThis.tblRowSelected(index, data) }} />
					</div>
				</div>

				{/* =========================== CREATE ROLE POP-UP =========================== */}
				{oThis.state.showPopup ?

					<div className='i-popup'>
						<div className='pop-up-content-small'>

							<div className='pop-header'>
								<h3 className='pop-title'>{this.state.isCreate == true ? i18n.create_a_role : i18n.update_a_role}</h3>
								<button className='pop-exit-btn right' onClick={oThis.popupCloseClick.bind(oThis)}>
									<div className="icon-simple-remove" />
								</button>
							</div>

							<div className='pop-body'>
								<div className='row-p'>
									<label>{i18n.role_title_label}</label>
									<input type='text' value={oThis.state.name} onChange={event => this.setState({ name: event.target.value })} />
								</div>

								<div className='row-p'>
									<label>{i18n.choose_features_for}</label>
								</div>

								<div className='row-p'>
									{/* === Address Book === */}

									<div className='role-group'>
										<div className='role-title'>
											<input type="checkbox" id="address" value="address" checked={isAddAllChecked} onChange={oThis.checkAll.bind(oThis, "address")} />
											<label className="slim-label bold" htmlFor="address">{i18n.address_book_features}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="config" value="config" checked={this.state.checked.indexOf("config") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "config")} />
											<label className="slim-label" htmlFor="config">{i18n.config_customer_profile}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="upload" value="upload" checked={this.state.checked.indexOf("upload") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "upload")} />
											<label className="slim-label" htmlFor="upload">{i18n.upload_address_book}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="manage" value="manage" checked={this.state.checked.indexOf("manage") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "manage")} />
											<label className="slim-label" htmlFor="manage">{i18n.manage_customer_profile}</label>
										</div>
									</div>
									{/* === Input Form === */}
									<div className='role-group'>
										<div className='role-option-container'>
											<input type="checkbox" id="InputForm" value="InputForm" checked={this.state.checked.indexOf("InputForm") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "InputForm")} />
											<label className="slim-label bold" htmlFor="InputForm">{i18n.input_form_features}</label>
										</div>
									</div>
									{/* === Campaign List === */}
									<div className='role-group'>
										<div className='role-title'>
											<input type="checkbox" id="campaign" value="campaign" checked={isCampaignAllChecked} onChange={oThis.checkAll.bind(oThis, "campaign")} />
											<label className="slim-labe bold" htmlFor="campaign">{i18n.campaign_list_features}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="map" value="map" checked={this.state.checked.indexOf("map") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "map")} />
											<label className="slim-label" htmlFor="map">{i18n.map_to_input_form}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="assign" value="assign" checked={this.state.checked.indexOf("assign") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "assign")} />
											<label className="slim-label" htmlFor="assign">{i18n.assign_campaign_list}</label>
										</div>
									</div>
									{/* === User Account === */}
									<div className='role-group'>
										<div className='role-title'>
											<input type="checkbox" id="UserAccount" value="UserAccount" checked={isUserAllChecked} onChange={oThis.checkAll.bind(oThis, "UserAccount")} />
											<label className="slim-label bold" htmlFor="UserAccount">{i18n.user_account_features}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="Roles" value="Roles" checked={this.state.checked.indexOf("Roles") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "Roles")} />
											<label className="slim-label" htmlFor="Roles">{i18n.user_roles}</label>
										</div>
										<div className='role-option-container'>
											<input type="checkbox" id="Accounts" value="Accounts" checked={this.state.checked.indexOf("Accounts") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "Accounts")} />
											<label className="slim-label" htmlFor="Accounts">{i18n.user_accounts}</label>
										</div>
									</div>
									{/* === Inbound === */}
									<div className='role-group'>
										<div className='role-option-container'>
											<input type="checkbox" id="InboundHome" value="InboundHome" checked={this.state.checked.indexOf("InboundHome") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "InboundHome")} />
											<label className="slim-label bold" htmlFor="InboundHome">{i18n.inbound_features}</label>
										</div>
									</div>
									{/* === Outbuond === */}
									<div className='role-group'>
										<div className='role-option-container'>
											<input type="checkbox" id="OutbuondHome" value="OutbuondHome" checked={this.state.checked.indexOf("OutbuondHome") > -1 ? true : false} onChange={oThis.checkChange.bind(oThis, "OutbuondHome")} />
											<label className="slim-label bold" htmlFor="OutbuondHome">{i18n.outbound_features}</label>
										</div>
									</div>
								</div>

								{this.state.isCreate ? null :
									<div className='row-p'>
										<button onClick={() => {
											console.log("delete clicked");
											oThis.setState({ showConfirm: true })
										}}>{i18n.delete_role}</button>
									</div>
								}
							</div>

							<div className="bottom-nav clearfix">
								<button
									className="btn-half"
									onClick={this.popCancel.bind(oThis)}>{i18nCommon.cancel}</button>
								<button
									className="btn-half"
									onClick={this.popCreateUpdateClick.bind(oThis)}>
									{this.state.isCreate == true ? i18n.create_role : i18n.update_role}</button>
							</div>

						</div>
						<AlertBox
							// setting of show confirm
							confirmMsg={i18n.delete_role_ques}
							showConfirm={oThis.state.showConfirm}
							confirmClick={() => {
								// TO DO: send selectedId to server
								console.log("this.state.selectedId");
								console.log(this.state.selectedId);
								// TO DO: when server replied success, splice the record from table
								let data = oThis.state.data.slice();
								data.splice(this.state.selectedIdIdx, 1);
								oThis.setState({ showPopup: false, data: data });
							}}
							cancelClick={() => oThis.setState({ showConfirm: false })}
							// setting of show alert
							showAlert={oThis.state.showAlert}
							alertMsg={this.state.alertMsg}
							alertClick={() => { oThis.setState({ showAlert: false }) }}
						/>
					</div>
					: null}


			</div>
		);
	}
}