/*************************************************************************************************
* Notes
* Name: user_account_home.jsx
* Class: UserAccountHome
* Details: Left Panel in User Account Features
*************************************************************************************************/
import React, { Component } from 'react';
import Sidebar from '../../common/sidebar.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.user_account_variable;

export default class UserAccountHome extends Component {
	render() {
		let details = {
			title: i18n.user_settings, linkArray: [
				{ link: "/home/UserAccount/Roles", icon: 'icon-multiple-stroke-24', name: i18n.user_roles },
				{ link: "/home/UserAccount/Accounts", icon: 'icon-user-stroke-24', name: i18n.user_accounts }
			]
		}
		return (
			<div>
				<Sidebar details={details} childrenContent={this.props.children} />
			</div>
		);
	}
}