import ITable from '../../common/ITable.jsx'
import React, { Component } from 'react';
import i18nCommon from '../../common/i18n.jsx';
import AlertBox from '../../common/AlertBox.jsx';
let i18n = i18nCommon.config_cust_variable;

export default class ConfigCustProfile extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        super(props);
        this.state = {
            checkedID: [],
            customNameChange: [],
            fields: [],
            customField: [],
            showAlert: false,
            alertMsg: "",
            // ========= TO BE UNCOMMENTED ==========
             reloadTable: false
            // ========= /TO BE UNCOMMENTED ==========
            // ========= TBD ==========
            //reloadTable: true
            // ========= /TBD ==========

        }
    }
    componentWillMount() {
        let oThis = this;

        //GET JSON, the fields here
        // ========= TO BE UNCOMMENTED ==========
        this.getData = jQuery.ajax({
            method: 'GET',
            url: 'http://api.wiser/fields',
            cache: false,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "x-requested-with"
            },
            data: {
                "company_id": sessionStorage.getItem("company_id")
            },
            success: function (res) {
                console.log("res.jsonData");
                console.log(res.jsonData);
                let jsonData = res.jsonData;
                if (jsonData != null) {
                    oThis.setState({ fields: jsonData.fixed, customField: jsonData.custom }, () => { oThis.setState({ reloadTable: true }) });
                }
            }
        });
        // ========= /TO BE UNCOMMENTED  ==========
        // ========= TBD ==========
        // let fields = [
        //     { name: "Full Name", type: "Alphanumeric", checked: false },
        //     { name: "First Name", type: "Alphanumeric", checked: false },
        //     { name: "Last Name", type: "Alphanumeric", checked: false },
        //     { name: "Personal Identification", type: "Alphanumeric", checked: false },
        //     { name: "Email Address", type: "Email", checked: false },
        //     { name: "Mobile Phone Number", type: "Mobile Phone Number", checked: false },
        //     { name: "Home Phone Number", type: "Home Phone Number", checked: false },
        //     { name: "Work Phone Number", type: "Work Phone Number", checked: false },
        //     { name: "Other Phone Number", type: "Other Phone Number", checked: false },
        //     { name: "Adress Line 1", type: "Alphanumeric", checked: false },
        //     { name: "Adress Line 2", type: "Alphanumeric", checked: false },
        //     { name: "Adress Line 3", type: "Alphanumeric", checked: false },
        //     { name: "Country", type: "Alphanumeric", checked: true },
        //     { name: "Revision Date", type: "Alphanumeric", checked: false }
        // ]
        // let customField = //originally should get from database, Query the fixed = false
        //     [
        //         { name: "Reference Number", type: "Digit", checked: true },
        //         { name: "Education", type: "Alphanumeric", checked: false }
        //     ];
        // this.setState({ fields: fields, customField: customField });
        // ========= /TBD ==========

        this.firstColumns = [
            { id: "name", type: '', name: i18n.fixed_field, flex: 3 },
            { id: "type", name: i18n.field_type, flex: 4 },
            {
                id: "checked", type: 'switch', name: i18n.hide, flex: 1, onChange: function (o, checked) {
                    let newArray = oThis.state.fields.slice();
                    newArray[o].checked = checked;
                    oThis.setState({ fields: newArray });
                }
            }
        ];

        this.secondColumns = [
            {
                id: "name", type: 'textField', name: i18n.display_name, flex: 3, onChange: function (o, event) {
                    let newArray = oThis.state.customField;
                    newArray[o].name = event.target.value
                    oThis.setState({ customField: newArray });

                }
            },
            {
                id: "type", name: i18n.field_type, flex: 4, type: 'combo', valueField: 'name', displayField: 'name', options: i18n.type_array, onChange: (val, i) => {
                    let _gridData = this.state.customField.slice();
                    if (val != null) {
                        _gridData[i].type = val.value;
                    } else {
                        _gridData[i].type = "";
                    }
                    this.setState({ customField: _gridData });
                }
            },
            {
                id: "checked", type: 'switch', name: i18n.hide, flex: 1, onChange: function (idx, checked) {
                    //let value = event.target.checked;
                    let newArray = oThis.state.customField.slice();
                    newArray[idx].checked = checked;
                    oThis.setState({ customField: newArray });
                }
            }
        ];
    }
    componentDidMount() {
        let oThis = this;

        //let oThis = this;



    }
    componentWillUnmount() {
        // ========= TO BE UNCOMMENTED ==========
        // this.getData.abort();
        // ========= /TO BE UNCOMMENTED ==========
    }
    // ------------------------------- /delegate ------------------------------- //
    // ------------------------------- handlers ------------------------------- //
    addClick() {
        let newArray = this.state.customField.slice();
        newArray.push({ id: -1, name: "", checked: false });
        this.setState({ customField: newArray });
    }
    submitClick(e) {
        e.preventDefault();
        console.log("Fields: ");
        console.log(this.state.fields);
        console.log("customField: ");
        console.log(this.state.customField);

        // VERIFY is that Custom Field all typed and Selected
        let customField = this.state.customField;
        for (let i = 0; i < customField.length; i++) {
            console.log("load here");
            let record = customField[i];
            if (record.name == "" || record.type == "") {
                console.log("warning here");
                this.setState({ showAlert: true, alertMsg: i18n.all_custom_typed });
                return
            }
        }

        // VERIFY no Duplicate name for both fixed fields and custom fields
        let combinedArr = this.state.fields.slice().concat(this.state.customField.slice());
        console.log("combinedArr");
        console.log(combinedArr);
        for (let i = 0; i < combinedArr.length; i++) {
            let name = combinedArr[i].name;
            for (let j = 0; j < combinedArr.length; j++) {
                if (i != j) {
                    if (combinedArr[j].name == name) {
                        this.setState({ showAlert: true, alertMsg: i18n.name_duplicated });
                        return
                    }
                }
            }
        }

        // post to server 
        let data = JSON.stringify({
            "company_id": sessionStorage.getItem("company_id"),
            "fixed": this.state.fields,
            "custom": this.state.customField
        })

        jQuery.ajax({
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json"
            },
            url: 'http://api.wiser/fields',
            crossDomain: true,
            contentType: "application/json",
            dataType: 'json',
            data:
                JSON.stringify({
                    "company_id": sessionStorage.getItem("company_id"),
                    "fixed": this.state.fields,
                    "custom": this.state.customField
                })
            , dataType: 'json'
        }).always(function (res) {
            console.log("res");
            console.log(res);
            let status = res.status;
            if (status == "success") {
                oThis.setState({ showAlert: true, alertMsg: i18n.config_submitted });
            }
        });
    }
    // ------------------------------- /handlers ------------------------------- //
    // ------------------------------- interface ------------------------------- //
    render() {
        let oThis = this;
        return (
            <div ref="cfg_cus_pro" id='cfg_cus_pro' className='main-card'>
                <div className='content-divide'>
                    <h3>{i18n.config_customer_profile}</h3>
                    <div>
                        <ITable columns={oThis.firstColumns} data={oThis.state.fields} tableName='config-cus-fix' reloadTable={this.state.reloadTable}
                            reloadTableTrue={() => { oThis.setState({ reloadTable: false }); }}
                            columnDefs={[{
                                "targets": -1,
                                className: 'noVis', //NOTES: no column visibility
                                "orderable": false,
                                "width": '35%'
                            }, {
                                "targets": [0, 1],
                                "width": '30%'
                            }
                            ]} />
                    </div>
                </div>

                <div className='content-divide'>
                    <h3>{i18n.custom_field}</h3>
                    <div>
                        <ITable columns={oThis.secondColumns} data={oThis.state.customField} tableName='custom' reloadTable={this.state.reloadTable}
                            reloadTableTrue={() => { oThis.setState({ reloadTable: false }); }}
                            columnDefs={[{
                                "targets": -1,
                                className: 'noVis', //NOTES: no column visibility
                                "orderable": false,
                                "width": '35%'
                            }, {
                                "targets": [0, 1],
                                "width": '30%'
                            }

                            ]} />
                    </div>
                    <button className='button small' onClick={oThis.addClick.bind(oThis)}><div className='icon-add-16 i-icon i-icon-left' />{i18n.add_customer_field}</button>
                </div>

                <div className='content-ender'>
                    <button onClick={oThis.submitClick.bind(oThis)}><div className='icon-disk-16 i-icon i-icon-left' />{i18n.submit}</button>
                </div>
                <AlertBox alertMsg={this.state.alertMsg} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
            </div>
        );
    }
    // ------------------------------- /interface ------------------------------- //
}//);





































