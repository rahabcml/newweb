import React, { Component } from 'react';
import Sidebar from '../../common/sidebar.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.address_book_variable;

export default class Address extends Component {
	render() {
		let details = {
			title: i18n.address_book, linkArray: [
				{ link: "/home/address/config", icon: 'icon-edit-stroke-24', name: i18n.config_customer_profile },
				{ link: "/home/address/upload", icon: 'icon-contact-stroke-24', name: i18n.upload_address_book },
				{ link: "/home/address/manage", icon: 'icon-search-stroke-24', name: i18n.manage_customer_profile }
			]
		}
		return (
			<div>
				<Sidebar details={details} childrenContent={this.props.children} />
			</div>
		);
	}
}