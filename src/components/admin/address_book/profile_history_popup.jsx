import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.cust_profile_variable;
import $ from 'jquery';
import foundation from 'foundation-sites';
import ReactFileReader from 'react-file-reader';
import CustomerProfile from './customer_profile.jsx';
import ContactHistory from './contact_history.jsx';

export default class ProHisPop extends Component {
    constructor(props) {
        super(props);
        let data = this.props.data;
        this.state = {
            cusId: data.id || -1,
            full: data.full || "",
            first: data.first || "",
            last: data.last || "",
            pid: data.pid || "",
            email: data.email || "",
            mobile: data.mobile || "",
            home: data.home || "",
            work: data.work || "",
            other: data.other || "",
            add1: data.add1 || "",
            add2: data.add2 || "",
            add3: data.add3 || "",
            country: data.country || "",
            source: data.source || "",
            source: data.source || "",
            file: this.props.file || '',
            imagePreviewUrl: this.props.imagePreviewUrl || ''
        }
    }

    componentWillReceiveProps(nextProps) {
        let data = nextProps.data;
        this.setState({
            cusId: data.id || -1,
            full: data.full || "",
            first: data.first || "",
            last: data.last || "",
            pid: data.pid || "",
            email: data.email || "",
            mobile: data.mobile || "",
            home: data.home || "",
            work: data.work || "",
            other: data.other || "",
            add1: data.add1 || "",
            add2: data.add2 || "",
            add3: data.add3 || "",
            country: data.country || "",
            source: data.source || "",
            source: data.source || "",
            file: data.file || '',
            imagePreviewUrl: data.imagePreviewUrl || ''
        })
    }

    componentDidMount() {
        $('#cus-tabs').foundation();
    }

    handler(value, field) {
        console.log("value,field");
        console.log(value, field);
        let obj = {};
        obj[field] = value;
        this.setState(obj);
    }

    handleFiles(files) {
        console.log("files");
        console.log(files);
        let oThis = this;
        if (files.length > 0) { //prevent error when user pressed cancel
            console.log("files.length");
            console.log(files.length);
            let fileName = files[0].name
            console.log("fileName");
            console.log(fileName);
            let result = fileName.match(".*\\.(png|jpg|gif)");
            if (result == null) {
                alert(i18n.warn_photo);
                return
            }
            let file = files[0];
            let reader = new FileReader();
            reader.onload = function (e) {
                // if (oThis.props.handler) {
                //     oThis.props.handler(file, 'file')
                //     oThis.props.handler(reader.result, 'imagePreviewUrl')
                // } else {
                oThis.setState({
                    file: file,
                    imagePreviewUrl: reader.result
                });
                // }
            }
            reader.readAsDataURL(file);
        }
    }

    epConfirmClick() {
        let oThis = this;
        let epState = Object.assign({}, oThis.state);
        this.props.epConfirmClick(oThis.state);
    }

    epCancelClick() {
        this.props.epCancelClick();
    }

    render() {
        let oThis = this;
        return (
            <div>
                <div className='i-popup'>
                    <div className='pop-up-content'>

                        <div className='pop-header'>
                            <h3 className='pop-title'>{i18n.manage_customer}</h3>
                            <button className='pop-exit-btn right' onClick={oThis.epCancelClick.bind(oThis)}>
                                <div className="icon-simple-remove" />
                            </button>
                        </div>

                        <div className='pop-body custom-scroll'>
                            <div className='tabs-container'>
                                <ul className="tabs" data-tabs id="cus-tabs">
                                    <li className="tabs-title right is-active"><a href="#panel1" aria-selected="true">{i18n.customer_profile}</a></li>
                                    <li className="tabs-title right"><a data-tabs-target="panel2" href="#panel2">{i18n.contact_history}</a></li>
                                </ul>
                            </div>

                            <div className="tabs-content" data-tabs-content="cus-tabs">
                                <div className="tabs-panel is-active" id="panel1">
                                    <CustomerProfile data={this.state} handler={oThis.handler.bind(oThis)} handleFiles={oThis.handleFiles.bind(oThis)} epConfirmClick={oThis.epConfirmClick.bind(oThis)} epCancelClick={oThis.epCancelClick.bind(oThis)} />
                                </div>
                                <div className="tabs-panel" id="panel2">
                                    <ContactHistory cusId={this.state.cusId} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
































