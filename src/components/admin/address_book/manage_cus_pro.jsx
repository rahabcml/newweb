import React, { Component } from 'react';
import ITable from '../../common/ITable.jsx'
import ICombo from '../../common/ICombo.jsx'
import IPaging from '../../common/IPaging.jsx'
import ProHisPop from './profile_history_popup.jsx';
import i18nCommon from '../../common/i18n.jsx';
import JSONToCSVConvertor from '../../common/JSONToCSVConvertor.jsx'
let i18n = i18nCommon.manage_cust_variable;
import $ from 'jquery';
import foundation from 'foundation-sites';
import DataTable from 'datatables.net';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-buttons';
import 'datatables.net-buttons-dt/css/buttons.dataTables.css';
import 'datatables.net-buttons/js/buttons.colVis.min.js';

export default class ManageCustomerProfile extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        super(props);
        this.reveal = null;
        this.state = {
            simpleSearch: true,
            simpleText: "",
            showResult: false,
            //total: 0,
            anyAll: "any",
            addArray: [{ condition: "", selector: "", text: "" }],
            campaignName: "",
            profileData: {},
            showProfile: false,
            queryData: {}
            //page: 1
        }
    }

    componentWillMount() {
        // ============ TBD ============
        this.condition = ["Full Name", "Address 1", "Address 2", "Address 3", "Work Phone Number", "Other Phone Number", "First Name", "Last Name", "Personal Identification", "Email Address", "Mobile Phone Number", "Home Phone Number"];
        // ============ /TBD ============
        //this.customerNumber = 1457;
        this.any_all_array = [{ name: "Match any of the following", value: 'any' }, { name: "Match all of the following", value: 'all' }];
    }

    componentDidMount() {
        let oThis = this;
        $('#profile').foundation();
        //TO DO: Get add condition combobox value from server
        //TO DO: the columns should get from server side as below

        //let columns = [{ data: "Mobile Phone Number", title: "Mobile Phone Number" }];

        let columns = [{ data: "user_id", title: "user_id" }, { data: "Full Name", title: "Full Name" }, { data: "Email Address", title: "Email Address" }];

        // let columns = [{ data: "full", title: "Full" }, { data: "first", title: "First" }, { data: "last", title: "Last" }, { data: "email", title: "Email" }, { data: "mobile", title: "Mobile" }, { data: "home", title: "Home" }, { data: "work", title: "Work" }, { data: "other", title: "Other" }, { data: "add1", title: "Add 1" }, { data: "add2", title: "Add 2" }, { data: "add3", title: "Add 3" }, { data: "country", title: "Country" }, { data: "ref", title: "Reference" }, { data: "occ", title: "Occupancy" }, { data: "education", title: "Education" }, { data: "pid", title: "PID" }, { data: "source", title: "Source" }];


        // ======================== TBD ========================
        let data = {
            total: 2,
            result: [{ id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 2, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 3, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 4, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 5, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 6, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 7, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 8, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 9, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 10, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 11, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 12, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 13, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 14, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 15, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }, { id: 1, full: "Kenneth Wong", first: "Kenneth", last: "Wong", email: "kwong@gmail.com", mobile: 97091235, home: "", work: 23147690, other: 53123456, add1: "4/F, Camelpaint Building,", add2: "Kwun Tong", add3: "Kowloon Tong", country: "Hong Kong", ref: "", occ: "", education: "", pid: "K123456(0)", source: "12:35pm 2017-05-02" },
            { id: 16, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 17, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" },
            { id: 18, full: "Kwan Wai Tam", first: "Wai Tam", last: "Kwan", email: "kwtam@yahoo.com", mobile: 98087676, home: 21146543, work: "", other: "", add1: "Rm F, 3/F, Goodview Garden,", add2: "Shatin", add3: "", country: "Hong Kong", ref: "", occ: "", education: "", pid: "Z123456(2)", source: "12:35pm 2018-05-02" }
            ]
        }
        // ======================== /TBD ========================
        columns.unshift(
            {
                data: null,
                className: 'noVis', //NOTES: no column visibility
                orderable: false,
                defaultContent: '<a onClick={} class="editor_edit icon-edit-16"></a>'
            }
        )


        let columnDefs = [];
        let columnLength = columns.length || 0;
        let targets = [];
        if (columnLength > 6) {
            for (let i = 7; i < columnLength; i++) {
                targets.push(i);
            }
            columnDefs.push(
                {
                    "targets": targets,
                    "visible": false
                });
        }


        //NOTES: DataTable reference: https://datatables.net/extensions/buttons/examples/html5/columns.html

        // let ajaxData = JSON.stringify({
        //     "simpleSearch": this.state.simpleSearch,
        //     "simpleText": this.state.simpleText,
        //     "anyAll": "Match any of the following",
        //     "query": this.state.queryData
        // });//ajaxData
        oThis.table = $('#resultDataTable').DataTable({
            stateSave: true,
            columns: columns,
            // ======== TBD ========
            //data: data.result,
            // ======== /TBD ========
            "bPaginate": true,
            "paging": true,
            "searching": false,
            "info": true,
            dom: '<"table-top"Bf><"scroll_table"rt><"table-btm"ip>',
            columnDefs: columnDefs,
            buttons: [
                {
                    extend: 'colvis',
                    columns: ':not(.noVis)',
                    // prefixButtons: [
                    //     {
                    //         extend: 'colvisGroup',
                    //         text: 'Select All',
                    //         // hide: ':visible',
                    //         action: function (e, dt, node, config) {
                    //             if (config.checked == true) {
                    //                 this.active(false);
                    //                 dt.columns('*').visible(false);
                    //             } else {
                    //                 this.active(true);
                    //                 dt.columns('*').visible(true);
                    //             }
                    //             config.checked = !config.checked
                    //         },
                    //         active: false,
                    //         checked: false
                    //     }
                    // ]
                }
            ],
            //NOTES: Uncomment below when Server Side is done
            //"processing": true,
            "serverSide": true,
            "deferLoading": 0,
            "ajax": {
                "url": "http://api.wiser/search", //profiles",
                "type": "POST",
                "data": function (d) {
                    // d = ajaxData;
                    d["simpleSearch"] = oThis.state.simpleSearch;
                    d["simpleText"] = oThis.state.simpleText;
                    d["anyAll"] = oThis.state.anyAll;
                    //d["query"] = oThis.state.queryData;
                    d["query"] = JSON.stringify(oThis.state.queryData);
                    //d.filter = JSON.stringify(oThis.state.queryData);
                }
            },
            /* No ordering applied by DataTables during initialisation */
            "order": []
        });
        // click icon triger selected
        $('#resultDataTable').on('click', 'a.editor_edit', function (e) {
            e.preventDefault();
            let data = oThis.table.row($(this).parents('tr')).data();
            oThis.setState({ profileData: data, showProfile: true });
        });
        // double click row triger selected
        $('#resultDataTable tbody').on('dblclick', 'tr', function (e) {
            e.preventDefault();
            let data = oThis.table.row($(this)).data();
            oThis.setState({ profileData: data, showProfile: true });
        });
        // hight light single click row
        $('#resultDataTable tbody').on('click', 'tr', function (e) {
            $('#resultDataTable tbody tr').removeClass('highlight');
            $(this).addClass('highlight')
        });
        //NOTES: When click Column visibility button, check is the visible is six already, if yes, all other rows disable
        $('#resultDataTable_wrapper').on(".buttons-colvis").click(function () {
            let bol = $(".buttons-columnVisibility.active").length > 5;
            if (bol == true) {
                $(".buttons-columnVisibility").not(".active").addClass("disabled");
            }
        });

        //NOTES: Function will be active when selecting collumn in column visibility
        $('#resultDataTable').on('column-visibility.dt', function (e, settings, column, state) {
            let bol = $(".buttons-columnVisibility.active").length > 5;
            if (bol == true) {
                $(".buttons-columnVisibility").not(".active").addClass("disabled");
            } else {
                $(".buttons-columnVisibility").not(".active").removeClass("disabled");
            }
        });
    }
    // ------------------------------- /delegate ------------------------------- //
    // ------------------------------- handlers ------------------------------- //
    filterClick(e) {
        this.setState({ simpleSearch: !this.state.simpleSearch, simpleText: "" });
        console.log(e);
    }

    addClick() {
        //NOTES: 'Add Condition' button clicked
        let addArray = this.state.addArray.slice();
        addArray.push({ condition: "", selector: i18n.is, text: "" })
        this.setState({ addArray: addArray });
    }

    // getData() {
    //     //NOTES: Search Clicked
    //     let data = {}

    //     data.simpleSearch = this.state.simpleSearch;
    //     if (data.simpleSearch == true) {
    //         data.query = this.state.simpleText
    //     } else {
    //         data.anyAll = this.state.anyAll
    //         data.query = this.state.addArray
    //     }
    //     this.setState({ queryData: data, showResult: true });
    //     // return data
    // }

    searchClick() {
        console.log(this.state);
        // create ajax request for getting customer profile
        // jQuery.ajax({
        //     method: 'POST',
        //     headers: {
        //         "Access-Control-Allow-Origin": "*",
        //         "Accept": "application/json"
        //     },
        //     url: 'http://api.wiser/search',
        //     crossDomain: true,
        //     contentType: "application/json",
        //     dataType: 'json',
        //     data:
        //         JSON.stringify({
        //             "simpleSearch": this.state.simpleSearch,
        //             "simpleText": this.state.simpleText,
        //             "anyAll": "Match any of the following",
        //             "query": this.state.queryData
        //         })
        //     , dataType: 'json'
        // }).always(function (res) {
        //     console.log("res");
        //     console.log(res);
        //     // let status = res.status;
        //     // if (status == "success") {
        //     //     oThis.setState({ showAlert: true, alertMsg: i18n.config_submitted });
        //     // }
        // });

        let oThis = this;
        let data = {}

        //data.simpleSearch = this.state.simpleSearch;
        // if (data.simpleSearch == true) {
        //     data.query = this.state.simpleText
        // } else {
        //     data.anyAll = this.state.anyAll
        //     data.query = this.state.addArray
        // }
        //this.setState({ showResult: true }, () => {
        this.setState({ queryData: this.state.addArray, showResult: true }, () => {
            console.log("redraw now");
            $('#resultDataTable').DataTable().draw();
        });
    }

    generateClick() {
        //TO DO: will send the data below to server
        let data = Object.assign({}, this.state.queryData);
        data.campaignName = this.state.campaignName;
        //this.setState({showAlert: true});
        //alert("Click F12 t see the query to send")
        console.log(data);
    }

    exportClick(e) {
        //NOTES: get the data from server now, not by local, because the records could be over thousands
        // jQuery.ajax({
        //     method		: 'GET',
        //     url			: '/profiles?filter',
        // }).always(function(res) { 
        //     // failed
        //     if (!res.status) {
        //         // message dialog
        //     // succeeded
        //     } else {
        //         let jsondata = res.data || {};
        //     }
        // });
        //TBD: sample data below, to be deleted
        let data = [
            {
                "full": "Kenneth Wong",
                "first": "Kenneth",
                "last": "Wong",
                "email": "kwong@gmail.com",
                "mobile": 97091235,
                "home": "",
                "work": 23147690,
                "other": 53123456,
                "add1": "4/F, Camelpaint Building,",
                "add2": "Kwun Tong",
                "add3": "Kowloon Tong",
                "country": "Hong Kong",
                "ref": "",
                "occ": "",
                "education": "",
                "pid": "K123456(0)"
            },
            {
                "full": "Kwan Wai Tam",
                "first": "Wai Tam",
                "last": "Kwan",
                "email": "kwtam@yahoo.com",
                "mobile": 98087676,
                "home": 21146543,
                "work": "",
                "other": "",
                "add1": "Rm F, 3/F, Goodview Garden,",
                "add2": "Shatin",
                "add3": "",
                "country": "Hong Kong",
                "ref": "",
                "occ": "",
                "education": "",
                "pid": "Z123456(2)"
            },
            {
                "full": "Bobby Lee",
                "first": "Bobby",
                "last": "Lee",
                "email": "bobby_lee@gmail.com",
                "mobile": 23092291,
                "home": "",
                "work": 55647729,
                "other": 11223342,
                "add1": "4/F, Camelpaint Building,",
                "add2": "Kwun Tong",
                "add3": "Kowloon Tong",
                "country": "Hong Kong",
                "ref": "",
                "occ": "",
                "education": "",
                "pid": "K123666(0)"
            }
        ]
        JSONToCSVConvertor(data, "Customer Profile Report", true); //NOTES: JSONToCSVConvertor(JSONData, ReportTitle, ShowHeaders, ShowTitle) 
    }

    //Edit Profile Buttons:
    epConfirmClick(data) {
        //upload all the data
        console.log('Data of customer');
        console.log(data);
        this.setState({ showProfile: false });
    }

    epCancelClick(data) {
        this.setState({ showProfile: false });
    }
    // ------------------------------- /handlers ------------------------------- /
    // ------------------------------- interface ------------------------------- //

    render() {
        let oThis = this;

        //Generate Condition Rows Below
        let conditionRows = [];
        let addArray = this.state.addArray.slice();
        if (addArray.length > 0) {
            for (let i = 0; i < addArray.length; i++) {
                conditionRows.push(
                    <div key={"query-rows" + i} className='expanded'>
                        <div className="filter-inputs">
                            <ICombo placeholder={i18nCommon.choose_condition} displayField={0} valueField={0} options={oThis.condition} value={addArray[i].condition} onChange={(o) => { addArray[i].condition = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
                            <ICombo displayField={0} valueField={0} options={i18nCommon.selector_array} value={addArray[i].selector} onChange={(o) => { addArray[i].selector = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
                            <div>
                                <input type='text' value={addArray[i].text} onChange={event => {
                                    addArray[i].text = event.target.value;
                                    this.setState({ addArray: addArray })
                                }} />
                            </div>
                            <div className="remove-icon">
                                <div className='icon-trash-16 i-icon' onClick={() => { addArray.splice(i, 1); this.setState({ addArray: addArray }); }} />
                            </div>
                        </div>
                    </div>
                )
            }
        }
        let displayResult = this.state.showResult ? 'block' : 'none';
        return (
            <div id='manage_cus_pro' className="multiple-cards" >
                {/* ================ FILTER ================ */}
                <div className="main-card">
                    <div className='content-divide'>
                        <h3>{i18n.search_for_customers}</h3>
                        <div className='row'>
                            <div className='medium-6 columns'>
                                <input type='text' style={{ display: 'inline', width: '300px' }} value={this.state.simpleText} onChange={event => this.setState({ simpleText: event.target.value })} placeholder={i18n.type_in_sth} disabled={!this.state.simpleSearch} />
                            </div>
                            <div className='medium-6 columns moreFilter'>
                                <button className='grey-button small right' data-toggle="collapse" data-target="#showFilter" aria-expanded="false" onClick={this.filterClick.bind(oThis)}><span className='icon-settings-16 i-icon i-icon-right' /></button>
                            </div>
                        </div>

                        <div id='showFilter' className='collapse'>
                            <div className='outer-padding'>
                                {/* An extra div container is made because of the animating issue */}
                                <div className='inner-padding'>
                                    <div className='filter-text-select'>
                                        <ICombo displayField='name' valueField='value' options={this.any_all_array} value={this.state.anyAll} onChange={(o) => { this.setState({ anyAll: o ? o.value : "" }) }} clearable={false} />
                                    </div>
                                    {conditionRows}

                                    <a className='add-condition-link' onClick={oThis.addClick.bind(oThis)}><span className='icon-add-16' />{i18nCommon.add_condition}</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className='content-ender'>
                        <button onClick={oThis.searchClick.bind(oThis)}>{i18nCommon.search}</button>
                    </div>
                </div>
                {/* ================ /FILTER ================ */}
                <div style={{ display: displayResult }} >
                    {/* Extra div is just to make the illusion of 2 separted div */}
                    < div className='main-card' >
                        <div className='row'>
                            <div className='medium-6 content-divide columns'>
                                <h3>{i18n.campaign_records_list}</h3>
                            </div>
                            <div className='medium-6 columns'>
                                <button className='right small grey-button' onClick={oThis.exportClick.bind(oThis)}><span className='icon-share-16 i-icon i-icon-left' />{i18n.export_list}</button>
                            </div>
                        </div>
                        <div id='table_wrapper'>
                            <table id='resultDataTable' />
                        </div>
                    </div >

                    <div className='main-card'>
                        <div className='content-divide'>
                            <h3>{i18n.campaign_list_name_label}</h3>
                            <div className='row'>
                                <div className='medium-6 columns'>
                                    <input type='text' value={this.state.campaignName} placeholder={i18n.campaign_list_name} onChange={event => {
                                        this.setState({ campaignName: event.target.value })
                                    }} />
                                </div>
                                <div className='medium-6 columns'>
                                    <button className='right' onClick={oThis.generateClick.bind(oThis)}><span className='icon-add-list-16 i-icon i-icon-left' />{i18n.generate}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >

                {this.state.showProfile ? <ProHisPop data={this.state.profileData} epConfirmClick={oThis.epConfirmClick.bind(oThis)} epCancelClick={oThis.epCancelClick.bind(oThis)} revealID="profile" /> : null}
            </div >
        );
    }
    // -------------------------------- /interface ------------------------------- //
}




























