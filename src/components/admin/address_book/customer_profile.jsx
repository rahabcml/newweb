import React, { Component } from 'react';
import ReactFileReader from 'react-file-reader';
import i18nCommon from '../../common/i18n.jsx';
import ICombo from '../../common/ICombo.jsx';
import AlertBox from '../../common/AlertBox.jsx'
let i18n = i18nCommon.cust_profile_variable;


export default class CustomerProfile extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        super(props);
        let data = this.props.data;
        this.state = {
            full: data.full || "",
            first: data.first || "",
            last: data.last || "",
            pid: data.pid || "",
            email: data.email || "",
            mobile: data.mobile || "",
            home: data.home || "",
            work: data.work || "",
            other: data.other || "",
            add1: data.add1 || "",
            add2: data.add2 || "",
            add3: data.add3 || "",
            country: data.country || "",
            source: data.source || "",
            file: data.file || '',
            imagePreviewUrl: data.imagePreviewUrl || '',
            readOnly: props.readOnly || false,
            showAlert: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        let data = nextProps.data;
        this.setState({
            full: data.full || "",
            first: data.first || "",
            last: data.last || "",
            pid: data.pid || "",
            email: data.email || "",
            mobile: data.mobile || "",
            home: data.home || "",
            work: data.work || "",
            other: data.other || "",
            add1: data.add1 || "",
            add2: data.add2 || "",
            add3: data.add3 || "",
            country: data.country || "",
            source: data.source || "",
            history: data.history || [],
            file: data.file || '',
            imagePreviewUrl: data.imagePreviewUrl || ''
        })
    }
    // ------------------------------- /delegate ------------------------------- //
    // ------------------------------- handlers ------------------------------- //
    //NOTES: Function below will read the Image File uploaded
    handleFiles(files) {
        let oThis = this;
        if (files.length > 0) { //NOTES: check the lengh to prevent error when user pressed cancel
            let fileName = files[0].name
            let result = fileName.match(".*\\.(png|jpg|gif)");
            if (result == null) {
                //alert(i18n.warn_photo);
                this.setState({ showAlert: true });
                return
            }
            let file = files[0];
            let reader = new FileReader();
            reader.onload = function (e) {
                oThis.setState({
                    file: file,
                    imagePreviewUrl: reader.result
                });
            }
            reader.readAsDataURL(file);
        }
    }
    // ------------------------------- /handlers ------------------------------- //
    // ------------------------------- interface ------------------------------- //
    render() {
        let oThis = this;
        let disabled = this.state.readOnly;

        //Render the image
        let { imagePreviewUrl } = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img className="img-center cp-circle" src={imagePreviewUrl} alt="" />);
        } else {
            $imagePreview = <div className="icon-focus-24 img-center cp-circle">
            </div>
        }
        return (
            <div className='cp-container'>
                <div style={{ display: 'flex' }}>
                    {/* ==================== LEFT COLUMN ==================== */}
                    <div style={{ flex: 1 }}>
                        <div className='cp-img-holder'>
                            <div className='cp-center-img'>
                                {$imagePreview}

                                {disabled ? null :
                                    <ReactFileReader handleFiles={oThis.props.handleFiles ? oThis.props.handleFiles : oThis.handleFiles.bind(oThis)} fileTypes={'image/*'}>
                                        <div className='cp-choose-pic' style={{}}>
                                            <a>{i18n.chosose_a_photo}</a>
                                        </div>
                                    </ReactFileReader>
                                }
                            </div>
                        </div>
                    </div>
                    {/* ==================== MIDDLE COLUMN ==================== */}
                    <div className='cp-second-col' style={{ flex: 1 }}>
                        <div className='row-p'>
                            <label className='gray-label'>{i18n.full_name}</label>
                            <input type='text' value={oThis.state.full} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'full') :
                                    this.setState({ full: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.first_name}</label>
                            <input type='text' value={oThis.state.first} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'first') :
                                    this.setState({ first: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.email_address}</label>
                            <input type='text' value={oThis.state.email} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'email') :
                                    this.setState({ email: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.work_phone_numbe}</label>
                            <input type='text' value={oThis.state.work} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'work') :
                                    this.setState({ work: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.address_line_1}</label>
                            <input type='text' value={oThis.state.add1} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'add1') :
                                    this.setState({ add1: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.address_line_3}</label>
                            <input type='text' value={oThis.state.add3} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'add3') :
                                    this.setState({ add3: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.source}</label>
                            <div>{i18n.updated_on}{this.state.source}</div>
                        </div>

                    </div>

                    {/* ==================== RIGHT COLUMN ==================== */}
                    <div style={{ flex: 1 }}>
                        <div className='row-p'>
                            <label className='gray-label'>{i18n.personal_id}</label>
                            <input type='text' value={oThis.state.pid} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'pid') :
                                    this.setState({ pid: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.last_name}</label>
                            <input type='text' value={oThis.state.last} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'last') :
                                    this.setState({ last: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.mobile_phone_numbe}</label>
                            <input type='text' value={oThis.state.mobile} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'mobile') :
                                    this.setState({ mobile: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.other_phone_numbe}</label>
                            <input type='text' value={oThis.state.other} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'other') :
                                    this.setState({ other: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.address_line_2}</label>
                            <input type='text' value={oThis.state.add2} disabled={disabled} onChange={event => {
                                this.props.handler ? this.props.handler(event.target.value, 'add2') :
                                    this.setState({ add2: event.target.value });
                            }} />
                        </div>

                        <div className='row-p'>
                            <label className='gray-label'>{i18n.country}</label>
                            <ICombo displayField={0} valueField={0} options={i18nCommon.country_array} value={oThis.state.country} disabled={disabled} onChange={
                                (o) => {
                                    if (o && o.value != null) {
                                        oThis.setState({ country: o.value });
                                    } else {
                                        oThis.setState({ country: "" });
                                    }
                                }
                            } />
                        </div>
                    </div>

                </div>
                {/* ==================== BUTTONS UNDERNEATH ==================== */}
                <div style={{ textAlign: 'center' }}>
                    <button style={{ margin: '10px' }}
                        onClick={oThis.props.epConfirmClick.bind(oThis)}>{i18nCommon.confirm}</button>
                    <button style={{ margin: '10px' }}
                        onClick={oThis.props.epCancelClick.bind(oThis)}>
                        {i18nCommon.cancel}</button>
                </div>
                <AlertBox alertMsg={i18n.warn_photo} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
            </div>
        );
    }
    // ------------------------------- /interface ------------------------------- //
}







































