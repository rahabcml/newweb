import React, { Component } from 'react';
import ICombo from '../../common/ICombo.jsx';
import IPaging from '../../common/IPaging.jsx';
import ReactFileReader from 'react-file-reader';
import $ from 'jquery';
import DatePicker from 'react-datepicker';
import Moment from 'moment';
import InputForm from '../input_form/input_form.jsx'
import 'react-datepicker/dist/react-datepicker.min.css';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.cust_profile_variable;

export default class ContactHistory extends Component {
    // ------------------------------- delegate ------------------------------- //
    constructor(props) {
        console.log("props in contact history");
        console.log(props);
        super(props);
        this.state = {
            checked: [true, true, true, true, true, true, true],
            colist: [],
            cilist: [],
            coValue: '',
            ciValue: '',
            dateFrom: Moment(new Date()),
            dateTo: Moment(new Date()),
            history: [],
            historyOrigin: [],
            icon: 'icon-phone-16',
            date: '',
            time: '',
            from: '',
            to: '',
            subject: '',
            status: '',
            content: '',
            file: '',
            link: '',
            internal: '',
            agent: '',
            page: 1,
            total: 0,
            numbers: 10, // NOTES: to control number of record to be shown in each page
            active: 0,
            recId: -1,
            recData: {}
        }
        let today = new Date();
        this.year = today.getFullYear();
        var mm = today.getMonth() + 1; //January is 0!
        var dd = today.getDate();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        this.fullDate = this.year + '-' + mm + '-' + dd;
        this.date = mm + '-' + dd;
        console.log("this.year");
        console.log(this.year);
        console.log("this.date");
        console.log(this.date);
    }

    componentWillMount() {
        // status: 0 = success, 1 = pending, 2 = failed
        // status only applied to fax, sms and email
        // subject only email and outbound fax have and will be shown
        // io = inbound outbound: 0 = outbound, 1 = inbound
        // cio = campaign belongs to inbound or outbound : 0 = outbound, 1 = inbound
        // TO DO: GET HISORY by this.props.cusId
        
        let jsondata = {// v1s = Voice 1 seconds          //"Karen(kw@yahoo.com)"
            colist: ["BTR201706"], cilist: ["Product Promotion Registration Hotline"], history: [
                {
                    id: 2, io: 0, type: 'email', date: "2018-01-09", time: "14:37", fm: "kw@yahoo.com", to: "ABC Company(abc.company@wisermail.com)", subject: " Enquiry about office hours",
                    content: "Hello ABC 'Company', \n \n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \n \n Thanks, \n Kenneth",
                    file: 'myAttachment.docx', link: 'myAttachmentLink',
                    agent: "Peter Chan", status: 1, internal: 'This is a internal message.', cio: 1, cname: "Spring Promotion"
                },
                {
                    id: 1, io: 0, type: 'call', date: this.fullDate, time: "14:37", cio: 0, cname: "Product Promotion Registration Hotline"
                },
                {
                    id: 2, io: 0, type: 'email', date: "2018-01-09", time: "14:37", fm: "Karen(kw@yahoo.com)", to: "ABC Company(abc.company@wisermail.com)", subject: " Enquiry about office hours",
                    content: "Hello ABC 'Company', \n \n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \n \n Thanks, \n Kenneth",
                    file: 'myAttachment.docx', link: 'myAttachmentLink',
                    agent: "Peter Chan", status: 2, internal: 'This is a internal message.', cio: 1, cname: "Summer Promotion"
                },
                {
                    id: 3, io: 1, type: 'call', date: "2018-01-03", time: "14:37", fm: 25463451, to: 91918888, ttt: 167, tht: 34,
                    v0: '2v0', v1: '2v1', cio: 0, cname: "BTR201706", rno: "3345", agent: "Peter Chan"
                },
                {
                    id: 4, io: 0, type: 'sms', date: "2017-03-04", time: "14:37", fm: 'Ken Cheung', to: 'peterchan', details: "demail", cio: 0, cname: "Speaker Best Buy",
                    agent: "Peter Chan", internal: 'This customer already took the trial membership.', content: 'This is just some random text I thought of so I can put some text into this lonely white area of this screen. Here is another usless sentence. Thank you very much.'
                },
                {
                    id: 5, io: 0, type: 'email', date: "2017-02-08", time: "04:37", fm: "ABC Company(abc.company@wisermail.com)", to: " Karen(kw@yahoo.com)", subject: " Enquiry the new rules for the api requirements", contents: "Hey ABC Company, I just wanted to ask about how the new rules will be laid out. When will it also start to take effect because Im considiring in upgrading my plan before this winter season ends. Hoping to hear back. Thanks",
                    agent: "Peter Chan", status: 1, internal: 'This is a internal message that I just randomly thought.', cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 6, io: 0, type: 'fax', date: "2017-01-04", subject: "Update about the summer time promo", time: "14:37", fm: 23456789, to: 23432165, details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', status: 0, cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 7, io: 1, type: 'sms', date: "2016-12-04", time: "14:37", fm: 23456789, to: 23432165, details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', status: 1, cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 8, io: 0, type: 'email', date: "2016-03-04", time: "14:37", fm: "Karen(kw@yahoo.com)", to: "ABC Company(abc.company@wisermail.com)", subject: " Enquiry about office address",
                    content: "Hello ABC 'Company', \n \n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \n \n Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \n \n Thanks, \n Kenneth",
                    file: 'myAttachment.docx', link: 'myAttachmentLink',
                    agent: "Peter Chan", status: 1, internal: 'This is a internal message.', cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 9, io: 1, type: 'sms', date: "2016-01-04", time: "14:37", fm: 23456789, to: 23432165, details: "demail",
                    agent: "Peter Chan", internal: 'This guy keeps on calling for an enquiry, then once called.', content: 'This is just another content that Im putting here to take on some lonely white pixels in the screen. Im adding up a little more cuz I want to see what it looks like if it has atleast 3 lines of phargraph sentences.', status: 0, cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 10, io: 1, type: 'fb', date: "2015-03-04", time: "14:37", fm: 'peter_chan@gmail.com', to: 'Ken Cheung', details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 11, io: 1, type: 'sms', date: "2014-12-04", time: "14:37", fm: 23456789, to: 23432165, details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', status: 0, cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 12, io: 0, type: 'web', date: "2014-03-04", time: "14:37", fm: 'Ken Cheung', to: 'peterchan', details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', cio: 0, cname: "Winterfell Discount"
                },
                {
                    id: 13, io: 1, type: 'wechat', date: "2013-03-04", time: "14:37", fm: 23456789, to: 23432165, details: "demail",
                    agent: "Peter Chan", internal: 'This is a internal message.', content: 'Hello World!', cio: 0, cname: "Winterfell Discount"
                }]
        };
        //NOTES: new contact history triggerred here, so will show default info
        let rec = jsondata.history ? jsondata.history.length > 0 ? jsondata.history[0] : {} : {};
        let total = 0;
        if (rec != {}) {
            this.tableClick(rec);
            total = jsondata.history.length;
        }
        this.setState({ colist: jsondata.colist, cilist: jsondata.cilist, historyOrigin: jsondata.history || [], history: jsondata.history || [], total: total });

    }

    componentDidMount() {
        // $(".history-card").click(function () {
        //     $(".history-card").removeClass("history-active");
        //     $(this).addClass("history-active");
        // });
        // this.hisCardActiveFirst();
    }
    // ------------------------------- /delegate ------------------------------- //
    // ------------------------------- handlers ------------------------------- //
    //NOTES: function below will be used when 1.Type of History chose 2. Campaign Name chose 3. Refresh Btn next to Date cliced 
    hisCardActiveFirst() {
        $(".history-card").first().addClass("history-active");
    }
    filterInfo() {
        let checked = this.state.checked;
        let coValue = this.state.coValue;
        let ciValue = this.state.ciValue;
        let history = this.state.historyOrigin.slice();
        let filteredOption, filteredOut, filteredDate = [];

        let isAllChecked = checked.filter(function (c) {
            return c == true;
        }).length === this.state.checked.length;

        if (isAllChecked == true) {
            filteredOption = history;
        } else {
            filteredOption = history.filter(function (rec) {
                return (checked[0] == true ? rec.type == 'call' : null) ||
                    (checked[1] == true ? rec.type == 'email' : null) ||
                    (checked[2] == true ? rec.type == 'fax' : null) ||
                    (checked[3] == true ? rec.type == 'sms' : null) ||
                    (checked[4] == true ? rec.type == 'wechat' : null) ||
                    (checked[5] == true ? rec.type == 'web' : null) ||
                    (checked[6] == true ? rec.type == 'fb' : null);
            })
        }

        if (coValue == "") {
            return filteredOption;
            //this.setState({ history: filteredOption });
        } else {
            let filteredOut = filteredOption.filter(function (rec) {
                return (rec.io == 1 && rec.cname == coValue)
            });
            return filteredOut
        }
        if (ciValue == "") {
            return filteredOption;
        } else {
            let filteredOut = filteredOption.filter(function (rec) {
                return (rec.io == 1 && rec.cname == ciValue)
            });
            return filteredOut
        }
    }

    //NOTES: Function below will be used when a radio chose in button group
    handleOptionChange(type, e) { //type: -1: all, 0: call, 1: email, 2: fax, 3: sms, 4, wechat, 5: web chat, 6: fb
        console.log("type, e");
        console.log(type, e);
        let isChecked = e.target.checked;
        console.log("isChecked");
        console.log(isChecked);
        let checked = this.state.checked.slice();
        if (type == -1) {
            if (isChecked == true) {
                checked = [true, true, true, true, true, true, true];
            } else {
                checked = [false, false, false, false, false, false, false];
            }
        } else {
            checked[type] = isChecked;
        }
        this.setState({ checked: checked });
    }

    //NOTES: Function below will be used when a campaign name is chose
    outboundSelect(o) {
        let value = o == null ? "" : o.value;
        this.setState({ coValue: value });
    }

    inboundSelect(o) {
        let value = o == null ? "" : o.value;
        this.setState({ ciValue: value });
    }

    //NOTES: Function below will be used when 'Refresh' button which is next to Date Picker is clicked
    applyFilterClick(formSubmitEvent) {
        formSubmitEvent.preventDefault();
        let dateFrom = (this.state.dateFrom) ? new Date(this.state.dateFrom) : new Date('1970-01-01');
        // setHours to zero to avoid compairing time
        dateFrom.setHours(0, 0, 0, 0);
        let dateTo = (this.state.dateTo) ? new Date(this.state.dateTo) : new Date('1970-01-01');
        // setHours to zero to avoid compairing time
        dateTo.setHours(0, 0, 0, 0);
        // let dateFrom = (this.state.dateFrom) ? Moment(new Date(this.state.dateFrom)) : Moment('1970-01-01');
        // let dateTo = (this.state.dateTo) ? Moment(new Date(this.state.dateTo)) : Moment('1970-01-01');
        console.log("dateFrom");
        console.log(dateFrom);
        console.log("dateTo");
        console.log(dateTo);
        let history = this.filterInfo();
        console.log("history in contact history's apply filter");
        console.log(history);
        let someRecs = history.filter(function (rec) {
            let recDate = new Date(rec.date);
            // setHours to zero to avoid compairing time
            recDate.setHours(0, 0, 0, 0);
            console.log("recDate");
            console.log(recDate);
            console.log("recDate <= dateTo");
            console.log(recDate <= dateTo);
            console.log("recDate >= dateFrom");
            console.log(recDate >= dateFrom);
            console.log("recDate == dateFrom");
            console.log(recDate == dateFrom);

            return (recDate <= dateTo && recDate >= dateFrom)
        });
        this.setState({ history: someRecs, total: someRecs.length || 0 }, () => { this.tableClick(this.state.history[0]); this.hisCardActiveFirst(); });
    }

    //NOTES: Function below will be used to decide the icon to be shown in Right Panel(tableClick function) and Left Panel(in render)
    returnIcon(type) {
        if (type == 'call') {
            return 'icon-phone-16';
        } else if (type == 'email') {
            return 'icon-at-sign-16'
        } else if (type == 'fax') {
            return 'icon-fax-16'
        } else if (type == 'sms') {
            return 'icon-chat-16'
        } else if (type == 'wechat') {
            return 'icon-wechat-16'
        } else if (type == 'web') {
            return 'icon-chat-16'
        } else if (type == 'fb') {
            return 'icon-fb-16'
        }
    }
    //NOTES: Function will be used when Left Panel Clicked
    tableClick(rec, icon, activeIdx) {
        if (rec != null) {
            let type = rec.type;
            if (icon == null) icon = this.returnIcon(type);
            this.setState({ recId: rec.id, type: type, from: rec.fm, to: rec.to, icon: icon, subject: rec.subject || '', status: rec.status || '', content: rec.content || '', file: rec.file || '', link: rec.link || '', date: rec.date, time: rec.time, agent: rec.agent, internal: rec.internal, active: activeIdx || 0, recData: rec || {} });
        }
    }
    //NOTES: Save Button in Right Panel Clicked
    saveClick() {
        //TO DO: get this.state.internal, etc to save to the server
    }
    handleDate(recDate) {
        // if same date, do not show date, show time only
        if (recDate == this.fullDate) {
            return "";
        }
        // remain not today
        let recYear = recDate.substring(0, 4); // substring will not affact recDate;
        if (recYear == this.year) {
            let mthDay = recDate.substring(5);
            // if same year, show the date only
            return mthDay;
        } else {
            // return whole date
            return recDate;
        }
    }
    // ------------------------------- /handlers ------------------------------- //
    // ------------------------------- interface ------------------------------- //
    render() {
        let oThis = this;
        let content = [];
        let history = this.state.history;
        let historyLen = history.length || 0;
        let type = this.state.type;
        let stateFrom = this.state.from ? (this.state.from).toString() : "";
        let fromIdx = stateFrom.indexOf('(');
        let fromName = stateFrom.substring(0, fromIdx);
        let fromEmail = stateFrom.substring(fromIdx);
        let stateTo = this.state.to ? (this.state.to).toString() : "";
        let toIdx = stateTo.indexOf('(');
        let toName = stateTo.substring(0, toIdx);
        let toEmail = stateTo.substring(toIdx);
        //========================= RENDER RIGHT PANEL BEGIN =========================
        let rightPanel = [];
        let recData = this.state.recData;
        let cio = recData.cio == 0 ? i18n.outbound_campaign : i18n.inbound_campaign;
        if (historyLen > 0) {
            rightPanel = (
                //NOTES: TYPE = CALL
                type == 'call' ?
                    <div className='columns medium-9 history-content'>
                        <InputForm recId={this.state.recId} readOnly={true} />
                    </div>
                    :
                    //NOTES: TYPE = OTHER TYPES 
                    <div className='columns medium-9 history-content'>
                        <div className='divide-content'>
                            <div className={"hc-icon" + " " + this.state.icon} />
                            {this.state.subject != "" ?
                                <div className='hc-combo'>
                                    {/* === right === */}
                                    < div className='right display-table'>
                                        <span className='row-span-label'><span className='hc-label'>{i18n.campaign_details_label}</span></span>
                                        <span className='hc-status two-rowspan'> {recData.cname + '\n'}<i className='color-grey'>{cio}</i></span>
                                        {i18n.status_end_label}
                                    </div>
                                    <div>
                                        <span className='hc-label'>{i18n.subject_label}</span>
                                        <span className='hc-subject'>{this.state.subject}</span>
                                    </div>
                                </div> :
                                <div className='hc-combo'>
                                    <div className='display-table'>
                                        <span className='row-span-label'><span className='hc-label'>{i18n.campaign_details_label}</span></span>
                                        <span className='hc-status two-rowspan'> {recData.cname + '\n'}<i className='color-grey'>{cio}</i></span>
                                        {i18n.status_end_label}
                                    </div>
                                </div>
                            }

                        </div>
                        <div className='clear' />

                        <div className='divide-content hc-participants'>
                            <div className='display-table'>
                                <span className={type == 'email' && fromIdx != -1 ? 'row-span-label' : 'ch-label'}>
                                    <span className='hc-label'>{i18n.from_label}</span>
                                </span>
                                {type == 'email' && fromIdx != -1 ?
                                    <span className='two-rowspan'> {fromName + '\n'}<i className='color-grey'>{fromEmail}</i></span>
                                    :
                                    <span className='vertical-middle'>{stateFrom}</span>
                                }
                            </div>
                            <div className='right display-table'>
                                <span className={type == 'email' && toIdx != -1 ? 'row-span-label' : 'ch-label'}>
                                    <span className='hc-label'>{i18n.to_label}</span>
                                </span>
                                {type == 'email' && toIdx != -1 ?
                                    <span className='two-rowspan'> {toName + '\n'}<i className='color-grey'>{toEmail}</i></span>
                                    :
                                    <span className='vertical-middle'>{stateTo}</span>
                                }
                            </div>
                        </div>

                        <div className='divide-content'>
                            <div className='hc-message-content display-table'>
                                <span className='row-span-label'>
                                    <span className='hc-label'>{i18n.message_content_label}</span>
                                </span>
                                <span className='wrap'>{type == 'email' ? i18n.content_html_label : i18n.content_label}
                                    {this.state.content}</span>
                            </div>
                        </div>

                        <div className='divide-content'>
                            {type == 'fax' || type == 'email'}
                            <span className='hc-label'>{i18n.attachment_label}</span>
                            <div className='hc-attachments'>
                                {this.state.file != '' ? <a href={this.state.link}>{this.state.file}</a> : null}
                            </div>
                        </div>

                        <div className='divide-content'>
                            <div>
                                <span>Handled by </span>
                                <span className='hc-agent'>{this.state.agent}</span>
                                <span> on </span>
                                <span className='hc-date'>{this.state.date}&nbsp;{this.state.time}</span>
                            </div>
                        </div>

                        <div className='divide-content'>
                            <textarea value={this.state.internal} onChange={e => this.setState({ internal: e.target.value })} />
                            <div className='row'>
                                <button className='right' onClick={oThis.saveClick.bind(oThis)}>{i18nCommon.save}</button>
                            </div>
                        </div>
                    </div >
            );
        } else {
            rightPanel = (
                <div className='columns medium-9 history-content'>
                    {i18n.no_records_found}
                </div>);
        }

        //========================= RENDER RIGHT PANEL END =========================
        //========================= RENDER LEFT PANEL BEGIN =========================
        let rows = [];
        if (historyLen > 0) {
            let page = this.state.page;
            let numbers = this.state.numbers;
            let total = this.state.total;
            let startSuppose = numbers * page - numbers
            let start = startSuppose < 0 ? 0 : startSuppose;
            let iSmallerLength = (start + numbers) > historyLen ? historyLen : start + numbers;

            for (let i = start; i < iSmallerLength; i++) {
                let rec = history[i];
                let type = rec.type;
                let io = rec.io;
                let icIcons = io == 1 ? 'icon-inbound-16' : 'icon-outbound-16';
                let subject = "";
                let icon = "";
                let date = this.handleDate(rec.date);
                let time = rec.date == this.fullDate ? rec.time : "";

                icon = this.returnIcon(type);

                let detailLine = (<div> <span className={icIcons}></span><span>{date}</span><span>{time}</span></div>)

                let status = ""; // success
                let statusClass = ""; // success
                switch (rec.status) {
                    case 1:
                        status = " - Pending";
                        statusClass = " pending-triangle";
                        break;
                    case 2:
                        status = " - Failed";
                        statusClass = " failed-triangle";
                        break;
                    default:
                        status = ""
                        statusClass = "";
                        break;
                }
                rows.push(
                    <div title={rec.type + status} className={this.state.active === i ? 'history-card history-active' : 'history-card'} key={i} onClick={oThis.tableClick.bind(oThis, rec, icon, i)}>
                        <div className={statusClass} />
                        <div className='wd-subject'>
                            <div className='connection-type'>
                                <span className={icon} />
                            </div>
                            <div className='info'>
                                <span className='subject'>{rec.cname}</span>
                                {detailLine}
                            </div>
                        </div>
                    </div>
                )
            }
        }
        content = (
            <div id="history-area" className='row'>
                <div className='columns medium-3 history-list'>
                    {rows}
                </div>
                {/*This part was put inside the {rightPanel}*/}
                {rightPanel}
            </div>
        )

        //========================= RENDER LEFT PANEL END =========================
        let isAllChecked = this.state.checked.filter(function (c) {
            return c == true;
        }).length === this.state.checked.length;

        return (
            <div className='history-filter-area'>
                <div className='filter'>
                    <button className='grey-button' data-toggle="collapse" data-target="#advanceFilter" aria-expanded="false">{i18n.advance_filter}</button>
                    <IPaging total={this.state.total} page={oThis.state.page} numbers={this.state.numbers} pageClick={(page) => { this.setState({ page: page }); }} />
                </div>

                <div id='advanceFilter' className='collapse'>

                    <div className='filterType-select'>
                        <input type="checkbox" name="editList" id="all" value="all" checked={isAllChecked} onChange={oThis.handleOptionChange.bind(oThis, -1)} />
                        <label className="slim-label" htmlFor="all">{i18n.all}</label>

                        <input type="checkbox" name="editList" id="call" value="call" checked={this.state.checked[0]} onChange={oThis.handleOptionChange.bind(oThis, 0)} />
                        <label className="slim-label" htmlFor="call">{i18n.call}</label>

                        <input type="checkbox" name="editList" id="email" value="email" checked={this.state.checked[1]} onChange={oThis.handleOptionChange.bind(oThis, 1)} />
                        <label className="slim-label" htmlFor="email">{i18nCommon.email}</label>

                        <input type="checkbox" name="editList" id="fax" value="fax" checked={this.state.checked[2]} onChange={oThis.handleOptionChange.bind(oThis, 2)} />
                        <label className="slim-label" htmlFor="fax">{i18nCommon.fax}</label>

                        <input type="checkbox" name="editList" id="sms" value="sms" checked={this.state.checked[3]} onChange={oThis.handleOptionChange.bind(oThis, 3)} />
                        <label className="slim-label" htmlFor="sms">{i18nCommon.sms}</label>

                        <input type="checkbox" name="editList" id="wechat" value="wechat" checked={this.state.checked[4]} onChange={oThis.handleOptionChange.bind(oThis, 4)} />
                        <label className="slim-label" htmlFor="wechat">{i18nCommon.wechat}</label>

                        <input type="checkbox" name="editList" id="web" value="web" checked={this.state.checked[5]} onChange={oThis.handleOptionChange.bind(oThis, 5)} />
                        <label className="slim-label" htmlFor="web">{i18nCommon.web_chat}</label>

                        <input type="checkbox" name="editList" id="fb" value="fb" checked={this.state.checked[6]} onChange={oThis.handleOptionChange.bind(oThis, 6)} />
                        <label className="slim-label" htmlFor="fb">{i18n.fb_messenger}</label>
                    </div>

                    <div className='fiterType-campaign'>
                        <label>{i18n.outbound_campaign_label}</label>
                        <span><ICombo displayField={0} valueField={0} value={this.state.coValue} options={this.state.colist} onChange={oThis.outboundSelect.bind(oThis)} /></span>
                    </div>

                    <div className='fiterType-campaign'>
                        <label>{i18n.inbound_campaign_label}</label>
                        <span><ICombo displayField={0} valueField={0} value={this.state.ciValue} options={this.state.cilist} onChange={oThis.inboundSelect.bind(oThis)} /></span>
                    </div>

                    <div className='filterType-dateRance'>
                        <div className='row'>
                            <div className='columns medium-6'>
                                <label>{i18n.date_from_label}</label>
                                <DatePicker selected={Moment(new Date(oThis.state.dateFrom))} todayButton={"Today"} className='module-input' onChange={(d) => oThis.setState({ dateFrom: d })} todayButton={"Today"} isClearable={true} />
                            </div>
                            <div className='columns medium-6'>
                                <label>{i18n.date_to_label}</label>
                                <DatePicker selected={Moment(new Date(oThis.state.dateTo))} todayButton={"Today"} className='module-input' onChange={(d) => oThis.setState({ dateTo: d })} todayButton={"Today"} isClearable={true} />
                            </div>
                        </div>
                    </div>

                    <button data-toggle="collapse" data-target="#advanceFilter" onClick={oThis.applyFilterClick.bind(oThis)}>{i18nCommon.apply_filter}</button>

                </div>
                {content}
            </div>
        );
    }
    // ------------------------------- /interface ------------------------------- //
}

































