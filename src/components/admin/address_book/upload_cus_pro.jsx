/*******************************************
* Notes
* Name: upload_cus_pro.jsx
* Details: Upload Class. Page full name: Upload Address Book
********************************************/
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import ReactFileReader from 'react-file-reader';
import csvjson from 'csvjson';
import ITable from '../../common/ITable.jsx'
import Select from 'react-select';
import i18nCommon from '../../common/i18n.jsx'
import AlertBox from '../../common/AlertBox.jsx';
import JSONsToCSVConvertor from '../../common/JSONsToCSVConvertor.jsx'
let i18n = i18nCommon.upload_variable;


class UploadCustomerProfile extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);
		this.state = {
			cusFields: [],
			fileName: i18n.no_file_chosen,
			confirmDisabled: true,
			csvData: [],
			csvFile: null,
			csvHeader: [],
			gridData: [],
			showUpload: true,
			showGrid: false,
			showResult: false,
			overwrite: false,
			showDuplicated: false,
			duplicatedColumns: [],
			existingColumns: [],
			duplicatedRow: [],
			existingArray: [],
			selectedIndex: -1,
			selectedRowNo: -1,
			duplicatedData: [],
			resultData: {},
			duplicateSelection: [], //NOTES: if result is duplicated will return this to the server
			result: [],
			showAlert: false
		}
	}

	componentWillMount() {
		let oThis = this;
		// Get default-fields and custom fields
		// ========= TO BE UNCOMMENTED ==========
		jQuery.ajax({
			method: 'POST',
			url: 'http://api.wiser/upload',
			data: { company_id: sessionStorage.company_id || -1 }
		}).always(function (res) {
			let jsonData = res.jsonData;
			if (jsonData.status == "success") {
				oThis.setState({ cusFields: jsonData.data }, () => { oThis.createColumns() });
			} else {
				oThis.createColumns();
			}
		});
		// ========= /TO BE UNCOMMENTED ==========
		// ======================== TBD ========================
		// let cusFields = [
		// 	{ name: "Full Name", type: "Alphanumeric" },
		// 	{ name: "First Name", type: "Alphanumeric" },
		// 	{ name: "Last Name", type: "Alphanumeric" },
		// 	{ name: "Personal Identification", type: "Alphanumeric" },
		// 	{ name: "Email Address", type: "Email" },
		// 	{ name: "Mobile Phone Number", type: "Mobile Phone Number" },
		// 	{ name: "Home Phone Number", type: "Home Phone Number" },
		// 	{ name: "Work Phone Number", type: "Work Phone Number" },
		// 	{ name: "Other Phone Number", type: "Other Phone Number" },
		// 	{ name: "Adress Line 1", type: "Alphanumeric" },
		// 	{ name: "Adress Line 2", type: "Alphanumeric" },
		// 	{ name: "Adress Line 3", type: "Alphanumeric" },
		// 	{ name: "Country", type: "Alphanumeric" },
		// 	{ name: "Reference Number(Custom Field 4)", type: "Digit" },
		// 	{ name: "Education(Custom Field 11)", type: "Alphanumeric" }
		// ]
		// oThis.setState({ cusFields: cusFields }, () => { oThis.createColumns() });
		// ======================== TBD ========================
	}
	// ------------------------------- /delegate ------------------------------- //
	// ------------------------------- handlers ------------------------------- //
	createColumns() {
		let oThis = this;
		this.columns = [
			{ id: "order", name: i18n.order },
			{ id: "headerName", name: i18n.header_name },
			{ id: "customField", name: i18n.customer_profile_field, type: 'combo', valueField: 'name', displayField: 'name', options: oThis.state.cusFields, onChange: oThis.comboChange.bind(oThis) },
			{ id: "fieldType", name: i18n.field_type },
			{
				id: "mandatory", name: i18n.mandatory, type: 'checkbox', onChange: function (idx, checked) {
					//let value = event.target.checked;
					let gridData = oThis.state.gridData.slice();
					gridData[idx].mandatory = checked;
					oThis.setState({ gridData: gridData });

				}
			},
			{
				id: "identifier", name: i18n.identifier, type: 'checkbox', onChange: function (idx, checked) {
					//let value = event.target.checked;
					let gridData = oThis.state.gridData.slice();
					gridData[idx].identifier = checked;
					oThis.setState({ gridData: gridData });
				}
			}
		]
	}
	//NOTES: Function below change the value of the combobox on the main talbe
	comboChange(val, i) {
		let _gridData = this.state.gridData.slice();
		//let valueObj = JSON.stringify(val);
		if (val != null) {
			_gridData[i].customField = val.value;
			_gridData[i].fieldType = val.obj.type;
		} else {
			_gridData[i].customField = "";
			_gridData[i].fieldType = "";
		}
		this.setState({ gridData: _gridData });
	}

	//NOTES: Function to convert csv to JSON, called in handleFiles function
	csvJSON(csv) {
		if (csv != null) {
			var lines = csv.split("\n");

			var result = [];

			var headers = lines[0].split(",");

			for (var i = 1; i < lines.length; i++) {

				var obj = {};
				var currentline = lines[i].split(",");

				for (var j = 0; j < headers.length; j++) {
					obj[headers[j]] = currentline[j];
				}
				result.push(obj);
			}
			return JSON.stringify(result);
		}
	}

	//NOTES: Function when confirm button on top clicked
	confirmClick() {
		let oThis = this;
		let gridData = [];
		if (oThis.state.csvData.length > 0) {
			let _csvHeader = oThis.state.csvHeader.slice();
			let _cusFields = oThis.state.cusFields.slice();
			//let _cusFields = oThis.cusFields.slice();

			for (let i = 0; i < _csvHeader.length; i++) {
				let relatedString = "";
				let relatedType = "";
				let headerName = _csvHeader[i];
				for (let j = 0; j < _cusFields.length; j++) {
					let cusName = _cusFields[j].name;
					if (cusName.indexOf(headerName) >= 0) {
						relatedString = cusName
						relatedType = _cusFields[j].type
						break;
					}
				}

				if (relatedString == "") {
					let headerNum = headerName.replace(/^\D+/g, ''); // replace all leading non-digits with nothing
					if (headerNum != "") {
						for (let j = 0; j < _cusFields.length; j++) {
							let cusFieldNum = _cusFields[j].name.replace(/^\D+/g, ''); // replace all leading non-digits with nothing
							if (headerNum == cusFieldNum) {
								relatedString = _cusFields[j].name
								relatedType = _cusFields[j].type
								break;
							}
						}
					}
				}
				gridData.push({
					order: i + 1,
					headerName: headerName,
					customField: relatedString,
					fieldType: relatedType,
					mandatory: false,
					identifier: false
				});
			}
			oThis.setState({ gridData: gridData, showGrid: true, showUpload: false });
		}
	}
	//NOTES: Upload button clicked
	uploadSuccessClick() {
		//TO DO: send overwrite to server
		console.log(this.stete);
		//alert('See console to see what to upload');
	}
	uploadDuplicateClick() {
		//TO DO: send overwrite and result object to server
		console.log(this.state);
		//alert('See console to see what to upload');
	}
	//NOTES: function to handle all kinds of input tag's change
	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	}

	//NOTES: "Proceed to prechecks" button clicked
	precheckClick(type) { //TBD: type
		let oThis = this;
		console.log("this.state.gridData");
		console.log(this.state.gridData);
		console.log("this.state.csvData");
		console.log(this.state.csvData);
		// create ajax request for queuing in validating csv file from server
		console.log(this.state.csvFile);
		let data = {
			"company_id": sessionStorage.getItem("company_id"),
			file: this.state.csvFile,
			settings: this.state.gridData
		}
		console.log("ajax data");
		console.log(data);
		jQuery.ajax({
			method: 'POST',
			url: 'http://api.wiser/uploads',
			contentType: false,
			processData: false,
			data: data
		}).always(function (res) {
			// failed
			if (!res.status) {
				// message dialog
				// succeeded
			} else {
				let jsonData = res.data || {};
			}
		});

		//will send csv data and get result: success, duplicated, unsuccess
		//TO DO: for duplicated result the sample should be as below. existing array is the last element of duplicated array and RED(the duplicated properties)

		//TBD: success sample data
		let jsonData = {};

		let successJson = { result: "success", "total": 279, created: 978, updated: 266, duplicated: 0, uploaded: 1244 };
		let duplicateJson = {
			"result": "duplicated", "total": 279, "created": 978, "updated": 260, "duplicated": 2, "uploaded": 1238,
			"columns": [{ id: "Name", name: "Name" }, { id: "Email", name: "Email" }, { id: "Mobile", name: "Mobile" }, { id: "Home", name: "Home" }, { id: "Work", name: "Work" }, { id: "Address1", name: "Address1" }, { id: "Address2", name: "Address2" }, { id: "Address3", name: "Address3" }, { id: "Country", name: "Country" }, { id: "Reference Number", name: "Reference Number" }, { id: "Occupation", name: "Occupation" }, { id: "Education", name: "Education" }],

			"duplicatedArray": [{
				"Row": 12, "Name": "Peter Chan", "Email": "pc@yahoo.com", "Mobile": "12345678", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Name", "Email"], "existingArray": [{
					"Name": "Peter Chan", "Email": "pc@yahoo.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Name", "Email"]
				},
				{
					"Name": "Peter Chan Tai Man", "Email": "pc@yahoo.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Email"]
				},
				{
					"Name": "Peter Chan", "Email": "peter_chan@gmail.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Name"]
				}]
			},
			{
				"Row": 29, "Name": "Nancy Wong", "Email": "nancy@gmail.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Name", "Email"], "existingArray": [{
					"Name": "Nancy Wong", "Email": "nancy@gmail.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Name", "Email"]
				}, {
					"Name": "Nancy Wong", "Email": "nancy@yahoo.com", "Mobile": "90987701", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "A11287", "Occupation": "", "Education": "", "RED": ["Name"]
				}]
			}]
		}

		let unsuccessJson = {
			result: "unsuccess", "total": 279, data: 3, mandatory: 2, identifier: 2, uploaded: 0, "columns": [{ id: "Row", name: "Row" }, { id: "Name", name: "Name" }, { id: "Email", name: "Email" }, { id: "Mobile", name: "Mobile" }, { id: "Home", name: "Home" }, { id: "Work", name: "Work" }, { id: "Address1", name: "Address1" }, { id: "Address2", name: "Address2" }, { id: "Address3", name: "Address3" }, { id: "Country", name: "Country" }, { id: "Reference Number", name: "Reference Number" }, { id: "Occupation", name: "Occupation" }, { id: "Education", name: "Education" }], dataRED: ["Email", "Mobile"], manRED: ["Mobile"], idRED: ["Name"], dataArray: [
				{ "Row": 11, "Name": "Wynes Wong", "Email": "xcys", "Mobile": "12345678", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Email"] },
				{ "Row": 29, "Name": "Vincent Chu", "Email": "vince@gmail.com", "Mobile": "hello", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Mobile"] },
				{ "Row": 78, "Name": "Nancy Wan", "Email": "nancywan@gmail.com", "Mobile": "world", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Mobile"] }], manArray: [
					{ "Row": 13, "Name": "John Bilbo", "Email": "john@gmail.com", "Mobile": "", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Mobile"] },
					{ "Row": 14, "Name": "Bob Harley", "Email": "bobhar@yahoo.com", "Mobile": "", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Mobile"] }], idArray: [
						{ "Row": 22, "Name": "Jason Karaan", "Email": "jason1@gmail.com", "Mobile": "12345678", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Name"] },
						{ "Row": 25, "Name": "Jason Karaan", "Email": "jason2@yahoo.com", "Mobile": "12345678", "Home": '', "Work": '', "Address1": '', "Address2": '', "Address3": '', "Country": "Hong Kong", "Reference Number": "RTUH", "Occupation": "", "Education": "", "RED": ["Name"] }
					]
		};

		if (type == 'success') { jsonData = successJson } else if (type == 'duplicated') { jsonData = duplicateJson } else { jsonData = unsuccessJson }

		this.setState({ resultData: jsonData, showResult: true });
		//this.setState({ resultData: jsonData, showResult: true, updated: jsonData.updated || 0 }, () => this.createResult());
	}
	//NOTES: "Choose Files" button clicked
	handleFiles(files) {
		let oThis = this;
		if (files.length > 0) { //prevent error when user pressed cancel
			let fileName = files[0].name
			let result = fileName.match('.csv');
			if (result == null) {
				//alert(i18n.select_csv);
				this.setState({ showAlert: true })
				return
			}

			let oThis = this;
			var reader = new FileReader();
			let file = files[0];
			console.log("file before onload");
			console.log(file);
			// for security reason, the file name cannot be too long, must end with ".csv" and size not over 5000
			console.log("file.name.length");
			console.log(file.name.length);
			console.log("file.name.substring(file.name.length - 4)");
			console.log(file.name.substring(file.name.length - 4));
			if (file.name.length < 50 && file.name.substring(file.name.length - 4) == ".csv" && file.size < 5000) {
				this.setState({ csvFile: file });
				reader.onload = function (e) {
					let objArray = csvjson.toObject(reader.result);
					oThis.setState({ csvData: objArray });

					let arrayArray = csvjson.toArray(reader.result);
					if (arrayArray.length > 0) {
						oThis.setState({ csvHeader: arrayArray[0] });
					}
				}
				reader.readAsText(files[0]);
				this.setState({ fileName: fileName, confirmDisabled: false });
			}
		}
	}
	//NOTES: Export button clicked
	exportClick() {
		let JSONData = [];

		let resultData = Object.assign({}, this.state.resultData);
		JSONData.push(resultData.dataArray, resultData.manArray, resultData.idArray);

		let ReportTitle = "Unsuccess_Log"
		let Headers = [];
		Headers.push(i18n.data_type_error, i18n.mandatory_field_error, i18n.identifier_field_error);

		JSONsToCSVConvertor(JSONData, ReportTitle, Headers);//
	}
	tblRowSelected(index, data) {
		let rowColumn = { id: "Row", name: "Row" };
		let jsonData = this.state.resultData;
		let jsonArray = jsonData.columns.slice();
		let rowColumns = [rowColumn].concat(jsonArray);

		let selectColumn = {
			id: "select", name: "", flex: 1, type: 'link', text: i18n.select, onClick: (index, data) => {
				//NOTES: Save the selection to duplicateSelection
				let duplicateSelection = this.state.duplicateSelection.slice();
				let resultObj = {};
				resultObj.selectedIndex = this.state.selectedIndex;
				resultObj.roleNo = this.state.selectedRowNo;
				resultObj.status = "replace";
				resultObj.dataToBeReplaced = data;
				resultObj.dataTobeDeletedIndex = index;
				duplicateSelection.push(resultObj);

				//NOTES: remove the element selected from duplicated table, add one record to updated
				let resultData = Object.assign({}, this.state.resultData);
				resultData.duplicatedArray.splice(this.state.selectedIndex, 1);
				resultData.updated += 1;
				resultData.duplicated -= 1;
				this.setState({ duplicateSelection: duplicateSelection, resultData: resultData, showDuplicated: false });
			}
		};

		let selectColumns = [selectColumn].concat(jsonArray);
		for (let i = 0; i < data.RED.length; i++) {
			let renderColumn = data.RED[i];
			for (let j = 0; j < selectColumns.length; j++) {
				if (selectColumns[j].id == renderColumn) {
					let columnObj = Object.assign({}, selectColumns[j]);
					columnObj.render = function (text, o) {
						let redLength = o.RED.length;
						if (redLength > 0) {
							for (let k = 0; k < redLength; k++) {
								if (renderColumn == o.RED[k]) {
									return <div style={{ color: 'red' }}>{text}</div>
								}
							}
						}
						return text
					}
					selectColumns[j] = columnObj;
					let rowObj = Object.assign({}, rowColumns[j]);
					rowObj.render = function (text) { return <div style={{ color: 'red' }}>{text}</div> }
					rowColumns[j] = rowObj;
					break;
				}
			}
		}

		this.setState({ existingColumns: selectColumns, duplicatedColumns: rowColumns, selectedIndex: index, selectedRowNo: data.Row, duplicatedRow: [data], existingArray: data.existingArray, showDuplicated: true });
	}
	// =========================== POP-UP FUNCTION ===========================
	//NOTES: In pop-up Create entry clicked
	popupCreateClick() {
		//NOTES: Save the selection to duplicateSelection
		let duplicateSelection = this.state.duplicateSelection.slice();
		let resultObj = {};
		resultObj.selectedIndex = this.state.selectedIndex;
		resultObj.roleNo = this.state.selectedRowNo;
		resultObj.status = "create";
		duplicateSelection.push(resultObj);

		//NOTES: remove the element selected from duplicated table, add one record to updated
		let resultData = Object.assign({}, this.state.resultData);
		resultData.duplicatedArray.splice(this.state.selectedIndex, 1);
		resultData.created += 1;
		resultData.duplicated -= 1;
		this.setState({ duplicateSelection: duplicateSelection, resultData: resultData, showDuplicated: false });
		//TO DO: send to server the duplicateSelection and resultData, etc.
	}
	//NOTES: In pop-up Close button clicked
	popupCloseClick() {
		this.setState({ showDuplicated: false });
	}
	// ------------------------------- /handlers ------------------------------- //
	// ------------------------------- interface ------------------------------- //
	render() {
		let oThis = this;
		let result = [];
		let jsonData = this.state.resultData;
		let bcd, e;

		if (jsonData.result != 'unsuccess') {
			bcd = (
				<div className='row stats-area'>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.no_of_customer_uploaded}</span>
						<span className='value'>{jsonData.total}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.no_of_customer_created}</span>
						<span className='value'>{jsonData.created}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.no_of_customer_updated}</span>
						<span className='value'>{jsonData.updated}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.no_of_customer_duplicated}</span>
						<span className='value'>{jsonData.duplicated}</span>
					</div>
				</div>
			);

			e = (
				<div className='stats-area'>
					<div className='stats'>
						<span className='stat-label'>{i18n.no_of_customer_uploaded}</span>
						<span className='value'>{jsonData.uploaded}</span>
					</div>
				</div>
			);
		} else {
			bcd = (
				<div className='row stats-area'>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.no_of_customer_uploaded}</span>
						<span className='value'>{jsonData.total}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.data_type_error}</span>
						<span className='value'>{jsonData.data}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.mandatory_field_error}</span>
						<span className='value'>{jsonData.mandatory}</span>
					</div>
					<div className='columns small-12 medium-3 stats'>
						<span className='stat-label'>{i18n.identifier_field_error}</span>
						<span className='value'>{jsonData.identifier}</span>
					</div>
				</div>
			);

			e = (
				<div className='stats-area'>
					<div className='stats'>
						<span className='stat-label'>{i18n.no_of_customer_uploaded}</span>
						<span className='value'>0</span>
					</div>
				</div>
			);
		}

		if (jsonData.result == "success") {
			result.push(
				<div key='success' className="main-card result-area">
					<div className='content-divide'>
						<h3 className='title'>{i18n.result_success}</h3>
						{bcd}
					</div>
					<div className='row content-ender'>
						<div className='columns medium-3'>
							{e}
						</div>
						<div className='columns medium-9 cta'>
							<div className='checkbox-group'>
								<input
									id='up-success-over'
									name="overwrite"
									type="checkbox"
									checked={oThis.state.overwrite}
									onChange={oThis.handleInputChange.bind(oThis)} />
								<label htmlFor='up-success-over'>{i18n.overwrite_if_existed}</label>
							</div>
							<button className='right' onClick={oThis.uploadSuccessClick.bind(oThis)}><div className='icon-cloud-upload-16 i-icon i-icon-left' />{i18n.upload_address_book}</button>
						</div>
					</div>
				</div>
			)
		}
		else if (jsonData.result == "duplicated") {
			let rowColumn = { id: "Row", name: "Row" };

			let jsonArray = jsonData.columns.slice(0);

			let iconColumn = {
				id: "icon", name: "", flex: 1, type: 'icon', className: 'before-color-theme icon-edit-16', onClick: (index, data) => {
					this.tblRowSelected(index, data);
				}
			};

			let mainColumns = jsonData.columns.slice();
			mainColumns.splice(0, 0, iconColumn, rowColumn);

			result.push(
				<div key='duplicated' className="main-card result-area">
					<div className='content-divide'>
						<h3>{i18n.result_success_but_duplicated}</h3>
						{bcd}
					</div>
					<div>
						<span className='labelDivider expanded'><i className='icon-warning-16 i-icon' />{"  " + jsonData.duplicated}{i18n.record_duplicated_label}</span>
						<ITable tableName='uploadResult' data={jsonData.duplicatedArray} columns={mainColumns} searching={false} colvis={true} columnDefs={[{
							"targets": 0,
							className: 'noVis', //NOTES: no column visibility
							"orderable": false
						}]} onDoubleClick={(index, data) => { oThis.tblRowSelected(index, data) }} />
					</div>
					<div className='row content-ender'>
						<div className='columns medium-3'>
							{e}
						</div>
						<div className='columns medium-9 cta'>
							<div className='checkbox-group'>
								<input
									id='up-duplicate-over'
									name="overwrite"
									type="checkbox"
									checked={oThis.state.overwrite}
									onChange={oThis.handleInputChange.bind(oThis)} />
								<label htmlFor='up-duplicate-over'>{i18n.overwrite_if_existed}</label>
							</div>
							<button className='right' onClick={oThis.uploadDuplicateClick.bind(oThis)}><div className='icon-cloud-upload-16 i-icon i-icon-left' />{i18n.upload_address_book}</button>
						</div>
					</div>
				</div>
			);
		} else if (jsonData.result == "unsuccess") {
			let dataRED = jsonData.dataRED;
			let manRED = jsonData.manRED;
			let idRED = jsonData.idRED;
			let rederFunction = function (REDColumns) {
				let basicColumns = jsonData.columns.slice();
				for (let i = 0; i < REDColumns.length; i++) {
					let renderColumn = REDColumns[i];
					for (let j = 0; j < basicColumns.length; j++) {
						if (basicColumns[j].id == renderColumn) {
							let columnObj = Object.assign({}, basicColumns[j]);
							columnObj.render = function (text, o) {
								let redLength = o.RED.length;
								if (redLength > 0) {
									for (let k = 0; k < redLength; k++) {
										if (renderColumn == o.RED[k]) {
											if (REDColumns == manRED) {
												return <div style={{ color: 'red' }}>{i18n.no_info}</div>
											} else {
												return <div style={{ color: 'red' }}>{text}</div>
											}
										}
									}
								}
								return text
							}
							basicColumns[j] = columnObj;
							break;
						}
					}
				}
				return basicColumns
			}

			result.push(
				<div key='unsuccess' className="main-card result-area">
					<div className='content-divide'>
						<h3 className='title'>{i18n.result_success}</h3>
						{bcd}
					</div>
					<div className='row content-ender'>

						<span className='upload-error'><i className='icon-warning-16 i-icon red' />{"  " + jsonData.data}{i18n.record_data_type_label}</span>

						<ITable tableName='dataError' data={jsonData.dataArray} columns={rederFunction(dataRED)} colvis={true} paging={false} info={false} searching={false} />

						<span className='upload-error'><i className='icon-warning-16 i-icon red' />{"  " + jsonData.mandatory}{i18n.record_mandatory_label}</span>

						<ITable tableName='mandatoryError' data={jsonData.manArray} columns={rederFunction(manRED)} colvis={true} paging={false} info={false} searching={false} />

						<span className='upload-error'><i className='icon-warning-16 i-icon red' />{"  " + jsonData.identifier}{i18n.record_identifier_label}</span>

						<ITable tableName='idError' data={jsonData.idArray} columns={rederFunction(idRED)} colvis={true} paging={false} info={false} searching={false} />

					</div>
					<br />
					<div className='row content-ender'>

						<div className='columns medium-3'>
							{e}
						</div>
						<div className='columns medium-9 cta'>
							<button className='right small grey-button' onClick={oThis.exportClick.bind(oThis)}><span className='icon-share-16 i-icon i-icon-left' />{i18nCommon.export_list}</button>
						</div>
					</div>
				</div>
			)
		}
		return (
			<div id='upload_cus_pro' className="multiple-cards">
				{this.state.showUpload ?
					<div className="main-card upload-card">

						<div className='blankstate'>
							<div className=''>
								<img src='assets/imgs/upload-smaller.png' />
							</div>

							<div className='blankstate-info'>
								<div className='blankstate-title'>{i18n.upload_an_address}</div>
								<div className=''>{i18n.choose_an_file}</div>
							</div>

							<div className='blankstate-action'>
								<ReactFileReader handleFiles={oThis.handleFiles.bind(oThis)} fileTypes={'.csv'}>
									<button className='grey-button small'>{i18n.choose_file_upload}</button>
								</ReactFileReader>
								<span className='i-label'>{this.state.fileName}</span>
							</div>
							<div className=' confirm-btn'><button className={'button' + (this.state.confirmDisabled == true ? " disabled" : "")} onClick={this.confirmClick.bind(this)}>{i18n.confirm}</button></div>
							
							<div className='blankstate-info'>
								<span className='block-element'>{i18n.need_a_template}</span>
								<a href='assets/imgs/upload_sample.csv' download>{i18n.download_csv_template}</a>
							</div>

						</div>

					</div> : null}

				{oThis.state.showGrid ?
					<div className='main-card'>
						<div className="content-divide">
							<h3>{i18n.header_details}</h3>
							<div>
								<ITable tableName="confirm-click-grid" data={oThis.state.gridData} columns={oThis.columns} columnDefs={[{
									"targets": [-1, -2],
									"orderable": false
								}]} />
							</div>
						</div>
						<div className='content-ender'>
							<button className='button' onClick={this.precheckClick.bind(this, 'success')}>{i18n.check}</button>
							<button className='button' onClick={this.precheckClick.bind(this, 'duplicated')}>Check2</button>
							<button className='button' onClick={this.precheckClick.bind(this, 'unsuccess')}>Check3</button>
						</div>
					</div> : null
				}
				{
					oThis.state.showResult ?
						result : null
				}
				{   //NOTES: If result is "Duplicated" and clicked a client, will show Pop-up below
					oThis.state.showDuplicated ?
						<div className='i-popup'>
							<div className='pop-up-content'>

								<div className='pop-header'>
									<h3 className='pop-title'>{i18n.fix_duplicated_information}</h3>
									<button className='pop-exit-btn right' onClick={oThis.popupCloseClick.bind(oThis)}>
										<div className="icon-simple-remove" />
									</button>
								</div>

								<div className='pop-body'>
									<div className="pop-content-divide">
										<span>{i18n.current_issue}</span>
										<ITable tableName='duplicate-row-grid' data={oThis.state.duplicatedRow} columns={oThis.state.duplicatedColumns} searching={false} paging={false} info={false} colvis={true} />
									</div>
									<div className="pop-content-divide">
										<span>{i18n.update_an_entry_label}</span>
										<ITable tableName='exit-rows-grid' data={oThis.state.existingArray} columns={oThis.state.existingColumns} colvis={true} columnDefs={[{
											"targets": 0,
											className: 'noVis', //NOTES: no column visibility
											"orderable": false
										}]} />
									</div>
									<div>{i18n.or_create_as_new}
										<button onClick={oThis.popupCreateClick.bind(oThis)}>{i18n.create_entry}</button>
									</div>
								</div>
							</div>
						</div> : null
				}
				<AlertBox alertMsg={i18n.select_csv} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
			</div>
		);
	}
	// ------------------------------- /interface ------------------------------- //
}

export default UploadCustomerProfile