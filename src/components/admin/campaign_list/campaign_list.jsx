import React, { Component } from 'react';
import Sidebar from '../../common/sidebar.jsx';
import i18nCommon from '../../common/i18n.jsx';
let i18n = i18nCommon.campaign_list_variable;

export default class CampaignList extends Component {
	render() {
		let details = {
			title: i18n.campaign, linkArray: [
				{ link: "/home/campaign/map", icon: 'icon-link-stroke-24', name: i18n.map_input_form },
				{ link: "/home/campaign/assign", icon: 'icon-todo-stoke-24', name: i18n.assign_campaign_list }
			]
		}
		return (
			<div>
				<Sidebar details={details} childrenContent={this.props.children} />
			</div>
		);
	}
}