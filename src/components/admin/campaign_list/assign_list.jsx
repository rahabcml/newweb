import React, { Component } from 'react';
import i18nCommon from '../../common/i18n.jsx';
import ITable from '../../common/ITable.jsx';
import ICombo from '../../common/ICombo.jsx';
import $ from 'jquery';
import foundation from 'foundation-sites';
let i18n = i18nCommon.assign_campaign_list;
import AlertBox from '../../common/AlertBox.jsx';

export default class AssignCampaignList extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);
		this.state = {
			selectedCampaign: "",
			filTotal: 0,
			anyAll: "any",
			addArray: [{ condition: "", selector: "", text: "" }],
			selectedAgent: -1,
			agentName: "",
			noToAssign: "",
			confirmAnyAll: "",
			confirmFilter: [],
			campaignList: [],
			agentList: [],
			selectedCampaignIdx: -1,
			alertMsg: "",
			showAlert: false
		}
	}
	componentDidMount() {
		$('#assign-campaign-tabs').foundation();
	}
	componentWillMount() {
		let oThis = this;
		//TO DO: GET THE CAMPAIGN LIST AND AGENT LIST
		this.jsonData = {
			campaign_list: [
				{ campaign_id: "TSMHA483968", total: 113, assigned: 13, unassigned: 100 },
				{ campaign_id: "TSMHA484564", total: 273, assigned: 73, unassigned: 200 },
				{ campaign_id: "TSMHA483958", total: 653, assigned: 53, unassigned: 600 },
				{ campaign_id: "TSMHA485468", total: 153, assigned: 53, unassigned: 100 },
				{ campaign_id: "TSMHA485988", total: 573, assigned: 73, unassigned: 500 },
				{ campaign_id: "TSMHA483558", total: 547, assigned: 50, unassigned: 497 },
				{ campaign_id: "TSMHA483888", total: 143, assigned: 30, unassigned: 113 }
			],
			agent_list: [
				{ id: 1, name: "June White", assigned: 34, not: 19 },
				{ id: 7, name: "Mary Lee", assigned: 117, not: 56 },
				{ id: 8, name: "Kelvin Lau", assigned: 46, not: 37 },
				{ id: 9, name: "Tony White", assigned: 54, not: 28 },
				{ id: 71, name: "Eva Chan", assigned: 13, not: 6 },
				{ id: 17, name: "Rahab Chung", assigned: 83, not: 29 },
				{ id: 72, name: "Kenneth Cheng", assigned: 63, not: 15 },
			],
			condition: ["Full Name", "Address 1", "Address 2", "Address 3", "Work Phone Number", "Other Phone Number", "First Name", "Last Name", "Personal Identification", "Email Address", "Mobile Phone Number", "Home Phone Number"]
		};
		
		this.setState({ campaignList: this.jsonData.campaign_list, agentList: this.jsonData.agent_list });
		this.campaignColumns = [
			{
				id: "campaign_id", type: 'radio', name: i18n.action, radioName: 'campaignRadio',
				onChange(idx, o) {
					console.log("o.campaign_id");
					console.log(o.campaign_id);
					oThis.setState({ selectedCampaign: o.campaign_id, filTotal: o.unassigned, selectedCampaignIdx: idx });
				}
			},
			{ id: "campaign_id", name: i18n.campaign_list_id },
			{ id: "total", name: i18n.total },
			{ id: "assigned", name: i18n.assigned },
			{ id: "unassigned", name: i18n.unassigned }
		];

		this.agentColumns = [
			{
				id: "id", type: 'radio', name: i18n.action, radioName: 'agentRadio',
				onChange(idx, o) {
					oThis.setState({ selectedAgent: o.id, agentName: o.name });
				}
			},
			{ id: "name", name: i18n.agent_name },
			{ id: "assigned", name: i18n.assigned },
			{ id: "not", name: i18n.not_used }
		];

		this.any_all_array = [{ name: "Match any of the following", value: 'any' }, { name: "Match all of the following", value: 'all' }];
	}
	// ------------------------------- /delegate ------------------------------- //
	// ------------------------------- handlers ------------------------------- //
	tabChange(previous, next) {

		if (previous == 1 && next == 2 && this.state.selectedCampaign == "") {
			this.setState({alertMsg: i18n.please_select_a_camapgin_next, showAlert: true});
			//alert(i18n.please_select_a_camapgin_next);
			return;
		}
		if (previous == 2 && next == 3 && this.state.selectedAgent == -1) {
			this.setState({alertMsg: i18n.please_select_an_agent, showAlert: true});
			//alert(i18n.please_select_an_agent);
			return;
		}
		console.log(previous, next);
		let pTab = "#assign-tab-" + previous;
		let nTab = "#assign-tab-" + next;
		let pContent = "#assign-panel-" + previous;
		let nContent = "#assign-panel-" + next;
		let paTab = "#a-tab-" + previous;
		let naTag = "#a-tab-" + next;

		$(pTab).removeClass("is-active sidebar-active");
		$(pContent).removeClass("is-active");

		$(nTab).addClass("is-active sidebar-active");
		$(nContent).addClass("is-active");

		$(paTab).attr("aria-selected", "false");
		$(naTag).attr("aria-selected", "true");
	}
	confirmClick() {
		let oThis = this;
		let noToAssign = Number(this.state.noToAssign);
		if (noToAssign == "") {
			this.setState({alertMsg: i18n.please_enter_no, showAlert: true});
			//alert(i18n.please_enter_no);
			return;
		}
		if (noToAssign <= 0) {
			this.setState({alertMsg: i18n.no_to_assign_bigger, showAlert: true});
			//alert(i18n.no_to_assign_bigger);
			return;
		}
		let filTotal = this.state.filTotal
		if (noToAssign > this.state.filTotal) {
			this.setState({alertMsg: i18n.assign_bigger_total, showAlert: true});
			//alert(i18n.assign_bigger_total);
			return;
		}
		//TO DO: send campaignid, agentid, confirmArray if not [], confirmanyAll if not "", no. to assign
		//TO DO: if server response is success, reset no. to assign, reduce filTotal and agentlist and return to page 2
		let agentList = this.state.agentList.slice();
		let agentRemoved = agentList.filter(function (el) {
			return el.id !== oThis.state.selectedAgent;
		});
		let newFilTotal = filTotal - noToAssign;
		let campaignList = this.state.campaignList.slice();
		campaignList[this.state.selectedCampaignIdx].unassigned -= noToAssign;
		campaignList[this.state.selectedCampaignIdx].assigned += noToAssign;
		this.setState({ filTotal: newFilTotal, noToAssign: "", agentList: agentRemoved, selectedAgent: -1, agentName: "" }, () => { oThis.tabChange(3, 2) });
	}
	addClick() {
		//NOTES: 'Add Condition' button clicked
		let addArray = this.state.addArray.slice();
		addArray.push({ condition: "", selector: i18n.is, text: "" })
		this.setState({ addArray: addArray });
	}
	applyFilterClick() {
		if (this.state.selectedCampaign == "") {
			this.setState({alertMsg: i18n.please_select_a_camapgin_filter, showAlert: true});
			//alert(i18n.please_select_a_camapgin_filter)
			return;
		}
		let addArray = this.state.addArray
		if (addArray.length > 0) {
			for (let i = 0; i < addArray.length; i++) {
				if (addArray[i].condition == "" || addArray[i].selector == "", addArray[i].text == "") {
					this.setState({alertMsg: i18n.please_complete_filter, showAlert: true});
					//alert(i18n.please_complete_filter);
					return;
				}
			}
		}
		if (addArray.length == 0) {
			let campaignList = this.state.campaignList.slice();
			let unassigned = campaignList[this.state.selectedCampaignIdx].unassigned;
			this.setState({ filTotal: unassigned, confirmFilter: [], confirmAnyAll: "" });
			return
		}
		//restore to no fitler


		//TO DO: Get the filterd data from the server, this will filter the unsassigned records, send Data, this.state.selectedCampaign, this.state.anyAll, this.state.addArray, return filtered number
		//NOTES: the confirmFilter and confirmAnyAll are for send to server in the last step
		this.setState({ filTotal: 80, confirmFilter: this.state.addArray, confirmAnyAll: this.state.anyAll });
	}
	// ------------------------------- /handlers ------------------------------- //
	render() {
		let oThis = this;
		console.log("oThis.state.selectedCampaign");
		console.log(oThis.state.selectedCampaign);
		let jsonData = this.jsonData;
		//NOTES: Generate Condition Rows Below
		let conditionRows = [];
		let addArray = this.state.addArray.slice();
		if (addArray.length > 0) {
			for (let i = 0; i < addArray.length; i++) {
				conditionRows.push(
					<div key={"query-rows" + i} className='expanded'>
						<div className="filter-inputs">
							<ICombo placeholder={i18nCommon.choose_condition} displayField={0} valueField={0} options={jsonData.condition} value={addArray[i].condition} onChange={(o) => { addArray[i].condition = o ? o.value : ""; oThis.setState({ addArray: addArray }) }} />
							<ICombo displayField={0} valueField={0} options={i18nCommon.selector_array} value={addArray[i].selector} onChange={(o) => { addArray[i].selector = o ? o.value : ""; this.setState({ addArray: addArray }) }} />
							<div>
								<input type='text' value={addArray[i].text} onChange={event => {
									addArray[i].text = event.target.value;
									this.setState({ addArray: addArray })
								}} />
							</div>
							<div className="remove-icon">
								<div className='icon-trash-16 i-icon' onClick={() => { addArray.splice(i, 1); this.setState({ addArray: addArray }); }} />
							</div>
						</div>
					</div>
				)
			}
		}

		return (
			<div id='assign_campaign' className='main-card'>
				<div className='content-divide'>
					<h3>{i18n.assign_campaign_list}</h3>
					<div className='tabs-container'>
						<div className="tabs" data-tabs id="assign-campaign-tabs">
							<div id='assign-tab-1' className="tabs-title is-active disabled sidebar-link sidebar-active">
								<a aria-selected="true" id="a-tab-1" data-tabs-target="assign-panel-1" href="#assign-panel-1" className=''>
									<div className='info-group'>
										<div className='icon-cont'>
											<i className="icon-speaker-30" />
										</div>
										<div className='text-cont'>
											<span className='title'>{i18n.one_choose_a_campaign}</span>
											<span className='desc'>{i18n.pick_a_campaign}</span>
										</div>
									</div>
								</a>
							</div>
							<div id='assign-tab-2' className="tabs-title disabled sidebar-link">
								<a id="a-tab-2" data-tabs-target="assign-panel2" href="#assign-panel2" className=''>
									<div className='info-group'>
										<div className='icon-cont'>
											<i className="icon-single-position-30" />
										</div>
										<div className='text-cont'>
											<span className='title'>{i18n.two_choose_an_angent}</span>
											<span className='desc'>{i18n.pick_an_agent}</span>
										</div>
									</div>
								</a>
							</div>
							<div id='assign-tab-3' className="tabs-title disabled sidebar-link">
								<a id="a-tab-3" data-tabs-target="assign-panel3" href="#assign-panel3" className=''>
									<div className='info-group'>
										<div className='icon-cont'>
											<i className="icon-todo-30" />
										</div>
										<div className='text-cont'>
											<span className='title'>{i18n.three_assign_records}</span>
											<span className='desc'>{i18n.input_quantity}</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>

				<div className="tabs-content" data-tabs-content="assign-campaign-tabs">
					{/* ======================== TAB 1 ======================== */}
					<div className="tabs-panel is-active" id="assign-panel-1">

						<h3>{i18n.assign_campaign_list}</h3>
						<ITable tableName="campaignTable" columns={this.campaignColumns} data={this.state.campaignList} checked={oThis.state.selectedCampaign} columnDefs={[{
							"targets": 0,
							className: 'noVis', //NOTES: no column visibility
							"orderable": false
						}]} />

						<div className='content-divide'>
							<h3>{i18n.filter_unassigned_records}</h3>
							<div className='moreFilter'>
								<button className='grey-button small' data-toggle="collapse" data-target="#showFilter" aria-expanded="false"><span className='icon-settings-16 i-icon i-icon-right' /></button>
								{this.state.selectedCampaign == "" ? <span>&nbsp;&nbsp;</span> : <span>{this.state.selectedCampaign}&nbsp;-&nbsp;</span>}<span>{this.state.filTotal}&nbsp;{i18n.records_filtered}</span>
							</div>

							<div id='showFilter' className='collapse'>
								<div className='outer-padding'>

									<div className='inner-padding'>
										<div className='filter-text-select'>
											<ICombo displayField='name' valueField='value' options={this.any_all_array} value={this.state.anyAll} onChange={(o) => { oThis.setState({ anyAll: o ? o.value : "" }) }} clearable={false} />
										</div>
										{conditionRows}

										<a className='add-condition-link' onClick={oThis.addClick.bind(oThis)}><span className='icon-add-16' />&nbsp;{i18n.add_new_fitler}</a>
										<button className='right' onClick={oThis.applyFilterClick.bind(oThis)}>{i18nCommon.apply_filter}</button>
									</div>
								</div>
							</div>
						</div>

						<div className='content-ender'>
							<button className='right' onClick={this.tabChange.bind(oThis, 1, 2)}>{i18n.choose_an_angent}<span className='icon-right-arrow-16 i-icon i-icon-right' /></button>
						</div>
					</div>
					{/* ======================== TAB 2 ======================== */}
					<div className="tabs-panel" id="assign-panel-2">
						<div className='content-divide'>
							<h3>{i18n.filter_unassigned_records}</h3>
							<ITable tableName="agentTable" columns={this.agentColumns} data={this.state.agentList} checked={oThis.state.selectedAgent} columnDefs={[{
								"targets": 0,
								className: 'noVis', //NOTES: no column visibility
								"orderable": false
							}]} />
						</div>

						<div className='content-ender'>
							<button className='left' onClick={this.tabChange.bind(oThis, 2, 1)}><span className='icon-left-arrow-16 i-icon i-icon-left' />{i18n.previous_step}</button>
							<button className='right' onClick={this.tabChange.bind(oThis, 2, 3)}>{i18n.assign_records}<span className='icon-right-arrow-16 i-icon i-icon-right' /></button>
						</div>
					</div>
					{/* ======================== TAB 3 ======================== */}
					<div className="tabs-panel" id="assign-panel-3">
						<div className='content-divide'>
							<h3>{i18n.assignment_overview_label}</h3>

							<div className='overview-area'>
								<div className='info-value-group'>
									<span className='info-label'>{i18n.campaign_information}</span>
									<span className='value'>{this.state.selectedCampaign}&nbsp;-&nbsp;{this.state.filTotal}{i18n.records}</span>
								</div>

								<div className="input-group">
									<span className="input-group-label">{i18n.overview_label}</span>
									<input className="input-group-field" type='text' value={this.state.noToAssign} onChange={e => {
										const re = /^[0-9\b]+$/;
										if (e.target.value == '' || re.test(e.target.value)) {
											this.setState({ noToAssign: e.target.value })
										} else {
											this.setState({alertMsg: i18n.please_enter_number, showAlert: true});
											//alert(i18n.please_enter_number);
										}

									}} />
								</div>

								<div className='info-value-group text-right'>
									<span className='info-label'>{i18n.agent_information}</span>
									<span className='value'>{this.state.agentName}</span>
								</div>
							</div>

							<div className='content-ender'>
								<button className='left' onClick={this.tabChange.bind(oThis, 3, 2)}>{i18n.previous_step}</button>
								<button className='right' onClick={oThis.confirmClick.bind(oThis)}><span className='icon-add-list-16 i-icon i-icon-left' />{i18n.confirm_assignment}</button>
							</div>
						</div>
					</div>
					<div className='clear' />
				</div>
				<AlertBox alertMsg={this.state.alertMsg} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
			</div >
		);
	}
}