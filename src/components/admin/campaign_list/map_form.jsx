import React, { Component } from 'react';
import i18nCommon from '../../common/i18n.jsx';
import ITable from '../../common/ITable.jsx';
import IDropDown from '../../common/IDropDown.jsx';
import AlertBox from '../../common/AlertBox.jsx';
let i18n = i18nCommon.campaign_list_variable;

export default class MapInputForm extends Component {
	// ------------------------------- delegate ------------------------------- //
	constructor(props) {
		super(props);
		this.state = {
			selectedCampaign: "",
			selectedIInputForm: "",
			selectedOInputForm: "",
			InOutSelection: i18n.inbound_campaigns,
			showGrid: false,
			gridData: [],
			inputFields: [],
			//NOTES :the below 3 datat is for sending to server when clicking save settings button at the last steps
			tableCampaign: "",
			tableInbound: true,
			tableInputForm: "",
			alertMsg: "",
			showAlert: false
		}
	}
	componentWillMount() {
		let oThis = this;
		this.columns = [];
		//TO DO: to get the data campaign lists and input forms from server
		let jsonData = {
			campaignLists: ["Summer Promotion", "50% off Discount", "Dec-Fec Campaign", "Premier Event Campaign"], inboundForms: ["Summer Promotion Form", "50% off Discount Form", "Dec-Fec Campaign Form", "Premier Event Campaign Form"], outboundForms: ["Winter Promotion Form", "30% off Discount Form", "March Campaign Form", "Premier Night Event Campaign Form"]
		}
		this.campaignLists = jsonData.campaignLists;
		this.inboundForms = jsonData.inboundForms;
		this.outboundForms = jsonData.outboundForms;
	}
	// ------------------------------- /delegate ------------------------------- //
	// ------------------------------- handlers ------------------------------- //
	//NOTES: Function below will be used when a radio chose in button group
	campaignListsChange(changeEvent) {
		let selectedCampaign = changeEvent.target.value;
		this.setState({ selectedCampaign: selectedCampaign });
	}
	inputFormsChange(isInbound, changeEvent) {
		console.log("changeEvent, isInbound");
		console.log(changeEvent, isInbound);
		let value = changeEvent.target.value;
		if (isInbound) {
			this.setState({ selectedIInputForm: value });
		} else {
			this.setState({ selectedOInputForm: value });
		}

	}
	inOutComboChange(o) {
		let value = o == null ? "" : o.value;
		this.setState({ InOutSelection: value });
	}
	confirmClick() {
		let oThis = this;
		//NOTES: verify selection
		let message = ""
		if (this.state.selectedCampaign == "") { message += i18n.please_select_a_campaign }
		if ((this.state.InOutSelection == i18n.inbound_campaigns && this.state.selectedIInputForm == "") ||
			(this.state.InOutSelection == i18n.outbound_campaigns && this.state.selectedOInputForm == "")) {
			if (message != "") message += "\n";
			message += i18n.please_select_a_input_form
		}
		if (message == "") {
			//TO DO: Send objToSend to server
			let objToSend = {}
			objToSend.selectedCampaign = this.state.selectedCampaign;
			objToSend.isInbound = this.state.InOutSelection == i18n.inbound_campaigns;
			if (objToSend.isInbound == true) {
				objToSend.inputForms = this.state.selectedIInputForm
			} else {
				objToSend.inputForms = this.state.selectedOInputForm
			}
			// TBD: sample data
			let jsonData = {
				gridData: [
					{ order: 1, name: "name", field: "Customer Name", checked: true },
					{ order: 2, name: "mobile", field: "Mobile Number", checked: false },
					{ order: 3, name: "home", field: "Home Number", checked: false },
					{ order: 4, name: "work", checked: false },
					{ order: 5, name: "add_1", field: "Address L1", checked: false },
					{ order: 6, name: "add_2", checked: false },
					{ order: 7, name: "add_3", checked: false },
					{ order: 8, name: "vplus_acct_nbr", checked: false },
					{ order: 9, name: "acct_nbr", checked: true },
					{ order: 10, name: "rbt_product_code", checked: false },
					{ order: 11, name: "avail_bal", checked: false },
					{ order: 12, name: "cr_limit_amt", field: "Credit Limit", checked: false },
					{ order: 13, name: "cal_base_appr", checked: false },
					{ order: 14, name: "seq_nbr", field: "Sequence Number", checked: false }
				],
				input: ["Customer Name", "Mobile Number", "Home Number", "Address L1", "Credit Limit", "Sequence Number"]
			}

			this.columns = [
				{ id: "order", name: i18n.order },
				{ id: "name", name: i18n.campaign_list_field },
				{ id: "field", name: i18n.input_form_field, type: 'combo', valueField: 0, displayField: 0, options: jsonData.input, onChange: oThis.comboChange.bind(oThis) },
				{
					id: "display", name: i18n.agent_list_display, type: 'checkbox', onChange: function (idx, checked) {
						console.log("idx,checked");
						console.log(idx, checked);

						//let value = event.target.checked;
						let gridData = oThis.state.gridData.slice();
						gridData[idx].checked = checked;
						console.log("gridData");
						console.log(gridData);
						oThis.setState({ gridData: gridData });

					}
				},
			]

			let tableInbound = this.state.InOutSelection == i18n.inbound_campaigns ? true : false;
			this.setState({ gridData: jsonData.gridData, inputFields: jsonData.input, showGrid: true, tableCampaign: this.state.selectedCampaign, talbeInbound: tableInbound, tableInputForm: tableInbound == true ? this.state.selectedIInputForm : this.state.selectedOInputForm });
		} else {
			this.setState({ alertMsg: message, showAlert: true });
			//alert(message);
		}
	}
	//NOTES: Function below change the value of the combobox on the main talbe
	comboChange(val, i) {
		let _gridData = this.state.gridData.slice();
		if (val != null) {
			_gridData[i].field = val.value;
		} else {
			_gridData[i].field = "";
		}
		this.setState({ gridData: _gridData });
	}
	saveSettingsClick() {
		//TO DO: send this.state.tableCampain, tableInbound, tableForm, and gridData to server
		//alert(i18n.setting_submitted);
		this.setState({ alertMsg: i18n.setting_submitted, showAlert: true });
		console.log(this.state.tableCampaign, this.state.talbeInbound, this.state.tableInputForm, this.state.gridData);
	}
	// ------------------------------- /handlers ------------------------------- //
	render() {
		console.log("render in map form");
		let oThis = this;
		let campaignsLists = [];
		for (let i = 0; i < this.campaignLists.length; i++) {
			let item = this.campaignLists[i];
			campaignsLists.push(
				<div className='radio-buttons' key={'campaignIndex' + i}>
					<input type="radio" name="campaignList" id={item} value={item} checked={oThis.state.selectedCampaign === item} onChange={oThis.campaignListsChange.bind(oThis)} />
					<div className="check"></div>
					<label className="slim-label" htmlFor={item}>{item}</label>
				</div>
			)
		}

		let InputForms = [];
		let isInbound = this.state.InOutSelection == i18n.inbound_campaigns;
		console.log("isInbound");
		console.log(isInbound);
		let InOutbound = isInbound ? "inbound" : "outbound";
		let InputFormLists = isInbound == true ? this.inboundForms : this.outboundForms;
		console.log("InputFormLists");
		console.log(InputFormLists);
		let checked = isInbound == true ? oThis.state.selectedIInputForm : oThis.state.selectedOInputForm;
		for (let j = 0; j < InputFormLists.length; j++) {
			let item = InputFormLists[j];
			InputForms.push(
				<div className='radio-buttons' key={InOutbound + j}>
					<input type="radio" name="InOutbound" id={item} value={item} checked={checked === item} onChange={oThis.inputFormsChange.bind(oThis, isInbound)} />
					<div className="check"></div>
					<label className="slim-label" htmlFor={item}>{item}</label>
				</div>
			)
		}

		return (
			<div id='map_form'>
				<div className='content-divide main-card'>
					<h3>{i18n.assign_a_campaign}</h3>
					<div className='row'>
						<div className='medium-6 columns'>
							<div className='gray-card-container'>
								<h3 className='title'>{i18n.campaigns_label}</h3>
								{campaignsLists}
							</div>
						</div>
						<div className='medium-6 columns'>
							<div className='gray-card-container label-input-group'>
								<h3 className='title'>{i18n.input_forms_label}</h3>
								<IDropDown displayField={0} valueField={0} value={this.state.InOutSelection} options={i18n.inbound_outbound_array} onChange={oThis.inOutComboChange.bind(oThis)} clearable={false} />
								{InputForms}
							</div>
						</div>
					</div>
					<button className='right margin-top' onClick={oThis.confirmClick.bind(oThis)}><span className='icon-add-list-16 i-icon i-icon-left' />{i18n.confirm_assignment}</button>
					<div className='clear' />
				</div>
				{oThis.state.showGrid ?
					<div className='main-card'>
						<div className="content-divide">
							<h3>{i18n.header_details}</h3>
							<div>
								<ITable tableName="confirm-click-grid" data={oThis.state.gridData} columns={oThis.columns} columnDefs={[{
									"targets": [-1, -2],
									"orderable": false
								}, {
									"targets": [3],
									"width": '15%'
								}
								]} />
							</div>
						</div>
						<div className='content-ender'>
							<button className='button' onClick={this.saveSettingsClick.bind(this, 'success')}><i className='i-icon-left i-icon icon-check-16' />{i18n.save_settings}</button>
						</div>
					</div> : null
				}

				<AlertBox alertMsg={this.state.alertMsg} showAlert={this.state.showAlert} alertClick={() => this.setState({ showAlert: false })} />
			</div>
		);
	}
}