import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Home from '../common/home.jsx';
import i18nCommon from '../common/i18n.jsx';

let i18n = i18nCommon.home_variable;




export default class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  moduleClicked(module, defaultPage) {
    let href = "";
    if (defaultPage != "") {
      console.log("defaultPage");
      console.log(defaultPage);
      href = 'home/' + module + "/" + defaultPage;
    } else {
      href = 'home/' + module;
    }
    //let href =  'home/' + module +'/default';
    window.location.hash = href;
    console.log("href");
    console.log(href);
  }

  render() {
    let oThis = this;
    return (
      <div className="dashboard-features">
        <div className="container">
          <div className="row">

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "address", "config")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/addressbook.png" />
                <span className="feature-title">{i18n.address_book}</span>
                <p className="feature-desc remove-space">Create and manage your address book contacts and configure its settings</p>
              </div>
            </div>

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "InputForm", "")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/input_form.png" />
                <span className="feature-title">{i18n.input_form}</span>
                <p className="feature-desc remove-space">Create input forms and assign to be used by campaign lists</p>
              </div>
            </div>

            <div className="small-12 medium-4 columns remove-space end" onClick={oThis.moduleClicked.bind(oThis, "campaign","map")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/campaign_list.png" />
                <span className="feature-title">{i18n.campaign_list}</span>
                <p className="feature-desc remove-space">Make campaigns to group your customers</p>
              </div>
            </div>
          </div>

          <div className="row">

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "VoiceLogMail","")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/voice_logs.png" />
                <span className="feature-title">{i18n.voice_log}</span>
                <p className="feature-desc remove-space">Manage your voice logs and voice mails through this page</p>
              </div>
            </div>

            <div className="small-12 medium-4 columns remove-space" onClick={oThis.moduleClicked.bind(oThis, "UserAccount","Roles")}>
              <div className="feature-card">
                <img className="feature-img" src="assets/imgs/user_account.png" />
                <span className="feature-title">{i18n.user_account}</span>
                <p className="feature-desc remove-space">Manage your user accounts through this page</p>
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}