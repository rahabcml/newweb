/**
+ * index.js
+ *
+ * @description :: Front-end starting point, including react-router setup, also its related config/routes.js
+ * @help        :: See https://github.com/reactjs/react-router
+ */

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Link, IndexRoute, hashHistory } from 'react-router';
import Loader from './loader.js';

export default class App extends Component {
//const App = React.createClass({
	render() {
		return (
			<Router history={hashHistory}>
                <Route path="/(:comp)" component={Loader}/>
                {/*
				<Route path="/" component={Container}>
					<Route path="login" component={Login} />
					<Route path="home" component={Home} >
						<Route path="features" component={Features} />
						<IndexRoute component={Features} />
						<Route path="agent" component={Agent} />
						<Route path="address" component={Address} >
							<Route path="default" component={() => <Default module="address_book_variable" />} />
							{ <IndexRoute component={() => <Default module="address_book_variable" />} /> }
							<Route path="config" component={ConfigCustProfile} />
							<Route path="upload" component={UploadCustomerProfile} />
							<Route path="manage" component={ManageCustomerProfile} />
						</Route>
					</Route>
				</Route>
                <Route path="*" component={NotFound} />
                */}
			</Router>
		);
	}
}//);

//export default App;
// --------------------------- routes --------------------------- //
// $(document).ready(() => {
// 	render((
// 		<App />
// 	), document.getElementById('app'))
// 	jQuery(document).foundation();
// });
$(document).ready(() => render((
	<App />
), document.getElementById('app')));
// --------------------------- /routes --------------------------- //
