/**
+ * index.js
+ *
+ * @description :: Front-end starting point, including react-router setup, also its related config/routes.js
+ * @help        :: See https://github.com/reactjs/react-router
+ */

import React, { Component } from 'react';
export default class loader extends Component {
//export default class loader Component {
//export default React.createClass({
	render() {
        var dom = null;
        try { dom = require('./views/' + this.props.params.comp + '.jsx'); } catch(e) { dom = null; }
		return dom ? dom.call(this) : <div>Page not found.</div>;
	}
}//);
