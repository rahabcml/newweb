/*******************************************
* Notes
* Name: ICombo.jsx
* Details: A combo box component
* Properties: 
*        valueField: String.
*        displayField: String. 
*        value: String. Current Value.
*        options: Array.
*        onChange: Function.
*        placeholder: (Optional) String. Default is "Select.."
*        clearable: (Optional) Boolearn. Deafult true.
*        disabled: (Optional) Boolearn. Deafult false.

*        Others go https://github.com/JedWatson/react-select for ref.
********************************************/

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import IFun from './IFun.jsx';
let isInteger = IFun.isInteger;

export default class ICombo extends React.Component {
    constructor(props) {
        super(props);
        let data = this.props.options.slice() || [];
        let reArragedData = [];
        if (data && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                reArragedData.push({
                    value: isInteger(this.props.valueField) ? data[i] : data[i][this.props.valueField],
                    label: isInteger(this.props.displayField) ? data[i] : data[i][this.props.displayField],
                    obj: this.props.options[i]
                })
            }
        }

        this.state = {
            data: reArragedData,
            value: this.props.value || ""
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps) {
            let data = nextProps.options ? nextProps.options.slice() : [];
            let dataLength = data.length;
            let reArragedData = [];
            if (data && dataLength > 0) {
                for (let i = 0; i < dataLength; i++) {
                    reArragedData.push({
                        value: isInteger(nextProps.valueField) ? data[i] : data[i][nextProps.valueField],
                        label: isInteger(nextProps.displayField) ? data[i] : data[i][nextProps.displayField],
                        obj: nextProps.options[i]
                    })
                }
            }
            this.setState({
                data: reArragedData,
                value: nextProps.value
            });
        }
    }

    render() {
        let oThis = this;
        let name = this.props.value || "";
        let options = this.state.data || [];
        let onChange = this.props.onChange || function () { };
        let placeholder = this.props.placeholder || "Select...";
        let clearable = this.props.clearable === false ? false : true
        let disabled = this.props.disabled === true ? true : false
        return (
            <Select
                placeholder={placeholder}
                name="form-field-name"
                value={name}
                options={options}
                onChange={onChange.bind(oThis)}
                clearable={clearable}
                disabled={disabled}
            /* Put the select variable here */
            />
        );
    }
};