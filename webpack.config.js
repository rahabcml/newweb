var webpack = require('webpack');
var path = require('path');

// dev_dir is used for storing development codes.
var DEV_DIR = path.resolve(__dirname, /^design$/i.test(process.env.NODE_ENV) ? 'interfaces' : 'src');

// prod_dir is used for storing compiled production codes.
var PROD_DIR = path.resolve(__dirname, 'prod/assets/js');

//  entry: [DEV_DIR + '/index.js', './styles/foundation.scss'],
//  entry: [DEV_DIR + '/index.js', './styles/main.scss'],
var config = {
  entry: [DEV_DIR + '/index.js', './styles/foundation.scss'],
  output: {
    path: PROD_DIR,
    filename: 'index.js'
  },
  module: {
    loaders: [
      {
        test: /\.js?/,
        include: DEV_DIR,
        loader: 'babel-loader',
        exclude: ['node_modules']
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        exclude: ['node_modules']
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components|public\/)/,
        loader: "babel-loader",
        exclude: ['node_modules']
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader?importLoaders=1', 'sass-loader'],
        exclude: ['node_modules']
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        //exclude: /(node_modules|bower_components)/,
        loader: "file-loader",
        exclude: ['node_modules']
      },
      {
        test: /\.(woff|woff2)$/,
        //exclude: /(node_modules|bower_components)/,
        loader: "url-loader?prefix=font/&limit=5000",
        options: {
          name: '/fonts/[name].[ext]?[hash]',
          publicPath: '..'
        },
        //loader: "url-loader?prefix=font/&limit=5000&file?publicPath=../&name=./fonts/[hash].[ext]",
        exclude: ['node_modules']
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        //exclude: /(node_modules|bower_components)/,
        loader: "url-loader?limit=10000&mimetype=application/octet-stream",
        options: {
          name: '/fonts/[name].[ext]?[hash]',
          publicPath: '..'
        },
        exclude: ['node_modules']
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader?limit=10000&mimetype=image/svg+xml",
        exclude: ['node_modules']
      },
      {
        test: /\.gif/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader?limit=10000&mimetype=image/gif",
        exclude: ['node_modules']
      },
      {
        test: /\.jpg/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader?limit=10000&mimetype=image/jpg",
        exclude: ['node_modules']
      },
      {
        test: /\.png/,
        exclude: /(node_modules|bower_components)/,
        loader: "url-loader?limit=10000&mimetype=image/png",
        exclude: ['node_modules']
      },
      {
        loader: 'json-loader',
        test: /\.json$/,
        include: [
          /node_modules/
        ]
      }
    ],
    // resolve: {
    //   extensions: ['', '.eot', '.js', '.jsx', '.css']
    // }
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || "local"),
        API_ENDPOINT: JSON.stringify(/^local$/i.test(process.env.NODE_ENV || "local") ? "http://localhost" : "http://sbx.epro.tel")
      }
    }),
    /^production$/i.test() ? new webpack.optimize.UglifyJsPlugin() : null
  ].filter(n => n)
};

module.exports = config;

